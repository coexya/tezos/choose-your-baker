package eu.coexya.cyb.model.entity

import eu.coexya.cyb.common.enums.OperationStatus
import org.springframework.data.annotation.CreatedDate
import org.springframework.data.annotation.Id
import org.springframework.data.annotation.LastModifiedDate
import org.springframework.data.mongodb.core.mapping.Document
import java.time.LocalDateTime

@Document(collection = "operations")
data class OperationEntity(
    @Id
    val id: String? = null,
    @LastModifiedDate
    val lastStatusUpdate: LocalDateTime = LocalDateTime.now(),
    @CreatedDate
    val creationDate: LocalDateTime = LocalDateTime.now(),
    val level: Long? = null,
    val requestedLevel: Long? = null,
    val fetchedLevel: Long? = null,
    val timesFetched: Int = 0,
    val errorMessage: String? = null,
    val bakerAddress: String? = null,
    val status: OperationStatus,
    val owner: String,
    val source: String,
    val totalFee: Long? = null,
    val opSize: Int,
    val estimatedConsumption: Consumption? = null,
    val actualConsumption: Consumption? = null,
    val tzOperation: TzOperation,
) {
    data class TzOperation(
        val hash: String,
        val contents: List<Map<String, Any>>,
        val branch: String,
        val signature: String?,
    )
    data class Consumption(
        val totalStorageSize: Long = 0,
        val totalConsumedGas: Long = 0,
    )
}

fun OperationEntity.hasRequestedLevel() = requestedLevel != null

fun OperationEntity.TzOperation.getGasLimit() = contents.sumOf { (it["gas_limit"] as String).toLong() }
