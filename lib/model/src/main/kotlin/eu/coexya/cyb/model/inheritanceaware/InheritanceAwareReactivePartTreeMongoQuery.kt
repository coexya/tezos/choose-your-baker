package eu.coexya.cyb.model.inheritanceaware

import kotlinx.coroutines.reactive.awaitFirst
import kotlinx.coroutines.reactor.mono
import org.springframework.data.annotation.TypeAlias
import org.springframework.data.mongodb.core.ReactiveMongoOperations
import org.springframework.data.mongodb.core.query.Criteria
import org.springframework.data.mongodb.core.query.Query
import org.springframework.data.mongodb.repository.query.ConvertingParameterAccessor
import org.springframework.data.mongodb.repository.query.ReactiveMongoQueryMethod
import org.springframework.data.mongodb.repository.query.ReactivePartTreeMongoQuery
import org.springframework.data.repository.query.ReactiveQueryMethodEvaluationContextProvider
import org.springframework.expression.ExpressionParser
import reactor.core.publisher.Mono


class InheritanceAwareReactivePartTreeMongoQuery(
    method: ReactiveMongoQueryMethod,
    mongoOperations: ReactiveMongoOperations,
    expressionParser: ExpressionParser,
    evaluationContextProvider: ReactiveQueryMethodEvaluationContextProvider
) :
    ReactivePartTreeMongoQuery(method, mongoOperations, expressionParser, evaluationContextProvider) {

    private val inheritanceCriteria =
        if (method.entityInformation.javaType.isAnnotationPresent(TypeAlias::class.java)) {
            Criteria().orOperator(
                Criteria.where("_class")
                    .`is`(method.entityInformation.javaType.getAnnotation(TypeAlias::class.java).value),
                Criteria.where("_class")
                    .`is`(method.entityInformation.javaType.canonicalName)
            )
        } else {
            null
        }

    override fun createQuery(accessor: ConvertingParameterAccessor): Mono<Query> = mono {
        val query = super.createQuery(accessor).awaitFirst()
        if (inheritanceCriteria !== null) {
            query.addCriteria(inheritanceCriteria)
        }
        query
    }


    override fun createCountQuery(accessor: ConvertingParameterAccessor): Mono<Query> = mono {
        val query = super.createQuery(accessor).awaitFirst()
        if (inheritanceCriteria !== null) {
            query.addCriteria(inheritanceCriteria)
        }
        query
    }


}
