package eu.coexya.cyb.model.inheritanceaware


import org.springframework.data.mapping.context.MappingContext
import org.springframework.data.mongodb.core.ReactiveMongoOperations
import org.springframework.data.mongodb.core.mapping.MongoPersistentEntity
import org.springframework.data.mongodb.core.mapping.MongoPersistentProperty
import org.springframework.data.mongodb.repository.query.ReactiveMongoQueryMethod
import org.springframework.data.mongodb.repository.query.ReactiveStringBasedMongoQuery
import org.springframework.data.mongodb.repository.support.ReactiveMongoRepositoryFactory
import org.springframework.data.projection.ProjectionFactory
import org.springframework.data.repository.core.NamedQueries
import org.springframework.data.repository.core.RepositoryMetadata
import org.springframework.data.repository.query.QueryLookupStrategy
import org.springframework.data.repository.query.QueryLookupStrategy.Key
import org.springframework.data.repository.query.QueryMethodEvaluationContextProvider
import org.springframework.data.repository.query.ReactiveQueryMethodEvaluationContextProvider
import org.springframework.data.repository.query.RepositoryQuery
import org.springframework.expression.spel.standard.SpelExpressionParser
import java.lang.reflect.Method
import java.util.*


class InheritanceAwareReactiveMongoRepositoryFactory(
    private val operations: ReactiveMongoOperations
) : ReactiveMongoRepositoryFactory(operations) {

    /**
     * Switch to our MongoQueryLookupStrategy.
     */
    override fun getQueryLookupStrategy(
        key: Key?,
        evaluationContextProvider: QueryMethodEvaluationContextProvider
    ): Optional<QueryLookupStrategy> {
        return Optional.of(
            MongoQueryLookupStrategy(
                operations, evaluationContextProvider as ReactiveQueryMethodEvaluationContextProvider,
                operations.converter.mappingContext
            )
        )
    }

    /**
     * Taken from the Spring Data for MongoDB source code and modified to return InheritanceAwarePartTreeMongoQuery
     * instead of PartTreeMongoQuery. It's a static private part so copy/paste was the only way...
     */
    private class MongoQueryLookupStrategy(
        private val operations: ReactiveMongoOperations,
        private val evaluationContextProvider: ReactiveQueryMethodEvaluationContextProvider,
        var mappingContext: MappingContext<out MongoPersistentEntity<*>?, MongoPersistentProperty>
    ) : QueryLookupStrategy {
        override fun resolveQuery(
            method: Method, metadata: RepositoryMetadata, factory: ProjectionFactory,
            namedQueries: NamedQueries
        ): RepositoryQuery {
            val queryMethod = ReactiveMongoQueryMethod(method, metadata, factory, mappingContext)
            val namedQueryName = queryMethod.namedQueryName
            return when {
                namedQueries.hasQuery(namedQueryName) -> {
                    val namedQuery = namedQueries.getQuery(namedQueryName)
                    ReactiveStringBasedMongoQuery(
                        namedQuery, queryMethod, operations, EXPRESSION_PARSER,
                        evaluationContextProvider
                    )
                }
                queryMethod.hasAnnotatedQuery() -> {
                    ReactiveStringBasedMongoQuery(
                        queryMethod,
                        operations, EXPRESSION_PARSER,
                        evaluationContextProvider
                    )
                }
                else -> {
                    InheritanceAwareReactivePartTreeMongoQuery(
                        queryMethod,
                        operations,
                        EXPRESSION_PARSER,
                        evaluationContextProvider
                    )
                }
            }
        }
    }

    companion object {
        private val EXPRESSION_PARSER = SpelExpressionParser()
    }
}
