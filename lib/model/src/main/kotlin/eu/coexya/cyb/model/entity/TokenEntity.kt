package eu.coexya.cyb.model.entity

import org.springframework.data.annotation.Id
import org.springframework.data.mongodb.core.index.IndexDirection
import org.springframework.data.mongodb.core.index.Indexed
import org.springframework.data.mongodb.core.mapping.Document
import java.time.OffsetDateTime

@Document(collection = "tokens")
data class TokenEntity(
    @Id
    val id: String? = null,
    val name: String,
    val expirationDate: OffsetDateTime? = null,
    val creationDate: OffsetDateTime,
    val roles: List<String> = emptyList(),
    val owner: String,
    val revoked: Boolean = false,
)
