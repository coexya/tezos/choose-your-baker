package eu.coexya.cyb.model.mapper

import com.querydsl.core.BooleanBuilder
import com.querydsl.core.types.Predicate
import eu.coexya.cyb.common.criteria.OperationCriteria
import eu.coexya.cyb.model.entity.QOperationEntity

fun OperationCriteria.toPredicate(): Predicate {
    val predicates = ArrayList<Predicate>()
    val qOperationEntity = QOperationEntity("operationEntity")

    if (id?.isNotBlank() == true) {
        predicates.add(qOperationEntity.id.eq(id))
    }

    if (level != null) {
        predicates.add(qOperationEntity.level.eq(level))
    }

    if (bakerAddress?.isNotBlank() == true) {
        predicates.add(qOperationEntity.bakerAddress.eq(bakerAddress))
    }

    if (status != null) {
        predicates.add(qOperationEntity.status.eq(status))
    }

    if (owner?.isNotBlank() == true) {
        predicates.add(qOperationEntity.owner.eq(owner))
    }

    if (source?.isNotBlank() == true) {
        predicates.add(qOperationEntity.source.eq(source))
    }

    return andTogetherPredicates(predicates)
}

fun andTogetherPredicates(predicates: List<Predicate>) = BooleanBuilder().apply {
    predicates.forEach {
        and(it)
    }
}
