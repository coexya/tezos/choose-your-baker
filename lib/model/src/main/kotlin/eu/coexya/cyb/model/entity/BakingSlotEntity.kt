package eu.coexya.cyb.model.entity

import org.springframework.data.annotation.Id
import org.springframework.data.mongodb.core.mapping.Document

@Document("bakingSlots")
data class BakingSlotEntity(
    @Id
    val id: String? = null,
    val baker: String,
    val level: Long,
    val cycle: Int,
)
