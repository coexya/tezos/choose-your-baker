package eu.coexya.cyb.model.repository

import eu.coexya.cyb.model.entity.OperationUpdateEventEntity
import org.springframework.boot.autoconfigure.condition.ConditionalOnProperty
import org.springframework.data.mongodb.repository.ReactiveMongoRepository
import org.springframework.data.mongodb.repository.Tailable
import org.springframework.stereotype.Repository
import reactor.core.publisher.Flux
import java.time.OffsetDateTime

@Repository
@ConditionalOnProperty(name= ["sse.enabled"], havingValue="true", matchIfMissing = false)
interface OperationUpdateEventTailableRepository : ReactiveMongoRepository<OperationUpdateEventEntity, String> {
    @Tailable
    fun findByDateAfter(dateAfter: OffsetDateTime): Flux<OperationUpdateEventEntity>
}
