package eu.coexya.cyb.model.entity

import org.springframework.data.annotation.Id
import org.springframework.data.mongodb.core.mapping.Document
import java.time.OffsetDateTime

@Document(collection = "operationUpdateEvents")
data class OperationUpdateEventEntity(
    @Id
    val id: String? = null,
    val date: OffsetDateTime,
    val operation: OperationEntity,
)
