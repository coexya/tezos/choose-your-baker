package eu.coexya.cyb.model.inheritanceaware

import org.springframework.data.mongodb.core.ReactiveMongoOperations
import org.springframework.data.mongodb.repository.support.ReactiveMongoRepositoryFactoryBean
import org.springframework.data.repository.Repository
import org.springframework.data.repository.core.support.RepositoryFactorySupport
import java.io.Serializable


class InheritanceAwareReactiveMongoRepositoryFactoryBean<T : Repository<S, ID>?, S, ID : Serializable?>(
    repositoryInterface: Class<out T>
) : ReactiveMongoRepositoryFactoryBean<T, S, ID>(repositoryInterface) {

    override fun getFactoryInstance(operations: ReactiveMongoOperations): RepositoryFactorySupport {
        return InheritanceAwareReactiveMongoRepositoryFactory(operations)
    }

}
