package eu.coexya.cyb.model.repository

import eu.coexya.cyb.common.enums.OperationStatus
import eu.coexya.cyb.model.entity.OperationEntity
import kotlinx.coroutines.flow.Flow
import org.springframework.data.mongodb.repository.ReactiveMongoRepository
import org.springframework.data.querydsl.ReactiveQuerydslPredicateExecutor
import java.time.LocalDateTime

interface OperationRepository : ReactiveMongoRepository<OperationEntity, String>,
    ReactiveQuerydslPredicateExecutor<OperationEntity> {

    fun findByStatus(status: OperationStatus): Flow<OperationEntity>

    fun findByCreationDateBefore(creationDate: LocalDateTime): Flow<OperationEntity>
}
