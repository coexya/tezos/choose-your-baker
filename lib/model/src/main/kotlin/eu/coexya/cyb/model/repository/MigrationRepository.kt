package eu.coexya.cyb.model.repository

import eu.coexya.cyb.model.entity.MigrationEntity
import org.springframework.data.mongodb.repository.ReactiveMongoRepository
import org.springframework.stereotype.Repository

@Repository
interface MigrationRepository : ReactiveMongoRepository<MigrationEntity, String>
