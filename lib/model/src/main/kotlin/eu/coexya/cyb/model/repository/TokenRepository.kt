package eu.coexya.cyb.model.repository

import eu.coexya.cyb.model.entity.TokenEntity
import kotlinx.coroutines.flow.Flow
import org.springframework.data.mongodb.repository.ReactiveMongoRepository
import org.springframework.stereotype.Repository
import reactor.core.publisher.Mono
import java.time.OffsetDateTime

@Repository
interface TokenRepository : ReactiveMongoRepository<TokenEntity, String> {
    fun findByOwnerAndRevoked(owner: String, revoked: Boolean): Mono<TokenEntity>

    fun findByCreationDateBefore(expirationDate: OffsetDateTime): Flow<TokenEntity>
}
