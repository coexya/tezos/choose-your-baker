package eu.coexya.cyb.model.repository

import eu.coexya.cyb.model.entity.BakingSlotEntity
import org.springframework.data.mongodb.repository.ReactiveMongoRepository
import org.springframework.stereotype.Repository
import reactor.core.publisher.Flux
import reactor.core.publisher.Mono

@Repository
interface BakingSlotRepository : ReactiveMongoRepository<BakingSlotEntity, String> {
    // IntelliJ is wrongly saying exists queries should return boolean

    @Suppress("SpringDataRepositoryMethodReturnTypeInspection")
    fun existsByBakerAndLevel(baker: String, level: Long): Mono<Boolean>

    @Suppress("SpringDataRepositoryMethodReturnTypeInspection")
    fun existsByBakerAndLevelBetween(baker: String, level: Long, level2: Long): Mono<Boolean>

    @Suppress("SpringDataRepositoryMethodReturnTypeInspection")
    fun existsByCycle(cycle: Int): Mono<Boolean>

    fun findByCycleBefore(cycle: Int): Flux<BakingSlotEntity>

    fun findByBakerAndLevelBetween(baker: String, level: Long, level2: Long): Flux<BakingSlotEntity>
}
