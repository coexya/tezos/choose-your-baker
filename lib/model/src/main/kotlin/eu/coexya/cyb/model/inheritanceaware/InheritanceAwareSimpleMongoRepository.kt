package eu.coexya.cyb.model.inheritanceaware

import kotlinx.coroutines.reactive.awaitFirst
import kotlinx.coroutines.reactor.mono
import org.bson.Document
import org.springframework.data.annotation.TypeAlias
import org.springframework.data.domain.Sort
import org.springframework.data.mongodb.core.ReactiveMongoOperations
import org.springframework.data.mongodb.core.query.Criteria
import org.springframework.data.mongodb.core.query.Query
import org.springframework.data.mongodb.repository.query.MongoEntityInformation
import org.springframework.data.mongodb.repository.support.SimpleReactiveMongoRepository
import reactor.core.publisher.Flux
import reactor.core.publisher.Mono
import java.io.Serializable


class InheritanceAwareSimpleMongoRepository<T, ID : Serializable?>(
    private val entityInformation: MongoEntityInformation<T, ID>,
    private val mongoOperations: ReactiveMongoOperations
) : SimpleReactiveMongoRepository<T, ID>(entityInformation, mongoOperations) {

    private val classCriteria = if (entityInformation.javaType.isAnnotationPresent(TypeAlias::class.java)) {
        Criteria().orOperator(
            Criteria.where("_class")
                .`is`(entityInformation.javaType.getAnnotation(TypeAlias::class.java).value),
            Criteria.where("_class")
                .`is`(entityInformation.javaType.canonicalName)
        )
    } else {
        null
    }
    private val classCriteriaDocument = classCriteria?.criteriaObject ?: Document()


    override fun count(): Mono<Long> {
        return if (null != classCriteria) {
            mono {
                val collection = mongoOperations.getCollection(entityInformation.collectionName).awaitFirst()
                collection.countDocuments(classCriteriaDocument).awaitFirst()
            }
        } else {
            super.count()
        }
    }

    private fun findAll(query: Query): Flux<T> {
        if (null != classCriteria) {
            query.addCriteria(classCriteria)
        }
        return mongoOperations.find(
            query,
            entityInformation.javaType,
            entityInformation.collectionName
        )
    }

    override fun findAll(): Flux<T> {
        return findAll(Query())
    }

    override fun findAll(sort: Sort): Flux<T> {
        return findAll(Query().with(sort))
    }

}
