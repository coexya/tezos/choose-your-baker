package eu.coexya.cyb.model.configuration

import eu.coexya.cyb.common.enums.OperationStatus
import eu.coexya.cyb.common.utils.minimalOffsetDateTime
import eu.coexya.cyb.model.entity.OperationEntity
import eu.coexya.cyb.model.entity.OperationUpdateEventEntity
import eu.coexya.cyb.model.inheritanceaware.InheritanceAwareReactiveMongoRepositoryFactoryBean
import eu.coexya.cyb.model.inheritanceaware.InheritanceAwareSimpleMongoRepository
import kotlinx.coroutines.reactive.awaitSingle
import kotlinx.coroutines.runBlocking
import org.springframework.context.annotation.Configuration
import org.springframework.data.mongodb.config.EnableReactiveMongoAuditing
import org.springframework.data.mongodb.core.CollectionOptions
import org.springframework.data.mongodb.core.ReactiveMongoTemplate
import org.springframework.data.mongodb.repository.config.EnableReactiveMongoRepositories
import javax.annotation.PostConstruct

@Configuration
@EnableReactiveMongoAuditing
@EnableReactiveMongoRepositories(
    basePackages = ["eu.coexya.cyb.model.repository"],
    repositoryBaseClass = InheritanceAwareSimpleMongoRepository::class,
    repositoryFactoryBeanClass = InheritanceAwareReactiveMongoRepositoryFactoryBean::class
)
class MongoConfiguration(
    private val mongoTemplate: ReactiveMongoTemplate,
) {
    @PostConstruct
    fun initCollections() {
        runBlocking {
            if (!mongoTemplate.collectionExists(OperationUpdateEventEntity::class.java).awaitSingle()) {
                mongoTemplate.createCollection(
                    OperationUpdateEventEntity::class.java,
                    CollectionOptions.empty().capped().size(250000).maxDocuments(100)
                ).subscribe()

                // Inserting first element in collection. (Empty capped collection closes a stream)
                mongoTemplate.insert(
                    OperationUpdateEventEntity(
                        date = minimalOffsetDateTime(),
                        operation = OperationEntity(
                            owner = "NO_OWNER",
                            source = "NO_SOURCE",
                            status = OperationStatus.STALE,
                            opSize = 0,
                            tzOperation = OperationEntity.TzOperation("", emptyList(), "", null)
                        )
                    )
                ).subscribe()
            }
        }
    }
}
