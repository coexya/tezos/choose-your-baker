package eu.coexya.cyb.model.lifecycle

import eu.coexya.cyb.model.entity.OperationEntity
import eu.coexya.cyb.model.entity.OperationUpdateEventEntity
import eu.coexya.cyb.model.repository.OperationUpdateEventRepository
import org.springframework.data.mongodb.core.mapping.event.AbstractMongoEventListener
import org.springframework.data.mongodb.core.mapping.event.AfterSaveEvent
import org.springframework.stereotype.Service
import java.time.OffsetDateTime

@Service
class OperationLifecycleListener(
    private val operationUpdateEventRepository: OperationUpdateEventRepository
) : AbstractMongoEventListener<OperationEntity>() {

    override fun onAfterSave(event: AfterSaveEvent<OperationEntity>) {
        super.onAfterSave(event)

        operationUpdateEventRepository.save(
            OperationUpdateEventEntity(
                date = OffsetDateTime.now(),
                operation = event.source,
            )
        ).subscribe()
    }

}
