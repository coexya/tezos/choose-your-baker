package eu.coexya.cyb.model.entity

import eu.coexya.cyb.model.utils.VersionNumberComparator
import org.springframework.data.annotation.CreatedDate
import org.springframework.data.annotation.Id
import org.springframework.data.mongodb.core.mapping.Document
import java.time.Instant
import java.util.*

@Document(collection = "migrations")
data class MigrationEntity(
    @Id
    val id: String? = null,
    val name: String,
    val version: String,
    val hash: String,
    @CreatedDate
    val createdDate: Date = Date.from(Instant.now()),
) {
    companion object {
        fun versionComparator() = Comparator<MigrationEntity> { migration1, migration2 ->
            VersionNumberComparator.getInstance().compare(migration1.version, migration2.version)
        }
    }
}
