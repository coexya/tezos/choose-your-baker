package eu.coexya.cyb.model.repository

import eu.coexya.cyb.model.entity.OperationUpdateEventEntity
import org.springframework.data.mongodb.repository.ReactiveMongoRepository
import reactor.core.publisher.Flux

interface OperationUpdateEventRepository : ReactiveMongoRepository<OperationUpdateEventEntity, String> {

    fun findByOperation_Owner(owner: String): Flux<OperationUpdateEventEntity>

}
