package eu.coexya.cyb.api.tezos

import eu.coexya.cyb.common.enums.FeeMode

data class BakerIdentity(
    val name: String,
    val nbBakingSlotSearch: Long,
    val bakerAddress: String,
    val kycSmartContractAddress: String,
    val allowUnauthenticated: Boolean,
    val feeMode: FeeMode,
    val minimalNtzFees: Long,
    val minimalNtzPerGasUnit: Long,
    val minimalNtzPerByte: Long,
)
