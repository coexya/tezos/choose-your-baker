package eu.coexya.cyb.api.operation

import com.fasterxml.jackson.annotation.JsonInclude
import eu.coexya.cyb.business.model.Operation
import eu.coexya.cyb.business.model.Token

@JsonInclude(JsonInclude.Include.NON_NULL)
data class ProcessOperationResponse(
    val message: String,
    val operationId: String,
    val operationHash: String,
    val token: Token? = null,
)
