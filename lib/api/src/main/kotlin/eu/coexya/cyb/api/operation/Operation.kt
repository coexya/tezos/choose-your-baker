package eu.coexya.cyb.api.operation

import com.fasterxml.jackson.annotation.JsonInclude
import eu.coexya.cyb.common.enums.OperationStatus
import java.time.LocalDateTime

@JsonInclude(JsonInclude.Include.NON_NULL)
data class Operation(
    val id: String? = null,
    val lastStatusUpdate: LocalDateTime = LocalDateTime.now(),
    val creationDate: LocalDateTime = LocalDateTime.now(),
    val level: Long? = null,
    val requestedLevel: Long? = null,
    val fetchedLevel: Long? = null,
    val timesFetched: Int = 0,
    val errorMessage: String? = null,
    val bakerAddress: String? = null,
    val status: OperationStatus,
    val owner: String,
    val totalFee: Long? = null,
    val estimatedConsumption: Consumption? = null,
    val actualConsumption: Consumption? = null,
) {
    data class Consumption(
        val totalStorageSize: Long = 0,
        val totalConsumedGas: Long = 0,
    )
}
