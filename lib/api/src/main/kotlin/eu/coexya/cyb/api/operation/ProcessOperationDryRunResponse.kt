package eu.coexya.cyb.api.operation

import com.fasterxml.jackson.annotation.JsonInclude

@JsonInclude(JsonInclude.Include.NON_NULL)
data class ProcessOperationDryRunResponse(
    val message: String,
    val operationHash: String,
    val operation: Operation,
)
