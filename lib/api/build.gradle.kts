val mongockVersion: String by project.extra

plugins {
    id("kotlin")
}

val artefactName = "communication-api"
description = "Bean representation of communication api between client and transaction manager"

dependencies {
    implementation("org.jetbrains.kotlin:kotlin-stdlib-jdk8")
    implementation("org.jetbrains.kotlin:kotlin-reflect")
    // Jackson
    implementation("com.fasterxml.jackson.module:jackson-module-kotlin")

    // Project libs
    api(project(":lib:common"))
    implementation(project(":lib:business"))
}

tasks {
    jar {
        enabled = true
    }
}

tasks.register<Jar>("sourcesJar") {
    archiveClassifier.set("sources")
    from(sourceSets.main.get().allSource)
}

publishing {
    publications {
        create<MavenPublication>("maven") {
            artifactId = artefactName
            from(components["kotlin"])
            artifact(tasks["sourcesJar"])
        }
    }
}
