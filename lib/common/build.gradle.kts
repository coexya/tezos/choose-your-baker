plugins {
    id("kotlin")
}

val artefactName = "common"
description = "Bean representation of common objects not linked to business or model"

dependencies {
    val bouncyCastleVersion: String by project.extra

    implementation("org.jetbrains.kotlin:kotlin-stdlib-jdk8")
    implementation("org.jetbrains.kotlin:kotlin-reflect")

    implementation("org.bouncycastle:bcprov-jdk15on:$bouncyCastleVersion")
}

tasks {
    jar {
        enabled = true
    }
}

publishing {
    publications {
        create<MavenPublication>("maven") {
            artifactId = artefactName
            artifact(tasks["jar"])
        }
    }
}
