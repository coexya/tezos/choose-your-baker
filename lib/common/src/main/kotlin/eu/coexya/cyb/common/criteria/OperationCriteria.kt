package eu.coexya.cyb.common.criteria

import eu.coexya.cyb.common.enums.OperationStatus

data class OperationCriteria(
    val id: String? = null,
    val level: Long? = null,
    val bakerAddress: String? = null,
    val status: OperationStatus? = null,
    val owner: String? = null,
    val source: String? = null,
)
