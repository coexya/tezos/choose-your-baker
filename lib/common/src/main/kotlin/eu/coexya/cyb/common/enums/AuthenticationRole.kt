package eu.coexya.cyb.common.enums

object AuthenticationRole {
    const val ADMIN = "ADMIN"
    const val ISSUER = "ISSUER"
    const val UNAUTHENTICATED_ISSUER = "UNAUTHENTICATED_ISSUER"
}
