package eu.coexya.cyb.common.enums

enum class OperationStatus {
    BAKED,
    BAKED_BY_ANOTHER,
    DELETED,
    IN_PROGRESS,
    PENDING,
    REJECTED,
    STALE,
}
