package eu.coexya.cyb.common.enums

enum class FeeMode {
    FEES,
    TRANSACTION,
}
