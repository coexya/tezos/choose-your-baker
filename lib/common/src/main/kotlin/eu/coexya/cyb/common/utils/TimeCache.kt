package eu.coexya.cyb.common.utils

import java.time.Duration
import java.time.OffsetDateTime

abstract class TimeCache<T>(
    private var cached: T,
    private var duration: Duration,
) {
    private var lastRetrieval: OffsetDateTime? = null

    fun get(): T {
        if (lastRetrieval == null || lastRetrieval!!.plus(duration).isBefore(OffsetDateTime.now())) {
            cached = update()
            lastRetrieval = OffsetDateTime.now()
        }
        return cached
    }

    abstract fun update(): T

}
