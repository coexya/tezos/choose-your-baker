package eu.coexya.cyb.common.utils

import java.time.OffsetDateTime

fun minimalOffsetDateTime(): OffsetDateTime = OffsetDateTime.parse("1970-01-01T00:00Z")

const val APPLICATION_CSV_VALUE = "application/csv"

fun bearer(jwtToken: String) = "Bearer $jwtToken"

const val REQUESTED_LEVEL_PASSED = "Requested level has passed."
const val OPERATION_NOT_APPLIED = "Operation was not applied in blockchain."
