val flapdoodleVersion: String by project.extra
val mockKVersion: String by project.extra
val springmockkVersion: String by project.extra
val tezosNodeConnectorVersion: String by project.extra
val flapdoodleEmbedMongoVersion: String by project.extra

plugins {
    id("kotlin")
    id("org.springframework.boot")
    kotlin("plugin.spring")
}

val artefactName = "business"
description = "Business representation of application data"

dependencies {
    // Kotlin
    implementation("org.jetbrains.kotlin:kotlin-stdlib-jdk8")
    implementation("org.jetbrains.kotlin:kotlin-reflect")
    implementation("org.jetbrains.kotlinx:kotlinx-coroutines-core")
    implementation("org.jetbrains.kotlinx:kotlinx-coroutines-reactor")

    // Jackson
    implementation("com.fasterxml.jackson.module:jackson-module-kotlin")

    // Common CSV
    implementation("org.apache.commons:commons-csv:1.9.0")

    // Spring
    implementation("org.springframework.boot:spring-boot-starter-integration")
    implementation("org.springframework.integration:spring-integration-mongodb")

    // MongoDB
    implementation("org.mongodb:mongodb-driver-sync")

    // Project libs
    implementation(project(":lib:model"))

    // Nexus
    api("eu.coexya.tezos:tezos-node-connector:$tezosNodeConnectorVersion")

    // Test
    testImplementation("org.springframework.boot:spring-boot-starter-test") {
        exclude(module = "mockito-core")
    }
    testImplementation("de.flapdoodle.embed:de.flapdoodle.embed.mongo.spring:$flapdoodleEmbedMongoVersion")
    testImplementation("io.mockk:mockk:$mockKVersion")
    testImplementation("com.ninja-squad:springmockk:$springmockkVersion")
}

tasks {
    jar {
        enabled = true
    }
    bootJar {
        enabled = false
    }
}
