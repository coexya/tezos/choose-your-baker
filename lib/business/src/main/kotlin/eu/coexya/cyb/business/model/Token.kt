package eu.coexya.cyb.business.model

import com.fasterxml.jackson.annotation.JsonInclude
import java.time.OffsetDateTime

@JsonInclude(JsonInclude.Include.NON_NULL)
data class Token(
    val id: String,
    val name: String,
    val expirationDate: OffsetDateTime? = null,
    val creationDate: OffsetDateTime,
    val roles: List<String>,
    val owner: String,
    val revoked: Boolean,
    val jwtToken: String? = null,
)
