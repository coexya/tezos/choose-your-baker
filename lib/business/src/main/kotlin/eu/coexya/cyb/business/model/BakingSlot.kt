package eu.coexya.cyb.business.model

data class BakingSlot(
    val baker: String,
    val level: Long,
    val cycle: Int,
)
