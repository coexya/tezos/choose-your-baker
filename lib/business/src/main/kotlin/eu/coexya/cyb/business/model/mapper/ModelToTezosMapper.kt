package eu.coexya.cyb.business.model.mapper

import eu.coexya.cyb.model.entity.OperationEntity
import eu.coexya.tezos.connector.node.model.TzOperation

fun OperationEntity.TzOperation.toTezos() = TzOperation(
    hash = hash,
    contents = contents,
    branch = branch,
    signature = signature,
)
