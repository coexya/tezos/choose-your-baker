package eu.coexya.cyb.business.model

import com.fasterxml.jackson.annotation.JsonInclude
import eu.coexya.cyb.common.enums.OperationStatus
import eu.coexya.tezos.connector.node.model.TzOperation
import java.time.LocalDateTime

@JsonInclude(JsonInclude.Include.NON_NULL)
data class Operation(
    val id: String? = null,
    val lastStatusUpdate: LocalDateTime = LocalDateTime.now(),
    val creationDate: LocalDateTime = LocalDateTime.now(),
    val level: Long? = null,
    val requestedLevel: Long? = null,
    val fetchedLevel: Long? = null,
    val timesFetched: Int = 0,
    val errorMessage: String? = null,
    val bakerAddress: String? = null,
    val status: OperationStatus,
    val source: String,
    val owner: String,
    val totalFee: Long? = null,
    val opSize: Int,
    val estimatedConsumption: Consumption? = null,
    val actualConsumption: Consumption? = null,
    val tzOperation: TzOperation,
) {
    data class Consumption(
        val totalStorageSize: Long = 0,
        val totalConsumedGas: Long = 0,
    )
}
