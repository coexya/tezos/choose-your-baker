package eu.coexya.cyb.business.exception

import eu.coexya.cyb.business.model.Token

/**
 * Server errors.
 * Translated to HTTP 5xx errors.
 */
sealed class ServiceException(
    open val errorCode: Int,
    override val message: String,
) : RuntimeException()

class BakerAddressNotConfiguredException :
    ServiceException(501, "Baker address not configured")

class UnsuccessfulDryRunException(cause: Throwable) :
    ServiceException(502, "Dry run was unsuccessful: ${cause.message}")

class UnsuccessfulForgeException(cause: Throwable) :
    ServiceException(503, "Forge was unsuccessful: ${cause.message}")

class UnSynchronizedNodeException :
    ServiceException(504, "Node is not synchronized")

class UnauthorizedConfigException :
    ServiceException(505, "Fees cannot be at 0 when in TRANSACTION Fee mode.")

/**
 * User input errors.
 * Translated to HTTP 4xx errors.
 */
sealed class UserServiceException(
    override val errorCode: Int,
    override val message: String,
) : ServiceException(errorCode, message)

sealed class AuthenticationException(
    override val errorCode: Int,
    override val message: String,
) : UserServiceException(errorCode, message) {
    class RevokedTokenException(tokenId: String) :
        AuthenticationException(1, "Token '$tokenId' does not exist or has been revoked.")

    class ExpiredTokenException(token: Token) :
        AuthenticationException(2, "Token '${token.name}' expired since ${token.expirationDate}.")
}

class EntityNotFoundException(name: String, id: String) :
    UserServiceException(3, "The $name with id='$id' was not found.")

class DuplicateException(message: String) :
    UserServiceException(4, message)

class OperationIsNotPendingException :
    UserServiceException(5, "Only pending operations can be deleted.")

class MissingBakingRightsException(msg: String) :
    UserServiceException(6, "Baker node is missing baking rights: $msg")

class InsufficientFeesException(fees: Long) :
    UserServiceException(7, "Fees are insufficient for this operation. Expected : $fees nanotez")

class OperationRetriedTooManyTimes :
    UserServiceException(8, "Operation was rejected by the baker too many times.")

class UnsupportedContentTypeException :
    UserServiceException(9, "Content-Type is not supported.")

class NotAnAdminFeatureException :
    UserServiceException(10, "Admins do not have access to this feature.")





