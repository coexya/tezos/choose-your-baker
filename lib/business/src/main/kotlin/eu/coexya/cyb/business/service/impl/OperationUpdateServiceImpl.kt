package eu.coexya.cyb.business.service.impl

import eu.coexya.cyb.business.exception.OperationRetriedTooManyTimes
import eu.coexya.cyb.business.exception.UnSynchronizedNodeException
import eu.coexya.cyb.business.logger
import eu.coexya.cyb.business.model.mapper.toModel
import eu.coexya.cyb.business.model.mapper.toTezos
import eu.coexya.cyb.business.service.BakerService
import eu.coexya.cyb.business.service.OperationUpdateService
import eu.coexya.cyb.common.enums.OperationStatus
import eu.coexya.cyb.common.utils.OPERATION_NOT_APPLIED
import eu.coexya.cyb.common.utils.REQUESTED_LEVEL_PASSED
import eu.coexya.cyb.model.entity.OperationEntity
import eu.coexya.cyb.model.entity.hasRequestedLevel
import eu.coexya.cyb.model.repository.OperationRepository
import eu.coexya.tezos.connector.node.model.*
import eu.coexya.tezos.connector.service.TezosReaderService
import kotlinx.coroutines.flow.toList
import kotlinx.coroutines.reactive.awaitSingle
import org.springframework.beans.factory.annotation.Value
import org.springframework.stereotype.Service
import java.time.Duration
import java.time.LocalDateTime
import java.time.OffsetDateTime
import java.time.Period

@Service
class OperationUpdateServiceImpl(
    private val bakerService: BakerService,
    private val tezosReaderService: TezosReaderService,
    private val operationRepository: OperationRepository,
    @Value("\${daemon.update.startupBlocks: #{60}}") val startupBlocks: Long,
    @Value("\${daemon.update.headOffset: #{30}}") val headOffset: Long,
    @Value("\${daemon.update.retries: #{2}}") val retries: Int,
    @Value("\${daemon.update.rejectAfter: #{10}}") val rejectAfter: Int,
    @Value("\${daemon.update.staleAfter:#{T(java.time.Duration).ofHours(2)}}") val staleAfter: Duration,
    @Value("\${daemon.purge.purgeAfter:#{T(java.time.Period).ofYears(1)}}") val purgeAfter: Period,
) : OperationUpdateService {

    var lastHead: TzBlock? = null

    override suspend fun startupUpdateOperationsStatus() {
        val headBlock = tezosReaderService.getHeadBlock(headOffset)
        val blocks = getPredecessors(headBlock, startupBlocks - 1).plus(headBlock)
        lastHead = headBlock
        updateInProgressOperations(blocks)

        updatePendingOperations(headBlock)
    }

    override suspend fun updateOperationStatus() {
        if (lastHead != null) {
            val headBlock = tezosReaderService.getHeadBlock(headOffset)
            val nbNewBlocks = headBlock.header.level - lastHead!!.header.level
            val blocks = getPredecessors(headBlock, nbNewBlocks).plus(headBlock)
            updateInProgressOperations(blocks)
            // Update the last head visited
            lastHead = headBlock
        } else {
            LOGGER.error("Variable: 'lastHead' is not defined, there was a problem at startup.")
        }
    }

    override suspend fun purgeOldOperations() {
        LOGGER.debug("Purge old operations.")
        operationRepository.findByCreationDateBefore(OffsetDateTime.now().minus(purgeAfter).toLocalDateTime())
            .toList()
            .onEach {
                operationRepository.delete(it).subscribe()
            }
            .let {
                if (it.isNotEmpty()) {
                    LOGGER.debug("Purged old operations [nb=${it.size}].")
                }
            }
    }

    suspend fun updateInProgressOperations(blocks: List<TzBlock>) {
        LOGGER.debug("Update IN_PROGRESS operations.")
        val operations = operationRepository.findByStatus(OperationStatus.IN_PROGRESS).toList()

        val nbUpdated = processOperationsToUpdate(
            operations = operations,
            blocks = blocks,
        ).map {
            operationRepository.save(it).awaitSingle()
        }.size

        if (nbUpdated != 0) {
            LOGGER.debug("Updated IN_PROGRESS operations [nbUpdated=$nbUpdated].")
        }
    }

    suspend fun updatePendingOperations(headBlock: TzBlock) {
        LOGGER.debug("Update PENDING operations.")
        val operations = operationRepository.findByStatus(OperationStatus.PENDING).toList()

        val nbUpdated = operations.map { operation ->
            if (operation.hasRequestedLevel() && operation.requestedLevel!! <= headBlock.header.level) {
                // The operation requested a specific level that passed, it won't be baked
                operation.copy(
                    status = OperationStatus.REJECTED,
                    errorMessage = REQUESTED_LEVEL_PASSED,
                )
            } else if (isStale(operation)) {
                // The operation is too old
                operation.copy(status = OperationStatus.STALE)
            } else {
                null
            }
        }.map {
            if (it != null) {
                operationRepository.save(it).awaitSingle()
            }
        }.size

        if (nbUpdated != 0) {
            LOGGER.debug("Updated PENDING operations [nbUpdated=$nbUpdated].")
        }
    }

    private suspend fun getPredecessors(headBlock: TzBlock, nbPredecessors: Long): List<TzBlock> {
        return (nbPredecessors downTo 1).map { depth ->
            tezosReaderService.getBlock(headBlock.hash, depth)
                ?: throw UnSynchronizedNodeException()
        }
    }

    private suspend fun processOperationsToUpdate(
        operations: List<OperationEntity>,
        blocks: List<TzBlock>,
    ): List<OperationEntity> {
        val toUpdate = mutableListOf<OperationEntity>()
        for (operation in operations) {
            for (block in blocks) {
                val blockOperations = block.operations.flatten()
                val operationInBlock = blockOperations.find { it.hash == operation.tzOperation.hash }
                val isOperationLevelPassed =
                    operation.fetchedLevel != null && operation.fetchedLevel!! <= block.header.level

                val operationToUpdate = if (operationInBlock != null) {

                    if (operationInBlock.isApplied()) {
                        operation.copy(
                            level = block.header.level,
                            bakerAddress = block.getBakerAddress(),
                            status = if (bakerService.isKnownBaker(block.getBakerAddress())) {
                                OperationStatus.BAKED
                            } else {
                                OperationStatus.BAKED_BY_ANOTHER
                            },
                            actualConsumption = OperationEntity.Consumption(
                                totalStorageSize = operationInBlock.totalStorageSize(),
                                totalConsumedGas = operationInBlock.totalConsumedGas(),
                            ),
                            tzOperation = operationInBlock.toModel(),
                        )
                    } else {
                        operation.copy(
                            status = OperationStatus.REJECTED,
                            errorMessage = OPERATION_NOT_APPLIED,
                            actualConsumption = OperationEntity.Consumption(
                                totalStorageSize = operationInBlock.totalStorageSize(),
                                totalConsumedGas = operationInBlock.totalConsumedGas(),
                            ),
                            tzOperation = operationInBlock.toModel(),
                        )
                    }

                } else { // Operation not in block
                    if (isOperationLevelPassed) {
                        if (operation.hasRequestedLevel()) {
                            // The operation requested a specific level that passed, it won't be baked
                            operation.copy(
                                status = OperationStatus.REJECTED,
                                errorMessage = REQUESTED_LEVEL_PASSED,
                            )

                        } else {
                            if (isStale(operation)) {
                                // The operation is too old
                                operation.copy(status = OperationStatus.STALE)

                            } else {

                                try {

                                    if (operation.timesFetched > rejectAfter) {
                                        throw OperationRetriedTooManyTimes()
                                    }

                                    // The operation was not yet baked, it returns to pending to be sent back to the baker
                                    if (operation.timesFetched > retries) {


                                        // TODO Fix : Will dry run an operation if multiple blocks are read for update
                                        LOGGER.debug("Operation with id : '${operation.id}' was fetched too many times, dry running it again.")
                                        tezosReaderService.dryRun(operation.tzOperation.toTezos())
                                        operation.copy(status = OperationStatus.PENDING, fetchedLevel = null)


                                    } else {
                                        operation.copy(status = OperationStatus.PENDING, fetchedLevel = null)
                                    }

                                } catch (e: Exception) {
                                    operation.copy(
                                        status = OperationStatus.REJECTED,
                                        errorMessage = e.message
                                    )
                                }
                            }
                        }
                    } else {
                        null
                    }
                }

                if (operationToUpdate != null && toUpdate.find { it.id == operation.id } == null) {
                    toUpdate.add(operationToUpdate)
                }

            }

            // Stop when all operations will be updated
            if (operations.size == toUpdate.size) {
                return toUpdate
            }

        }
        return toUpdate
    }

    private fun isStale(operation: OperationEntity): Boolean {
        return LocalDateTime.now().isAfter(
            operation.creationDate.plus(staleAfter)
        )
    }

    companion object {
        val LOGGER = logger()
    }
}
