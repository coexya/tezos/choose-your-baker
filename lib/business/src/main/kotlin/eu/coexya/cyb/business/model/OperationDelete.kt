package eu.coexya.cyb.business.model

data class OperationDelete(
    val id: String,
)
