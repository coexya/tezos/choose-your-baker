package eu.coexya.cyb.business.service.impl

import eu.coexya.cyb.business.configuration.TezosConfig
import eu.coexya.cyb.business.exception.InsufficientFeesException
import eu.coexya.cyb.business.service.FeeManagementService
import eu.coexya.cyb.model.entity.OperationEntity
import org.springframework.stereotype.Service

class FeesFMS(
    override val tezosConfig: TezosConfig,
) : FeeManagementService {

    override fun checkFees(operation: OperationEntity) {
        val expectedNtzFees = getExpectedFees(operation)

        val actualNtzFees = (operation.totalFee ?: 0) * 1000

        if (actualNtzFees < expectedNtzFees) {
            throw InsufficientFeesException(expectedNtzFees)
        }
    }

}
