package eu.coexya.cyb.business.service

import eu.coexya.cyb.business.exception.MissingBakingRightsException
import eu.coexya.cyb.business.model.BakingSlot

interface BakerService {

    /**
     * Check if given delegate is a known baker
     */
    suspend fun isKnownBaker(delegate: String): Boolean

    /**
     * Checks if the given baker has baking rights in the next range levels
     * @throws MissingBakingRightsException when there is no baking slot in the next configured amount of levels
     */
    @Throws(MissingBakingRightsException::class)
    suspend fun checkHasBakingSlot()

    /**
     * Checks if the given baker has baking rights in the given level
     * @param level the level to check
     * @throws MissingBakingRightsException when there is no baking slot at the given level
     */
    @Throws(MissingBakingRightsException::class)
    suspend fun checkHasBakingSlotAtLevel(level: Long)

    /**
     * Update the cache with all bakings slots for the baker
     */
    suspend fun updateBakingSlotsCache()

    /**
     * Purges the database from all the baking slots
     */
    suspend fun purgeBakingSlotsCache()

    /**
     * Gets the next known baking slots
     */
    suspend fun getBakingSlots(): List<BakingSlot>
}
