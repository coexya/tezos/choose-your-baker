package eu.coexya.cyb.business

import kotlinx.coroutines.flow.Flow
import kotlinx.coroutines.flow.drop
import kotlinx.coroutines.flow.take
import org.slf4j.Logger
import org.slf4j.LoggerFactory
import org.springframework.data.domain.Pageable

inline fun <reified R : Any> R.logger(): Logger =
    LoggerFactory.getLogger(this::class.java.name.substringBefore("\$Companion"))

fun <T> Flow<T>.paginate(pageable: Pageable) =if (pageable.isUnpaged) {
    this
} else {
    drop(pageable.offset.toInt()).take(pageable.pageSize)
}
