package eu.coexya.cyb.business.service

import eu.coexya.cyb.business.model.OperationUpdateEvent
import kotlinx.coroutines.flow.Flow

interface OperationNotificationService {

    /**
     * Subscribe to all events on requester's operations
     * @param requester the requester
     * @return An open flux of events
     */
    fun subscribeOperationUpdateEvents(requester: String): Flow<OperationUpdateEvent>

}
