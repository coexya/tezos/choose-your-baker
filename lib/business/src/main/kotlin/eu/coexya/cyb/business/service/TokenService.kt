package eu.coexya.cyb.business.service

import eu.coexya.cyb.business.exception.AuthenticationException
import eu.coexya.cyb.business.model.Token
import eu.coexya.cyb.business.model.TokenCreate
import eu.coexya.cyb.business.model.TokenPatch
import kotlinx.coroutines.flow.Flow

interface TokenService {

    /**
     * Create a new access token.
     * @param tokenDetails Details about the token to create.
     * @return The created token.
     */
    suspend fun createToken(tokenDetails: TokenCreate): Token

    /**
     * Retrieve the list of tokens.
     * @return The list of tokens belonging to the requester.
     */
    suspend fun findAll(): Flow<Token>

    /**
     * Check a token validity.
     * @param owner ID of the token to retrieve.
     * @throws AuthenticationException if the check fails.
     */
    @Throws(AuthenticationException::class)
    suspend fun getAndCheckToken(owner: String): Token

    /**
     * Check a token validity.
     * @param owner ID of the token to retrieve.
     */
    suspend fun checkIfAdmin(owner: String): Boolean

    /**
     * Update a token with provided fields.
     * @param tokenId ID of the token to update.
     * @param tokenDetails Fields to update.
     * @return The updated token.
     */
    suspend fun patchToken(tokenId: String, tokenDetails: TokenPatch): Token

    /**
     * Delete the token with the provided id.
     * @param tokenId ID of the token to remove.
     */
    suspend fun deleteToken(tokenId: String)

    /**
     * Purges all expired tokens from the database
     */
    suspend fun purgeExpiredTokens()
}
