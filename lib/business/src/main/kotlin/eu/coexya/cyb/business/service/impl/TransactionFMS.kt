package eu.coexya.cyb.business.service.impl

import eu.coexya.cyb.business.configuration.TezosConfig
import eu.coexya.cyb.business.exception.InsufficientFeesException
import eu.coexya.cyb.business.service.FeeManagementService
import eu.coexya.cyb.model.entity.OperationEntity
import eu.coexya.tezos.connector.node.model.OperationKind
import eu.coexya.tezos.connector.node.model.getLong

class TransactionFMS(
    override val tezosConfig: TezosConfig,
) : FeeManagementService {

    override fun checkFees(operation: OperationEntity) {
        val expectedNtzFees = getExpectedFees(operation)

        val transactions =
            operation.tzOperation.contents.filter {
                it["kind"] == OperationKind.TRANSACTION
                        && it["destination"] == tezosConfig.bakerAddress
            }

        val actualNtzFees = transactions.sumOf { it.getLong("amount") } * 1000

        if (actualNtzFees < expectedNtzFees) {
            throw InsufficientFeesException(expectedNtzFees)
        }
    }

}
