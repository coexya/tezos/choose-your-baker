package eu.coexya.cyb.business.model

import java.time.Duration

data class TokenCreate(
    val name: String,
    val duration: Duration? = null,
    val roles: List<String>? = null,
)
