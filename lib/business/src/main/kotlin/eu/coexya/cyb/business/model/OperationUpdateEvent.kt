package eu.coexya.cyb.business.model

import java.time.OffsetDateTime

data class OperationUpdateEvent(
    val date: OffsetDateTime,
    val operation: Operation,
)
