package eu.coexya.cyb.business.configuration

import eu.coexya.cyb.business.exception.BakerAddressNotConfiguredException
import eu.coexya.cyb.common.enums.FeeMode
import org.springframework.boot.context.properties.ConfigurationProperties
import org.springframework.stereotype.Component

@Component
@ConfigurationProperties(prefix = "tezos")
class TezosConfig {
    var name: String = "Unnamed"
    var nbBakingSlotSearch: Long = 10
    var allowUnauthenticated: Boolean = false
    var bakerAddress: String = ""
        get() = field.ifEmpty {
            throw BakerAddressNotConfiguredException()
        }
    var kycSmartContractAddress: String = ""

    var node: Node = Node()
    class Node {
        var url: String? = null
    }

    var fees: Fees = Fees()
    class Fees {
        var mode: FeeMode = FeeMode.FEES
        var minimalNtzFees: Long = 100000
        var minimalNtzPerGasUnit: Long = 100
        var minimalNtzPerByte: Long = 1000
    }
}
