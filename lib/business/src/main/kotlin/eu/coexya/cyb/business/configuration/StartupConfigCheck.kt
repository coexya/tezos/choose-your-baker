package eu.coexya.cyb.business.configuration

import eu.coexya.cyb.business.exception.UnauthorizedConfigException
import eu.coexya.cyb.common.enums.FeeMode
import org.springframework.boot.context.event.ApplicationReadyEvent
import org.springframework.context.event.EventListener
import org.springframework.stereotype.Component

@Component
class StartupConfigCheck(
    val tezosConfig: TezosConfig,
) {
    @EventListener(ApplicationReadyEvent::class)
    fun startupConfigCheck() {
        if (
            tezosConfig.fees.mode == FeeMode.TRANSACTION
            && tezosConfig.fees.minimalNtzFees == 0L
            && tezosConfig.fees.minimalNtzPerGasUnit == 0L
            && tezosConfig.fees.minimalNtzPerByte == 0L
        ) {
            throw UnauthorizedConfigException()
        }
    }
}
