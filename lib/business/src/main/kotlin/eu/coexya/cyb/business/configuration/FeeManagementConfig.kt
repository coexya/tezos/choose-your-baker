package eu.coexya.cyb.business.configuration

import eu.coexya.cyb.business.service.FeeManagementService
import eu.coexya.cyb.business.service.impl.FeesFMS
import eu.coexya.cyb.business.service.impl.TransactionFMS
import eu.coexya.cyb.common.enums.FeeMode
import org.springframework.context.annotation.Bean
import org.springframework.context.annotation.Configuration

@Configuration
class FeeManagementConfig {

    @Bean
    fun feeManagementService(tezosConfig: TezosConfig): FeeManagementService {
        return when (tezosConfig.fees.mode) {
            FeeMode.FEES -> FeesFMS(tezosConfig)
            FeeMode.TRANSACTION -> TransactionFMS(tezosConfig)
        }
    }

}
