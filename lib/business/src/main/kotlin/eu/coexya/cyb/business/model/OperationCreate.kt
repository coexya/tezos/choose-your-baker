package eu.coexya.cyb.business.model

import eu.coexya.tezos.connector.node.model.TzOperation

data class OperationCreate(
    val level: Long? = null,
    val tzOperation: TzOperation,
)
