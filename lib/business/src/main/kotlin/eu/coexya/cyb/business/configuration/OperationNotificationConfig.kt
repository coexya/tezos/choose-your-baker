package eu.coexya.cyb.business.configuration

import eu.coexya.cyb.business.logger
import eu.coexya.cyb.business.model.OperationUpdateEvent
import eu.coexya.cyb.business.model.integration.OperationUpdateEventMessage
import eu.coexya.cyb.business.model.mapper.toBusiness
import eu.coexya.cyb.model.repository.OperationUpdateEventTailableRepository
import org.springframework.boot.autoconfigure.condition.ConditionalOnProperty
import org.springframework.context.annotation.Bean
import org.springframework.context.annotation.Configuration
import org.springframework.integration.channel.FluxMessageChannel
import org.springframework.integration.util.IntegrationReactiveUtils
import java.time.OffsetDateTime

@Configuration
@ConditionalOnProperty(name = ["sse.enabled"], havingValue = "true", matchIfMissing = false)
class OperationNotificationConfig {

    @Bean
    fun operationUpdateEventsMessageChannel(operationUpdateEventTailableRepository: OperationUpdateEventTailableRepository): FluxMessageChannel {
        val messageChannel = FluxMessageChannel()
        messageChannel.subscribeTo(
            operationUpdateEventTailableRepository.findByDateAfter(OffsetDateTime.now())
                .map { OperationUpdateEventMessage(it.toBusiness()) }
        )
        IntegrationReactiveUtils.messageChannelToFlux<OperationUpdateEvent>(messageChannel).doOnEach { signal ->
            signal.get()?.let {
                LOGGER.debug("Operation status updated [id=${it.payload.operation.id}, status=${it.payload.operation.status}].")
            }
        }.subscribe()
        return messageChannel
    }

    companion object {
        val LOGGER = logger()
    }

}


