package eu.coexya.cyb.business.service.impl

import eu.coexya.cyb.business.exception.*
import eu.coexya.cyb.business.logger
import eu.coexya.cyb.business.model.Operation
import eu.coexya.cyb.business.model.OperationCreate
import eu.coexya.cyb.business.model.mapper.toBusiness
import eu.coexya.cyb.business.model.mapper.toModel
import eu.coexya.cyb.business.model.mapper.toTezos
import eu.coexya.cyb.business.paginate
import eu.coexya.cyb.business.service.BakerService
import eu.coexya.cyb.business.service.FeeManagementService
import eu.coexya.cyb.business.service.OperationService
import eu.coexya.cyb.business.service.TokenService
import eu.coexya.cyb.common.criteria.OperationCriteria
import eu.coexya.cyb.common.enums.OperationStatus
import eu.coexya.cyb.model.entity.OperationEntity
import eu.coexya.cyb.model.entity.hasRequestedLevel
import eu.coexya.cyb.model.mapper.toPredicate
import eu.coexya.cyb.model.repository.OperationRepository
import eu.coexya.tezos.connector.node.model.*
import eu.coexya.tezos.connector.service.TezosReaderService
import kotlinx.coroutines.flow.Flow
import kotlinx.coroutines.flow.filter
import kotlinx.coroutines.flow.map
import kotlinx.coroutines.flow.toList
import kotlinx.coroutines.reactive.asFlow
import kotlinx.coroutines.reactive.awaitFirstOrNull
import kotlinx.coroutines.reactive.awaitSingle
import kotlinx.coroutines.runBlocking
import org.springframework.data.domain.Pageable
import org.springframework.stereotype.Service

@Service
class OperationServiceImpl(
    private val bakerService: BakerService,
    private val tokenService: TokenService,
    private val feeManagementService: FeeManagementService,
    private val tezosReaderService: TezosReaderService,
    private val operationRepository: OperationRepository,
) : OperationService {

    override suspend fun getOperations(criteria: OperationCriteria?, pageable: Pageable): Flow<Operation> {
        LOGGER.debug("Get operations.")
        return if (criteria != null) {
            operationRepository.findAll(criteria.toPredicate(), pageable.sort)
        } else {
            operationRepository.findAll(pageable.sort)
        }.asFlow().paginate(pageable)
            .map { it.toBusiness() }
    }

    override suspend fun findByIdOperation(requester: String, id: String): Operation {
        LOGGER.debug("Find operation [id=$id].")
        return operationRepository.findById(id).awaitFirstOrNull()?.let {
            if (!tokenService.checkIfAdmin(requester) && it.owner != requester) {
                throw IllegalAccessException("User does not have access to this operation.")
            }
            it.toBusiness()
        } ?: throw EntityNotFoundException("operation", id)
    }

    override suspend fun getMempool(): List<TzOperation> {
        LOGGER.debug("Fetch mempool.")
        val pendingOperations = operationRepository.findByStatus(OperationStatus.PENDING)
        val bakingSlot = tezosReaderService.getHeadBlockLevel() + 1

        val filteredTzOperations = pendingOperations
            // TODO filter by operation age
            .filter { !it.hasRequestedLevel() || it.requestedLevel == bakingSlot }
            // Update operations statuses
            .map {
                operationRepository.save(
                    it.copy(
                        status = OperationStatus.IN_PROGRESS,
                        fetchedLevel = bakingSlot,
                        timesFetched = it.timesFetched + 1,
                    )
                ).awaitSingle()
            }
            .map { it.tzOperation.toTezos() }.toList()

        LOGGER.info("Mempool fetched [level=$bakingSlot, nbOperationsSent=${filteredTzOperations.size}].")

        return filteredTzOperations
    }

    override suspend fun processOperation(requester: String, operationDetails: OperationCreate): Operation {
        LOGGER.debug("Process operation.")
        val operation = verifyOperation(requester, operationDetails)
        LOGGER.debug("Operation processed [id=${operation.id}, hash=${operation.tzOperation.hash}].")
        return operationRepository.save(operation).awaitSingle().toBusiness()
    }

    private suspend fun checkBakingSlot(level: Long?) {
        if (level != null) {
            bakerService.checkHasBakingSlotAtLevel(level)
        } else {
            bakerService.checkHasBakingSlot()
        }
    }

    private suspend fun forgeTzOperation(tzOperation: TzOperation): String {
        return try {
            tezosReaderService.forgeOperation(
                ForgeOperation(
                    contents = tzOperation.contents,
                    branch = tzOperation.branch,
                )
            )
        } catch (e: Exception) {
            throw UnsuccessfulForgeException(e)
        }
    }

    private suspend fun dryRunTzOperation(tzOperation: TzOperation): PreApplyResponse {
        return try {
            tezosReaderService.dryRun(tzOperation)
        } catch (e: Exception) {
            throw UnsuccessfulDryRunException(e)
        }
    }

    private suspend fun verifyOperation(requester: String, operationDetails: OperationCreate): OperationEntity {
        // Check for baking slot
        checkBakingSlot(operationDetails.level)

        // Dry run operation.
        val dryRunResult = dryRunTzOperation(operationDetails.tzOperation)

        // Forge operation to get its size
        val operationSize = forgeTzOperation(operationDetails.tzOperation).length / 2

        val operation = OperationEntity(
            requestedLevel = operationDetails.level,
            status = OperationStatus.PENDING,
            timesFetched = 0,
            owner = requester,
            source = operationDetails.tzOperation.contents[0]["source"] as String,
            totalFee = dryRunResult.totalFee(),
            opSize = operationSize,
            estimatedConsumption = OperationEntity.Consumption(
                totalStorageSize = dryRunResult.totalStorageSize(),
                totalConsumedGas = dryRunResult.totalConsumedGas(),
            ),
            tzOperation = operationDetails.tzOperation.toModel(),
        )

        // Verify fees
        feeManagementService.checkFees(operation)

        return operation
    }

    override suspend fun deleteOperation(requester: String, id: String): Operation {
        LOGGER.debug("Delete operation [id=$id].")
        if (runBlocking { tokenService.checkIfAdmin(requester) }) {
            throw NotAnAdminFeatureException()
        }

        val operation = operationRepository.findById(id).awaitFirstOrNull()?.let {
            if (it.owner != requester) {
                throw IllegalAccessException("User does not have access to this operation.")
            }
            it
        } ?: throw EntityNotFoundException("operation", id)

        if (operation.status != OperationStatus.PENDING) {
            throw OperationIsNotPendingException()
        }

        val deleted =
            operationRepository.save(operation.copy(status = OperationStatus.DELETED)).awaitSingle().toBusiness()
        LOGGER.debug("Operation deleted [id=$id].")
        return deleted
    }

    companion object {
        val LOGGER = logger()
    }

}
