package eu.coexya.cyb.business.service.impl

import eu.coexya.cyb.business.configuration.TezosConfig
import eu.coexya.cyb.business.exception.MissingBakingRightsException
import eu.coexya.cyb.business.logger
import eu.coexya.cyb.business.model.BakingSlot
import eu.coexya.cyb.business.model.mapper.toBusiness
import eu.coexya.cyb.business.service.BakerService
import eu.coexya.cyb.common.utils.TimeCache
import eu.coexya.cyb.model.entity.BakingSlotEntity
import eu.coexya.cyb.model.repository.BakingSlotRepository
import eu.coexya.tezos.connector.node.model.BakingRight
import eu.coexya.tezos.connector.node.model.getCycle
import eu.coexya.tezos.connector.service.TezosReaderService
import kotlinx.coroutines.flow.map
import kotlinx.coroutines.flow.toList
import kotlinx.coroutines.reactive.asFlow
import kotlinx.coroutines.reactive.awaitFirstOrNull
import kotlinx.coroutines.reactive.awaitSingle
import kotlinx.coroutines.runBlocking
import org.springframework.stereotype.Service
import java.time.Duration

@Service
class BakerServiceImpl(
    private val tezosConfig: TezosConfig,
    private val tezosReaderService: TezosReaderService,
    private val bakingSlotRepository: BakingSlotRepository,
) : BakerService {

    private val bakingSlotCache: TimeCache<List<BakingSlot>> = object : TimeCache<List<BakingSlot>>(
        cached = emptyList(),
        duration = Duration.ofMinutes(1),
    ) {
        override fun update(): List<BakingSlot> {
            LOGGER.debug("Update baking slot memory cache")
            return runBlocking {
                val currentLevel = tezosReaderService.getHeadBlockLevel()
                bakingSlotRepository.findByBakerAndLevelBetween(
                    tezosConfig.bakerAddress,
                    currentLevel - 1,
                    currentLevel + tezosConfig.nbBakingSlotSearch + 1
                ).asFlow().map { it.toBusiness() }.toList()
            }
        }
    }

    override suspend fun isKnownBaker(delegate: String): Boolean {
        return delegate == tezosConfig.bakerAddress
    }

    override suspend fun checkHasBakingSlot() {
        LOGGER.debug("Check baking slots [nbSlotsSearch=${tezosConfig.nbBakingSlotSearch}].")
        val currentLevel = tezosReaderService.getHeadBlockLevel()

        if (bakingSlotRepository.existsByBakerAndLevelBetween(
                tezosConfig.bakerAddress,
                currentLevel - 1,
                currentLevel + tezosConfig.nbBakingSlotSearch + 1
            ).awaitSingle()
        ) return

        throw MissingBakingRightsException("In the next ${tezosConfig.nbBakingSlotSearch} levels")
    }

    override suspend fun checkHasBakingSlotAtLevel(level: Long) {
        LOGGER.debug("Check baking slot [level=$level].")

        if (bakingSlotRepository.existsByBakerAndLevel(
                tezosConfig.bakerAddress, level,
            ).awaitSingle()
        ) return

        throw MissingBakingRightsException("At level ${level}.")
    }

    override suspend fun updateBakingSlotsCache() {
        LOGGER.debug("Update baking slots cache.")
        val currentCycle = tezosReaderService.getHeadBlock().getCycle()
        var nbBakingSlotsCached = 0

        for (cycle in (currentCycle..currentCycle + 2)) {
            if (!isCachedCycle(cycle)) {
                tezosReaderService.getBakingSlots(
                    delegateAddress = tezosConfig.bakerAddress,
                    priority = 0,
                    cycle = cycle,
                )?.map {
                    saveBakingRight(it, cycle)
                    nbBakingSlotsCached++
                }
            }
        }

        val nbDeleteBakingSlots =
            bakingSlotRepository.findByCycleBefore(currentCycle).asFlow().toList()
                .map { bakingSlotRepository.delete(it).awaitFirstOrNull() }.size

        LOGGER.debug("Baking slots cache updated [nbAdded=$nbBakingSlotsCached, nbRemoved=$nbDeleteBakingSlots].")
    }

    override suspend fun purgeBakingSlotsCache() {
        LOGGER.debug("Purge all baking slots.")
        bakingSlotRepository.deleteAll().subscribe()
    }

    override suspend fun getBakingSlots(): List<BakingSlot> {
        return bakingSlotCache.get()
    }

    private suspend fun isCachedCycle(cycle: Int): Boolean {
        return bakingSlotRepository.existsByCycle(cycle).awaitSingle()
    }

    private suspend fun saveBakingRight(bakingRight: BakingRight, cycle: Int) {
        bakingSlotRepository.save(
            BakingSlotEntity(
                baker = bakingRight.delegate,
                level = bakingRight.level,
                cycle = cycle,
            )
        ).awaitSingle()
    }

    companion object {
        private val LOGGER = logger()
    }
}
