package eu.coexya.cyb.business.model.mapper

import eu.coexya.cyb.business.model.BakingSlot
import eu.coexya.cyb.business.model.Operation
import eu.coexya.cyb.business.model.OperationUpdateEvent
import eu.coexya.cyb.business.model.Token
import eu.coexya.cyb.model.entity.BakingSlotEntity
import eu.coexya.cyb.model.entity.OperationEntity
import eu.coexya.cyb.model.entity.OperationUpdateEventEntity
import eu.coexya.cyb.model.entity.TokenEntity

fun OperationEntity.toBusiness() = Operation(
    id = id,
    lastStatusUpdate = lastStatusUpdate,
    creationDate = creationDate,
    level = level,
    requestedLevel = requestedLevel,
    fetchedLevel = fetchedLevel,
    bakerAddress = bakerAddress,
    timesFetched = timesFetched,
    errorMessage = errorMessage,
    status = status,
    source = source,
    owner = owner,
    totalFee = totalFee,
    opSize = opSize,
    estimatedConsumption = estimatedConsumption?.toBusiness(),
    actualConsumption = actualConsumption?.toBusiness(),
    tzOperation = tzOperation.toTezos(),
)

fun OperationEntity.Consumption.toBusiness() = Operation.Consumption(
    totalStorageSize = totalStorageSize,
    totalConsumedGas = totalConsumedGas,
)

fun TokenEntity.toBusiness() = Token(
    id = id!!,
    name = name,
    expirationDate = expirationDate,
    creationDate = creationDate,
    roles = roles,
    owner = owner,
    revoked = revoked,
)

fun OperationUpdateEventEntity.toBusiness() = OperationUpdateEvent(
    date = date,
    operation = operation.toBusiness(),
)

fun BakingSlotEntity.toBusiness() = BakingSlot(
    baker = baker,
    level = level,
    cycle = cycle,
)
