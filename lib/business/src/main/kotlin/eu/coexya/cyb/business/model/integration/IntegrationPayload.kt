package eu.coexya.cyb.business.model.integration

import eu.coexya.cyb.business.model.OperationUpdateEvent
import org.springframework.messaging.Message
import org.springframework.messaging.MessageHeaders

class OperationUpdateEventMessage(private val payload: OperationUpdateEvent) : Message<OperationUpdateEvent> {
    override fun getPayload(): OperationUpdateEvent {
        return payload
    }

    override fun getHeaders(): MessageHeaders {
        return MessageHeaders(emptyMap())
    }
}
