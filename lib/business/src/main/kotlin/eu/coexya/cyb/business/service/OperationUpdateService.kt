package eu.coexya.cyb.business.service

interface OperationUpdateService {

    suspend fun startupUpdateOperationsStatus()

    suspend fun updateOperationStatus()

    suspend fun purgeOldOperations()

}
