package eu.coexya.cyb.business.model

data class TokenPatch(
    val name: String? = null,
    val revoked: Boolean? = null
)
