package eu.coexya.cyb.business.model.mapper

import eu.coexya.cyb.model.entity.OperationEntity
import eu.coexya.tezos.connector.node.model.TzOperation

fun TzOperation.toModel() = OperationEntity.TzOperation(
    hash = hash,
    contents = contents,
    branch = branch,
    signature = signature,
)
