package eu.coexya.cyb.business.model.mapper

import org.apache.commons.csv.CSVFormat
import org.apache.commons.csv.CSVPrinter
import org.apache.commons.csv.QuoteMode
import java.io.ByteArrayInputStream
import java.io.ByteArrayOutputStream
import java.io.PrintWriter
import java.time.LocalDateTime
import kotlin.reflect.full.memberProperties

inline fun <reified T : Any> List<T>.toCsv(): ByteArrayInputStream {
    val format = CSVFormat.Builder.create().setQuoteMode(QuoteMode.MINIMAL).build()

    try {
        ByteArrayOutputStream().use { out ->
            CSVPrinter(PrintWriter(out), format).use { csvPrinter ->
                // Print headers
                csvPrinter.printRecord(T::class.java.getFlattenedFieldsNames())

                for (value in this) {
                    // Print each value
                    csvPrinter.printRecord(T::class.java.getFlattenedFieldsValues(value))
                }

                csvPrinter.flush()
                return ByteArrayInputStream(out.toByteArray())
            }
        }
    } catch (e: Exception) {
        throw RuntimeException("fail to import data to CSV file: ${e.message}")
    }
}

fun Class<*>.getFlattenedFieldsNames(): List<String> {
    return declaredFields.map { field ->
        if (field.type.isDataType()) {
            listOf(field.name)
        } else {
            field.type.getFlattenedFieldsNames().map { "${field.name}_$it" }
        }
    }.flatten()
}

fun Class<*>.getFlattenedFieldsValues(value: Any): List<String> {
    return declaredFields.map { field ->
        when {
            field.type.isDataType() -> listOf(value.getField<Any>(field.name).toString())
            else -> {
                val fields = value.getField<Any>(field.name)
                if (fields != null) {
                    field.type.getFlattenedFieldsValues(fields)
                } else {
                    emptyList()
                }
            }
        }
    }.flatten()
}

fun Class<*>.isDataType(): Boolean {
    if (this.isEnum) return true
    return when (this) {
        Long::class.javaObjectType,
        Long::class.javaPrimitiveType,
        Double::class.javaObjectType,
        Double::class.javaPrimitiveType,
        Character::class.javaObjectType,
        Character::class.javaPrimitiveType,
        Byte::class.javaObjectType,
        Byte::class.javaPrimitiveType,
        Short::class.javaObjectType,
        Short::class.javaPrimitiveType,
        Integer::class.javaObjectType,
        Integer::class.javaPrimitiveType,
        Boolean::class.javaObjectType,
        Boolean::class.javaPrimitiveType,
        Float::class.javaObjectType,
        Float::class.javaPrimitiveType,
        String::class.javaObjectType,
        List::class.javaObjectType,
        LocalDateTime::class.javaObjectType,
        -> true
        else -> false
    }
}

inline fun <reified T> Any.getField(fieldName: String): T? {
    this::class.memberProperties.forEach { kCallable ->
        if (fieldName == kCallable.name) {
            return kCallable.getter.call(this) as T?
        }
    }
    return null
}

