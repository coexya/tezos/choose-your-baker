package eu.coexya.cyb.business.service.impl

import eu.coexya.cyb.business.exception.AuthenticationException
import eu.coexya.cyb.business.exception.EntityNotFoundException
import eu.coexya.cyb.business.exception.ServiceException
import eu.coexya.cyb.business.logger
import eu.coexya.cyb.business.model.Token
import eu.coexya.cyb.business.model.TokenCreate
import eu.coexya.cyb.business.model.TokenPatch
import eu.coexya.cyb.business.model.mapper.toBusiness
import eu.coexya.cyb.business.service.TokenService
import eu.coexya.cyb.common.enums.AuthenticationRole
import eu.coexya.cyb.model.entity.TokenEntity
import eu.coexya.cyb.model.repository.TokenRepository
import kotlinx.coroutines.flow.Flow
import kotlinx.coroutines.flow.map
import kotlinx.coroutines.flow.toList
import kotlinx.coroutines.reactive.asFlow
import kotlinx.coroutines.reactive.awaitFirstOrNull
import kotlinx.coroutines.reactive.awaitSingle
import org.bson.types.ObjectId
import org.springframework.beans.factory.annotation.Value
import org.springframework.stereotype.Service
import org.springframework.transaction.annotation.Transactional
import java.time.OffsetDateTime
import java.time.Period

@Service
class TokenServiceImpl(
    private val tokenRepository: TokenRepository,
    @Value("\${daemon.purge.purgeAfter:#{T(java.time.Period).ofYears(1)}}") val purgeAfter: Period,
) : TokenService {

    @Transactional(rollbackFor = [ServiceException::class])
    override suspend fun createToken(tokenDetails: TokenCreate): Token {
        LOGGER.debug("Create token.")

        val expirationDate =
            if (tokenDetails.duration != null) {
                OffsetDateTime.now().plus(tokenDetails.duration)
            } else {
                null
            }

        // Insert token info in database
        val toCreate = TokenEntity(
            name = tokenDetails.name,
            expirationDate = expirationDate,
            owner = ObjectId().toHexString(),
            creationDate = OffsetDateTime.now(),
            roles = tokenDetails.roles ?: emptyList(),
        )
        val createdToken = tokenRepository.save(toCreate).awaitSingle().toBusiness()

        LOGGER.debug("Token created [id=${createdToken.id}, detail=$tokenDetails].")

        return createdToken
    }

    override suspend fun findAll(): Flow<Token> {
        return tokenRepository.findAll().asFlow().map { it.toBusiness() }
    }

    override suspend fun getAndCheckToken(owner: String): Token {
        LOGGER.debug("Check token [owner=$owner].")
        // Check that token has not been revoked.
        return tokenRepository.findByOwnerAndRevoked(owner = owner, revoked = false).awaitFirstOrNull()?.toBusiness()
            ?.let {
                // Check expiration date.
                if (it.expirationDate != null && it.expirationDate < OffsetDateTime.now()) {
                    throw AuthenticationException.ExpiredTokenException(it)
                }
                LOGGER.debug("Token checked [id=${it.id}, owner=$owner].")
                it
            } ?: throw AuthenticationException.RevokedTokenException(owner)
    }

    override suspend fun checkIfAdmin(owner: String): Boolean {
        return getAndCheckToken(owner).roles.contains(AuthenticationRole.ADMIN)
    }


    @Transactional(rollbackFor = [ServiceException::class])
    override suspend fun patchToken(tokenId: String, tokenDetails: TokenPatch): Token {
        LOGGER.debug("Patch token [id=$tokenId].")
        val token: TokenEntity = tokenRepository.findById(tokenId).awaitFirstOrNull()
            ?: throw EntityNotFoundException("token", tokenId)

        val toPatch = token.copy(
            name = tokenDetails.name ?: token.name,
            revoked = tokenDetails.revoked ?: token.revoked
        )
        val updatedToken = tokenRepository.save(toPatch).awaitSingle().toBusiness()
        LOGGER.debug("Token patched [id=$tokenId, detail=$tokenDetails].")
        return updatedToken
    }


    @Transactional(rollbackFor = [ServiceException::class])
    override suspend fun deleteToken(tokenId: String) {
        LOGGER.debug("Delete token [id=$tokenId].")
        val token: TokenEntity = tokenRepository.findById(tokenId).awaitFirstOrNull()
            ?: throw EntityNotFoundException("token", tokenId)

        tokenRepository.delete(token).awaitFirstOrNull()
        LOGGER.debug("Token deleted [id=$tokenId].")
    }

    override suspend fun purgeExpiredTokens() {
        LOGGER.debug("Purge expired tokens.")
        tokenRepository.findByCreationDateBefore(OffsetDateTime.now().minus(purgeAfter)).toList()
            .filter { it.revoked || (it.expirationDate != null && it.expirationDate!!.isBefore(OffsetDateTime.now())) }
            .onEach { tokenRepository.delete(it).subscribe() }
            .let {
                if (it.isNotEmpty()) {
                    LOGGER.debug("Purged expired tokens [nb=${it.size}].")
                }
            }
    }

    companion object {
        private val LOGGER = logger()
    }
}
