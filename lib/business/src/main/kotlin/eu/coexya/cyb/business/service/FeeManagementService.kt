package eu.coexya.cyb.business.service

import eu.coexya.cyb.business.configuration.TezosConfig
import eu.coexya.cyb.model.entity.OperationEntity
import eu.coexya.cyb.model.entity.getGasLimit

interface FeeManagementService {

    val tezosConfig: TezosConfig

    fun getExpectedFees(operation: OperationEntity): Long {
        return tezosConfig.fees.minimalNtzFees +
                tezosConfig.fees.minimalNtzPerGasUnit * (operation.tzOperation.getGasLimit()) +
                tezosConfig.fees.minimalNtzPerByte * operation.opSize
    }

    fun checkFees(operation: OperationEntity)

}
