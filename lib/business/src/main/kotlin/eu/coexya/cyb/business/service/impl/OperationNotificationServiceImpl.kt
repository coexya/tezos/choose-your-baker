package eu.coexya.cyb.business.service.impl

import eu.coexya.cyb.business.exception.NotAnAdminFeatureException
import eu.coexya.cyb.business.logger
import eu.coexya.cyb.business.model.OperationUpdateEvent
import eu.coexya.cyb.business.service.OperationNotificationService
import eu.coexya.cyb.business.service.TokenService
import kotlinx.coroutines.flow.Flow
import kotlinx.coroutines.flow.emitAll
import kotlinx.coroutines.flow.flow
import kotlinx.coroutines.reactive.asFlow
import org.springframework.boot.autoconfigure.condition.ConditionalOnProperty
import org.springframework.integration.channel.FluxMessageChannel
import org.springframework.integration.util.IntegrationReactiveUtils
import org.springframework.stereotype.Service


@Service
@ConditionalOnProperty(name = ["sse.enabled"], havingValue = "true", matchIfMissing = false)
class OperationNotificationServiceImpl(
    private val tokenService: TokenService,
    private val operationUpdateEventsMessageChannel: FluxMessageChannel,
) : OperationNotificationService {

    override fun subscribeOperationUpdateEvents(requester: String): Flow<OperationUpdateEvent> {
        LOGGER.debug("Subscribe operation events [owner=$requester].")

        return flow {
            if (tokenService.checkIfAdmin(requester)) {
                throw NotAnAdminFeatureException()
            }
            emitAll(
                IntegrationReactiveUtils
                    .messageChannelToFlux<OperationUpdateEvent>(operationUpdateEventsMessageChannel)
                    .filter { it.payload.operation.owner == requester }
                    .map { it.payload }
                    .asFlow()
            )
        }
    }

    companion object {
        val LOGGER = logger()
    }
}
