package eu.coexya.cyb.business.service

import eu.coexya.cyb.business.model.Operation
import eu.coexya.cyb.business.model.OperationCreate
import eu.coexya.cyb.common.criteria.OperationCriteria
import eu.coexya.tezos.connector.node.model.TzOperation
import kotlinx.coroutines.flow.Flow
import org.springframework.data.domain.Pageable

interface OperationService {

    /**
     * Retrieve all operations
     * @return operations
     */
    suspend fun getOperations(criteria: OperationCriteria?, pageable: Pageable): Flow<Operation>

    /**
     * Retrieve an operation
     * @param id id of the operation
     * @return the operation
     */
    suspend fun findByIdOperation(requester: String, id: String): Operation

    /**
     * Retrieve operations to be baked
     * @return mempool
     */
    suspend fun getMempool(): List<TzOperation>

    /**
     * Add an operation to database
     * @param operationDetails details of the operation
     * @return added operation
     */
    suspend fun processOperation(requester: String, operationDetails: OperationCreate): Operation

    /**
     * Delete an operation
     * @param id id of the operation
     * @return added operation
     */
    suspend fun deleteOperation(requester: String, id: String): Operation

}
