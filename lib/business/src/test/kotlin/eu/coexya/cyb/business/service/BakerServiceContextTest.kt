package eu.coexya.cyb.business.service

import com.ninjasquad.springmockk.MockkBean
import eu.coexya.cyb.business.configuration.TezosConfig
import eu.coexya.cyb.business.exception.MissingBakingRightsException
import eu.coexya.cyb.model.entity.BakingSlotEntity
import eu.coexya.cyb.model.migration.MigrationHandler
import eu.coexya.cyb.model.repository.BakingSlotRepository
import eu.coexya.tezos.connector.node.model.BakingRight
import eu.coexya.tezos.connector.node.model.TzBlock
import eu.coexya.tezos.connector.service.TezosReaderService
import io.mockk.clearMocks
import io.mockk.coEvery
import kotlinx.coroutines.flow.toList
import kotlinx.coroutines.reactive.asFlow
import kotlinx.coroutines.runBlocking
import org.assertj.core.api.Assertions.assertThat
import org.junit.jupiter.api.*
import org.junit.jupiter.api.extension.ExtendWith
import org.springframework.beans.factory.annotation.Autowired
import org.springframework.data.mongodb.core.ReactiveMongoTemplate
import org.springframework.test.context.junit.jupiter.SpringExtension
import java.nio.file.Path
import java.time.OffsetDateTime

@ExtendWith(SpringExtension::class)
class BakerServiceContextTest @Autowired constructor(
    private val bakerService: BakerService,
    private val tezosConfig: TezosConfig,
    private val bakingSlotRepository: BakingSlotRepository,
    override val mongoTemplate: ReactiveMongoTemplate,
    override val migrationHandler: MigrationHandler,
) : AbstractServiceContextTest() {

    @MockkBean
    private lateinit var tezosReaderService: TezosReaderService

    val levelWithBakingRights = 18L
    val levelWithoutBakingRights = 16L

    @BeforeEach
    fun beforeEach() {
        resetDatabase()
        importJsonDatasets(Path.of("src/test/resources/datasets/baker_service/bakingSlots.json"))
        clearMocks(tezosReaderService)
    }

    @Test
    fun `Test is known baker`() {
        runBlocking {
            Assertions.assertTrue(bakerService.isKnownBaker(tezosConfig.bakerAddress))
            Assertions.assertFalse(bakerService.isKnownBaker("unknown_baker"))
        }
    }

    @Nested
    inner class CheckBakingSlotTest {

        @Test
        fun `No slot`() {
            coEvery { tezosReaderService.getHeadBlockLevel() } returns 50

            runBlocking {
                assertThrows<MissingBakingRightsException> {
                    bakerService.checkHasBakingSlot()
                }
                assertThrows<MissingBakingRightsException> {
                    bakerService.checkHasBakingSlotAtLevel(levelWithoutBakingRights)
                }
            }
        }

        @Test
        fun `With slot`() {
            coEvery { tezosReaderService.getHeadBlockLevel() } returns 10
            runBlocking {
                bakerService.checkHasBakingSlot()
                bakerService.checkHasBakingSlotAtLevel(levelWithBakingRights)
            }
        }

        @Test
        fun `One slot at current level`() {
            coEvery { tezosReaderService.getHeadBlockLevel() } returns 34
            runBlocking {
                bakerService.checkHasBakingSlot()
            }
        }

        @Test
        fun `One slot at last level checked`() {
            coEvery { tezosReaderService.getHeadBlockLevel() } returns 1
            runBlocking {
                bakerService.checkHasBakingSlot()
            }
        }
    }

    @Nested
    inner class UpdateBakingSlotCacheTest() {

        @Test
        fun `Nothing to update`() {
            coEvery { tezosReaderService.getHeadBlock() } returns TzBlock(
                hash = "block_address",
                header = TzBlock.Header(level = 11, timestamp = OffsetDateTime.parse("2021-10-12T08:59:30.929615Z")),
                metadata = mapOf("level_info" to mapOf("cycle" to 0)),
                operations = emptyList()
            )

            runBlocking {
                val bakingSlotsBeforeUpdate = bakingSlotRepository.findAll().asFlow().toList()
                bakerService.updateBakingSlotsCache()
                val bakingSlotsAfterUpdate = bakingSlotRepository.findAll().asFlow().toList()
                assertThat(bakingSlotsAfterUpdate).isEqualTo(bakingSlotsBeforeUpdate)
            }
        }

        @Test
        fun `Update when cycle changed`() {
            coEvery { tezosReaderService.getHeadBlock() } returns TzBlock(
                hash = "block_address",
                header = TzBlock.Header(level = 17, timestamp = OffsetDateTime.parse("2021-10-12T08:59:30.929615Z")),
                metadata = mapOf("level_info" to mapOf("cycle" to 1)),
                operations = emptyList()
            )

            val bakingRight = BakingRight(
                level = 49,
                delegate = tezosConfig.bakerAddress,
                priority = 0,
            )
            val bakingSlotsOfThirdCycle = listOf(
                bakingRight, bakingRight.copy(level = 53), bakingRight.copy(level = 63)
            )

            coEvery {
                tezosReaderService.getBakingSlots(tezosConfig.bakerAddress, 0, 3)
            } returns bakingSlotsOfThirdCycle

            runBlocking {
                val bakingSlotsBeforeUpdate = bakingSlotRepository.findAll().asFlow().toList()
                bakerService.updateBakingSlotsCache()
                val bakingSlotsAfterUpdate = bakingSlotRepository.findAll().asFlow().toList()

                val expectedBakingSlots = bakingSlotsBeforeUpdate.filter { it.cycle >= 1 } // remove older cycle
                    .plus(bakingSlotsOfThirdCycle.map {
                        BakingSlotEntity(
                            cycle = 3,
                            baker = it.delegate,
                            level = it.level,
                        )
                    })

                assertThat(bakingSlotsAfterUpdate).usingElementComparatorIgnoringFields("id")
                    .isEqualTo(expectedBakingSlots)
            }
        }
    }
}
