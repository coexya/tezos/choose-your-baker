package eu.coexya.cyb.business.service.fms

import eu.coexya.cyb.business.configuration.TestContextConfiguration
import eu.coexya.cyb.business.exception.InsufficientFeesException
import eu.coexya.cyb.business.service.FeeManagementService
import eu.coexya.cyb.business.service.impl.TransactionFMS
import eu.coexya.cyb.model.entity.OperationEntity
import eu.coexya.tezos.connector.node.model.OperationKind
import org.junit.jupiter.api.Test
import org.junit.jupiter.api.assertThrows
import org.junit.jupiter.params.ParameterizedTest
import org.junit.jupiter.params.provider.Arguments
import org.junit.jupiter.params.provider.MethodSource
import org.springframework.beans.factory.annotation.Autowired
import org.springframework.boot.test.context.SpringBootTest
import java.util.stream.Stream

@SpringBootTest(
    classes = [TestContextConfiguration::class],
    properties = [
        "tezos.bakerAddress=bXlCYWilekFkZHJlc3M=",
        "tezos.fees.mode=transaction",
        "tezos.fees.minimalNtzFees=100000",
        "tezos.fees.minimalNtzPerGasUnit=100",
        "tezos.fees.minimalNtzPerByte=1000",
    ],
)
class TransactionFMSTest @Autowired constructor(
    override val feeManagementService: FeeManagementService,
) : FMSTest() {

    val bakerAddress = "bXlCYWilekFkZHJlc3M="

    @Test
    fun `FeeManagementService is in TRANSACTION mode`() {
        assert(feeManagementService is TransactionFMS)
    }

    @ParameterizedTest(name = "[{index}]: {2}")
    @MethodSource("checkFeesProvider")
    fun `Check fees`(
        operation: OperationEntity,
        expectingException: Boolean,
        testName: String,
    ) {
        if (expectingException) {
            assertThrows<InsufficientFeesException> { feeManagementService.checkFees(operation) }
        } else {
            feeManagementService.checkFees(operation)
        }
    }

    fun checkFeesArgs(
        operation: OperationEntity,
        expectingException: Boolean,
        testName: String,
    ): Arguments = Arguments.of(operation, expectingException, testName)

    fun checkFeesProvider(): Stream<Arguments> {
        return listOf(

            checkFeesArgs(
                operation = operationWithContents(
                    listOf(
                        mapOf(
                            "fee" to "0",
                            "gas_limit" to "1527",
                            "storage_limit" to "0",
                        ),
                        mapOf(
                            "kind" to OperationKind.TRANSACTION,
                            "amount" to "200",
                            "destination" to bakerAddress,
                            "fee" to "0",
                            "gas_limit" to "1527",
                            "storage_limit" to "0",
                        ),
                    )
                ),
                expectingException = true,
                testName = "Insufficient fees transaction",
            ),

            checkFeesArgs(
                operation = operationWithContents(
                    listOf(
                        mapOf(
                            "fee" to "0",
                            "gas_limit" to "1520",
                            "storage_limit" to "0",
                        ),
                        mapOf(
                            "fee" to "0",
                            "gas_limit" to "1520",
                            "storage_limit" to "0",
                        ),
                        mapOf(
                            "kind" to OperationKind.TRANSACTION,
                            "amount" to "725",
                            "destination" to bakerAddress,
                            "fee" to "0",
                            "gas_limit" to "1527",
                            "storage_limit" to "0",
                        ),
                    )
                ),
                expectingException = false,
                testName = "Enough fees with transactions",
            ),

            ).stream()
    }

}
