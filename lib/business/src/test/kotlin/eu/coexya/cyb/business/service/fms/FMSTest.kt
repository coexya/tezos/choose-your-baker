package eu.coexya.cyb.business.service.fms

import eu.coexya.cyb.business.service.FeeManagementService
import eu.coexya.cyb.common.enums.OperationStatus
import eu.coexya.cyb.model.entity.OperationEntity
import org.junit.jupiter.api.Assertions
import org.junit.jupiter.api.TestInstance
import org.junit.jupiter.params.ParameterizedTest
import org.junit.jupiter.params.provider.Arguments
import org.junit.jupiter.params.provider.MethodSource
import org.springframework.test.context.ActiveProfiles
import java.util.stream.Stream

@TestInstance(TestInstance.Lifecycle.PER_CLASS)
@ActiveProfiles("test")
abstract class FMSTest {

    abstract val feeManagementService: FeeManagementService

    fun operationWithContents(contents: List<Map<String, Any>>): OperationEntity = OperationEntity(
        status = OperationStatus.PENDING,
        source = "tz1",
        owner = "",
        opSize = contents.size * 50,
        tzOperation = OperationEntity.TzOperation(
            hash = "hash",
            contents = contents,
            branch = "branch",
            signature = "signature",
        )
    )

    @ParameterizedTest(name = "[{index}]: {2}")
    @MethodSource("expectedFeesProvider")
    fun `Expected fees`(
        operation: OperationEntity,
        expectedValue: Long,
        testName: String,
    ) {
        Assertions.assertEquals(expectedValue, feeManagementService.getExpectedFees(operation))
    }

    private fun expectedFeesArgs(
        operation: OperationEntity,
        expectedValue: Long,
        testName: String,
    ): Arguments = Arguments.of(operation, expectedValue, testName)

    private fun expectedFeesProvider(): Stream<Arguments> {
        return listOf(

            expectedFeesArgs(
                operation = operationWithContents(
                    listOf(
                        mapOf(
                            "gas_limit" to "1527",
                            "storage_limit" to "0",
                        )
                    )
                ),
                expectedValue = expectedFee(
                    gasFee = 152700L,
                    sizeFee = 50 * 1000L,
                ),
                testName = "Classic transaction"
            ),

            expectedFeesArgs(
                operation = operationWithContents(
                    listOf(
                        mapOf(
                            "gas_limit" to "1527",
                            "storage_limit" to "0",
                        ),
                        mapOf(
                            "gas_limit" to "1527",
                            "storage_limit" to "0",
                        )
                    )
                ),
                expectedValue = expectedFee(
                    gasFee = 305400L,
                    sizeFee = 100 * 1000L,
                ),
                testName = "Two transactions no storage"
            ),

            expectedFeesArgs(
                operation = operationWithContents(
                    listOf(
                        mapOf(
                            "gas_limit" to "1527",
                            "storage_limit" to "100",
                        ),
                        mapOf(
                            "gas_limit" to "1527",
                            "storage_limit" to "100",
                        ),
                        mapOf(
                            "gas_limit" to "1527",
                            "storage_limit" to "100",
                        )
                    )
                ),
                expectedValue = expectedFee(
                    gasFee = 458100L,
                    sizeFee = 150 * 1000L,
                ),
                testName = "Three transactions with storage"
            ),

            ).stream()
    }

    private fun expectedFee(
        gasFee: Long,
        sizeFee: Long,
        minimalFee: Long = 100000L,
    ): Long = minimalFee + gasFee + sizeFee

}
