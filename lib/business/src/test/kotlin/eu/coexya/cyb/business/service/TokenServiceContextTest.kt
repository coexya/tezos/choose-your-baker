package eu.coexya.cyb.business.service

import eu.coexya.cyb.business.exception.AuthenticationException
import eu.coexya.cyb.business.model.Token
import eu.coexya.cyb.business.model.TokenCreate
import eu.coexya.cyb.business.model.TokenPatch
import eu.coexya.cyb.model.migration.MigrationHandler
import eu.coexya.cyb.model.repository.TokenRepository
import kotlinx.coroutines.flow.toList
import kotlinx.coroutines.reactive.asFlow
import kotlinx.coroutines.reactive.awaitFirstOrNull
import kotlinx.coroutines.runBlocking
import org.assertj.core.api.Assertions.assertThat
import org.assertj.core.api.Assertions.within
import org.junit.jupiter.api.BeforeEach
import org.junit.jupiter.api.Disabled
import org.junit.jupiter.api.Nested
import org.junit.jupiter.api.Test
import org.junit.jupiter.api.assertThrows
import org.junit.jupiter.api.extension.ExtendWith
import org.springframework.beans.factory.annotation.Autowired
import org.springframework.data.mongodb.core.ReactiveMongoTemplate
import org.springframework.test.context.junit.jupiter.DisabledIf
import org.springframework.test.context.junit.jupiter.SpringExtension
import java.nio.file.Path
import java.time.Duration
import java.time.OffsetDateTime
import java.time.ZoneOffset
import java.time.temporal.ChronoUnit

@ExtendWith(SpringExtension::class)
class TokenServiceContextTest @Autowired constructor(
    private val tokenService: TokenService,
    private val tokenRepository: TokenRepository,
    override val mongoTemplate: ReactiveMongoTemplate,
    override val migrationHandler: MigrationHandler,
) : AbstractServiceContextTest() {

    private val datasetToken = Token(
        id = "7165585b7b2107354ff102c3",
        name = "name",
        creationDate = OffsetDateTime.parse("2021-10-12T08:59:30.929615Z"),
        roles = listOf(
            "ISSUER"
        ),
        owner = "61891ba53a8815214c14717c",
        revoked = false,
    )

    private val revokedToken = datasetToken.copy(
        id = "7165585b7b2107354ff102c4",
        name = "revoked_token",
        owner = "61891ba53a8815214c14717d",
        revoked = true,
    )

    private val expiredToken = datasetToken.copy(
        id = "7165585b7b2107354ff102c5",
        name = "expired_token",
        owner = "61891ba53a8815214c14717e",
        creationDate = OffsetDateTime.parse("1920-10-12T08:59:30.929615Z"),
        expirationDate = OffsetDateTime.parse("1925-10-12T08:59:30.929615Z"),
    )

    @BeforeEach
    fun beforeEach() {
        resetDatabase()
    }

    @Disabled
    @Test
    fun `Purge old operations`() {
        // Mockk now() for constant test results
        mockkNow(
            OffsetDateTime.of(
                2020,
                10,
                10,
                10,
                10,
                10,
                10,
                ZoneOffset.UTC
            )
        )
        runBlocking {
            tokenRepository.deleteAll().subscribe()
            importJsonDatasets(Path.of("src/test/resources/datasets/token_service/purge.json"))
            tokenService.purgeExpiredTokens()
            val allTokensLeft = tokenRepository.findAll().asFlow().toList()
            assertThat(allTokensLeft.map { it.id }).containsExactlyInAnyOrder(
                "7165585b7b2107354ff10aa0",
                "7165585b7b2107354ff10aa1",
                "7165585b7b2107354ff10aa3",
            )
        }
        unmockkNow()
    }

    @Nested
    inner class CRUDTests {

        @BeforeEach
        fun beforeEach() {
            importJsonDatasets(Path.of("src/test/resources/datasets/token_service/tokens.json"))
        }

        @Test
        fun `Create token`() {

            val duration = Duration.ofDays(1L)

            val toAdd = TokenCreate(
                name = "token",
                duration = duration,
                roles = listOf(
                    "ISSUER"
                ),
            )
            runBlocking {
                val createdToken = tokenService.createToken(toAdd)

                val actualCreatedToken = tokenRepository.findById(createdToken.id).awaitFirstOrNull()
                assertThat(actualCreatedToken).isNotNull
                actualCreatedToken!!

                assertThat(actualCreatedToken.name).isEqualTo(toAdd.name)
                assertThat(actualCreatedToken.expirationDate).isCloseTo(
                    OffsetDateTime.now().plus(duration),
                    within(3, ChronoUnit.SECONDS)
                )
                assertThat(actualCreatedToken.roles).isEqualTo(toAdd.roles)
            }
        }

        @Test
        fun `Delete token`() {
            runBlocking {
                assertThat(tokenRepository.findById(datasetToken.id).awaitFirstOrNull()).isNotNull
                tokenService.deleteToken(datasetToken.id)
                assertThat(tokenRepository.findById(datasetToken.id).awaitFirstOrNull()).isNull()
            }
        }

        @Test
        fun `Patch token`() {
            runBlocking {
                val tokenPatch = TokenPatch(
                    name = "newName",
                    revoked = true,
                )
                tokenService.patchToken(datasetToken.id, tokenPatch)
                val actualToken = tokenRepository.findById(datasetToken.id).awaitFirstOrNull()
                assertThat(actualToken).isNotNull
                assertThat(actualToken!!.name).isEqualTo(tokenPatch.name)
                assertThat(actualToken.revoked).isEqualTo(tokenPatch.revoked)
            }
        }

        @Disabled
        @Test
        fun `Check if admin`() {
            runBlocking {
                assertThat(tokenService.checkIfAdmin(adminToken.owner)).isTrue
                assertThat(tokenService.checkIfAdmin(datasetToken.owner)).isFalse
                assertThrows<AuthenticationException.RevokedTokenException> {
                    tokenService.checkIfAdmin(revokedToken.owner)
                }
                assertThrows<AuthenticationException.ExpiredTokenException> {
                    tokenService.checkIfAdmin(expiredToken.owner)
                }
            }
        }

        @Disabled
        @Test
        fun `Get and check token`() {
            runBlocking {
                assertThat(tokenService.getAndCheckToken(adminToken.owner)).isEqualTo(adminToken)
                assertThat(tokenService.getAndCheckToken(datasetToken.owner)).isEqualTo(datasetToken)
                assertThrows<AuthenticationException.RevokedTokenException> {
                    tokenService.getAndCheckToken(revokedToken.owner)
                }
                assertThrows<AuthenticationException.ExpiredTokenException> {
                    tokenService.getAndCheckToken(expiredToken.owner)
                }
            }
        }
    }
}
