package eu.coexya.cyb.business.service

import eu.coexya.cyb.business.configuration.TestContextConfiguration
import eu.coexya.cyb.business.model.Token
import eu.coexya.cyb.model.migration.MigrationHandler
import io.mockk.every
import io.mockk.mockkStatic
import io.mockk.unmockkStatic
import kotlinx.coroutines.reactive.awaitFirstOrNull
import kotlinx.coroutines.reactive.awaitLast
import kotlinx.coroutines.reactive.awaitSingle
import kotlinx.coroutines.runBlocking
import org.bson.Document
import org.junit.jupiter.api.extension.ExtendWith
import org.springframework.boot.autoconfigure.EnableAutoConfiguration
import org.springframework.boot.test.context.SpringBootTest
import org.springframework.data.mongodb.core.ReactiveMongoTemplate
import org.springframework.test.context.ActiveProfiles
import org.springframework.test.context.junit.jupiter.SpringExtension
import java.nio.file.Files
import java.nio.file.Path
import java.time.OffsetDateTime

@Suppress("UNCHECKED_CAST")
@ExtendWith(SpringExtension::class)
@ActiveProfiles("test")
@EnableAutoConfiguration(exclude = [org.springframework.boot.autoconfigure.mongo.embedded.EmbeddedMongoAutoConfiguration::class])
@SpringBootTest(classes = [TestContextConfiguration::class])
abstract class AbstractServiceContextTest {
    abstract val mongoTemplate: ReactiveMongoTemplate
    abstract val migrationHandler: MigrationHandler

    fun importJsonDatasets(vararg paths: Path) {
        paths.forEach {
            val collections = Document.parse(Files.readString(it))
            runBlocking {
                collections.keys.forEach { collection ->
                    val documents = collections[collection] as List<Document>
                    mongoTemplate.getCollection(collection).awaitSingle().insertMany(documents).awaitLast()
                }
            }
        }
    }

    fun resetDatabase() {
        runBlocking {
            // Drop all collections.
            for (collectionName in mongoTemplate.collectionNames.toIterable()) {
                mongoTemplate.dropCollection(collectionName).awaitFirstOrNull()
            }

            // Init the migration (indices + migrations).
            migrationHandler.initIndices()
            migrationHandler.applyMigrations()
        }

    }

    fun mockkNow(date: OffsetDateTime) {
        mockkStatic(OffsetDateTime::class)
        every {
            OffsetDateTime.now()
        } returns date
    }

    fun unmockkNow() {
        unmockkStatic(OffsetDateTime::class)
    }

    val adminToken = Token(
        id = "61892bfaea786564cb17ac46",
        name = "admin_token",
        creationDate = OffsetDateTime.parse("2021-11-08T13:41:21.170593Z"),
        roles = listOf(
            "ADMIN"
        ),
        owner = "61892bfaea786564cb17ac45",
        revoked = false,
    )

}

