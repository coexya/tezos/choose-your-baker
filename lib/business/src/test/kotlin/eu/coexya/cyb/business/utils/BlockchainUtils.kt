package eu.coexya.cyb.business.utils

import eu.coexya.tezos.connector.node.model.OperationKind
import eu.coexya.tezos.connector.node.model.TzBlock
import eu.coexya.tezos.connector.node.model.TzOperation
import java.time.OffsetDateTime

const val BLOCK_HASH_SIZE = 51
const val OPERATION_HASH_SIZE = 51
const val PUB_KEY_SIZE = 54
const val DELEGATOR_HASH_SIZE = 36
const val SIGNATURE_SIZE = 96

private val charPool: List<Char> = ('a'..'z') + ('A'..'Z') + ('0'..'9')
private fun generateRandomString(length: Int): String {
    return (1..length)
        .map { kotlin.random.Random.nextInt(0, charPool.size) }
        .map(charPool::get)
        .joinToString("")
}

fun generateBlockchainWithBlocks(
    blockchainSize: Int,
    mandatoryBlocks: List<TzBlock>
): List<TzBlock> {
    val ret = (blockchainSize downTo 1).map { i ->
        mandatoryBlocks.find { it.header.level == i.toLong() }
            ?: generateTzBlock().let { it.copy(header = it.header.copy(level = i.toLong())) }
    }
    return ret
}

fun generateTzBlock(): TzBlock =
    TzBlock(
        hash = generateRandomString(BLOCK_HASH_SIZE),
        metadata = mapOf("baker" to generateRandomString(DELEGATOR_HASH_SIZE)),
        header = TzBlock.Header(
            level = 0,
            timestamp = OffsetDateTime.parse("2007-12-03T10:15:30+01:00")
        ),
        operations = listOf(
            (0..(0..5).random()).map {
                generateTzOperation(OperationKind.ENDORSEMENT)
            },
            emptyList(),
            emptyList(),
            (0..(0..5).random()).map {
                if (0 == (0..10).random()) {
                    generateTzOperation(OperationKind.TRANSACTION)
                } else {
                    generateTzOperation(OperationKind.REVEAL)
                }
            },
        ),
    )

fun generateTzOperation(kind: String = OperationKind.TRANSACTION): TzOperation =
    TzOperation(
        hash = generateRandomString(OPERATION_HASH_SIZE),
        contents = listOf(
            when (kind) {
                OperationKind.TRANSACTION -> mapOf(
                    "kind" to OperationKind.TRANSACTION,
                    "source" to generateRandomString(DELEGATOR_HASH_SIZE),
                    "fee" to "307",
                    "counter" to "5",
                    "gas_limit" to "1527",
                    "storage_limit" to "0",
                    "amount" to "2000000",
                    "destination" to generateRandomString(DELEGATOR_HASH_SIZE),
                    "metadata" to mapOf(
                        "balance_updates" to listOf(
                            mapOf(
                                "kind" to "contract",
                                "contract" to "tz1VSUr8wwNhLAzempoch5d6hLRiTh8Cjcjb",
                                "change" to "-307",
                            ),
                            mapOf(
                                "kind" to "freezer",
                                "category" to "fees",
                                "delegate" to "tz1Ke2h7sDdakHJQh8WX4Z372du1KChsksyU",
                                "cycle" to 9,
                                "change" to "307",
                            ),
                        ),
                        "operation_result" to mapOf(
                            "status" to "applied",
                            "balance_updates" to listOf(
                                mapOf(
                                    "kind" to "contract",
                                    "contract" to "tz1VSUr8wwNhLAzempoch5d6hLRiTh8Cjcjb",
                                    "change" to "-2000000",
                                ),
                                mapOf(
                                    "kind" to "contract",
                                    "contract" to "tz1aSkwEot3L2kmUvcoxzjMomb9mvBNuzFK6",
                                    "change" to "2000000",
                                ),
                            ),
                            "consumed_gas" to "1427",
                            "consumed_milligas" to "1427000",
                        )
                    )
                )
                OperationKind.REVEAL -> mapOf(
                    "kind" to OperationKind.REVEAL,
                    "source" to generateRandomString(DELEGATOR_HASH_SIZE),
                    "fee" to "357",
                    "counter" to "4",
                    "gas_limit" to "1000",
                    "storage_limit" to "0",
                    "public_key" to generateRandomString(PUB_KEY_SIZE),
                    "metadata" to mapOf(
                        "balance_updates" to listOf(
                            mapOf(
                                "kind" to "contract",
                                "contract" to "tz1VSUr8wwNhLAzempoch5d6hLRiTh8Cjcjb",
                                "change" to "-357",
                            ),
                            mapOf(
                                "kind" to "freezer",
                                "category" to "fees",
                                "delegate" to "tz1Ke2h7sDdakHJQh8WX4Z372du1KChsksyU",
                                "cycle" to 9,
                                "change" to "357",
                            ),
                        ),
                        "operation_result" to mapOf(
                            "status" to "applied",
                            "consumed_gas" to "1000",
                            "consumed_milligas" to "1000000",
                        )
                    )
                )
                OperationKind.ENDORSEMENT -> mapOf(
                    "kind" to OperationKind.ENDORSEMENT,
                    "level" to 5,
                )
                else -> emptyMap()
            }
        ),
        branch = generateRandomString(BLOCK_HASH_SIZE),
        signature = if (kind == OperationKind.TRANSACTION || kind == OperationKind.REVEAL) {
            generateRandomString(SIGNATURE_SIZE)
        } else {
            null
        },
    )
