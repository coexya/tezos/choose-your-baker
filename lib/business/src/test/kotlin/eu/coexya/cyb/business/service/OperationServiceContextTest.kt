package eu.coexya.cyb.business.service

import com.ninjasquad.springmockk.MockkBean
import eu.coexya.cyb.business.exception.MissingBakingRightsException
import eu.coexya.cyb.business.exception.NotAnAdminFeatureException
import eu.coexya.cyb.business.exception.OperationIsNotPendingException
import eu.coexya.cyb.business.exception.UnsuccessfulDryRunException
import eu.coexya.cyb.business.model.Operation
import eu.coexya.cyb.business.model.OperationCreate
import eu.coexya.cyb.business.model.mapper.toModel
import eu.coexya.cyb.common.enums.OperationStatus
import eu.coexya.cyb.model.entity.OperationEntity
import eu.coexya.cyb.model.migration.MigrationHandler
import eu.coexya.cyb.model.repository.OperationRepository
import eu.coexya.cyb.model.repository.OperationUpdateEventRepository
import eu.coexya.tezos.connector.node.exception.PreApplyFailedException
import eu.coexya.tezos.connector.node.model.ForgeOperation
import eu.coexya.tezos.connector.node.model.PreApplyResponse
import eu.coexya.tezos.connector.node.model.TzOperation
import eu.coexya.tezos.connector.service.TezosReaderService
import io.mockk.clearMocks
import io.mockk.coEvery
import kotlinx.coroutines.flow.toList
import kotlinx.coroutines.reactive.asFlow
import kotlinx.coroutines.reactive.awaitFirstOrNull
import kotlinx.coroutines.reactive.awaitSingle
import kotlinx.coroutines.reactor.awaitSingleOrNull
import kotlinx.coroutines.runBlocking
import org.assertj.core.api.Assertions.assertThat
import org.junit.jupiter.api.BeforeEach
import org.junit.jupiter.api.Nested
import org.junit.jupiter.api.Test
import org.junit.jupiter.api.assertThrows
import org.junit.jupiter.api.extension.ExtendWith
import org.springframework.beans.factory.annotation.Autowired
import org.springframework.data.mongodb.core.ReactiveMongoTemplate
import org.springframework.test.context.junit.jupiter.SpringExtension
import java.nio.file.Path

@ExtendWith(SpringExtension::class)
class OperationServiceContextTest @Autowired constructor(
    private val operationService: OperationService,
    private val operationRepository: OperationRepository,
    private val operationUpdateEventRepository: OperationUpdateEventRepository,
    override val mongoTemplate: ReactiveMongoTemplate,
    override val migrationHandler: MigrationHandler,
) : AbstractServiceContextTest() {

    @MockkBean
    private lateinit var tezosReaderService: TezosReaderService

    final val anyTzOperation = TzOperation(
        hash = "opDnqCqKUFLv6NJkm4GowVuAapzciXR9gQF99f6rDaA8qkXhkHQ",
        contents = listOf(
            mapOf(
                "kind" to "reveal",
                "source" to "tz1VSUr8wwNhLAzempoch5d6hLRiTh8Cjcjb",
                "fee" to "357",
                "counter" to "4",
                "gas_limit" to "1000",
                "storage_limit" to "0",
                "public_key" to "edpkvGfYw3LyB1UcCahKQk4rF2tvbMUk8GFiTuMjL75uGXrpvKXhjn",
            ),
            mapOf(
                "kind" to "transaction",
                "source" to "tz1aSkwEot3L2kmUvcoxzjMomb9mvBNuzFK6",
                "fee" to "403",
                "counter" to "4",
                "gas_limit" to "1527",
                "storage_limit" to "0",
                "amount" to "2000000",
                "destination" to "tz1VSUr8wwNhLAzempoch5d6hLRiTh8Cjcjb",
            )
        ),
        branch = "BMKWZMCjvDFVryC9c2DmJuVvsokasAjR8vK4vYFGBcnF8B2mK6e",
        signature = "sigPnhdL1HqwpgM7CTiyJkMzEueYVhKmzcJFx34SzgQckBH83dBRPVARdtsXPyyk4T3BGWgNibNgtMRbsaKUfiXruaEFXLF8",
    )

    final val anyUserId = "6165585b7b2107354ff102c1"
    val otherUserId = "6165585b7b2107354ff102c2"
    val adminId = "61892bfaea786564cb17ac45"

    val datasetOperation = OperationEntity(
        id = "6165acc338facf697184e0ab",
        timesFetched = 0,
        status = OperationStatus.PENDING,
        source = "tz1aSkwEot3L2kmUvcoxzjMomb9mvBNuzFK6",
        owner = anyUserId,
        opSize = 80,
        tzOperation = OperationEntity.TzOperation(
            hash = "op6cuxQNf1rewWq9eKcXXKXmcY2qAgRT4NucWGBb1mAQYBEJZZP",
            contents = listOf(
                mapOf(
                    "kind" to "transaction",
                    "source" to "tz1aSkwEot3L2kmUvcoxzjMomb9mvBNuzFK6",
                    "fee" to "403",
                    "counter" to "4",
                    "gas_limit" to "1527",
                    "storage_limit" to "0",
                    "amount" to "2000000",
                    "destination" to "tz1VSUr8wwNhLAzempoch5d6hLRiTh8Cjcjb"
                ),
            ),
            branch = "BLhaQ3LSisqLFJXuAbbpKdgaVWZSast6CQfrDHri6wDkmnymBgX",
            signature = "sigfpZaJreeaDM5DwRc72L8nZDa2UH7vC1AFjiisvnbVWjtx3kwnVCBMifJPQpjxs6T7dfsL7wzg1FW2dwedF8hJAxpJkXP1",
        ),
    )

    val levelWithBakingRights = 18L
    val levelWithoutBakingRights = 16L

    @BeforeEach
    fun beforeEach() {
        resetDatabase()
        importJsonDatasets(Path.of("src/test/resources/datasets/operation_service/tokens.json"))
        importJsonDatasets(Path.of("src/test/resources/datasets/operation_service/operations.json"))
        importJsonDatasets(Path.of("src/test/resources/datasets/operation_service/bakingSlots.json"))
        clearMocks(tezosReaderService)
    }

    @Nested
    inner class ProcessOperationTest {

        private val toProcess = OperationCreate(
            tzOperation = anyTzOperation
        )

        @Test
        fun `Without baking rights`() {
            coEvery { tezosReaderService.getHeadBlockLevel() } returns 20
            assertThrows<MissingBakingRightsException> {
                runBlocking { operationService.processOperation(anyUserId, toProcess) }
            }

            assertThrows<MissingBakingRightsException> {
                runBlocking {
                    operationService.processOperation(
                        anyUserId,
                        toProcess.copy(level = levelWithoutBakingRights),
                    )
                }
            }
        }

        @Nested
        inner class WithBakingRights {

            @BeforeEach
            fun beforeAll() {
                coEvery { tezosReaderService.getHeadBlockLevel() } returns 10
            }

            @Test
            fun `With bad tzOperation`() {
                coEvery { tezosReaderService.dryRun(anyTzOperation) } throws PreApplyFailedException("")

                assertThrows<UnsuccessfulDryRunException> {
                    runBlocking { operationService.processOperation(anyUserId, toProcess) }
                }
                assertThrows<UnsuccessfulDryRunException> {
                    runBlocking {
                        operationService.processOperation(
                            anyUserId,
                            toProcess.copy(level = levelWithBakingRights),
                        )
                    }
                }
            }

            @Test
            fun `With good tzOperation`() {

                coEvery { tezosReaderService.dryRun(anyTzOperation) } returns PreApplyResponse(
                    contents = listOf(
                        anyTzOperation.contents[0].plus(
                            Pair(
                                "metadata", mapOf(
                                    "operation_result" to mapOf(
                                        // Only useful info
                                        "consumed_gas" to "1000",
                                    )
                                )
                            )
                        ),
                        anyTzOperation.contents[1].plus(
                            Pair(
                                "metadata", mapOf(
                                    "operation_result" to mapOf(
                                        // Only useful info
                                        "consumed_gas" to "1427",
                                    )
                                )
                            )
                        )
                    ),
                    signature = anyTzOperation.signature!!
                )

                coEvery {
                    tezosReaderService.forgeOperation(ForgeOperation(
                        contents = anyTzOperation.contents,
                        branch = anyTzOperation.branch,
                    ))
                } returns "a".repeat(160)

                runBlocking {
                    val addedOperation = operationService.processOperation(anyUserId, toProcess)

                    assertThat(addedOperation).usingRecursiveComparison()
                        .ignoringFields("id", "lastStatusUpdate", "creationDate")
                        .isEqualTo(
                            Operation(
                                status = OperationStatus.PENDING,
                                tzOperation = anyTzOperation,
                                timesFetched = 0,
                                source = anyTzOperation.contents[0]["source"] as String,
                                owner = anyUserId,
                                totalFee = 760,
                                opSize = 80,
                                estimatedConsumption = Operation.Consumption(
                                    totalStorageSize = 0,
                                    totalConsumedGas = 2427,
                                ),
                            )
                        )

                    val addedOperationEntity = operationRepository.findById(addedOperation.id!!).awaitSingle()
                    val notifications =
                        operationUpdateEventRepository.findByOperation_Owner(addedOperation.owner).asFlow().toList()

                    assertThat(notifications.size).isEqualTo(1)
                    assertThat(notifications[0].operation).usingRecursiveComparison()
                        .ignoringFields("lastStatusUpdate", "creationDate")
                        .isEqualTo(addedOperationEntity)
                }

                runBlocking {
                    val addedOperation =
                        operationService.processOperation(anyUserId, toProcess.copy(level = levelWithBakingRights))

                    assertThat(addedOperation).usingRecursiveComparison()
                        .ignoringFields("id", "lastStatusUpdate", "creationDate")
                        .isEqualTo(
                            Operation(
                                requestedLevel = levelWithBakingRights,
                                status = OperationStatus.PENDING,
                                tzOperation = anyTzOperation,
                                timesFetched = 0,
                                source = anyTzOperation.contents[0]["source"] as String,
                                owner = anyUserId,
                                totalFee = 760,
                                opSize = 80,
                                estimatedConsumption = Operation.Consumption(
                                    totalStorageSize = 0,
                                    totalConsumedGas = 2427,
                                ),
                            )
                        )

                    val addedOperationEntity = operationRepository.findById(addedOperation.id!!).awaitSingle()
                    val notifications =
                        operationUpdateEventRepository.findByOperation_Owner(addedOperation.owner).asFlow().toList()

                    assertThat(notifications.size).isEqualTo(2)
                    assertThat(notifications[1].operation).usingRecursiveComparison()
                        .ignoringFields("lastStatusUpdate", "creationDate")
                        .isEqualTo(addedOperationEntity)
                }
            }
        }
    }

    @Nested
    inner class DeleteOperationTest {

        @Test
        fun `Only non PENDING operation can be deleted`() {
            OperationStatus.values().map { status ->
                if (status != OperationStatus.PENDING) {
                    val operation = OperationEntity(
                        id = "7165585b7b2107354ff102b3",
                        status = status,
                        source = anyTzOperation.contents[0]["source"] as String,
                        owner = anyUserId,
                        opSize = 90,
                        tzOperation = anyTzOperation.toModel(),
                    )
                    runBlocking {
                        operationRepository.save(operation).awaitSingle()
                        assertThrows<OperationIsNotPendingException> {
                            operationService.deleteOperation(operation.owner, operation.id!!)
                        }
                        operationRepository.delete(operation)
                    }
                }
            }
        }

        @Test
        fun `Can't delete as admin`() {
            assertThrows<NotAnAdminFeatureException> {
                runBlocking {
                    operationService.deleteOperation(adminId, datasetOperation.id!!)
                }
            }
        }

        @Test
        fun `Delete as user`() {
            runBlocking {
                deleteWithRequester(anyUserId)
            }
        }

        private suspend fun deleteWithRequester(requester: String) {
            operationService.deleteOperation(requester, datasetOperation.id!!)

            // Verification
            val updatedOperation = operationRepository.findById(datasetOperation.id!!).awaitSingleOrNull()

            assertThat(updatedOperation).isNotNull
            assertThat(updatedOperation!!.status).isEqualTo(OperationStatus.DELETED)
            assertThat(updatedOperation)
                .usingRecursiveComparison()
                .ignoringFields("status", "lastStatusUpdate", "creationDate")
                .isEqualTo(datasetOperation)

            val notification =
                operationUpdateEventRepository.findByOperation_Owner(anyUserId).awaitFirstOrNull()
            assertThat(notification).isNotNull
            assertThat(notification!!.operation).usingRecursiveComparison()
                .ignoringFields("lastStatusUpdate", "creationDate")
                .isEqualTo(updatedOperation)
        }

        @Test
        fun `Can't delete if not owner`() {
            runBlocking {
                assertThrows<IllegalAccessException> {
                    operationService.deleteOperation(otherUserId, datasetOperation.id!!)
                }
            }
        }

    }

    @Nested
    inner class GetOperationTest {

        @Test
        fun `Get as admin`() {
            runBlocking {
                getWithRequester(adminId)
            }
        }

        @Test
        fun `Get as user`() {
            runBlocking {
                getWithRequester(anyUserId)
            }
        }

        private suspend fun getWithRequester(requester: String) {
            val operation = operationService.findByIdOperation(requester, datasetOperation.id!!)

            // Verification
            assertThat(operation).usingRecursiveComparison()
                .ignoringFields("lastStatusUpdate", "creationDate")
                .isEqualTo(datasetOperation)
        }

        @Test
        fun `Can't get if not admin or owner`() {
            runBlocking {
                assertThrows<IllegalAccessException> {
                    operationService.findByIdOperation(otherUserId, datasetOperation.id!!)
                }
            }
        }
    }
}
