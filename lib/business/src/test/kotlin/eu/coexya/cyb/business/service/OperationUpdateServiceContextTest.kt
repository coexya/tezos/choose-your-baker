package eu.coexya.cyb.business.service

import com.ninjasquad.springmockk.MockkBean
import eu.coexya.cyb.business.configuration.TezosConfig
import eu.coexya.cyb.business.model.mapper.toModel
import eu.coexya.cyb.business.model.mapper.toTezos
import eu.coexya.cyb.business.utils.generateBlockchainWithBlocks
import eu.coexya.cyb.business.utils.generateTzBlock
import eu.coexya.cyb.business.utils.generateTzOperation
import eu.coexya.cyb.common.enums.OperationStatus
import eu.coexya.cyb.common.utils.REQUESTED_LEVEL_PASSED
import eu.coexya.cyb.model.entity.OperationEntity
import eu.coexya.cyb.model.migration.MigrationHandler
import eu.coexya.cyb.model.repository.OperationRepository
import eu.coexya.tezos.connector.node.model.TzBlock
import eu.coexya.tezos.connector.service.TezosReaderService
import io.mockk.InternalPlatformDsl.toStr
import io.mockk.clearMocks
import io.mockk.coEvery
import kotlinx.coroutines.flow.toList
import kotlinx.coroutines.reactive.asFlow
import kotlinx.coroutines.reactive.awaitSingle
import kotlinx.coroutines.runBlocking
import org.assertj.core.api.Assertions.assertThat
import org.bson.types.ObjectId
import org.junit.jupiter.api.BeforeEach
import org.junit.jupiter.api.Test
import org.junit.jupiter.api.TestInstance
import org.junit.jupiter.api.extension.ExtendWith
import org.junit.jupiter.params.ParameterizedTest
import org.junit.jupiter.params.provider.Arguments
import org.junit.jupiter.params.provider.MethodSource
import org.springframework.beans.factory.annotation.Autowired
import org.springframework.beans.factory.annotation.Value
import org.springframework.data.mongodb.core.ReactiveMongoTemplate
import org.springframework.test.context.junit.jupiter.SpringExtension
import java.nio.file.Path
import java.time.OffsetDateTime
import java.time.ZoneOffset
import java.util.stream.Stream

@TestInstance(TestInstance.Lifecycle.PER_CLASS)
@ExtendWith(SpringExtension::class)
class OperationUpdateServiceContextTest @Autowired constructor(
    private val operationUpdateService: OperationUpdateService,
    private val tezosConfig: TezosConfig,
    private val operationRepository: OperationRepository,
    override val mongoTemplate: ReactiveMongoTemplate,
    override val migrationHandler: MigrationHandler,
    @Value("\${daemon.update.startupBlocks: #{60}}") val startupBlocks: Long,
    @Value("\${daemon.update.headOffset: #{30}}") val headOffset: Long,
) : AbstractServiceContextTest() {

    @MockkBean
    private lateinit var tezosReaderService: TezosReaderService

    @BeforeEach
    fun beforeEach() {
        resetDatabase()
        clearMocks(tezosReaderService)
    }

    @Test
    fun `Purge old operations`() {
        // Mockk now() for constant test results
        mockkNow(
            OffsetDateTime.of(
                2020,
                10,
                10,
                10,
                10,
                10,
                10,
                ZoneOffset.UTC
            )
        )
        runBlocking {
            importJsonDatasets(Path.of("src/test/resources/datasets/operation_update_service/purge.json"))
            operationUpdateService.purgeOldOperations()
            val allOperationsLeft = operationRepository.findAll().asFlow().toList()
            assertThat(allOperationsLeft.size).isEqualTo(4)
            assertThat(allOperationsLeft.map { it.id }).containsExactlyInAnyOrder(
                "6165acc338facf697184e0ac",
                "6165acc338facf697184e0ad",
                "6165acc338facf697184e0ae",
                "6165acc338facf697184e0af"
            )
        }
        unmockkNow()
    }

    @ParameterizedTest(name = "[{index}]: {2}")
    @MethodSource("inProgressOperationsUpdateProvider")
    fun `Start up operations update`(
        operationsWithExpectedValues: List<Pair<OperationEntity, OperationEntity>>,
        blockchain: List<TzBlock>, // Blockchain state, blockchain[0] is head then blockchain[N] is head's Nth predecessor
        testName: String
    ) {
        // Sanity check before testing
        if (blockchain.size.toLong() != startupBlocks)
            throw IllegalArgumentException("Wrong number of blocks: ${blockchain.size.toLong()} != $startupBlocks")

        // Mockk blockchain state
        (0 until blockchain.size.toLong()).map {
            if (it == 0L) {
                coEvery { tezosReaderService.getHeadBlock(headOffset) } returns blockchain[0]
            } else {
                coEvery { tezosReaderService.getBlock(any(), it) } returns blockchain[it.toInt()]
            }
        }

        runBlocking {
            // Initialize database
            operationsWithExpectedValues.map { operationRepository.save(it.first).awaitSingle() }

            // Call tested method
            operationUpdateService.startupUpdateOperationsStatus()

            // Validate result
            operationsWithExpectedValues.forEach { (operation, expectedOperation) ->
                val updatedOperation = operationRepository.findById(operation.id!!).awaitSingle()

                assertThat(updatedOperation.creationDate.isEqual(expectedOperation.creationDate))
                assertThat(updatedOperation).usingRecursiveComparison()
                    .ignoringFields("lastStatusUpdate")
                    .ignoringFields("creationDate")
                    .isEqualTo(expectedOperation)
                if (updatedOperation.status != operation.status) {
                    assertThat(updatedOperation.lastStatusUpdate.isAfter(operation.lastStatusUpdate))
                }
            }
        }
    }

    fun inProgressOperationsUpdateProvider(): Stream<Arguments> {

        val anyHeader = TzBlock.Header(level = 0, timestamp = OffsetDateTime.MIN)
        val blockchainSize = startupBlocks.toInt()
        val levels = (1..startupBlocks).toList().shuffled()

        return listOf(

            anyOperationEntity().let { generatedOperation ->
                Arguments.of(
                    listOf(
                        operationsWithExpectedValues(
                            operation = generatedOperation.copy(
                                fetchedLevel = levels[0],
                            ),
                            expectedOperation = generatedOperation.copy(
                                status = OperationStatus.BAKED,
                                bakerAddress = tezosConfig.bakerAddress,
                                fetchedLevel = levels[0],
                                level = levels[0],
                                actualConsumption = OperationEntity.Consumption(
                                    totalStorageSize = 0,
                                    totalConsumedGas = 1427
                                ),
                            ),
                        )
                    ),
                    generateBlockchainWithBlocks(
                        blockchainSize,
                        listOf(
                            generateTzBlock().copy(
                                header = anyHeader.copy(level = levels[0]),
                                metadata = mapOf("baker" to tezosConfig.bakerAddress),
                                operations = listOf(listOf(generatedOperation.tzOperation.toTezos())),
                            )
                        )
                    ),
                    "in_progress to baked"
                )
            },

            anyOperationEntity().let { generatedOperation ->
                Arguments.of(
                    listOf(
                        operationsWithExpectedValues(
                            operation = generatedOperation.copy(
                                fetchedLevel = levels[0],
                            ),
                            expectedOperation = generatedOperation.copy(
                                status = OperationStatus.BAKED_BY_ANOTHER,
                                bakerAddress = "another_baker_address",
                                fetchedLevel = levels[0],
                                level = levels[0],
                                actualConsumption = OperationEntity.Consumption(
                                    totalStorageSize = 0,
                                    totalConsumedGas = 1427
                                ),
                            ),
                        )
                    ),
                    generateBlockchainWithBlocks(
                        blockchainSize,
                        listOf(
                            generateTzBlock().copy(
                                header = anyHeader.copy(level = levels[0]),
                                metadata = mapOf("baker" to "another_baker_address"),
                                operations = listOf(listOf(generatedOperation.tzOperation.toTezos())),
                            )
                        )
                    ),
                    "in_progress to baked_by_another"
                )
            },

            anyOperationEntity().let { generatedOperation ->
                Arguments.of(
                    listOf(
                        operationsWithExpectedValues(
                            operation = generatedOperation.copy(
                                requestedLevel = blockchainSize + 1L,
                                fetchedLevel = blockchainSize + 1L,
                            ),
                            expectedOperation = generatedOperation.copy(
                                requestedLevel = blockchainSize + 1L,
                                fetchedLevel = blockchainSize + 1L,
                                status = OperationStatus.IN_PROGRESS,
                            ),
                        )
                    ),
                    generateBlockchainWithBlocks(
                        blockchainSize,
                        emptyList()
                    ),
                    "in_progress to in_progress mandatory level not reached"
                )
            },

            anyOperationEntity().let { generatedOperation ->
                Arguments.of(
                    listOf(
                        operationsWithExpectedValues(
                            operation = generatedOperation.copy(
                                requestedLevel = levels[0],
                                fetchedLevel = levels[0],
                            ),
                            expectedOperation = generatedOperation.copy(
                                requestedLevel = levels[0],
                                fetchedLevel = levels[0],
                                errorMessage = REQUESTED_LEVEL_PASSED,
                                status = OperationStatus.REJECTED,
                            ),
                        )
                    ),
                    generateBlockchainWithBlocks(
                        blockchainSize,
                        emptyList()
                    ),
                    "in_progress to rejected mandatory level passed"
                )
            },

            anyOperationEntity().let { generatedOperation ->
                Arguments.of(
                    listOf(
                        operationsWithExpectedValues(
                            operation = generatedOperation.copy(
                                fetchedLevel = blockchainSize + 1L,
                            ),
                            expectedOperation = generatedOperation.copy(
                                fetchedLevel = blockchainSize + 1L,
                                status = OperationStatus.IN_PROGRESS,
                            ),
                        )
                    ),
                    generateBlockchainWithBlocks(
                        blockchainSize,
                        emptyList()
                    ),
                    "in_progress to in_progress not mandatory level not reached"
                )
            },

            anyOperationEntity().let { generatedOperation ->
                Arguments.of(
                    listOf(
                        operationsWithExpectedValues(
                            operation = generatedOperation.copy(
                                fetchedLevel = levels[0],
                            ),
                            expectedOperation = generatedOperation.copy(
                                status = OperationStatus.PENDING,
                            ),
                        )
                    ),
                    generateBlockchainWithBlocks(
                        blockchainSize,
                        emptyList()
                    ),
                    "in_progress to pending not mandatory level passed"
                )
            },

            anyOperationEntity().let { generatedOperation ->
                Arguments.of(
                    listOf(
                        operationsWithExpectedValues(
                            operation = generatedOperation.copy(
                                requestedLevel = levels[0],
                                status = OperationStatus.PENDING,
                            ),
                            expectedOperation = generatedOperation.copy(
                                requestedLevel = levels[0],
                                errorMessage = REQUESTED_LEVEL_PASSED,
                                status = OperationStatus.REJECTED,
                            ),
                        )
                    ),
                    generateBlockchainWithBlocks(
                        blockchainSize,
                        emptyList()
                    ),
                    "pending to rejected mandatory level passed"
                )
            },

            anyOperationEntity().let { generatedOperation ->
                anyOperationEntity().let { generatedOperation2 ->
                    Arguments.of(
                        listOf(
                            operationsWithExpectedValues(
                                operation = generatedOperation.copy(
                                    requestedLevel = levels[0],
                                    status = OperationStatus.PENDING,
                                ),
                                expectedOperation = generatedOperation.copy(
                                    requestedLevel = levels[0],
                                    errorMessage = REQUESTED_LEVEL_PASSED,
                                    status = OperationStatus.REJECTED,
                                ),
                            ),
                            operationsWithExpectedValues(
                                operation = generatedOperation2.copy(
                                    requestedLevel = levels[1],
                                    fetchedLevel = levels[0],
                                ),
                                expectedOperation = generatedOperation2.copy(
                                    requestedLevel = levels[1],
                                    fetchedLevel = levels[0],
                                    errorMessage = REQUESTED_LEVEL_PASSED,
                                    status = OperationStatus.REJECTED,
                                ),
                            )
                        ),
                        generateBlockchainWithBlocks(
                            blockchainSize,
                            emptyList()
                        ),
                        "multiple operations update pending and rejected"
                    )
                }
            },

            anyOperationEntity().let { generatedOperation ->
                anyOperationEntity().let { generatedOperation2 ->
                    Arguments.of(
                        listOf(
                            operationsWithExpectedValues(
                                operation = generatedOperation.copy(
                                    fetchedLevel = levels[0],
                                ),
                                expectedOperation = generatedOperation.copy(
                                    status = OperationStatus.BAKED,
                                    bakerAddress = tezosConfig.bakerAddress,
                                    fetchedLevel = levels[0],
                                    level = levels[0],
                                    actualConsumption = OperationEntity.Consumption(
                                        totalStorageSize = 0,
                                        totalConsumedGas = 1427
                                    ),
                                ),
                            ),
                            operationsWithExpectedValues(
                                operation = generatedOperation2.copy(
                                    fetchedLevel = levels[1],
                                ),
                                expectedOperation = generatedOperation2.copy(
                                    status = OperationStatus.BAKED_BY_ANOTHER,
                                    bakerAddress = "another_baker_address",
                                    fetchedLevel = levels[1],
                                    level = levels[1],
                                    actualConsumption = OperationEntity.Consumption(
                                        totalStorageSize = 0,
                                        totalConsumedGas = 1427
                                    ),
                                ),
                            )
                        ),
                        generateBlockchainWithBlocks(
                            blockchainSize,
                            listOf(
                                generateTzBlock().copy(
                                    header = anyHeader.copy(level = levels[0]),
                                    metadata = mapOf("baker" to tezosConfig.bakerAddress),
                                    operations = listOf(listOf(generatedOperation.tzOperation.toTezos())),
                                ),
                                generateTzBlock().copy(
                                    header = anyHeader.copy(level = levels[1]),
                                    metadata = mapOf("baker" to "another_baker_address"),
                                    operations = listOf(listOf(generatedOperation2.tzOperation.toTezos())),
                                ),
                            )
                        ),
                        "multiple operations update baked and baked_by_another"
                    )
                }
            },

            anyOperationEntity().let { generatedOperation ->
                anyOperationEntity().let { generatedOperation2 ->
                    anyOperationEntity().let { generatedOperation3 ->
                        Arguments.of(
                            listOf(
                                operationsWithExpectedValues(
                                    operation = generatedOperation.copy(
                                        fetchedLevel = levels[0],
                                    ),
                                    expectedOperation = generatedOperation.copy(
                                        status = OperationStatus.BAKED,
                                        bakerAddress = tezosConfig.bakerAddress,
                                        fetchedLevel = levels[0],
                                        level = levels[0],
                                        actualConsumption = OperationEntity.Consumption(
                                            totalStorageSize = 0,
                                            totalConsumedGas = 1427
                                        ),
                                    ),
                                ),
                                operationsWithExpectedValues(
                                    operation = generatedOperation2.copy(
                                        fetchedLevel = levels[0],
                                    ),
                                    expectedOperation = generatedOperation2.copy(
                                        status = OperationStatus.PENDING,
                                    ),
                                ),
                                operationsWithExpectedValues(
                                    operation = generatedOperation3.copy(
                                        requestedLevel = levels[1],
                                        fetchedLevel = levels[1],
                                    ),
                                    expectedOperation = generatedOperation3.copy(
                                        requestedLevel = levels[1],
                                        fetchedLevel = levels[1],
                                        errorMessage = REQUESTED_LEVEL_PASSED,
                                        status = OperationStatus.REJECTED,
                                    ),
                                )
                            ),
                            generateBlockchainWithBlocks(
                                blockchainSize,
                                listOf(
                                    generateTzBlock().copy(
                                        header = anyHeader.copy(level = levels[0]),
                                        metadata = mapOf("baker" to tezosConfig.bakerAddress),
                                        operations = listOf(listOf(generatedOperation.tzOperation.toTezos())),
                                    ),
                                )
                            ),
                            "multiple operations update baked, pending and rejected"
                        )
                    }
                }
            },

            anyOperationEntity().let { generatedOperation ->
                anyOperationEntity().let { generatedOperation2 ->
                    anyOperationEntity().let { generatedOperation3 ->
                        Arguments.of(
                            listOf(
                                operationsWithExpectedValues(
                                    operation = generatedOperation.copy(
                                        fetchedLevel = levels[0],
                                    ),
                                    expectedOperation = generatedOperation.copy(
                                        status = OperationStatus.BAKED,
                                        bakerAddress = tezosConfig.bakerAddress,
                                        fetchedLevel = levels[0],
                                        level = levels[0],
                                        actualConsumption = OperationEntity.Consumption(
                                            totalStorageSize = 0,
                                            totalConsumedGas = 1427
                                        ),
                                    ),
                                ),
                                operationsWithExpectedValues(
                                    operation = generatedOperation2.copy(
                                        fetchedLevel = levels[0],
                                    ),
                                    expectedOperation = generatedOperation2.copy(
                                        status = OperationStatus.BAKED,
                                        bakerAddress = tezosConfig.bakerAddress,
                                        fetchedLevel = levels[0],
                                        level = levels[0],
                                        actualConsumption = OperationEntity.Consumption(
                                            totalStorageSize = 0,
                                            totalConsumedGas = 1427
                                        ),
                                    ),
                                ),
                                operationsWithExpectedValues(
                                    operation = generatedOperation3.copy(
                                        fetchedLevel = levels[1],
                                    ),
                                    expectedOperation = generatedOperation3.copy(
                                        status = OperationStatus.BAKED_BY_ANOTHER,
                                        bakerAddress = "another_baker_address",
                                        fetchedLevel = levels[1],
                                        level = levels[1],
                                        actualConsumption = OperationEntity.Consumption(
                                            totalStorageSize = 0,
                                            totalConsumedGas = 1427
                                        ),
                                    ),
                                )
                            ),
                            generateBlockchainWithBlocks(
                                blockchainSize,
                                listOf(
                                    generateTzBlock().copy(
                                        header = anyHeader.copy(level = levels[0]),
                                        metadata = mapOf("baker" to tezosConfig.bakerAddress),
                                        operations = listOf(
                                            listOf(
                                                generatedOperation.tzOperation.toTezos(),
                                                generatedOperation2.tzOperation.toTezos()
                                            )
                                        ),
                                    ),
                                    generateTzBlock().copy(
                                        header = anyHeader.copy(level = levels[1]),
                                        metadata = mapOf("baker" to "another_baker_address"),
                                        operations = listOf(listOf(generatedOperation3.tzOperation.toTezos())),
                                    ),
                                )
                            ),
                            "multiple operations same block update baked and rejected"
                        )
                    }
                }
            },

            ).stream()
    }

    fun anyOperationEntity() = generateTzOperation().let {
        OperationEntity(
            id = ObjectId().toStr(),
            status = OperationStatus.IN_PROGRESS,
            tzOperation = it.toModel(),
            timesFetched = 1,
            source = it.contents[0]["source"] as String,
            owner = ObjectId().toStr(),
            opSize = 90,
        )
    }

    // Corresponds to the first argument of method `Start up operations update`
    fun operationsWithExpectedValues(
        operation: OperationEntity,
        expectedOperation: OperationEntity = operation,
    ) = Pair(operation, expectedOperation)

}
