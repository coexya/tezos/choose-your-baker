package eu.coexya.cyb.business.service.fms

import eu.coexya.cyb.business.configuration.TestContextConfiguration
import eu.coexya.cyb.business.exception.InsufficientFeesException
import eu.coexya.cyb.business.service.FeeManagementService
import eu.coexya.cyb.business.service.impl.FeesFMS
import eu.coexya.cyb.model.entity.OperationEntity
import org.junit.jupiter.api.Test
import org.junit.jupiter.api.TestInstance
import org.junit.jupiter.api.assertThrows
import org.junit.jupiter.params.ParameterizedTest
import org.junit.jupiter.params.provider.Arguments
import org.junit.jupiter.params.provider.MethodSource
import org.springframework.beans.factory.annotation.Autowired
import org.springframework.boot.test.context.SpringBootTest
import java.util.stream.Stream

@TestInstance(TestInstance.Lifecycle.PER_CLASS)
@SpringBootTest(
    classes = [TestContextConfiguration::class],
    properties = [
        "tezos.fees.mode=fees",
        "tezos.fees.minimalNtzFees=100000",
        "tezos.fees.minimalNtzPerGasUnit=100",
        "tezos.fees.minimalNtzPerByte=1000",
    ],
)
class FeesFMSTest @Autowired constructor(
    override val feeManagementService: FeeManagementService,
) : FMSTest() {

    @Test
    fun `FeeManagementService is in FEES mode`() {
        assert(feeManagementService is FeesFMS)
    }

    @ParameterizedTest(name = "[{index}]: {2}")
    @MethodSource("checkFeesProvider")
    fun `Check fees`(
        operation: OperationEntity,
        expectingException: Boolean,
        testName: String,
    ) {
        if (expectingException) {
            assertThrows<InsufficientFeesException> { feeManagementService.checkFees(operation) }
        } else {
            feeManagementService.checkFees(operation)
        }
    }

    fun checkFeesArgs(
        operation: OperationEntity,
        expectingException: Boolean,
        testName: String,
    ): Arguments = Arguments.of(operation, expectingException, testName)

    fun checkFeesProvider(): Stream<Arguments> {
        return listOf(

            checkFeesArgs(
                operation = operationWithContents(
                    listOf(
                        mapOf(
                            "fee" to "204",
                            "gas_limit" to "1527",
                            "storage_limit" to "0",
                        ),
                    )
                ).copy(totalFee = 204),
                expectingException = true,
                testName = "Insufficient fees transaction",
            ),

            checkFeesArgs(
                operation = operationWithContents(
                    listOf(
                        mapOf(
                            "fee" to "275",
                            "gas_limit" to "1520",
                            "storage_limit" to "0",
                        ),
                        mapOf(
                            "fee" to "275",
                            "gas_limit" to "1520",
                            "storage_limit" to "0",
                        ),
                    )
                ).copy(totalFee = 550),
                expectingException = false,
                testName = "Enough fees with transactions",
            ),

            ).stream()
    }

}
