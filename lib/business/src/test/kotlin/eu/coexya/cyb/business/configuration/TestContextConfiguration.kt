package eu.coexya.cyb.business.configuration

import org.springframework.boot.autoconfigure.EnableAutoConfiguration
import org.springframework.context.annotation.ComponentScan
import org.springframework.context.annotation.Configuration
import org.springframework.context.annotation.Profile
import org.springframework.test.context.ActiveProfiles

@Configuration
@EnableAutoConfiguration
@ComponentScan("eu.coexya")
@ActiveProfiles("test")
class TestContextConfiguration
