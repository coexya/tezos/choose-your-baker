package eu.coexya.cyb.webcore.mapper

import eu.coexya.cyb.api.operation.Operation

fun eu.coexya.cyb.business.model.Operation.toWeb() = Operation(
    id = id,
    lastStatusUpdate = lastStatusUpdate,
    creationDate = creationDate,
    level = level,
    requestedLevel = requestedLevel,
    fetchedLevel = fetchedLevel,
    timesFetched = timesFetched,
    errorMessage = errorMessage,
    bakerAddress = bakerAddress,
    status = status,
    owner = owner,
    totalFee = totalFee,
    estimatedConsumption = estimatedConsumption?.toWeb(),
    actualConsumption = actualConsumption?.toWeb(),
)

fun eu.coexya.cyb.business.model.Operation.Consumption.toWeb() = Operation.Consumption(
    totalStorageSize = totalStorageSize,
    totalConsumedGas = totalConsumedGas,
)
