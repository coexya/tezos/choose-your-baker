package eu.coexya.cyb.webcore.authentication

import java.time.OffsetDateTime

data class JwtTokenDetails(
    val owner: String,
    val roles: List<String>,
    val creationTime: OffsetDateTime?,
    val persisted: Boolean,
    val expirationTime: OffsetDateTime?,
)
