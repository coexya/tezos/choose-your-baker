package eu.coexya.cyb.webcore.authentication

import org.springframework.security.core.authority.SimpleGrantedAuthority
import org.springframework.security.core.userdetails.UserDetails

class CustomUserDetails(
    roles: List<String>,
    val id: String,
    private val enabled: Boolean = true,
    private val username: String = "",
    private val credentialsNonExpired: Boolean = true,
    private val password: String = "",
    private val accountNonExpired: Boolean = true,
    private val accountNonLocked: Boolean = true,
) : UserDetails {

    private val authorities = roles.map { SimpleGrantedAuthority("ROLE_$it") }.toMutableList()

    override fun getAuthorities() = authorities

    override fun isEnabled() = enabled

    override fun getUsername() = username

    override fun isCredentialsNonExpired() = credentialsNonExpired

    override fun getPassword() = password

    override fun isAccountNonExpired() = accountNonExpired

    override fun isAccountNonLocked() = accountNonLocked

    fun hasAnyRole(vararg roles: String) = roles.any { authorities.contains(SimpleGrantedAuthority("ROLE_$it")) }

}
