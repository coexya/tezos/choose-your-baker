package eu.coexya.cyb.webcore.authentication

import eu.coexya.cyb.business.service.TokenService
import kotlinx.coroutines.reactor.mono
import org.springframework.security.authentication.AccountExpiredException
import org.springframework.security.authentication.ReactiveAuthenticationManager
import org.springframework.security.core.Authentication
import org.springframework.security.oauth2.server.resource.BearerTokenAuthenticationToken
import org.springframework.stereotype.Component
import reactor.core.publisher.Mono
import java.time.OffsetDateTime

@Component
class CybAuthenticationManager(
    private val jwtTokenService: JwtTokenService,
    private val tokenService: TokenService,
) : ReactiveAuthenticationManager {

    /**
     * Authenticate the user using a token or username/password.
     * @param authentication Bearer token containing the JWT token for authentication or username/password token.
     * @return Authentication composed of the authenticated user with its token/password and authorities.
     */
    override fun authenticate(authentication: Authentication): Mono<Authentication> {
        return when (authentication) {
            is BearerTokenAuthenticationToken -> mono {
                val bearerToken = authentication.token

                val tokenInfo = jwtTokenService.parseToken(bearerToken)

                if (!tokenInfo.persisted && tokenInfo.expirationTime?.isBefore(OffsetDateTime.now()) == true) {
                    throw AccountExpiredException("Expired token")
                }

                tokenService.getAndCheckToken(tokenInfo.owner)

                val user = CustomUserDetails(
                    id = tokenInfo.owner,
                    roles = tokenInfo.roles,
                )

                CybAuthenticationToken(
                    principal = user,
                    credentials = bearerToken,
                    authorities = user.authorities
                )
            }
            else -> throw IllegalArgumentException("Illegal authentication method.")
        }
    }
}
