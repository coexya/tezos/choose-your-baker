//Launch npm run test

import {HttpBackend, HttpResponseError, STATUS_CODE} from '@taquito/http-utils';
import {InMemorySigner} from '@taquito/signer';
import {CybSdk, CybAuth} from '@coexya/cyb-sdk';
import {v4 as uuidv4} from 'uuid';


import {
    RawBlockHeaderResponse,
    OperationEntry,
    OperationContentsAndResultOrigination,
} from '@taquito/rpc';
import {TezosToolkit} from '@taquito/taquito';
import {OperationUpdateEvent} from "@coexya/cyb-sdk/dist/lib/types/api";
import {OperationStatus} from "@coexya/cyb-sdk/dist/lib/types/enums";
import {Estimate} from "@taquito/taquito/dist/types/contract/estimate";
import {stat} from "fs";
import {Operation, TzOperation} from "@coexya/cyb-sdk/dist/types/types/api";

export const adminToken="eyJ0eXAiOiJKV1QiLCJhbGciOiJIUzI1NiJ9.eyJvd25lciI6IjYxODkyYmZhZWE3ODY1NjRjYjE3YWM0NSIsImNyZWF0aW9uVGltZSI6IjIwMjEtMTEtMDhUMDU6NTQ6MDIuOTYwNjgyLTA4OjAwIiwicm9sZXMiOlsiQURNSU4iXSwiaXNzIjoiVGV6b3NAQ3liIiwicGVyc2lzdGVkIjp0cnVlfQ.Gy2q0YhRsndfEJgQ1boa6JhIj5XOSYytt9JMkwOLgCE"
export const issuerToken="eyJ0eXAiOiJKV1QiLCJhbGciOiJIUzI1NiJ9.eyJvd25lciI6IjYxODkyYmQ1ZWE3ODY1NjRjYjE3YWM0MyIsImNyZWF0aW9uVGltZSI6IjIwMjEtMTEtMDhUMDU6NTM6MjUuMzgxMzg4LTA4OjAwIiwicm9sZXMiOlsiSVNTVUVSIl0sImlzcyI6IlRlem9zQEN5YiIsInBlcnNpc3RlZCI6dHJ1ZX0.xOdzsl7OffzRe3BYyAxexxlGHMceHXfRVX2veB3mFN8"
const bakerAddress = "tz1YPSCGWXwBdTncK2aCctSZAXWvGsGwVJqU";

export const tmHost = "localhost"
export const tmPort = "9090"
export const tmUrl = "http://" + tmHost + ":" + tmPort

export const auth = new CybAuth(tmUrl, issuerToken)
export const authWithoutToken = new CybAuth(tmUrl, undefined)
export const nodeUrl = "http://localhost:20000"
export const chain = "main"
export const mainAddress = "tz1VSUr8wwNhLAzempoch5d6hLRiTh8Cjcjb"



export const bobAddress = 'tz1aSkwEot3L2kmUvcoxzjMomb9mvBNuzFK6'
export const pkAlice = 'edsk3QoqBuvdamxouPhin7swCvkQNgq4jP5KZPbwWNnwdZpSpJiEbq'
export const pkBob = 'edsk3RFfvaFaxbHx8BMtEW1rKQcPtDML3LXjNqMNLCzC3wLC1bWbAt'
export const owner = '61892bd5ea786564cb17ac43'
export const wrongAddress = 'tz1toto12312312312312312312312312322'

//Basic contract
//store one string
// If the input strin == immedaiateFailure => Failure
// If the input String == conditionalFailure and the storage==becareful => failure
export  const codeContract = `{ parameter (or (unit %getHello) (string %updateName)) ;
  storage string ;
  code { UNPAIR ;
         IF_LEFT
           { DROP }
           { PUSH string "immediateFailure" ;
             SWAP ;
             DUP ;
             DUG 2 ;
             COMPARE ;
             EQ ;
             IF { DROP 2 ; PUSH string "immediate failure" ; FAILWITH }
                { PUSH string "becareful" ;
                  DIG 2 ;
                  COMPARE ;
                  EQ ;
                  PUSH string "conditionalFailure" ;
                  DUP 3 ;
                  COMPARE ;
                  EQ ;
                  AND ;
                  IF { DROP ; PUSH string "conditional failure" ; FAILWITH } {} } } ;
         NIL operation ;
         PAIR } }`


export const httpbackend = new HttpBackend();
export const aliceSdk: CybSdk = new CybSdk(auth, nodeUrl, chain, httpbackend)
export const aliceSdkWithoutToken: CybSdk = new CybSdk(authWithoutToken, nodeUrl, chain, httpbackend)

aliceSdk.setProvider({
    signer: new InMemorySigner(pkAlice)
})

aliceSdkWithoutToken.setProvider({
    signer: new InMemorySigner(pkAlice)
})

export const bobSdk: CybSdk = new CybSdk(auth, nodeUrl, chain, httpbackend)

bobSdk.setProvider({
    signer: new InMemorySigner(pkBob)
})

export const tezosToolkitAlice = new TezosToolkit(nodeUrl)
tezosToolkitAlice.setProvider({signer: new InMemorySigner(pkAlice)});

export const tezosToolkitBob = new TezosToolkit(nodeUrl)
tezosToolkitBob.setProvider({signer: new InMemorySigner(pkBob)});

export const insufficientFees = 7
export const deleteNonPendingOperation = 5

export function delay(ms: number) {
    return new Promise(resolve => setTimeout(resolve, ms));
}

export async function waitForResult(interval: number, duration: number, checkFunction: () => Promise<Object>): Promise<Object> {
    for (let i = 0; i < duration; i++) {
        await delay(1000);
        const ret = await checkFunction();
        console.log("ret=" + ret);
        if (ret != undefined) {
            console.log("found");
            return ret;
        }
    }
    return undefined;
}

export async function waitForSomething(interval: number, duration: number, checkFunction: () => Promise<Boolean>): Promise<boolean> {
    for (let i = 0; i < duration; i++) {
        await delay(1000);
        const ret = await checkFunction();
        //console.log("ret=" + ret);
        if (ret) {
            console.log("found");
            return true;
        }
    }
    return false;
}


export async function waitForValue(interval: number, duration: number, contract, expectedValue: string): Promise<boolean> {
    let checkFunction = async () => {
        const storage = await contract.storage();
        return (storage == expectedValue);
    }
    return waitForSomething(interval, duration, checkFunction)

}


export async function waitForStatus(interval: number, duration: number, sdk: CybSdk, hash: string, status: string): Promise<boolean> {

    let checkFunction = async () => {
        const operation = await sdk.operationService.getOperation({hash : hash});
        //console.log(`status=${operation.status}`)
        return (operation.status == status);
    }
    return waitForSomething(interval, duration, checkFunction)
}

export async function waitForSlot(interval: number, duration: number, bakerAddress: string): Promise<number> {

    let checkFunction = async () => {
        const level = await getSlots(bakerAddress, 9, 3)
        return level
    }
    const level = await waitForResult(interval, duration, checkFunction)
    return level as number

}


export async function waitForDoubleSlots(interval: number, duration: number, bakerAddress: string): Promise<number[]> {

    let checkFunction = async () => {
        const level = await getDoubleSlots(bakerAddress, 9, 3)
        return level
    }
    const level = await waitForResult(interval, duration, checkFunction)
    return level as number[]

}


export async function getDoubleSlots(bakerAddress: string, nbSlot: number, initOffset?: number): Promise<number[]> {

    if (initOffset == undefined) {
        initOffset = 1;
    }

    //Get the head level of the blockchain
    const header = await httpbackend.createRequest<Object>(
        {
            url: `${nodeUrl}/chains/main/blocks/head/header`,
            method: 'GET'
        })

    //console.log(header['level'])
    const initLevel = header['level']

    //Check for the x next block if the delegate will be the baker
    let cpt = 0
    let ret: number[] = [];

    for (let i = initLevel; i < initLevel + nbSlot; i++) {
        const bakingRights = await httpbackend.createRequest<Object[]>(
            {
                url: `${nodeUrl}/chains/main/blocks/head/helpers/baking_rights?level=${i}&delegate=${bakerAddress}`,
                method: 'GET'
            })

        if (bakingRights.length > 0 && bakingRights[0]['priority'] == 0) {
            if (i <= initOffset) {
                return undefined
            } else {
                console.log(bakingRights[0])
                ret[cpt] = i
                cpt++
                if (cpt == 2) {
                    return ret
                }
            }
        }

    }
    return undefined

}


export async function getSlots(bakerAddress: string, nbSlot: number, initOffset?: number): Promise<number> {

    if (initOffset == undefined) {
        initOffset = 1;
    }

    //Get the head level of the blockchain
    const header = await httpbackend.createRequest<Object>(
        {
            url: `${nodeUrl}/chains/main/blocks/head/header`,
            method: 'GET'
        })

    //console.log(header['level'])
    const initLevel = header['level']

    //Check for the x next block if the delegate will be the baker
    for (let i = initLevel; i < initLevel + nbSlot; i++) {
        const bakingRights = await httpbackend.createRequest<Object[]>(
            {
                url: `${nodeUrl}/chains/main/blocks/head/helpers/baking_rights?level=${i}&delegate=${bakerAddress}`,
                method: 'GET'
            })

        if (bakingRights.length > 0 && bakingRights[0]['priority'] == 0) {
            if (i <= initOffset) {
                return undefined
            } else {
                //console.log(bakingRights[0])
                return bakingRights[0]['level']
            }
        }

    }
    return undefined

}

export async function eventListenerCreation(sdk: CybSdk): Promise<any> {
    let operationEventSource = sdk.operationService.getOperationEventSource()
    operationEventSource.onmessage = (e) => {
        const opUpdateEvent: OperationUpdateEvent = JSON.parse(e.data)
        let status = opUpdateEvent.operation.status
        console.log(`Operation ${opUpdateEvent.operation.id} updated with status: ${status}`)
    }
    return operationEventSource
}

//Return true when the status of an operation take the expected value
export async function onStatusEvent(sdk: CybSdk, hash: string, status: OperationStatus): Promise<boolean> {

    let promise = new Promise<boolean>((resolve) => {
        let operationEventSource = sdk.operationService.getOperationEventSource()
        operationEventSource.onmessage = (e) => {
            console.log("MessageReceived")
            const opUpdateEvent: OperationUpdateEvent = JSON.parse(e.data)
            let statusToCheck = opUpdateEvent.operation.status
            let hashToCheck = opUpdateEvent.operation.tzOperation.hash
            if (hash === hashToCheck && status === statusToCheck) {
                console.log("Operation has been processed.")
                resolve(true)
            }
        }
    })
    return promise
}

export async function contractCreation(tzToolKit: TezosToolkit, codeContract: string, init: string): Promise<string> {
    let simpleContractAccount: string
    await tzToolKit.contract.originate({
        code: codeContract,
        init: init,
    })
        .then((originationOp) => {
            console.log(`Waiting for confirmation of origination for ${originationOp.contractAddress}...`);
            simpleContractAccount = originationOp.contractAddress
            return originationOp.contract();
        })
        .then((contract) => {
            console.log(`Origination completed.`);

        })
        .catch((error) => console.log(`Error: ${JSON.stringify(error, null, 2)}`));
    return simpleContractAccount
}

export async function estimateTransfer(sdk: CybSdk, amount: number, address: string): Promise<Estimate> {
    const est = await sdk.estimate.transfer({to: address, amount: amount})
    console.log(`Burn fees : ${est.burnFeeMutez} Gas limit : ${est.gasLimit} Minimal Fees : ${est.minimalFeeMutez} Storage Limit : ${est.storageLimit}
            Suggested Fee : ${est.suggestedFeeMutez} Total Cost : ${est.totalCost} Specified base fee : ${est.usingBaseFeeMutez}`)
    return est
}

export async function estimateContractCall(sdk: CybSdk, contractAccount: string, input: string): Promise<Estimate> {
    const contract = await sdk.contract.at(contractAccount)
    const op = contract.methods.updateName(input).toTransferParams()
    const estContractCall = await sdk.estimate.transfer(op)
    console.log(`Burn fees : ${estContractCall.burnFeeMutez} Gas limit : ${estContractCall.gasLimit} Minimal Fees : ${estContractCall.minimalFeeMutez}
         Storage Limit : ${estContractCall.storageLimit} Suggested Fee : ${estContractCall.suggestedFeeMutez} Total Cost : ${estContractCall.totalCost}
         Specified base fee : ${estContractCall.usingBaseFeeMutez}`)
    return estContractCall
}

export async function createToken(duration:number, name:string, roles: string[], adminToken:string):Promise<Object>{
    const issuer = {
        duration: duration,
        name: name,
        roles: roles
    }
    const token = await httpbackend.createRequest<Object>(
        {
            url: `${tmUrl}/api/tokens`,
            method: 'POST',
            headers: {"Authorization": `Bearer ${adminToken}`}
        }, issuer
    ).catch((error) => {
        console.log(`Error: ${error} ${JSON.stringify(error, null, 2)}`);
        return undefined

    })
    return token
}

 

export async function getTokens(adminToken:string):Promise<Object[]>{
    
    const token = await httpbackend.createRequest<Object>(
        {
            url: `${tmUrl}/api/tokens`,
            method: 'GET',
            headers: {"Authorization": `Bearer ${adminToken}`}
        }
    ).catch((error) => {
        console.log(`Error: ${error} ${JSON.stringify(error, null, 2)}`);
        return undefined

    })
    return token
}
