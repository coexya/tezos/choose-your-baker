//Launch npm run test

import {OpKind, ParamsWithKind} from "@taquito/taquito"
import {OperationStatus} from "@coexya/cyb-sdk/dist/lib/types/enums";

import {
    aliceSdk,
    bobAddress,
    codeContract,
    contractCreation,
    eventListenerCreation,
    getSlots,
    onStatusEvent,
    tezosToolkitAlice,
    waitForSlot,
} from './common';

const amount = 0.1;
const tooBigAmount = 1000;

// Nominal test

describe('fee', () => {


    
    it('test', async () => {

        //Check the baker identity
        const bakerIdentity = await aliceSdk.getBakerIdentity();
        console.log(bakerIdentity);
        const level = await getSlots(bakerIdentity.bakerAddress, 11)
        console.log(`level = ${level}`);


    })


    //jest --forceExit -t 'fee0_0 FEE_0_All0'
    // TM 
    //fees:
    //  mode: FEES
    //  minimalNtzFees: 0
    //  minimalNtzPerGasUnit: 0
    //  minimalNtzPerByte: 0
    //Baker
    //  minimalNtzFees: 0
    //  minimalNtzPerGasUnit: 0
    //  minimalNtzPerByte: 0

    it('FEE_0_All0', async () => {

        //Event Listener creation
        const operationEventSource = eventListenerCreation(aliceSdk)

        // Contract creation
        let simpleContractAccount: string = await contractCreation(tezosToolkitAlice, codeContract, '"test"')

        //Check the baker identity
        const bakerIdentity = await aliceSdk.getBakerIdentity();
        console.log(`bakerIdentity=${JSON.stringify(bakerIdentity)}`);
        expect(bakerIdentity.minimalNtzFees).toEqual(0)
        expect(bakerIdentity.minimalNtzPerGasUnit).toEqual(0)
        expect(bakerIdentity.minimalNtzPerByte).toEqual(0)

        //CCreate a simple contract
        const contractNominal = await aliceSdk.wallet.at(simpleContractAccount)

        //Check the next slot
        const level = await waitForSlot(1000, 30, bakerIdentity.bakerAddress)
        if (level == undefined) {
            fail('No slot available!!!');
        }
        console.log(`The next slot is ${level}`)

        //Add a transfer and a contract call to the batch
        const targetString="test123456"
        const params : ParamsWithKind[] =[
            {
                kind : OpKind.TRANSACTION,
                to: bobAddress,
                amount: 0.1,
                fee: 0,
            },
            {
                kind: OpKind.TRANSACTION,
                ...contractNominal.methods.updateName(targetString).toTransferParams({})
            },
        ]

        //Estimate for Taquito
        const estimateTaquito = await aliceSdk.estimate.batch(params)
        console.log(`estimateTaquito=${JSON.stringify(estimateTaquito)}`);

        //Estimate for CYB
        const estimateCyb = await aliceSdk.cybEstimate(params)
        console.log(`estimateCyb=${JSON.stringify(estimateCyb)}`);

        //Send the transaction
        const sendOp = await aliceSdk.generateBatch(estimateCyb).send()

        //Retrieve the transaction
        const insertedOp = await aliceSdk.operationService.getOperation (sendOp.opHash)
        console.log(`insertedOp=${JSON.stringify(insertedOp)}`);

        //Do some tests here to check the fees.
        const event = await onStatusEvent(aliceSdk,insertedOp.tzOperation.hash,OperationStatus.BAKED)
        expect(event).toEqual(true)
        expect(insertedOp.totalFee).toEqual(0)
        aliceSdk.operationService.getOperationEventSource().close()

    }, 150000)

})
