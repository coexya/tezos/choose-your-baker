//Launch npm run test

import {CybAuth, CybSdk} from '@coexya/cyb-sdk';
import {v4 as uuidv4} from 'uuid';
import {OpKind, ParamsWithKind} from "@taquito/taquito"
import {OperationUpdateEvent} from "@coexya/cyb-sdk/dist/lib/types/api";
import {OperationStatus} from "@coexya/cyb-sdk/dist/lib/types/enums";

import {
    adminToken,
    aliceSdk,
    bobAddress,
    chain,
    codeContract,
    contractCreation,
    deleteNonPendingOperation,
    estimateContractCall,
    estimateTransfer,
    eventListenerCreation,
    getSlots,
    httpbackend,
    insufficientFees,
    nodeUrl,
    onStatusEvent,
    owner,
    tezosToolkitAlice,
    tezosToolkitBob,
    tmUrl,
    waitForDoubleSlots,
    waitForSlot,
    waitForStatus,
    waitForValue,
    wrongAddress
} from './common';

const amount = 0.1;
const tooBigAmount = 1000;

// Nominal test
describe('getBlocks', () => {

    it('test', async () => {

        //Check the baker identity
        const bakerIdentity = await aliceSdk.getBakerIdentity();
        console.log(bakerIdentity);
        const level = await getSlots(bakerIdentity.bakerAddress, 11)
        console.log(`level = ${level}`);


    })

    it('OSI_0_Simple_call_with_one_transfer_and_one_contract_call', async () => {

        //Event Listener creation
        const operationEventSource = eventListenerCreation(aliceSdk)

        // Contract creation
        let simpleContractAccount: string = await contractCreation(tezosToolkitAlice, codeContract, '"test"')

        //Check the baker identity
        const bakerIdentity = await aliceSdk.getBakerIdentity();
        console.log(bakerIdentity);

        //Check the next slot
        const level = await waitForSlot(1000, 30, bakerIdentity.bakerAddress)
        if (level == undefined) {
            fail('No slot available!!!');
        }
        console.log(`The slot is ${level}`)

        //Estimate the fees for a transfer
        await estimateTransfer(aliceSdk, amount, bobAddress)

        //Estimate the fees for a contract call
        await estimateContractCall(aliceSdk, simpleContractAccount, "123")

        //Call the nominal contract and a small transfer
        const contractNominal = await aliceSdk.wallet.at(simpleContractAccount)
        let myuuid = uuidv4();
        const cc = contractNominal.methods.updateName(myuuid).toTransferParams({
            amount: amount,
            storageLimit: 3000,
            gasLimit: 30000,
            fee: 30000
        });

        //Add a transfer and a contract call to the batch
        const batch = aliceSdk.wallet.batch()
            .withTransfer({
                to: bobAddress, amount: amount,
                storageLimit: 3000,
                gasLimit: 3000,
                fee: 30000
            })
            .withContractCall(contractNominal.methods.updateName(myuuid));

        const operationTaquito = await batch.send()
            .then((hash) => {
                console.log(`Operation injected ${hash}`);
                return hash
            })
            .catch((error) => {
                console.log(`Error: ${error} ${JSON.stringify(error, null, 2)}`);
                return undefined
            });

        console.log(`hash=${operationTaquito.opHash}`)
        let ret = await onStatusEvent(aliceSdk, operationTaquito.opHash, OperationStatus.BAKED)

        expect(ret).toEqual(true)
        const operation = await aliceSdk.operationService.getOperation({hash : operationTaquito.opHash});
        console.log(operation);

        expect(operation.owner).toEqual(owner)
        expect(operationTaquito.opHash).toEqual(operation.tzOperation.hash)
        console.log(`expected slot=${level} / real slot=${operation.level}`)
        //expect(operation.level).toEqual(level)
        expect(operation.timesFetched).toEqual(1)


    }, 100000)

    it('OSI_WRGFEE_Transfer_with_wrong_fees', async () => {

        //Event Listener creation
        const operationEventSource = eventListenerCreation(aliceSdk)

        //Check the baker identity
        const bakerIdentity = await aliceSdk.getBakerIdentity();
        console.log(bakerIdentity);

        //Check the next slot
        const level = await waitForSlot(1000, 30, bakerIdentity.bakerAddress)
        if (level == undefined) {
            fail('No slot available!!!');
        }

        console.log(`The slot is ${level}`)


        const batch = aliceSdk.wallet.batch()
            .withTransfer({
                to: bobAddress, amount: amount,
                storageLimit: 3000,
                gasLimit: 3000,
                fee: 0
            });

        const operationTaquito = await batch.send()
            .then((hash) => {
                console.log(`Operation injected ${hash}`);
                return hash
            })
            .catch((error) => {
                console.log(`Error: ${JSON.parse(error.body).errorCode} ${JSON.parse(error.body).message} \n ${JSON.stringify(error, null, 2)}`);
                expect(JSON.parse(error.body).errorCode).toEqual(insufficientFees)
                return undefined
            });

        expect(operationTaquito).toEqual(undefined)
        //TODO to complete with the error code

    }, 100000)

    it('OSI_WRGFEE_DRY_Transfer_with_wrong_fees_in_dry_run', async () => {

        //Event Listener creation
        const operationEventSource = eventListenerCreation(aliceSdk)

        //Check the baker identity
        const bakerIdentity = await aliceSdk.getBakerIdentity();
        console.log(bakerIdentity);

        //Check the next slot
        const level = await waitForSlot(1000, 30, bakerIdentity.bakerAddress)
        if (level == undefined) {
            fail('No slot available!!!');
        }

        console.log(`The slot is ${level}`)

        if (!aliceSdk.isContextLocked) {
            aliceSdk.modifyContext({

            })
        }

        const batch = aliceSdk.wallet.batch()
            .withTransfer({
                to: bobAddress, amount: amount,
                storageLimit: 3000,
                gasLimit: 3000,
                fee: 0,
            });

        const operationTaquito = await batch.send()
            .then((hash) => {
                console.log(`Operation injected ${hash}`);
                return hash
            })
            .catch((error) => {
                console.log(`Error: ${JSON.parse(error.body).errorCode} ${JSON.parse(error.body).message} \n ${JSON.stringify(error, null, 2)}`);
                expect(JSON.parse(error.body).errorCode).toEqual(insufficientFees)
                return undefined
            });

        expect(operationTaquito).toEqual(undefined)
        //TODO to complete with the error code

    }, 100000)

    it('OSI_DRY_Transfer_with_correct_fees_in_dry_run', async () => {

        //Event Listener creation
        const operationEventSource = eventListenerCreation(aliceSdk)

        //Check the baker identity
        const bakerIdentity = await aliceSdk.getBakerIdentity();
        console.log(bakerIdentity);

        //Check the next slot
        const level = await waitForSlot(1000, 30, bakerIdentity.bakerAddress)
        if (level == undefined) {
            fail('No slot available!!!');
        }

        console.log(`The slot is ${level}`)

        if (!aliceSdk.isContextLocked) {
            aliceSdk.modifyContext({
            })
        }

        const batch = aliceSdk.wallet.batch()
            .withTransfer({
                to: bobAddress, amount: amount,
                storageLimit: 3000,
                gasLimit: 3000,
                fee: 30000
            });

        const operationTaquito = await batch.send()
            .then((hash) => {
                console.log(`Operation injected ${hash}`);
                return hash
            })
            .catch((error) => {
                console.log(`Error: ${JSON.parse(error.body).errorCode} ${JSON.parse(error.body).message} \n ${JSON.stringify(error, null, 2)}`);
                expect(JSON.parse(error.body).errorCode).toEqual(insufficientFees)
                return undefined
            });

        console.log(`hash=${operationTaquito.opHash}`)

        let ret = await onStatusEvent(aliceSdk, operationTaquito.opHash, OperationStatus.BAKED)
        expect(ret).toEqual(true)
        const operation = await aliceSdk.operationService.getOperation({hash : operationTaquito.opHash});
        console.log(operation);

        expect(operationTaquito).toEqual(undefined)

    }, 100000)

    it('OSI_WRGADD_Transfer_to_a_wrong_formalism_address', async () => {

        //Check the baker identity
        const bakerIdentity = await aliceSdk.getBakerIdentity();
        console.log(bakerIdentity);

        //Check the next slot
        const level = await waitForSlot(1000, 30, bakerIdentity.bakerAddress)
        if (level == undefined) {
            fail('No slot available!!!');
        }

        console.log(`The slot is ${level}`)


        const batch = aliceSdk.wallet.batch()
            .withTransfer({
                to: wrongAddress, amount: amount,
                storageLimit: 3000,
                gasLimit: 3000,
                fee: 30000
            });

        const operationTaquito = await batch.send()
            .then((hash) => {
                console.log(`Operation injected ${hash}`);
                return hash
            })
            .catch((error) => {
                console.log(`Error: ${error} ${JSON.stringify(error, null, 2)}`);
                return undefined
            });

        expect(operationTaquito).toEqual(undefined)
        //TODO to complete with the error code

    }, 100000)

    it('OSI_TOBGTSF_Transfer_with_a_too_big_transfer', async () => {

        //Check the baker identity
        const bakerIdentity = await aliceSdk.getBakerIdentity();
        console.log(bakerIdentity);

        //Check the next slot
        const level = await waitForSlot(1000, 30, bakerIdentity.bakerAddress)
        if (level == undefined) {
            fail('No slot available!!!');
        }

        console.log(`The slot is ${level}`)

        const batch = aliceSdk.wallet.batch()
            .withTransfer({
                to: bobAddress, amount: tooBigAmount,
                storageLimit: 3000,
                gasLimit: 3000,
                fee: 30000
            });


        const operationTaquito = await batch.send()
            .then((hash) => {
                console.log(`Operation injected ${hash}`);
                return hash
            })
            .catch((error) => {
                console.log(`Error: ${error} ${JSON.stringify(error, null, 2)}`);
                return undefined
            });

        expect(operationTaquito).toEqual(undefined)

    }, 100000)

    it('OSI_DBLTSF_Double_Transfer', async () => {

        //Event Listener creation
        const operationEventSource = eventListenerCreation(aliceSdk)

        //Check the baker identity
        const bakerIdentity = await aliceSdk.getBakerIdentity();
        console.log(bakerIdentity);

        //Check the next slot
        const level = await waitForSlot(1000, 30, bakerIdentity.bakerAddress)
        if (level == undefined) {
            fail('No slot available!!!');
        }

        console.log(`The slot is ${level}`)


        const batch = aliceSdk.wallet.batch()
            .withTransfer({
                to: bobAddress, amount: amount,
                storageLimit: 3000,
                gasLimit: 3000,
                fee: 30000
            }).withTransfer({
                to: bobAddress, amount: amount,
                storageLimit: 3000,
                gasLimit: 3000,
                fee: 30000
            });


        const operationTaquito = await batch.send()
            .then((hash) => {
                console.log(`Operation injected ${hash}`);
                return hash
            })
            .catch((error) => {
                console.log(`Error: ${error} ${JSON.stringify(error, null, 2)}`);
                return undefined
            });

        console.log(`hash=${operationTaquito.opHash}`)
        let ret = await onStatusEvent(aliceSdk, operationTaquito.opHash, OperationStatus.BAKED)

        expect(ret).toEqual(true)
        const operation = await aliceSdk.operationService.getOperation({hash : operationTaquito.opHash});
        console.log(operation);

        expect(operation.owner).toEqual(owner)
        expect(operationTaquito.opHash).toEqual(operation.tzOperation.hash)
        console.log(`expected slot=${level} / real slot=${operation.level}`)
        //expect(operation.level).toEqual(level)
        expect(operation.timesFetched).toEqual(1)

    }, 100000)

    it('OSD_DELUKNWOP_Delete_Unknown_Operation', async () => {

        const deletedOperation = await aliceSdk.operationService.deleteOperation({hash :"Test123", id: "Test123"}).catch((error) => {
            console.log(`Error: ${error} ${JSON.stringify(error, null, 2)}`);
            return undefined
        });

        expect(deletedOperation).toEqual(undefined)

    }, 100000)

    it('OSD_DELBAKOP_Delete_Baked_Operation', async () => {

        //Event Listener creation
        const operationEventSource = eventListenerCreation(aliceSdk)

        //Check the baker identity
        const bakerIdentity = await aliceSdk.getBakerIdentity();
        console.log(bakerIdentity);

        //Check the next slot
        const level = await waitForSlot(1000, 30, bakerIdentity.bakerAddress)
        if (level == undefined) {
            fail('No slot available!!!');
        }

        console.log(`The slot is ${level}`)

        const batch = aliceSdk.wallet.batch()
            .withTransfer({
                to: bobAddress, amount: amount,
                storageLimit: 3000,
                gasLimit: 3000,
                fee: 30000
            })

        const operationTaquito = await batch.send()
            .then((hash) => {
                console.log(`Operation injected ${hash}`);
                return hash
            })
            .catch((error) => {
                console.log(`Error: ${error} ${JSON.stringify(error, null, 2)}`);
                return undefined
            });

        //Wait for the operation to be baked
        console.log(`hash=${operationTaquito.opHash}`)
        let ret = await onStatusEvent(aliceSdk, operationTaquito.opHash, OperationStatus.BAKED)
        expect(ret).toEqual(true)

        //Try to delete the operation
        const deletedOperation = await aliceSdk.operationService.deleteOperation({hash: operationTaquito.opHash}).catch((error) => {
            console.log(`Error: ${JSON.parse(error.body).errorCode} ${JSON.parse(error.body).message} \n ${JSON.stringify(error, null, 2)}`);
            expect(JSON.parse(error.body).errorCode).toEqual(deleteNonPendingOperation)
            return undefined
        });
        expect(deletedOperation).toEqual(undefined)

        const operation = await aliceSdk.operationService.getOperation({hash : operationTaquito.opHash});
        console.log(operation);

        expect(operation.owner).toEqual(owner)
        expect(operationTaquito.opHash).toEqual(operation.tzOperation.hash)
        console.log(`expected slot=${level} / real slot=${operation.level}`)
        expect(operation.timesFetched).toEqual(1)

    }, 100000)

    it('OSD_DELINPRGOP_Delete_in_progress_operation', async () => {

        //Event Listener creation
        const operationEventSource = eventListenerCreation(aliceSdk)

        //Check the baker identity
        const bakerIdentity = await aliceSdk.getBakerIdentity();
        console.log(bakerIdentity);

        //Check the next slot
        const level = await waitForSlot(1000, 30, bakerIdentity.bakerAddress)
        if (level == undefined) {
            fail('No slot available!!!');
        }

        console.log(`The slot is ${level}`)

        const batch = aliceSdk.wallet.batch()
            .withTransfer({
                to: bobAddress, amount: amount,
                storageLimit: 3000,
                gasLimit: 3000,
                fee: 30000
            })

        const operationTaquito = await batch.send()
            .then((hash) => {
                console.log(`Operation injected ${hash}`);
                return hash
            })
            .catch((error) => {
                console.log(`Error: ${error} ${JSON.stringify(error, null, 2)}`);
                return undefined
            });

        //Wait for the operation to be in progress
        console.log(`hash=${operationTaquito.opHash}`)
        let ret = await onStatusEvent(aliceSdk, operationTaquito.opHash, OperationStatus.IN_PROGRESS)
        expect(ret).toEqual(true)

        //Try to delete the operation
        const deletedOperation = await aliceSdk.operationService.deleteOperation({hash: operationTaquito.opHash}).catch((error) => {
            console.log(`Error: ${JSON.parse(error.body).errorCode} ${JSON.parse(error.body).message} \n ${JSON.stringify(error, null, 2)}`);
            //expect(JSON.parse(error.body).errorCode).toEqual(deleteBakedOperation)
            return undefined
        });
        expect(deletedOperation).toEqual(undefined)

        //Wait for the operation to be baked
        ret = await onStatusEvent(aliceSdk, operationTaquito.opHash, OperationStatus.BAKED)
        expect(ret).toEqual(true)

        const operation = await aliceSdk.operationService.getOperation({hash : operationTaquito.opHash});
        console.log(operation);

        expect(operation.owner).toEqual(owner)
        expect(operationTaquito.opHash).toEqual(operation.tzOperation.hash)
        console.log(`expected slot=${level} / real slot=${operation.level}`)
        expect(operation.level).toEqual(level)
        expect(operation.timesFetched).toEqual(1)

    }, 100000)

    it('OSD_DELPNDGOP_Delete_pending_operation', async () => {

        //Event Listener creation
        const operationEventSource = eventListenerCreation(aliceSdk)

        //Check the baker identity
        const bakerIdentity = await aliceSdk.getBakerIdentity();
        console.log(bakerIdentity);

        //Check the next slot
        const level = await waitForSlot(1000, 30, bakerIdentity.bakerAddress)
        if (level == undefined) {
            fail('No slot available!!!');
        }
        console.log(`The slot is ${level}`)

        const batch = aliceSdk.wallet.batch()
            .withTransfer({
                to: bobAddress, amount: amount,
                storageLimit: 3000,
                gasLimit: 3000,
                fee: 30000
            })

        const operationTaquito = await batch.send()
            .then((hash) => {
                console.log(`Operation injected ${hash}`);
                return hash
            })
            .catch((error) => {
                console.log(`Error: ${error} ${JSON.stringify(error, null, 2)}`);
                return undefined
            });
        console.log(`hash=${operationTaquito.opHash}`)

        //Try to delete the operation
        const deletedOperation = await aliceSdk.operationService.deleteOperation({hash: operationTaquito.opHash}).catch((error) => {
            console.log(`Error: ${JSON.parse(error.body).errorCode} ${JSON.parse(error.body).message} \n ${JSON.stringify(error, null, 2)}`);
            return undefined
        });
        expect(deletedOperation.status).toEqual(OperationStatus.DELETED)
        console.log(`hash=${deletedOperation.opHash}`)
        console.log(`${JSON.stringify(deletedOperation, null, 2)}`)

    }, 100000)

    it('OSR_INS&RTRV_Insert_operation_&_retrieve_status', async () => {

        //Event Listener creation
        const operationEventSource = eventListenerCreation(aliceSdk)

        //Check the baker identity
        const bakerIdentity = await aliceSdk.getBakerIdentity();
        console.log(bakerIdentity);

        //Check the next slot
        const level = await waitForSlot(1000, 30, bakerIdentity.bakerAddress)
        if (level == undefined) {
            fail('No slot available!!!');
        }

        console.log(`The slot is ${level}`)


        const batch = aliceSdk.wallet.batch()
            .withTransfer({
                to: bobAddress, amount: amount,
                storageLimit: 3000,
                gasLimit: 3000,
                fee: 30000
            })


        const operationTaquito = await batch.send()
            .then((hash) => {
                console.log(`Operation injected ${hash.opHash}`);
                return hash
            })
            .catch((error) => {
                console.log(`Error: ${error} ${JSON.stringify(error, null, 2)}`);
                return undefined
            });
        const status: OperationStatus = await aliceSdk.operationService.getOperation({hash : operationTaquito.opHash})
            .then(operation => {
                return operation.status
            })

        let ret = await onStatusEvent(aliceSdk, operationTaquito.opHash, OperationStatus.BAKED)
        expect(ret).toEqual(true)
        const operation = await aliceSdk.operationService.getOperation({hash : operationTaquito.opHash});
        console.log(operation);

        expect(operation.owner).toEqual(owner)
        expect(operationTaquito.opHash).toEqual(operation.tzOperation.hash)
        console.log(`expected slot=${level} / real slot=${operation.level}`)
        //expect(operation.level).toEqual(level)
        expect(operation.timesFetched).toEqual(1)

    }, 100000)

    it('OSR_DBLCOTCALL_Double_contract_call', async () => { //insert a contract call  with "be careful" and insert a second contract call with "conditionalFailure" to make the first operation fail => the status is REJECTED

        //Event Listener creation
        const operationEventSource = eventListenerCreation(aliceSdk)

        // Contract creation
        console.log("Before Creation")
        let simpleContractAccount: string = await contractCreation(tezosToolkitAlice, codeContract, `"becareful"`)
        console.log("After Creation")
        //Check the baker identity
        const bakerIdentity = await aliceSdk.getBakerIdentity();
        console.log(bakerIdentity);

        //Check the next slot
        const level = await waitForSlot(1000, 30, bakerIdentity.bakerAddress)
        if (level == undefined) {
            fail('No slot available!!!');
        }

        console.log(`The slot is ${level}`)
        //Call the nominal contract
        const contractNominal = await aliceSdk.wallet.at(simpleContractAccount)

        //Add a contract call to the batch
        let batch = aliceSdk.wallet.batch()
            .withContractCall(contractNominal.methods.updateName("conditionalFailure"));

        let operationTaquito = await batch.send()
            .then((operation) => {
                console.log(`Operation injected ${operation.opHash}`);
                return operation
            })
            .catch((error) => {
                console.log(`Error: ${error} ${JSON.stringify(error, null, 2)}`);
                return undefined
            });

        expect(operationTaquito).toEqual(undefined)


    }, 100000)

    it('OSR_INS&STP&STLD - Stop daemon, insert operation, wait for staled and launch daemon', async () => {

        //Event Listener creation
        const operationEventSource = eventListenerCreation(aliceSdk)

        //Check the baker identity
        const bakerIdentity = await aliceSdk.getBakerIdentity();
        console.log(bakerIdentity);

        //Check the next slot
        const level = await waitForSlot(1000, 30, bakerIdentity.bakerAddress)
        if (level == undefined) {
            fail('No slot available!!!');
        }

        console.log(`The slot is ${level}`)

        const batch = aliceSdk.wallet.batch()
            .withTransfer({
                to: bobAddress, amount: amount,
                storageLimit: 3000,
                gasLimit: 3000,
                fee: 30000
            })


        const operationTaquito = await batch.send()
            .then((hash) => {
                console.log(`Operation injected ${hash.opHash}`);
                return hash
            })
            .catch((error) => {
                console.log(`Error: ${error} ${JSON.stringify(error, null, 2)}`);
                return undefined
            });

        const operation = await aliceSdk.operationService.getOperation({hash : operationTaquito.opHash});
        console.log(operation);

        let ret = await onStatusEvent(aliceSdk, operationTaquito.opHash, OperationStatus.STALE)
        expect(ret).toEqual(true)

        const status: OperationStatus = await aliceSdk.operationService.getOperation({hash : operationTaquito.opHash})
            .then(operation => {
                return operation.status
            })
        expect(status).toEqual(OperationStatus.STALE)


    }, 200000)

    it('EvCh_Service_Call_with_new_token', async () => {

        const nonAuth = new CybAuth(tmUrl, adminToken)
        const nonExistentSdk: CybSdk = new CybSdk(nonAuth, nodeUrl, chain, httpbackend)

        //Event Listener creation
        const operationEventSource = eventListenerCreation(nonExistentSdk)

        //Check the baker identity
        const bakerIdentity = await aliceSdk.getBakerIdentity();
        console.log(bakerIdentity);

        //Check the next slot
        const level = await waitForSlot(1000, 30, bakerIdentity.bakerAddress)
        if (level == undefined) {
            fail('No slot available!!!');
        }

        console.log(`The slot is ${level}`)


        const batch = aliceSdk.wallet.batch()
            .withTransfer({
                to: bobAddress, amount: amount,
                storageLimit: 3000,
                gasLimit: 3000,
                fee: 30000
            })


        const operationTaquito = await batch.send()
            .then((hash) => {
                console.log(`Operation injected ${hash.opHash}`);
                return hash
            })
            .catch((error) => {
                console.log(`Error: ${error} ${JSON.stringify(error, null, 2)}`);
                return undefined
            });
        const status: OperationStatus = await aliceSdk.operationService.getOperation({hash : operationTaquito.opHash})
            .then(operation => {
                return operation.status
            })

        let ret = await onStatusEvent(aliceSdk, operationTaquito.opHash, OperationStatus.BAKED)
        expect(ret).toEqual(true)
        const operation = await aliceSdk.operationService.getOperation({hash : operationTaquito.opHash});
        console.log(operation);

        expect(operation.owner).toEqual(owner)
        expect(operationTaquito.opHash).toEqual(operation.tzOperation.hash)
        console.log(`expected slot=${level} / real slot=${operation.level}`)
        expect(operation.timesFetched).toEqual(1)

    }, 100000)


// Nominal test with a specific slot


    it('Nominal_test_with_a_specific_slot', async () => {

        let operationEventSource = aliceSdk.operationService.getOperationEventSource()
        operationEventSource.onmessage = (e) => {
            const opUpdateEvent: OperationUpdateEvent = JSON.parse(e.data)
            let status = opUpdateEvent.operation.status
            console.log(`Operation ${opUpdateEvent.operation.id} updated with status: ${status}`)
        }
        // Contract creation
        let simpleContractAccount: string
        await tezosToolkitAlice.contract.originate({
            code: codeContract,
            init: `"test"`,
        })
            .then((originationOp) => {
                console.log(`Waiting for confirmation of origination for ${originationOp.contractAddress}...`);
                simpleContractAccount = originationOp.contractAddress
                return originationOp.contract();
            })
            .then((contract) => {
                console.log(`Origination completed.`);

            })
            .catch((error) => console.log(`Error: ${JSON.stringify(error, null, 2)}`));


        //Check the baker identity
        const bakerIdentity = await aliceSdk.getBakerIdentity();
        console.log(bakerIdentity);

        //Check the next slot
        const slots = await aliceSdk.getTmBakingSlots();
        console.log(slots);
        if (slots.length <=1) {
            fail('No double slots available!!!');
        }

        const lastLevel=slots[slots.length-1].level
        console.log(`The slot is ${lastLevel}`)

        const currentSlot = await aliceSdk.getBakingSlots({maxPriority : 0})

        //Call the nominal contract and a small transfer
        const contractNominal = await aliceSdk.wallet.at(simpleContractAccount)
        let myuuid = uuidv4();
        const batch = aliceSdk.wallet.batch()
            .withTransfer({
                to: bobAddress, amount: amount,
                storageLimit: 3000,
                gasLimit: 3000,
                fee: 30000
            })
            .withContractCall(contractNominal.methods.updateName(myuuid));

        aliceSdk.modifyContext({level: lastLevel})    

        const operationTaquito = await batch.send()
            .then((hash) => {
                console.log(`Operation injected ${hash}`);
                return hash
            })
            .catch((error) => {
                console.log(`Error: ${error} ${JSON.stringify(error, null, 2)}`);
                return undefined
            });

        console.log(`hash=${operationTaquito.opHash}`)

        //We check the update of the blockchain
        /*let ret = await waitForValue(1000, 30, contractNominal, myuuid)
        expect(ret).toEqual(true)*/

        console.log(`The current level ${currentSlot[0].level}`)
        console.log(`The first slot is ${slots[0].level}`)
        console.log(`The last slot is ${lastLevel}`)
        console.log(`The number of slots is ${slots.length}`)

        let ret = await waitForStatus(1000, 120, aliceSdk, operationTaquito.opHash, "BAKED")
        expect(ret).toEqual(true)
        const operation = await aliceSdk.operationService.getOperation({hash : operationTaquito.opHash});
        console.log(operation);
        expect(operation.owner).toEqual("61892bd5ea786564cb17ac43")
        expect(operationTaquito.opHash).toEqual(operation.tzOperation.hash)
        //console.log(`expected slot=${level}`)
        expect(operation.level).toEqual(lastLevel)

    }, 100000)


    //Failure call
    it('failure_call', async () => {


        // Simple contract creation
        let simpleContractAccount: string
        await tezosToolkitAlice.contract.originate({
            code: codeContract,
            init: `"test"`,
        })
            .then((originationOp) => {
                console.log(`Waiting for confirmation of origination for ${originationOp.contractAddress}...`);
                simpleContractAccount = originationOp.contractAddress
                return originationOp.contract();
            })
            .then((contract) => {
                console.log(`Origination completed.`);

            })
            .catch((error) => console.log(`Error: ${JSON.stringify(error, null, 2)}`));

        // do the transfer. There is a failure (immediateFailure)
        const contractNominal = await aliceSdk.wallet.at(simpleContractAccount)
        const batch = aliceSdk.wallet.batch()
            .withTransfer({
                to: bobAddress, amount: amount,
                storageLimit: 3000,
                gasLimit: 3000,
                fee: 30000
            })
            .withContractCall(contractNominal.methods.updateName("immediateFailure"));

        const hash = await batch.send()
            .then((hash) => {
                console.log(`Operation injected ${hash}`);
                return hash
            })
            .catch((error) => {
                console.log(`Error: ${error} ${JSON.stringify(error, null, 2)}`);
                return undefined
            });

        // We check the failure
        expect(hash).toEqual(undefined)
        let ret = await waitForValue(1000, 30, contractNominal, "test")
        expect(ret).toEqual(true)


    }, 100000)


    // test with a conditional failure
    it('cond_failure_call', async () => {


        // simple contract creation
        let simpleContractAccount: string
        await tezosToolkitAlice.contract.originate({
            code: codeContract,
            init: `"test"`,
        })
            .then((originationOp) => {
                console.log(`Waiting for confirmation of origination for ${originationOp.contractAddress}...`);
                simpleContractAccount = originationOp.contractAddress
                return originationOp.contract();
            })
            .then((contract) => {
                console.log(`Origination completed.`);

            })
            .catch((error) => console.log(`Error: ${JSON.stringify(error, null, 2)}`));


        // We call the contract with conditionalfailure. The dry run is OK becaus the storage is test
        const contractNominal = await aliceSdk.wallet.at(simpleContractAccount)
        const batch = aliceSdk.wallet.batch()
            .withTransfer({
                to: bobAddress, amount: amount,
                storageLimit: 3000,
                gasLimit: 3000,
                fee: 30000
            })
            .withContractCall(contractNominal.methods.updateName("conditionalFailure"));

        const operationTaquito = await batch.send()
            .then((hash) => {
                console.log(`Operation injected ${hash}`);
                return hash
            })
            .catch((error) => {
                console.log(`Error: ${error} ${JSON.stringify(error, null, 2)}`);
                return undefined
            });

        expect(operationTaquito).not.toEqual(undefined)


        // Here bob call the same contract and put becareful in the storage
        const contractNominalBob = await tezosToolkitBob.wallet.at(simpleContractAccount)
        const opBob = await contractNominalBob.methods.updateName("becareful").send();
        await opBob.confirmation();

        let ret = await waitForStatus(1000, 30, aliceSdk, operationTaquito.opHash, "REJECTED")
        expect(ret).toEqual(true)
        const operation = await aliceSdk.operationService.getOperation({hash : operationTaquito.opHash});
        console.log(operation);


        /*
        const opBob2 = await contractNominalBob.methods.updateName("conditionalFailure").send();
        await opBob2.confirmation().catch((error) =>  {console.log(`Error: ${error} ${JSON.stringify(error, null, 2)}`);});
        */

    }, 100000)


    //jest --forceExit -t 'getBlocks Target_Level'
    // TM 
    //fees:
    //  mode: TRANSACTION
    //  minimalNtzFees: 0
    //  minimalNtzPerGasUnit: 0
    //  minimalNtzPerByte: 0
    //Baker
    //  minimalNtzFees: 0
    //  minimalNtzPerGasUnit: 0
    //  minimalNtzPerByte: 0
    // We test the target level by modifying the context
    it('Target_Level', async () => {

        //Event Listener creation
        const operationEventSource = eventListenerCreation(aliceSdk)

        // Contract creation
        let simpleContractAccount: string = await contractCreation(tezosToolkitAlice, codeContract, '"test"')

        //Check the baker identity
        const bakerIdentity = await aliceSdk.getBakerIdentity();
        console.log(`bakerIdentity=${JSON.stringify(bakerIdentity)}`);
        expect(bakerIdentity.minimalNtzFees).toEqual(0)
        expect(bakerIdentity.minimalNtzPerGasUnit).toEqual(0)
        expect(bakerIdentity.minimalNtzPerByte).toEqual(0)

        //CCreate a simple contract
        const contractNominal = await aliceSdk.wallet.at(simpleContractAccount)


        let nextSlot = 0;
        while (true){
            //Check the current slot
            const currentSlot = await aliceSdk.getBakingSlots({maxPriority : 0})
            //console.log(`bakingSlots=${JSON.stringify(currentSlot)}`);
            const currentLevel=currentSlot[0].level
            //console.log(`currentLevel=${currentLevel}`);  
            
            //Get the next slots
            const levels = Array.from({length: 10}, (_, i) => i + currentLevel + 5)
            const bakingSlots = await aliceSdk.getBakingSlots({maxPriority : 0, levels: levels, delegates: [bakerIdentity.bakerAddress]}).then((bakingSlots) => {
                return bakingSlots
            })
            .catch((error) => {
                console.log(`Error: ${JSON.parse(error.body).errorCode} ${JSON.parse(error.body).message} \n ${JSON.stringify(error, null, 2)}`);
                return undefined
            });

            if (bakingSlots != undefined && bakingSlots.length >=2){
                console.log(`bakingSlots=${JSON.stringify(bakingSlots)}`);
                nextSlot=bakingSlots[1].level
                break
            }
        }      

        //Add a transfer and a contract call to the batch
        const targetString="test123456"
        const params : ParamsWithKind[] =[
            {
                kind: OpKind.TRANSACTION,
                to: bobAddress,
                amount: 0.1,
                fee: 0,
            },
            {
                kind : OpKind.TRANSACTION,
                ...contractNominal.methods.updateName(targetString).toTransferParams({})
            },
        ]
       

       //Estimate for CYB
       const estimateCyb = await aliceSdk.cybEstimate(params)
       console.log(`estimateCyb=${JSON.stringify(estimateCyb)}`);


       //Send the transaction
       aliceSdk.modifyContext({level: nextSlot})
       const sendOp = await aliceSdk.generateBatch(estimateCyb).send()


       let ret = await onStatusEvent(aliceSdk, sendOp.opHash, OperationStatus.BAKED)
       expect(ret).toEqual(true)


        //Retrieve the transaction
        const insertedOp = await aliceSdk.operationService.getOperation ({hash: sendOp.opHash})
        console.log(`insertedOp=${JSON.stringify(insertedOp)}`);

        //Do some tests here to check the fees.
        expect(insertedOp.level).toEqual(nextSlot)
   
        


    }, 100000)

})
