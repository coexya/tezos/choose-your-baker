//Launch npm run test

import {InMemorySigner} from '@taquito/signer';



import {MichelsonMap, OpKind, ParamsWithKind} from '@taquito/taquito';
import {OperationStatus} from "@coexya/cyb-sdk/dist/lib/types/enums";
import {CybKycAdminSdk} from "../sdk/src/services/CybKycAdminSdk"
import {CybSdk, CybAuth} from '@coexya/cyb-sdk';

import {
    aliceSdk, auth,
    bobAddress,
    chain,
    codeContract,
    contractCreation,
    eventListenerCreation, httpbackend, issuerToken, mainAddress,
    nodeUrl,
    onStatusEvent, pkAlice, pkBob,
    tezosToolkitAlice, tmUrl,
    waitForSlot
} from './common';
import {inspect} from "util";

const amount = 0.1;
const tooBigAmount = 1000;
const bakerAddress = "tz1YPSCGWXwBdTncK2aCctSZAXWvGsGwVJqU";
const kycContractAddress = "KT1Ms3jgBf34d2J3yjUkNv8NuaSTY19Y5Gtr";

//jest --forceExit -t 'fee BAKED_BY_ANOTHER'
// TM
//fees:
//  mode: FEES
//  minimalNtzFees: 0
//  minimalNtzPerGasUnit: 0
//  minimalNtzPerByte: 0
//Baker
//  minimalNtzFees: 100000
//  minimalNtzPerGasUnit: 100
//  minimalNtzPerByte: 100

// Nominal test
describe('KYC_SDK', () => {

    it('CR_CRT_BK', async () => { //Create a certified baker in the smart contract

        //Check the baker identity
        const bakerIdentity = await aliceSdk.getBakerIdentity();
        console.log(`bakerIdentity=${JSON.stringify(bakerIdentity)}`);

        //Create a contract using the steps described in:
        //https://edivgitlab.coexya.lan/tezos/cyb/choose-your-baker/-/blob/develop/kyc/KycContract.md

        //Create a KYC admin CYB from the contract previously created
        const adminKycSdk: CybKycAdminSdk = new CybKycAdminSdk(kycContractAddress, nodeUrl, chain, httpbackend)
        adminKycSdk.setProvider({
            signer: new InMemorySigner(pkAlice),
        })

        let param = new Map<string,string> ([
            ["country", "Britain :-) !"],
            ["company", "COEXYA"],
            ["url", "Coexya.com"]
            ])
        await adminKycSdk.addBaker(bakerAddress,param)
        const contract = await adminKycSdk.rpc.getContract(kycContractAddress)
        console.log(`contract info=${JSON.stringify(contract)}`);

        //Add a certified baker using the CybSdk :
        const contractNominal = await aliceSdk.wallet.at(kycContractAddress)

        //Create params for to call the contract
        const map: MichelsonMap<string, string> = new MichelsonMap()
        map.set("country", "Republique populaire of Randomistan") // /!\ Special french characters does not work "é" "è" etc.
        map.set("company", "Random Bzh.")
        map.set("url", "Random.bzh")
        map.set("Param Tutut", "Random Tutut")
        const params : ParamsWithKind[] =[
            {
                kind : OpKind.TRANSACTION,
                ...contractNominal.methods.add_baker(bobAddress,map).toTransferParams()
            }]

        const operation = await aliceSdk.generateBatch(params).send()
            .then((batchWalletOp) => {
                console.log(`Operation injected ${batchWalletOp}`);
                return batchWalletOp
            })
            .catch((error) => {
                console.log(`Error: ${error} ${JSON.stringify(error, null, 2)}`);
                return undefined
            });
        console.log(`hash=${operation.opHash}`)
        let ret = await onStatusEvent(aliceSdk, operation.opHash, OperationStatus.BAKED)
        await aliceSdk.operationService.getOperationEventSource().close()


    }, 150000)

    it('UPDT_CRT_BK', async () => { //Update a certified baker in the smart contract

        //Check the baker identity
        const bakerIdentity = await aliceSdk.getBakerIdentity();
        console.log(`bakerIdentity=${JSON.stringify(bakerIdentity)}`);

        //Create a contract using the steps described in:
        //http://edivgitlab.ren-dev2.lan/tezos/cyb/choose-your-baker/-/blob/develop/kyc/KycContract.md

        //Create a KYC admin CYB from the contract previously created
        const adminKycSdk: CybKycAdminSdk = new CybKycAdminSdk(kycContractAddress, nodeUrl, chain, httpbackend)
        adminKycSdk.setProvider({
            signer: new InMemorySigner(pkAlice),
        })


        let param = new Map<string,string> ([
            ["country", "Wonderland"],
            ["company", "Alice SARL"],
            ["url", "NotAnURL.com"]
        ])
        await adminKycSdk.addBaker(mainAddress,param)
        const contract = await adminKycSdk.rpc.getContract(kycContractAddress)
        console.log(`contract info=${JSON.stringify(contract.script.storage)}`);

        //Create params to call the contract
        param = new Map<string,string> ([
            ["country", "FarFarAway"],
            ["company", "Bakery Gmbh"],
            ["url", "NotAnURl.eu"]
        ])

        await adminKycSdk.addBaker(mainAddress,param)
        const contractModified = await adminKycSdk.rpc.getContract(kycContractAddress)
        console.log(`contract info=${JSON.stringify(contractModified.script.storage)}`);

    }, 150000)

    it('RMV_CRT_BK', async () => { //Remove a certified baker in the smart contract

        //Check the baker identity
        const bakerIdentity = await aliceSdk.getBakerIdentity();
        console.log(`bakerIdentity=${JSON.stringify(bakerIdentity)}`);

        //Create a contract using the steps described in:
        //http://edivgitlab.ren-dev2.lan/tezos/cyb/choose-your-baker/-/blob/develop/kyc/KycContract.md

        //Create a KYC admin CYB from the contract previously created
        const adminKycSdk: CybKycAdminSdk = new CybKycAdminSdk(kycContractAddress, nodeUrl, chain, httpbackend)
        adminKycSdk.setProvider({
            signer: new InMemorySigner(pkAlice),
        })

        let param = new Map<string,string> ([
            ["country", "Wonderland"],
            ["company", "Alice SARL"],
            ["url", "NotAnURL.com"]
        ])
        await adminKycSdk.addBaker("tz1SUjpVZe18i2VbPEeQBxcv5erERFuGk1Q8", param)
        const contract = await adminKycSdk.rpc.getContract(kycContractAddress)
        console.log(`contract info=${JSON.stringify(contract.script.storage)}`);
        expect(JSON.stringify(contract.script.storage).includes("tz1SUjpVZe18i2VbPEeQBxcv5erERFuGk1Q8")).toBe(true)

        //Remove the newly added baker
        await adminKycSdk.removeBaker("tz1SUjpVZe18i2VbPEeQBxcv5erERFuGk1Q8")
        const contractModified = await adminKycSdk.rpc.getContract(kycContractAddress)
        console.log(`contract info=${JSON.stringify(contractModified.script.storage)}`);
        expect(JSON.stringify(contractModified.script.storage).includes("tz1SUjpVZe18i2VbPEeQBxcv5erERFuGk1Q8")).toBe(false)

    }, 150000)

})
