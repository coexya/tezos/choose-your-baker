//Launch npm run test
// Nominal test

import {
    eventListenerCreation,
    waitForSlot,
    aliceSdk,
    bobAddress,
    onStatusEvent,
    httpbackend,
    tmUrl,
    adminToken,
    issuerToken,
    auth,
    nodeUrl,
    chain,
    createToken,
    contractCreation,
    tezosToolkitAlice,
    codeContract,
    owner,
    pkAlice,
    pkBob, tmHost, tmPort, bobSdk, mainAddress
} from './common';
import {OperationStatus} from "@coexya/cyb-sdk/dist/lib/types/enums";
import {OperationUpdateEvent} from '@coexya/cyb-sdk/dist/types/types/api';
import {CybAuth, CybSdk} from '@coexya/cyb-sdk';
import {v4 as uuidv4} from 'uuid';
import {InMemorySigner} from "@taquito/signer";
import * as http from 'http';
import {OpKind, ParamsWithKind} from "@taquito/taquito";

const amount = 0.1;

describe('authorization_table', () => {


    //jest --forceExit -t 'authorization_table ADMIN_OSI_Authorization'
    //Test that an admin cannot insert operation
    it('ADMIN_OSI_Authorization', async () => { // Test to insert operation as an Admin

        //Create a new admin token
        const token = await createToken(10000, "New Admin Token", ["ADMIN"], adminToken)
        console.log(token)

        //Get and show the info for the new admin token generated
        const idNewAdmin = JSON.parse(JSON.stringify(token)).id
        const jwtToken = JSON.parse(JSON.stringify(token)).jwtToken
        console.log(`Id : ${idNewAdmin}\njwtToken : ${jwtToken}`)

        //Create a new Sdk for the new admin token
        const adminAuth = new CybAuth(tmUrl, jwtToken)
        const adminSdk: CybSdk = new CybSdk(adminAuth, nodeUrl, chain, httpbackend)
        adminSdk.setProvider({signer: new InMemorySigner(pkAlice)});

        //Insert an operation in the batch
        const batch = adminSdk.wallet.batch()
            .withTransfer({
                to: bobAddress, amount: amount,
                storageLimit: 3000,
                gasLimit: 3000,
                fee: 3000
            })

        //Try to send the operation with the sdk
        const operationTaquito = await batch.send()
            .then((op) => {
                console.log(`Operation injected ${op}`);
                return op
            })
            .catch((error) => {
                console.log(`Error: ${error} ${JSON.stringify(error, null, 2)}`);
                expect(error.status).toEqual(403)
                return undefined
            });
        //Check if the insertion failed
        console.log(operationTaquito)
        expect(operationTaquito).toEqual(undefined)


    }, 100000)

    it('ADMIN_OSD Authorization', async () => { // Test to delete an operation as an admin using the sdk and api request

        //Create a new admin token
        const token = await createToken(10000, "ADMIN OSD", ["ADMIN"], adminToken)
        //get the id and jwtToken of the token
        const adminId = JSON.parse(JSON.stringify(token)).id
        const jwtToken = JSON.parse(JSON.stringify(token)).jwtToken
        console.log(`Id : ${adminId}\njwtToken : ${jwtToken}`)

        //Create a new Sdk for the admin token created above
        const adminAuth = new CybAuth(tmUrl, jwtToken)
        const adminSdk: CybSdk = new CybSdk(adminAuth, nodeUrl, chain, httpbackend)
        adminSdk.setProvider({signer: new InMemorySigner(pkAlice)});

        //Insert an operation in the batch as the standard Issuer
        const batch = aliceSdk.wallet.batch()
            .withTransfer({
                to: bobAddress, amount: amount,
                storageLimit: 3000,
                gasLimit: 3000,
                fee: 30000
            })

        //Try to send the operation
        const operationTaquito = await batch.send()
            .then((hash) => {
                console.log(`Operation injected ${hash}`);
                return hash
            })
            .catch((error) => {
                console.log(`Error: ${error} ${JSON.stringify(error, null, 2)}`);
                return undefined
            });

        console.log(`hash=${operationTaquito.opHash}`)
        const operation = await aliceSdk.operationService.getOperation({hash: operationTaquito.opHash})
        console.log(`Operation ${JSON.stringify(operation)} \n`)
        const query = {id: operation.id}

        //Try to delete the operation as an admin using the sdk --> Operation is unknown because not in the same CybSdk
        const deletedOperation = await adminSdk.operationService.deleteOperation({hash: operationTaquito.opHash}).catch((error) => {
            console.log(`${JSON.stringify(error, null, 2)}`);
            return undefined
        });
        console.log("Wait just a bit")
        expect(deletedOperation).toEqual(undefined)

        //Try to delete the operation as an admin using the api request
        const request = await httpbackend.createRequest<Object>(
            {
                url: `${tmUrl}/api/operation/delete`,
                method: 'POST',
                headers: {"Authorization": `Bearer ${jwtToken}`}
            },
            query).catch((error) => {
            console.log(`Error: ${error} ${JSON.stringify(error, null, 2)}`);
            expect(error.status).toEqual(403)
            return undefined
        })
        expect(request).toEqual(undefined)

    }, 100000)


    it('ISSUER_OSD Authorization', async () => { // Test to delete an operation as an issuer using the sdk and api request

        //Create a new issuer token
        const token = await createToken(10000, "Issuer Token", ["ISSUER"], adminToken)
        //get the id and jwtToken of the token
        const issuerId = JSON.parse(JSON.stringify(token)).id
        const jwtToken = JSON.parse(JSON.stringify(token)).jwtToken
        console.log(`Id : ${issuerId}\njwtToken : ${jwtToken}`)

        //Create a new Sdk for the admin token created above
        const issuerAuth = new CybAuth(tmUrl, jwtToken)
        const issuerSdk: CybSdk = new CybSdk(issuerAuth, nodeUrl, chain, httpbackend)
        issuerSdk.setProvider({signer: new InMemorySigner(pkAlice)});

        //Insert an operation in the batch as the standard Issuer
        const batch = aliceSdk.wallet.batch()
            .withTransfer({
                to: bobAddress, amount: amount,
                storageLimit: 3000,
                gasLimit: 3000,
                fee: 30000
            })

        //Try to send the operation
        const operationTaquito = await batch.send()
            .then((hash) => {
                console.log(`Operation injected ${hash}`);
                return hash
            })
            .catch((error) => {
                console.log(`Error: ${error} ${JSON.stringify(error, null, 2)}`);
                return undefined
            });
        console.log(`hash=${operationTaquito.opHash}`)

        //get the operation id of the transaction
        const idToCheck = aliceSdk.operationService.getOperation({hash: operationTaquito.opHash})
        console.log(`Operation id : ${idToCheck} \n`)
        const query = {id: idToCheck}

        //Try to delete the operation as an issuer using the sdk
        const deletedOperation = await issuerSdk.operationService.deleteOperation({hash: operationTaquito.opHash}).catch((error) => {
            console.log(`${JSON.stringify(error, null, 2)}`);
            return undefined
        });
        expect(deletedOperation).toEqual(undefined)

        //Try to delete the operation as an issuer using the api request
        const operation = await httpbackend.createRequest<Object>(
            {
                url: `${tmUrl}/api/operation/delete`,
                method: 'POST',
                headers: {"Authorization": `Bearer ${jwtToken}`}
            },
            query).catch((error) => {
            console.log(`Error: ${error} ${JSON.stringify(error, null, 2)}`);
            return undefined
        })
        console.log(operation)

    }, 100000)


    it('ADMIN_OSR Authorization', async () => { // Test to retrieve the status of operations as admin

        //Create a CybSdk
        const adminAuth = new CybAuth(tmUrl, adminToken)
        const adminSdk: CybSdk = new CybSdk(adminAuth, nodeUrl, chain, httpbackend)
        adminSdk.setProvider({signer: new InMemorySigner(pkAlice)});

        const params: ParamsWithKind[] = [{
            kind: OpKind.TRANSACTION,
            to: bobAddress,
            amount: 0.1,
        }]

        //Send the transactions
        const sendOp = await aliceSdk.generateBatch(params).send()

        //Retrieve the transaction
        let insertedOp = await aliceSdk.operationService.getOperation({hash: sendOp.opHash})
        console.log(`insertedOp=${JSON.stringify(insertedOp)}`);

        //Not working with the sdk
        let retrieveOpAdmin = await adminSdk.operationService.getOperation({id: insertedOp.id})
            .catch((error) => {
                console.log(`Error: ${error} ${JSON.stringify(error, null, 2)}`);
                return undefined
            });
        console.log(`insertedOp=${JSON.stringify(retrieveOpAdmin)}`);

    }, 100000)

    it('ISSUER_OSR Authorization', async () => { // Test to retrieve the status of operations as issuer

        //Create a new issuer token
        const token = await createToken(10000, "Issuer Token", ["ISSUER"], adminToken)
        //get the id and jwtToken of the token
        const issuerId = JSON.parse(JSON.stringify(token)).id
        const jwtToken = JSON.parse(JSON.stringify(token)).jwtToken
        console.log(`Id : ${issuerId}\njwtToken : ${jwtToken}`)

        //Insert an operation in the batch as the standard Issuer
        const batch = aliceSdk.wallet.batch()
            .withTransfer({
                to: bobAddress, amount: amount,
                storageLimit: 3000,
                gasLimit: 3000,
                fee: 30000
            })

        //Try to send the operation
        const operationTaquito = await batch.send()
            .then((hash) => {
                console.log(`Operation injected ${hash}`);
                return hash
            })
            .catch((error) => {
                console.log(`Error: ${error} ${JSON.stringify(error, null, 2)}`);
                return undefined
            });
        console.log(`hash=${operationTaquito.opHash}`)

        //Get the id and the hash of the newly created operation
        const op = await aliceSdk.operationService.getOperation({hash: operationTaquito.opHash})
        console.log(`Operation id : ${op.id} \n`)
        const query = {id: op.id}

        //Try to retrieve the operation as standard issuer
        let request = await httpbackend.createRequest<Object>(
            {
                url: `${tmUrl}/api/operation`,
                method: 'GET',
                query: query,
                headers: {"Authorization": `Bearer ${issuerToken}`}
            })
        console.log(`Request : ${JSON.stringify(request)}`)

        //Try to retrieve the operation as the newly generated issuer
        request = await httpbackend.createRequest<Object>(
            {
                url: `${tmUrl}/api/operation`,
                method: 'GET',
                query: query,
                headers: {"Authorization": `Bearer ${jwtToken}`}
            })
            .catch((error) => {
                console.log(`Error: ${error} ${JSON.stringify(error, null, 2)}`);
                return undefined
            });
        expect(request).toEqual(undefined)
        console.log(request)

    }, 100000)

    it('ADMIN_EV Event Channel', async () => { // Test to subscribe to the event channel of an operation as an admin

        //Create an issuer Token
        const token = await createToken(10000, "Token to test", ["ISSUER"], adminToken)
        console.log(`Token : ${JSON.stringify(token)}`)
        const jwtToken = JSON.parse(JSON.stringify(token)).jwtToken

        //Create an issuer Sdk
        const issuerAuth = new CybAuth(tmUrl, jwtToken)
        const issuerSdk: CybSdk = new CybSdk(issuerAuth, nodeUrl, chain, httpbackend)
        issuerSdk.setProvider({signer: new InMemorySigner(pkBob)});

        //Create an admin Sdk
        const adminAuth = new CybAuth(tmUrl, adminToken)
        const adminSdk: CybSdk = new CybSdk(adminAuth, nodeUrl, chain, httpbackend)
        adminSdk.setProvider({signer: new InMemorySigner(pkAlice)});

        //Create an event channel with an admin sdk
        const operationEventSourceAdmin = eventListenerCreation(adminSdk)

        const params: ParamsWithKind[] = [{
            kind: OpKind.TRANSACTION,
            to: bobAddress,
            amount: 0.1,
        }]

        const paramsIssuer: ParamsWithKind[] = [{
            kind: OpKind.TRANSACTION,
            to: mainAddress,
            amount: 0.1,
        }]

        //Send the transactions
        const sendOp = await aliceSdk.generateBatch(params).send()
        const sendOpIssuer = await issuerSdk.generateBatch(paramsIssuer).send()

        //Retrieve the transaction
        let insertedOp = await aliceSdk.operationService.getOperation({hash: sendOp.opHash})
        console.log(`insertedOp=${JSON.stringify(insertedOp)}`);

        //Retrieve the Issuer transaction
        let insertedOpIssuer = await issuerSdk.operationService.getOperation({hash: sendOpIssuer.opHash})
        console.log(`insertedOpIssuer=${JSON.stringify(insertedOpIssuer)}`);

        let ret = onStatusEvent(adminSdk, insertedOp.tzOperation.hash, OperationStatus.BAKED)
        let retIssuer = await adminSdk.operationService.getOperationEventSource().close()

        expect(ret).toEqual(true)

    }, 150000)

    it('ISSUER_EV Event Channel', async () => { // Test to subscribe to the event channel of an operation as an issuer

        //Create a new issuer token
        const token = await createToken(10000, "Issuer Token", ["ISSUER"], adminToken)
        //get the id and jwtToken of the token
        const issuerId = JSON.parse(JSON.stringify(token)).id
        const jwtToken = JSON.parse(JSON.stringify(token)).jwtToken
        console.log(`Id : ${issuerId}\njwtToken : ${jwtToken}`)

        //Create an issuer Sdk
        const issuerAuth = new CybAuth(tmUrl, jwtToken)
        const issuerSdk: CybSdk = new CybSdk(issuerAuth, nodeUrl, chain, httpbackend)
        issuerSdk.setProvider({signer: new InMemorySigner(pkBob)});
        const operationEventSourceIssuer = eventListenerCreation(issuerSdk)
        const operationEventSourceIssuerAlice = eventListenerCreation(aliceSdk)


        //Insert an operation in the batch as the standard Issuer
        const batch = issuerSdk.wallet.batch()
            .withTransfer({
                to: bobAddress, amount: amount,
                storageLimit: 3000,
                gasLimit: 3000,
                fee: 30000
            })

        //Try to send the operation as standard issuer
        const operationTaquito = await batch.send()
            .then((hash) => {
                console.log(`Operation injected ${hash}`);
                return hash
            })
            .catch((error) => {
                console.log(`Error: ${error} ${JSON.stringify(error, null, 2)}`);
                return undefined
            });

        console.log(`hash=${operationTaquito.opHash}`)

        let ret = await onStatusEvent(issuerSdk, operationTaquito.opHash, OperationStatus.BAKED)
        expect(ret).toEqual(true)
        issuerSdk.operationService.getOperationEventSource().close()

    }, 100000)

    it('ADMIN_BI Get the baker identity as an admin', async () => {

        //Create an admin Sdk
        const adminAuth = new CybAuth(tmUrl, adminToken)
        const adminSdk: CybSdk = new CybSdk(adminAuth, nodeUrl, chain, httpbackend)
        adminSdk.setProvider({signer: new InMemorySigner(pkAlice)});

        //Check the baker identity
        const bakerIdentity = await aliceSdk.getBakerIdentity();
        console.log(bakerIdentity);

    }, 100000)

    it('ISSUER_TOK - Try to create, revoke and delete a token as an issuer', async () => { //TODO Add delete test when the functionality is done

        //Create an issuer token
        const token = await createToken(10000, "Token To Create", ["ISSUER"], issuerToken)
        console.log(token)
        expect(token).toEqual(undefined)

        //Create a token with an admin token
        let tokenToRevoke = await createToken(10000, "Token To Create", ["ISSUER"], adminToken)
        const idToRevoke = JSON.parse(JSON.stringify(tokenToRevoke)).id

        // Try to revoke the newly created token with an issuer token
        tokenToRevoke = await httpbackend.createRequest<Object>(
            {
                url: `${tmUrl}/api/tokens/revoke/${idToRevoke}`,
                method: 'POST',
                headers: {"Authorization": `Bearer ${issuerToken}`}
            }
        ).catch((error) => {
            console.log(`Error: ${error} ${JSON.stringify(error, null, 2)}`);
            return undefined
        })
        console.log(tokenToRevoke)
        expect(tokenToRevoke).toEqual(undefined)

    }, 100000)

    it('ADMIN-ISSUER_AUTHORIZATION', async () => { // Test to check the rights of an super ADMIN-ISSUER token

        //Create an issuer Token
        const token = await createToken(10000, "Admin-ISSUER", ["ADMIN", "ISSUER"], adminToken)
        console.log(`Token : ${JSON.stringify(token)}`)
        const jwtToken = JSON.parse(JSON.stringify(token)).jwtToken

        //Create an Admin-Issuer Sdk
        const adminIssuerAuth = new CybAuth(tmUrl, jwtToken)
        const adminIssuerSdk: CybSdk = new CybSdk(adminIssuerAuth, nodeUrl, chain, httpbackend)
        adminIssuerSdk.setProvider({signer: new InMemorySigner(pkBob)});

        //Create an event channel with an admin sdk
        const operationEventSourceAdmin = eventListenerCreation(adminIssuerSdk)

        const params: ParamsWithKind[] = [{
            kind: OpKind.TRANSACTION,
            to: bobAddress,
            amount: 0.1,
        }]

        //Send the transactions
        const sendOp = await aliceSdk.generateBatch(params).send()
        const sendOpAdminIssuer = await adminIssuerSdk.generateBatch(params).send()

        //Retrieve the transaction
        let insertedOp = await aliceSdk.operationService.getOperation({hash: sendOp.opHash})
        console.log(`insertedOp=${JSON.stringify(insertedOp)}`);

        //Retrieve the Issuer transaction
        let insertedOpIssuer = await adminIssuerSdk.operationService.getOperation({hash: sendOpAdminIssuer.opHash})
        console.log(`insertedOpIssuer=${JSON.stringify(insertedOpIssuer)}`);

        //Try to delete the inserted operation with the Admin Issuer Sdk
        const deletedOperation = await adminIssuerSdk.operationService.deleteOperation({hash: sendOpAdminIssuer.opHash}).catch((error) => {
            console.log(`${JSON.stringify(error, null, 2)}`);
            return undefined
        });
        console.log(`${JSON.stringify(deletedOperation)}`)

        const query = {id: insertedOp.id}

        //Create a request to get the operation created with aliceSdk using the adminIssuer token
        const request = await httpbackend.createRequest<Object>(
            {
                url: `${tmUrl}/api/operation`,
                method: 'GET',
                query: query,
                headers: {"Authorization": `Bearer ${jwtToken}`}
            })
        console.log(`Request result : ${JSON.stringify(request)}`)

        let ret = await onStatusEvent(adminIssuerSdk, insertedOp.tzOperation.hash, OperationStatus.BAKED)
        await adminIssuerSdk.operationService.getOperationEventSource().close()

        expect(ret).toEqual(true)

    }, 150000)


})

