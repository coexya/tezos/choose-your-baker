//Launch npm run test

import {OpKind, ParamsWithKind} from "@taquito/taquito"

import {aliceSdk, eventListenerCreation, getSlots, tezosToolkitAlice, waitForSlot,} from './common';

const amount = 0.1;
const tooBigAmount = 1000;

// Nominal test

describe('fee', () => {


    it('test', async () => {

        //Check the baker identity
        const bakerIdentity = await aliceSdk.getBakerIdentity();
        console.log(bakerIdentity);
        const level = await getSlots(bakerIdentity.bakerAddress, 11)
        console.log(`level = ${level}`);


    })


    //jest --forceExit -t 'fee10nbyte FEE_100nbyte_All'
    // TM 
    //fees:
    //  mode: FEES
    //  minimalNtzFees: 0
    //  minimalNtzPerGasUnit: 0
    //  minimalNtzPerByte: 100
    //Baker
    //  minimalNtzFees: 0
    //  minimalNtzPerGasUnit: 0
    //  minimalNtzPerByte: 100

    it('FEE_100nbyte_All', async () => {

        //Event Listener creation
        const operationEventSource = eventListenerCreation(aliceSdk)

        // Contract creation
        //let simpleContractAccount: string = await contractCreation(tezosToolkitAlice, codeContract, '"test"')

        //Check the baker identity
        const bakerIdentity = await aliceSdk.getBakerIdentity();
        console.log(`bakerIdentity=${JSON.stringify(bakerIdentity)}`);
        //expect(bakerIdentity.minimalNtzFees).toEqual(0)
        //expect(bakerIdentity.minimalNtzPerGasUnit).toEqual(0)
        //expect(bakerIdentity.minimalNtzPerByte).toEqual(110)

        //CCreate a simple contract
        const contractNominal = await aliceSdk.wallet.at("KT1MWi7LrduGuoHin6AxryFnBVaVAipP7Cuc")

        //Check the next slot
        const level = await waitForSlot(1000, 30, bakerIdentity.bakerAddress)
        if (level == undefined) {
            fail('No slot available!!!');
        }
        console.log(`The next slot is ${level}`)

        //Add a transfer and a contract call to the batch
        const targetString = "toto"
        const params: ParamsWithKind[] = [
            {
                kind : OpKind.TRANSACTION,
                ...contractNominal.methods.updateName(targetString).toTransferParams({})
            },
        ]

        //Estimate for Taquito
        const estimateTaquito = await tezosToolkitAlice.estimate.batch(params)
        console.log(`estimateTaquito=${JSON.stringify(estimateTaquito)}`)
        console.log(`estimateTaquito total cost=${JSON.stringify(estimateTaquito[0].totalCost)}\n
        minimal fee mutez=${JSON.stringify(estimateTaquito[0].minimalFeeMutez)}\n
        suggested fee mutez=${JSON.stringify(estimateTaquito[0].suggestedFeeMutez)}\n
        using base fee mutez=${JSON.stringify(estimateTaquito[0].usingBaseFeeMutez)}
        `);

        //Estimate for CYB
        const estimateCyb = await aliceSdk.cybEstimate(params)
        console.log(`estimateCyb=${JSON.stringify(estimateCyb)}`);

        //Send the transaction
        //const sendOp = await aliceSdk.generateBatch(params).send()
        const sendOpTz = await tezosToolkitAlice.contract.batch(params).send()
        //await console.log(`insertedOp=${JSON.stringify(sendOpTz.hash)}`);


        //Retrieve the transaction
        //let insertedOp = await aliceSdk.operationService.getOperationByHash(sendOpTz.hash)
        //console.log(`insertedOp=${JSON.stringify(insertedOp)}`);

        //Wait for the baked status
        //let event = await onStatusEvent(aliceSdk, insertedOp.tzOperation.hash, OperationStatus.BAKED)
        //expect(event).toEqual(true)

        //Do some tests here to check the fees.
        //insertedOp = await aliceSdk.operationService.getOperationByHash(sendOp.opHash)
        //expect(insertedOp.totalFee).toBeGreaterThan(bakerIdentity.minimalNtzPerByte * insertedOp.actualConsumption.totalStorageSize / 1000)

        /*const newTargetString = "test12345678"
        const newParams: ParamsWithKind[] = [
            {
                kind: OpKind.TRANSACTION,
                to: bobAddress,
                amount: 0.1,
            },
            {
                kind: OpKind.TRANSACTION,
                ...contractNominal.methods.updateName(newTargetString).toTransferParams({})
            },
        ]

        //Estimate for CYB
        const newEstimateCyb = await aliceSdk.cybEstimate(newParams)
        console.log(`estimateCyb=${JSON.stringify(newEstimateCyb)}`);

        //Send the transaction
        const newSendOp = await aliceSdk.generateBatch(newEstimateCyb).send()

        //Retrieve the transaction
        let newInsertedOp = await aliceSdk.operationService.getOperationByHash(newSendOp.opHash)
        console.log(`insertedOp=${JSON.stringify(newInsertedOp)}`);

        //Wait for the baked status
        event = await onStatusEvent(aliceSdk, newInsertedOp.tzOperation.hash, OperationStatus.BAKED)
        expect(event).toEqual(true)

        //Do some tests here to check the fees.
        newInsertedOp = await aliceSdk.operationService.getOperationByHash(newSendOp.opHash)
        expect(newInsertedOp.totalFee).toBeGreaterThan(bakerIdentity.minimalNtzPerByte * newInsertedOp.actualConsumption.totalStorageSize / 1000)
        expect(newInsertedOp.totalFee).toBeGreaterThan(insertedOp.totalFee)*/
        aliceSdk.operationService.getOperationEventSource().close()

    }, 200000)


})
