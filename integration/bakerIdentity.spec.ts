//Launch npm run test
// Nominal test


import {eventListenerCreation, waitForSlot, aliceSdk,  onStatusEvent, httpbackend, tmUrl, adminToken} from './common';
import {OperationStatus} from "@coexya/cyb-sdk/dist/lib/types/enums";


const amount = 0.1;


// Nominal test
//jest --forceExit -t 'bakerIdentity BI_NOMINAL'
describe('bakerIdentity', () => {

    it('BI_NOMINAL', async () => {

        //Check the baker identity
        const bakerIdentity = await aliceSdk.getBakerIdentity();
        console.log(bakerIdentity);
        expect(bakerIdentity.name).toEqual('The Chosen Baker')
        //expect(bakerIdentity.nbBakingSlotSearch).toEqual(10)
        expect(bakerIdentity.bakerAddress).toEqual('tz1YPSCGWXwBdTncK2aCctSZAXWvGsGwVJqU')
        expect(bakerIdentity.kycSmartContractAddress).toEqual('KT1Q7PPnRrWw3GKGz28GmK6f5BSkwS5wtLPQ')
        expect(bakerIdentity.allowUnauthenticated).toEqual(true)
        //expect(bakerIdentity.feeMode).toEqual('transaction')
        //expect(bakerIdentity.minimalNtzFees).toEqual(100000)
        //expect(bakerIdentity.minimalNtzPerGasUnit).toEqual(100)
        //expect(bakerIdentity.minimalNtzPerByte).toEqual(1000)


    })


    it('BI_CHANGE', async () => {

        //Check the baker identity
        const bakerIdentity = await aliceSdk.getBakerIdentity();
        console.log(bakerIdentity);
        expect(bakerIdentity.name).toEqual('The Chosen Baker')
        //expect(bakerIdentity.nbBakingSlotSearch).toEqual(10)
        expect(bakerIdentity.bakerAddress).toEqual('tz1YPSCGWXwBdTncK2aCctSZAXWvGsGwVJqU')
        expect(bakerIdentity.kycSmartContractAddress).toEqual('KT1Q7PPnRrWw3GKGz28GmK6f5BSkwS5wtLPQ')
        expect(bakerIdentity.allowUnauthenticated).toEqual(true)
        expect(bakerIdentity.feeMode).toEqual('FEES')
        expect(bakerIdentity.minimalNtzFees).toEqual(0)
        expect(bakerIdentity.minimalNtzPerGasUnit).toEqual(0)
        expect(bakerIdentity.minimalNtzPerByte).toEqual(100)


    })


   
})


/*
describe('CybRpcClient', () => {

  const bakerIdentity = await sdk.getBakerIdentity();
  console.log(bakerIdentity);


  const contractNominal = await sdk.wallet.at(contractAddressNominal)
  console.log(Object.getOwnPropertyNames(contractNominal.methods));

  const batch = sdk.wallet.batch()
  .withTransfer({ to: address, amount: amount,
    storageLimit: 3000,
    gasLimit: 3000,
    fee: 30000
   })
  .withContractCall(contractNominal.methods.updateName("bcr3"));
  const batchOp = await batch.send()
  .then((hash) =>  console.log(`Operation injected ${hash}`))
  .catch((error) =>  console.log(`Error: ${error} ${JSON.stringify(error, null, 2)}`));

  waitForValue(1000, contractNominal, "bcr3")

})
*/
