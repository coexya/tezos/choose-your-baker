//Launch npm run test


import {HttpBackend, HttpResponseError, STATUS_CODE} from '@taquito/http-utils';
import {InMemorySigner} from '@taquito/signer';
import {CybSdk, CybAuth} from '@coexya/cyb-sdk';
import {v4 as uuidv4} from 'uuid';
import {OpKind, ParamsWithKind} from "@taquito/taquito"

import {TezosToolkit} from '@taquito/taquito';
import {OperationUpdateEvent} from "@coexya/cyb-sdk/dist/lib/types/api";
import {OperationStatus} from "@coexya/cyb-sdk/dist/lib/types/enums";
import {Estimate} from "@taquito/taquito/dist/types/contract/estimate";

import {
    eventListenerCreation,
    waitForSlot,
    aliceSdk,
    bobAddress,
    getSlots,
    contractCreation,
    codeContract,
    waitForValue,
    tezosToolkitAlice, onStatusEvent, delay,
} from './common';

const amount = 0.1;
const tooBigAmount = 1000;

// Nominal test

describe('fee', () => {


    it('test', async () => {

        //Check the baker identity
        const bakerIdentity = await aliceSdk.getBakerIdentity();
        console.log(bakerIdentity);
        const level = await getSlots(bakerIdentity.bakerAddress, 11)
        console.log(`level = ${level}`);


    })


    //jest --forceExit -t 'fee0baker_stdTM FEE_0Baker_STD_TM'
    // TM 
    //fees:
    //  mode: TRANSACTION
    //  minimalNtzFees: 100000
    //  minimalNtzPerGasUnit: 100
    //  minimalNtzPerByte: 1000
    //Baker
    //  minimalNtzFees: 0
    //  minimalNtzPerGasUnit: 0
    //  minimalNtzPerByte: 0

    it('TRANSACTION_0Baker_STD_TM', async () => {

        //Event Listener creation
        const operationEventSource = eventListenerCreation(aliceSdk)

        // Contract creation
        let simpleContractAccount: string = await contractCreation(tezosToolkitAlice, codeContract, '"test"')

        //Check the baker identity
        const bakerIdentity = await aliceSdk.getBakerIdentity();
        console.log(`bakerIdentity=${JSON.stringify(bakerIdentity)}`);
        expect(bakerIdentity.minimalNtzFees).toEqual(100000)
        expect(bakerIdentity.minimalNtzPerGasUnit).toEqual(100)
        expect(bakerIdentity.minimalNtzPerByte).toEqual(1000)

        //CCreate a simple contract
        const contractNominal = await aliceSdk.wallet.at(simpleContractAccount)

        //Check the next slot
        const level = await waitForSlot(1000, 30, bakerIdentity.bakerAddress)
        if (level == undefined) {
            fail('No slot available!!!');
        }
        console.log(`The next slot is ${level}`)

        //Add a transfer and a contract call to the batch
        const targetString = "tototo"
        const params: ParamsWithKind[] = [
            {
                kind: OpKind.TRANSACTION,
                to: bobAddress,
                amount: 0.1,
            },
            {
                kind: OpKind.TRANSACTION,
                ...contractNominal.methods.updateName(targetString).toTransferParams({})
            },
        ]

        //Estimate for Taquito
        const estimateTaquito = await aliceSdk.estimate.batch(params)
        console.log(`estimateTaquito=${JSON.stringify(estimateTaquito)}`);

        //Estimate for CYB
        const estimateCyb = await aliceSdk.cybEstimate(params)
        console.log(`estimateCyb=${JSON.stringify(estimateCyb)}`);

        //Send the transaction
        const sendOp = await aliceSdk.generateBatch(estimateCyb).send()

        //Retrieve the transaction
        let insertedOp = await aliceSdk.operationService.getOperation({hash : sendOp.opHash})
        console.log(`insertedOp=${JSON.stringify(insertedOp)}`);

        //Wait for the baked status
        const event = await onStatusEvent(aliceSdk, insertedOp.tzOperation.hash, OperationStatus.BAKED)
        expect(event).toEqual(true)

        //Do some tests here to check the fees.
        aliceSdk.operationService.getOperationEventSource().close()

    }, 150000)

    it('TRANSACTION_0Baker_STD_TM_REVEAL', async () => {

        //Event Listener creation
        const operationEventSource = eventListenerCreation(aliceSdk)

        // Contract creation
        let simpleContractAccount: string = await contractCreation(tezosToolkitAlice, codeContract, '"test"')

        //Check the baker identity
        const bakerIdentity = await aliceSdk.getBakerIdentity();
        console.log(`bakerIdentity=${JSON.stringify(bakerIdentity)}`);
        expect(bakerIdentity.minimalNtzFees).toEqual(100000)
        expect(bakerIdentity.minimalNtzPerGasUnit).toEqual(100)
        expect(bakerIdentity.minimalNtzPerByte).toEqual(1000)

        //CCreate a simple contract
        const contractNominal = await aliceSdk.wallet.at(simpleContractAccount)

        //Check the next slot
        const level = await waitForSlot(1000, 30, bakerIdentity.bakerAddress)
        if (level == undefined) {
            fail('No slot available!!!');
        }
        console.log(`The next slot is ${level}`)

        //Add a transfer and a contract call to the batch
        const targetString = "tototo"
        const params: ParamsWithKind[] = [
            {
                kind: OpKind.TRANSACTION,
                to: "tz1Maz4pGAtQpKMdaQpELW22ESqSwDdXetJ2",
                amount: 0.1,
            },
            {
                kind: OpKind.TRANSACTION,
                ...contractNominal.methods.updateName(targetString).toTransferParams({})
            },
        ]

        //Estimate for Taquito
        const estimateTaquito = await aliceSdk.estimate.batch(params)
        console.log(`estimateTaquito=${JSON.stringify(estimateTaquito)}`);

        //Estimate for CYB
        const estimateCyb = await aliceSdk.cybEstimate(params)
        console.log(`estimateCyb=${JSON.stringify(estimateCyb)}`);

        //Send the transaction
        const sendOp = await aliceSdk.generateBatch(estimateCyb).send()

        //Retrieve the transaction
        let insertedOp = await aliceSdk.operationService.getOperation({hash : sendOp.opHash})
        console.log(`insertedOp=${JSON.stringify(insertedOp)}`);

        //Wait for the baked status
        const event = await onStatusEvent(aliceSdk, insertedOp.tzOperation.hash, OperationStatus.BAKED)
        expect(event).toEqual(true)

        //Do some tests here to check the fees.
        aliceSdk.operationService.getOperationEventSource().close()

    }, 150000)


})
