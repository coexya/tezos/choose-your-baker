//Launch npm run test

import {OpKind, ParamsWithKind} from "@taquito/taquito"
import {OperationStatus} from "@coexya/cyb-sdk/dist/lib/types/enums";

import {
    aliceSdk,
    bobAddress,
    codeContract,
    contractCreation,
    eventListenerCreation,
    onStatusEvent,
    tezosToolkitAlice,
    waitForSlot,
} from './common';

const amount = 0.1;
const tooBigAmount = 1000;

// Nominal test

describe('fee0baker_stdTM', () => {

    //jest --forceExit -t 'fee0baker_stdTM FEE_0Baker_STD_TM'
    // TM 
    //fees:
    //  mode: FEES
    //  minimalNtzFees: 100000
    //  minimalNtzPerGasUnit: 100
    //  minimalNtzPerByte: 1000
    //Baker
    //  minimalNtzFees: 0
    //  minimalNtzPerGasUnit: 0
    //  minimalNtzPerByte: 0

    it('FEE_0Baker_STD_TM', async () => {

        //Event Listener creation
        const operationEventSource = eventListenerCreation(aliceSdk)

        // Contract creation
        let simpleContractAccount: string = await contractCreation(tezosToolkitAlice, codeContract, '"test"')

        //Check the baker identity
        const bakerIdentity = await aliceSdk.getBakerIdentity();
        console.log(`bakerIdentity=${JSON.stringify(bakerIdentity)}`);
        expect(bakerIdentity.minimalNtzFees).toEqual(100000)
        expect(bakerIdentity.minimalNtzPerGasUnit).toEqual(100)
        expect(bakerIdentity.minimalNtzPerByte).toEqual(1000)

        //CCreate a simple contract
        const contractNominal = await aliceSdk.wallet.at(simpleContractAccount)

        //Check the next slot
        const level = await waitForSlot(1000, 30, bakerIdentity.bakerAddress)
        if (level == undefined) {
            fail('No slot available!!!');
        }
        console.log(`The next slot is ${level}`)

        //Add a transfer and a contract call to the batch
        const targetString = "tototo"
        const params: ParamsWithKind[] = [
            {
                kind : OpKind.TRANSACTION,
                to: bobAddress,
                amount: 0.1,
            },
            {
                kind: OpKind.TRANSACTION,
                ...contractNominal.methods.updateName(targetString).toTransferParams({})
            },
        ]

        //Estimate for Taquito
        const estimateTaquito = await aliceSdk.estimate.batch(params)
        console.log(`estimateTaquito=${JSON.stringify(estimateTaquito)}`);

        //Estimate for CYB
        const estimateCyb = await aliceSdk.cybEstimate(params)
        console.log(`estimateCyb=${JSON.stringify(estimateCyb)}`);

        //Send the transaction
        const sendOp = await aliceSdk.generateBatch(estimateCyb).send()

        //Retrieve the transaction
        let insertedOp = await aliceSdk.operationService.getOperation({hash : sendOp.opHash})
        console.log(`insertedOp=${JSON.stringify(insertedOp)}`);

        //Wait for the baked status
        const event = await onStatusEvent(aliceSdk, insertedOp.tzOperation.hash, OperationStatus.BAKED)
        expect(event).toEqual(true)

        //Do some tests here to check the fees.
        aliceSdk.operationService.getOperationEventSource().close()

    }, 150000)

    it('FEE_0Baker_STD_TM_EST', async () => {

        // Contract creation
        let simpleContractAccount: string = await contractCreation(tezosToolkitAlice, codeContract, '"test"')

        //Check the baker identity
        const bakerIdentity = await aliceSdk.getBakerIdentity();
        console.log(`bakerIdentity=${JSON.stringify(bakerIdentity)}`);
        expect(bakerIdentity.minimalNtzFees).toEqual(100000)
        expect(bakerIdentity.minimalNtzPerGasUnit).toEqual(100)
        expect(bakerIdentity.minimalNtzPerByte).toEqual(1000)

        //CCreate a simple contract
        const contractNominal = await aliceSdk.wallet.at(simpleContractAccount)

        //Check the next slot
        const level = await waitForSlot(1000, 30, bakerIdentity.bakerAddress)
        if (level == undefined) {
            fail('No slot available!!!');
        }
        console.log(`The next slot is ${level}`)

        //Add a transfer and a contract call to the batch
        const targetString = "tototo"
        const params: ParamsWithKind[] = [
            {
                kind: OpKind.TRANSACTION,
                to: bobAddress,
                amount: 0.1,
                fee: 500
            },
            {
                kind: OpKind.TRANSACTION,
                ...contractNominal.methods.updateName(targetString).toTransferParams({fee: 500})
            },
        ]

        const paramsLowStorageLimit: ParamsWithKind[] = [
            {
                kind: OpKind.TRANSACTION,
                to: bobAddress,
                amount: 0.1,
            },
            {
                kind: OpKind.TRANSACTION,
                ...contractNominal.methods.updateName(targetString).toTransferParams({storageLimit: 0})
            },
        ]

        const paramsHighStorageLimit: ParamsWithKind[] = [
            {
                kind: OpKind.TRANSACTION,
                to: bobAddress,
                amount: 0.1,
            },
            {
                kind: OpKind.TRANSACTION,
                ...contractNominal.methods.updateName(targetString).toTransferParams({storageLimit: 60000})
            },
        ]

        const paramsLowGasLimit: ParamsWithKind[] = [
            {
                kind: OpKind.TRANSACTION,
                to: bobAddress,
                amount: 0.1,
            },
            {
                kind: OpKind.TRANSACTION,
                ...contractNominal.methods.updateName(targetString).toTransferParams({gasLimit: 10})
            },
        ]

        const paramsHighGasLimit: ParamsWithKind[] = [
            {
                kind: OpKind.TRANSACTION,
                to: bobAddress,
                amount: 0.1,
            },
            {
                kind: OpKind.TRANSACTION,
                ...contractNominal.methods.updateName(targetString).toTransferParams({gasLimit: 1000000})
            },
        ]

        //Estimate for CYB
        const estimateCyb = await aliceSdk.cybEstimate(params)
        console.log(`estimateCyb=${JSON.stringify(estimateCyb)}`);


        const estimateCybLowGas = await aliceSdk.cybEstimate(paramsLowGasLimit).catch((error) => {
            console.log(`Error: ${error} ${JSON.stringify(error, null, 2)}`);
            return undefined
        })
        if (estimateCybLowGas != undefined) console.log(`estimateCybLowGas=${JSON.stringify(estimateCybLowGas)}`);


        const estimateCybLowStorage = await aliceSdk.cybEstimate(paramsLowStorageLimit).catch((error) => {
            console.log(`Error: ${error} ${JSON.stringify(error, null, 2)}`);
            return undefined
        })
        if (estimateCybLowStorage != undefined) console.log(`estimateCybLowStorage=${JSON.stringify(estimateCybLowStorage)}`);


        const estimateCybHighGas = await aliceSdk.cybEstimate(paramsHighGasLimit).catch((error) => {
            console.log(`Error: ${error} ${JSON.stringify(error, null, 2)}`);
            return undefined
        })
        if (estimateCybHighGas != undefined) console.log(`estimateCybHighGas=${JSON.stringify(estimateCybHighGas)}`);


        const estimateCybHighStorage = await aliceSdk.cybEstimate(paramsHighStorageLimit).catch((error) => {
            console.log(`Error: ${error} ${JSON.stringify(error, null, 2)}`);
            return undefined
        })
        if (estimateCybHighStorage != undefined) console.log(`estimateCybHighStorage=${JSON.stringify(estimateCybHighStorage)}`);

        /*//Send the transaction
        const sendOp = await aliceSdk.generateBatch(estimateCyb).send()

        //Retrieve the transaction
        let insertedOp = await aliceSdk.operationService.getOperation({hash : sendOp.opHash})
        console.log(`insertedOp=${JSON.stringify(insertedOp)}`);

        //Wait for the baked status
        const event = await onStatusEvent(aliceSdk, insertedOp.tzOperation.hash, OperationStatus.BAKED)
        expect(event).toEqual(true)*/

    }, 150000)

})