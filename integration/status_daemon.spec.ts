//Launch npm run test

import {OpKind, ParamsWithKind} from '@taquito/taquito';
import {OperationStatus} from "@coexya/cyb-sdk/dist/lib/types/enums";

import {
    aliceSdk,
    bobAddress,
    codeContract,
    contractCreation,
    eventListenerCreation,
    onStatusEvent,
    tezosToolkitAlice,
    waitForSlot
} from './common';

const amount = 0.1;
const tooBigAmount = 1000;

//jest --forceExit -t 'fee BAKED_BY_ANOTHER'
// TM
//fees:
//  mode: FEES
//  minimalNtzFees: 0
//  minimalNtzPerGasUnit: 0
//  minimalNtzPerByte: 0
//Baker
//  minimalNtzFees: 100000
//  minimalNtzPerGasUnit: 100
//  minimalNtzPerByte: 100

// Nominal test
describe('status_daemon', () => {

    it('STATUS_DAEMON_WI_BLK', async () => {

        //Event Listener creation
        const operationEventSource = eventListenerCreation(aliceSdk)

        // Contract creation
        let simpleContractAccount: string = await contractCreation(tezosToolkitAlice, codeContract, '"test"')

        //Check the baker identity
        const bakerIdentity = await aliceSdk.getBakerIdentity();
        console.log(`bakerIdentity=${JSON.stringify(bakerIdentity)}`);

        //Create a simple contract
        const contractNominal = await aliceSdk.wallet.at(simpleContractAccount)

        //Check the next slot
        const level = await waitForSlot(1000, 30, bakerIdentity.bakerAddress)
        if (level == undefined) {
            fail('No slot available!!!');
        }
        console.log(`The next slot is ${level}`)

        let nextSlot = 0;
        while (true){
            //Check the current slot
            const currentSlot = await aliceSdk.getBakingSlots({maxPriority : 0})
            //console.log(`bakingSlots=${JSON.stringify(currentSlot)}`);
            const currentLevel=currentSlot[0].level
            //console.log(`currentLevel=${currentLevel}`);

            //Get the next slots
            const levels = Array.from({length: 10}, (_, i) => i + currentLevel + 5)
            const bakingSlots = await aliceSdk.getBakingSlots({maxPriority : 0, levels: levels, delegates: [bakerIdentity.bakerAddress]}).then((bakingSlots) => {
                return bakingSlots
            })
                .catch((error) => {
                    console.log(`Error: ${JSON.parse(error.body).errorCode} ${JSON.parse(error.body).message} \n ${JSON.stringify(error, null, 2)}`);
                    return undefined
                });

            if (bakingSlots != undefined && bakingSlots.length >=2){
                console.log(`bakingSlots=${JSON.stringify(bakingSlots)}`);
                nextSlot=bakingSlots[0].level
                break
            }
        }

        //Add a transfer and a contract call to the batch
        const targetString="test123456"
        const params : ParamsWithKind[] =[
            {
                kind : OpKind.TRANSACTION,
                to: bobAddress,
                amount: 0.1,
                fee: 0,
            },
            {
                kind: OpKind.TRANSACTION,
                ...contractNominal.methods.updateName(targetString).toTransferParams({}),
                fee: 0
            },
        ]

        //Send the transaction
        aliceSdk.modifyContext({level: nextSlot})
        const sendOp = await aliceSdk.generateBatch(params).send()

        //Retrieve the transaction
        const insertedOp = await aliceSdk.operationService.getOperation({hash : sendOp.opHash})
        console.log(`insertedOp=${JSON.stringify(insertedOp)}`);

        const event = await onStatusEvent(aliceSdk,insertedOp.tzOperation.hash,OperationStatus.REJECTED)
        aliceSdk.operationService.getOperationEventSource().close()

    }, 150000)

    it('DAEMON_0_FEES_WI_BLK_STOP', async () => { //Create a transaction with not enough fees

        //Event Listener creation
        const operationEventSource = eventListenerCreation(aliceSdk)

        // Contract creation
        let simpleContractAccount: string = await contractCreation(tezosToolkitAlice, codeContract, '"test"')

        //Check the baker identity
        const bakerIdentity = await aliceSdk.getBakerIdentity();
        console.log(`bakerIdentity=${JSON.stringify(bakerIdentity)}`);

        //Create a simple contract
        const contractNominal = await aliceSdk.wallet.at(simpleContractAccount)

        //Check the next slot
        const level = await waitForSlot(1000, 30, bakerIdentity.bakerAddress)
        if (level == undefined) {
            fail('No slot available!!!');
        }
        console.log(`The next slot is ${level}`)

        let nextSlot = 0;
        while (true){
            //Check the current slot
            const currentSlot = await aliceSdk.getBakingSlots({maxPriority : 0})
            //console.log(`bakingSlots=${JSON.stringify(currentSlot)}`);
            const currentLevel=currentSlot[0].level
            //console.log(`currentLevel=${currentLevel}`);

            //Get the next slots
            const levels = Array.from({length: 10}, (_, i) => i + currentLevel + 5)
            const bakingSlots = await aliceSdk.getBakingSlots({maxPriority : 0, levels: levels, delegates: [bakerIdentity.bakerAddress]}).then((bakingSlots) => {
                return bakingSlots
            })
                .catch((error) => {
                    console.log(`Error: ${JSON.parse(error.body).errorCode} ${JSON.parse(error.body).message} \n ${JSON.stringify(error, null, 2)}`);
                    return undefined
                });

            if (bakingSlots != undefined && bakingSlots.length >=2){
                console.log(`bakingSlots=${JSON.stringify(bakingSlots)}`);
                nextSlot=bakingSlots[1].level
                break
            }
        }

        //Add a transfer and a contract call to the batch
        const targetString="test123456"
        const params : ParamsWithKind[] =[
            {
                kind: OpKind.TRANSACTION,
                to: bobAddress,
                amount: 0.1,
                fee: 0,
            },
            {
                kind: OpKind.TRANSACTION,
                ...contractNominal.methods.updateName(targetString).toTransferParams({}),
                fee: 0
            },
        ]

        //Send the transaction
        aliceSdk.modifyContext({level: nextSlot})
        const sendOp = await aliceSdk.generateBatch(params).send()

        //Retrieve the transaction
        const insertedOp = await aliceSdk.operationService.getOperation({hash : sendOp.opHash})
        console.log(`insertedOp=${JSON.stringify(insertedOp)}`);

        const event = await onStatusEvent(aliceSdk,insertedOp.tzOperation.hash,OperationStatus.BAKED_BY_ANOTHER)
        aliceSdk.operationService.getOperationEventSource().close()

    }, 150000)

    it('DAEMON_0_FEES_NO_BLK_STOP', async () => { //Create a transaction with not enough fees not targeting a block

        //Event Listener creation
        const operationEventSource = eventListenerCreation(aliceSdk)

        // Contract creation
        let simpleContractAccount: string = await contractCreation(tezosToolkitAlice, codeContract, '"test"')

        //Check the baker identity
        const bakerIdentity = await aliceSdk.getBakerIdentity();
        console.log(`bakerIdentity=${JSON.stringify(bakerIdentity)}`);

        //Create a simple contract
        const contractNominal = await aliceSdk.wallet.at(simpleContractAccount)

        //Check the next slot
        const level = await waitForSlot(1000, 30, bakerIdentity.bakerAddress)
        if (level == undefined) {
            fail('No slot available!!!');
        }
        console.log(`The next slot is ${level}`)

        //Add a transfer and a contract call to the batch
        const targetString="test123456"
        const params : ParamsWithKind[] =[
            {
                kind: OpKind.TRANSACTION,
                to: bobAddress,
                amount: 0.1,
                fee: 0,
            },
            {
                kind: OpKind.TRANSACTION,
                ...contractNominal.methods.updateName(targetString).toTransferParams({}),
                fee: 0
            },
        ]

        //Send the transaction
        const sendOp = await aliceSdk.generateBatch(params).send()

        //Retrieve the transaction
        const insertedOp = await aliceSdk.operationService.getOperation({hash : sendOp.opHash})
        console.log(`insertedOp=${JSON.stringify(insertedOp)}`);

        const event = await onStatusEvent(aliceSdk,insertedOp.tzOperation.hash,OperationStatus.IN_PROGRESS)
        const event2 = await onStatusEvent(aliceSdk,insertedOp.tzOperation.hash,OperationStatus.PENDING)
        aliceSdk.operationService.getOperationEventSource().close()

    }, 180000)


    it('DAEMON_0_FEES_WI_BLK_STO_PNG', async () => { //Create a transaction with not enough fees targeting a block and stop the daemon at pending status

        //Event Listener creation
        const operationEventSource = eventListenerCreation(aliceSdk)

        // Contract creation
        let simpleContractAccount: string = await contractCreation(tezosToolkitAlice, codeContract, '"test"')

        //Check the baker identity
        const bakerIdentity = await aliceSdk.getBakerIdentity();
        console.log(`bakerIdentity=${JSON.stringify(bakerIdentity)}`);

        //Create a simple contract
        const contractNominal = await aliceSdk.wallet.at(simpleContractAccount)

        //Check the next slot
        const level = await waitForSlot(1000, 30, bakerIdentity.bakerAddress)
        if (level == undefined) {
            fail('No slot available!!!');
        }
        console.log(`The next slot is ${level}`)

        //Add a transfer and a contract call to the batch
        const targetString="test123456"
        const params : ParamsWithKind[] =[
            {
                kind: OpKind.TRANSACTION,
                to: bobAddress,
                amount: 0.1,
                fee: 0,
            },
            {
                kind: OpKind.TRANSACTION,
                ...contractNominal.methods.updateName(targetString).toTransferParams({}),
                fee: 0
            },
        ]

        let nextSlot = 0;
        while (true){
            //Check the current slot
            const currentSlot = await aliceSdk.getBakingSlots({maxPriority : 0})
            //console.log(`bakingSlots=${JSON.stringify(currentSlot)}`);
            const currentLevel=currentSlot[0].level
            //console.log(`currentLevel=${currentLevel}`);

            //Get the next slots
            const levels = Array.from({length: 10}, (_, i) => i + currentLevel + 5)
            const bakingSlots = await aliceSdk.getBakingSlots({maxPriority : 0, levels: levels, delegates: [bakerIdentity.bakerAddress]}).then((bakingSlots) => {
                return bakingSlots
            })
                .catch((error) => {
                    console.log(`Error: ${JSON.parse(error.body).errorCode} ${JSON.parse(error.body).message} \n ${JSON.stringify(error, null, 2)}`);
                    return undefined
                });

            if (bakingSlots != undefined && bakingSlots.length >=2){
                console.log(`bakingSlots=${JSON.stringify(bakingSlots)}`);
                nextSlot=bakingSlots[0].level
                break
            }
        }

        //Send the transaction
        aliceSdk.modifyContext({level: nextSlot})
        const sendOp = await aliceSdk.generateBatch(params).send()

        //Retrieve the transaction
        const insertedOp = await aliceSdk.operationService.getOperation({hash : sendOp.opHash})
        console.log(`insertedOp=${JSON.stringify(insertedOp)}`);

        const event2 = await onStatusEvent(aliceSdk,insertedOp.tzOperation.hash,OperationStatus.REJECTED)
        aliceSdk.operationService.getOperationEventSource().close()

    }, 180000)


})
