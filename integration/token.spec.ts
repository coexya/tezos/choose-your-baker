//Launch npm run test
// Nominal test
import * as http from 'http';
import {
    adminToken,
    aliceSdk,
    aliceSdkWithoutToken,
    bobAddress,
    chain,
    codeContract,
    contractCreation,
    createToken,
    eventListenerCreation,
    httpbackend,
    nodeUrl,
    pkAlice,
    tezosToolkitAlice,
    tmUrl,
    issuerToken,
    auth,
    getTokens,
    waitForSlot,
    waitForValue, tmHost, tmPort


} from './common';
import {CybAuth, CybSdk} from '@coexya/cyb-sdk';
import {InMemorySigner} from "@taquito/signer";
import {OpKind, ParamsWithKind} from "@taquito/taquito"



const amount = 0.1;
const admin_IssuerToken = "eyJ0eXAiOiJKV1QiLCJhbGciOiJIUzI1NiJ9.eyJvd25lciI6IjYxYTllYjQ2NTdlZTQ4MjJlMjBhZmVlNiIsImNyZWF0aW9uVGltZSI6IjIwMjEtMTItMDNUMTE6MDI6NDYuNzc3MzUzKzAxOjAwIiwiZXhwaXJhdGlvblRpbWUiOiIyMDIxLTEyLTAzVDExOjE5OjI2Ljc3NzM4MyswMTowMCIsInJvbGVzIjpbIkFETUlOIiwiSVNTVUVSIl0sImlzcyI6IlRlem9zQEN5YiIsInBlcnNpc3RlZCI6ZmFsc2V9.YwC8SVj9MSUt7Y9pj1I1AtPdF9zul9KbV3P73ZblF3M"
const new_AdminToBeRevoked = "eyJ0eXAiOiJKV1QiLCJhbGciOiJIUzI1NiJ9.eyJvd25lciI6IjYxYWEyNzY5YzM5YmRkNzAwYmJiMDEzOCIsImNyZWF0aW9uVGltZSI6IjIwMjEtMTItMDNUMTU6MTk6MjEuMzk4NDA2KzAxOjAwIiwiZXhwaXJhdGlvblRpbWUiOiIyMDIxLTEyLTAzVDE4OjA2OjAxLjM5ODQzMiswMTowMCIsInJvbGVzIjpbIkFETUlOIl0sImlzcyI6IlRlem9zQEN5YiIsInBlcnNpc3RlZCI6ZmFsc2V9.JGPNoEhZlHVYbMCm3l8ibs9D6WwvQTwtjsc7xK1Z4Ck"

describe('token', () => {


    // Call the token service with CSV
    it('TOKEN_NOMINAL', async () => {

        const token = await httpbackend.createRequest<Object>(
            {
                url: `${tmUrl}/api/tokens`,
                method: 'GET',
                headers: {"Authorization": `Bearer ${adminToken}`}
            })
        console.log(token)

    }, 100000)

    it('CREATE_TOKEN', async () => {

        const token = await createToken(10000, "New Token", ["ISSUER"], adminToken)
        console.log(token)

    }, 100000)

    it('AUTO_REVOKE_TOKEN', async () => { // just for trial and error purpose

        //Revoke the newly created token
        const tokenToRevoke = await httpbackend.createRequest<Object>(
            {
                url: `${tmUrl}/api/tokens/revoke/61aa2769c39bdd700bbb0139`,
                method: 'POST',
                headers: {"Authorization": `Bearer ${new_AdminToBeRevoked}`}
            }
        )
        console.log(tokenToRevoke)

    }, 100000)


    it('RV_TK - Revoke a token', async () => {

        //Create an admin token
        const token = await createToken(10000, "Token To Revoke", ["ADMIN"], adminToken)
        console.log(token)

        //get the id of the token
        const idToRevoke = JSON.parse(JSON.stringify(token)).id

        //Revoke the newly created token
        const tokenToRevoke = await httpbackend.createRequest<Object>(
            {
                url: `${tmUrl}/api/tokens/revoke/${idToRevoke}`,
                method: 'POST',
                headers: {"Authorization": `Bearer ${adminToken}`}
            }
        )
        console.log(tokenToRevoke)

    }, 100000)

    it('RV_TK&TST_TK - Revoke a token and create an operation with this token', async () => {

        //Create an issuer token
        const token = await createToken(10000, "Token To Revoke", ["ISSUER"], adminToken)
        console.log(token)

        //get the id and jwtToken of the token
        const idToRevoke = JSON.parse(JSON.stringify(token)).id
        const jwtToken = JSON.parse(JSON.stringify(token)).jwtToken
        console.log(`Id : ${idToRevoke}\njwtToken : ${jwtToken}`)

        //Revoke the newly created token
        const tokenToRevoke = await httpbackend.createRequest<Object>(
            {
                url: `${tmUrl}/api/tokens/revoke/${idToRevoke}`,
                method: 'POST',
                headers: {"Authorization": `Bearer ${adminToken}`}
            }
        )
        console.log(tokenToRevoke)

        //Create a new Sdk
        const revokedAuth = new CybAuth(tmUrl, jwtToken)
        const revokedSdk: CybSdk = new CybSdk(revokedAuth, nodeUrl, chain, httpbackend)
        revokedSdk.setProvider({signer: new InMemorySigner(pkAlice)});

        //Insert an operation in the batch
        const batch = revokedSdk.wallet.batch()
            .withTransfer({
                to: bobAddress, amount: amount,
                storageLimit: 3000,
                gasLimit: 3000,
                fee: 30000
            })

        //Try to send the operation
        const operationTaquito = await batch.send()
            .then((hash) => {
                console.log(`Operation injected ${hash}`);
                return hash
            })
            .catch((error) => {
                console.log(`Error: ${error} ${JSON.stringify(error, null, 2)}`);
                return undefined
            });
        //Check if the function is undefined
        console.log(operationTaquito)
        expect(operationTaquito).toEqual(undefined)

    }, 100000)

    //jest --forceExit -t 'token DEL_TK_Delete_a_token'
    //Test the deletion of a token
    it('DEL_TK_Delete_a_token', async () => { 
    
        //Create an admin token
        const token = await createToken(10000, "Token To Delete", ["ADMIN"], adminToken)
        console.log(token)

        const tokens1 = await getTokens(adminToken)
        console.log(tokens1)
       

        //get the id of the token
        const idToDelete = JSON.parse(JSON.stringify(token)).id
        console.log(`idToDelete=${idToDelete}`)

        // Call the deletion
        let request_call = new Promise((resolve, reject) => {console.log("http request");
        const req = http.request({
            'host': tmHost,
            'port': tmPort,
            'path': `/api/tokens/${idToDelete}`,
            'method':'DELETE',
            'headers': {"Authorization": `Bearer ${adminToken}`}
        }, res=>{
            console.log(`statusCode: ${res.statusCode}`)
            if (res.statusCode==200){
                resolve(res.statusCode);
            }
            else {
                reject(res.statusCode);
            }
        })
        req.on('error', (e) => {
            console.error(`problem with request: ${e.message}`);
          });
        req.write('');
        req.end();
    
    })


        // promise resolved or rejected asynchronously
        await request_call.then((response) => {
            console.log(response);
        }).catch((error) => {
            console.log(error);
        });

        const tokens2 = await getTokens(adminToken)
        console.log(tokens2)

        // Check that the token has been deleted
        expect(tokens2.length).toEqual(tokens1.length-1)


    }, 100000)


    it('NO_TOKEN', async () => {

        //Event Listener creation
        const operationEventSource = eventListenerCreation(aliceSdk)

        // Contract creation
        let simpleContractAccount: string = await contractCreation(tezosToolkitAlice, codeContract, '"test"')

        //Check the baker identity
        const bakerIdentity = await aliceSdk.getBakerIdentity();
        console.log(`bakerIdentity=${JSON.stringify(bakerIdentity)}`);
        expect(bakerIdentity.minimalNtzFees).toEqual(0)
        expect(bakerIdentity.minimalNtzPerGasUnit).toEqual(0)
        expect(bakerIdentity.minimalNtzPerByte).toEqual(0)

        //CCreate a simple contract
        const contractNominal = await aliceSdkWithoutToken.wallet.at(simpleContractAccount)

        //Check the next slot
        const level = await waitForSlot(1000, 30, bakerIdentity.bakerAddress)
        if (level == undefined) {
            fail('No slot available!!!');
        }
        console.log(`The next slot is ${level}`)




        //Add a transfer and a contract call to the batch
        const targetString="test123456"
        const params : ParamsWithKind[] =[
            {
                kind : OpKind.TRANSACTION,
                to: bobAddress,
                amount: 0.1,
                fee: 0,
            },
            {
                kind: OpKind.TRANSACTION,
                ...contractNominal.methods.updateName(targetString).toTransferParams({})
            },
        ]

        //Estimate for Taquito
        const estimateTaquito = await aliceSdkWithoutToken.estimate.batch(params)
        console.log(`estimateTaquito=${JSON.stringify(estimateTaquito)}`);

        //Estimate for CYB
        const estimateCyb = await aliceSdkWithoutToken.cybEstimate(params)
        console.log(`estimateCyb=${JSON.stringify(estimateCyb)}`);


        //Send the transaction
        const sendOp = await aliceSdkWithoutToken.generateBatch(estimateCyb).send().then((hash) => {
            console.log(`Operation injected ${hash}`);
            return hash
        })
        .catch((error) => {
            console.log(`Error: ${error} ${JSON.stringify(error, null, 2)}`);
            fail(`Error: ${error} ${JSON.stringify(error, null, 2)}`)
            return undefined
        });

        //Retrieve the transaction
        const insertedOp = await aliceSdkWithoutToken.operationService.getOperation (sendOp.opHash)
        console.log(`insertedOp=${JSON.stringify(insertedOp)}`);

        //Do some tests here to check the fees.

        let ret = await waitForValue(1000, 30, contractNominal, targetString)
        expect(ret).toEqual(true)

        //We do not test the BAKED status. The storage update is enough

    }, 100000)


})

