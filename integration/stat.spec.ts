//Launch npm run test
// Nominal test

import {eventListenerCreation, waitForSlot, aliceSdk, bobAddress, onStatusEvent, httpbackend, tmUrl, adminToken} from './common';
import {OperationStatus} from "@coexya/cyb-sdk/dist/lib/types/enums";
import { OperationUpdateEvent } from '@coexya/cyb-sdk/dist/types/types/api';
import { CybSdk } from '@coexya/cyb-sdk';

const amount = 0.1;



//jest --forceExit -t 'stat STAT_NOMINAL'
describe('stat', () => {


    // Call the stat service with CSV
    it('STAT_NOMINAL', async () => {

        //Event Listener creation
        const operationEventSource = eventListenerCreation(aliceSdk)

        //Check the baker identity
        const bakerIdentity = await aliceSdk.getBakerIdentity();
        console.log(bakerIdentity);
        //Check the next slot
        const level = await waitForSlot(1000, 30, bakerIdentity.bakerAddress)
        if (level == undefined) {
            fail('No slot available!!!');
        }
        console.log(`The slot is ${level}`)
        
        const transfer = aliceSdk.wallet.transfer({
            to: bobAddress, amount: amount,
            storageLimit: 3000,
            gasLimit: 3000,
            fee: 1000
        });

        const operationTaquito = await transfer.send()
        .then((hash) => {
             return hash
        })
        .catch((error) => {
            console.log(`Error: ${error} ${JSON.stringify(error, null, 2)}`);
            return undefined
        });

        console.log(`hash=${operationTaquito.opHash}`)

        const stat = await onStatusEvent(aliceSdk, operationTaquito.opHash, OperationStatus.BAKED).then( ret => {
            const pStat = httpbackend.createRequest<Object>(
            {
                url: `${tmUrl}/api/stats/operations`,
                method: 'GET',
                headers: {"Authorization": `Bearer ${adminToken}` , "Content-Type" : "application/csv"},
                json: false
            })
            return pStat})

        console.log(stat)

        const op = await aliceSdk.operationService.getOperation({hash : operationTaquito.opHash})
        console.log(op.tzOperation.contents)

    }, 100000)

    // Call the stat service with json output
    it('STAT_JSON_OWNER', async () => {

       
        const stat = await httpbackend.createRequest<Object>(
        {
            url: `${tmUrl}/api/stats/operations?owner=61892bd5ea786564cb17ac43`,
            method: 'GET',
            headers: {"Authorization": `Bearer ${adminToken}` , "Content-Type" : "application/json"},
            json: true

        })

        console.log(stat)

    }, 100000)


  // Call the stat service with json output and owner criteria
  it('STAT_JSON_OWNER', async () => {

       
    const stat = await httpbackend.createRequest<Object>(
    {
        url: `${tmUrl}/api/stats/operations?owner=61892bd5ea786564cb17ac43`,
        method: 'GET',
        headers: {"Authorization": `Bearer ${adminToken}` , "Content-Type" : "application/json"},
        json: true

    })

    console.log(stat)

})


 // Call the stat service with json output and level criteria
 it('STAT_JSON_LEVEL', async () => {

       
    const stat = await httpbackend.createRequest<Object>(
    {
        url: `${tmUrl}/api/stats/operations?level=5`,
        method: 'GET',
        headers: {"Authorization": `Bearer ${adminToken}` , "Content-Type" : "application/json"},
        json: true

    })

    console.log(stat)

})


 // Call the stat service with json output and status criteria
 it('STAT_JSON', async () => {

       
    const stat = await httpbackend.createRequest<Object>(
    {
        url: `${tmUrl}/api/stats/operations?level=5`,
        method: 'GET',
        headers: {"Authorization": `Bearer ${adminToken}` , "Content-Type" : "application/json"},
        json: true

    })

    console.log(stat)

})

   
})

