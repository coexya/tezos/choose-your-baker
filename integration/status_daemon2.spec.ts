//Launch npm run test

import {OpKind, ParamsWithKind} from '@taquito/taquito';
import {OperationStatus} from "@coexya/cyb-sdk/dist/lib/types/enums";

import {
    aliceSdk,
    bobAddress,
    codeContract,
    contractCreation,
    eventListenerCreation,
    onStatusEvent,
    tezosToolkitAlice,
    waitForSlot
} from './common';


const amount = 0.1;
const tooBigAmount = 1000;

//jest --forceExit -t 'fee BAKED_BY_ANOTHER'
// TM
//fees:
//  mode: FEES
//  minimalNtzFees: 100000
//  minimalNtzPerGasUnit: 100
//  minimalNtzPerByte: 100
//Baker
//  minimalNtzFees: 100000
//  minimalNtzPerGasUnit: 100
//  minimalNtzPerByte: 100

// Nominal test
describe('status_daemon2', () => {

    it('BAKED_BY_ANOTHER_OFFSET', async () => {

        //Event Listener creation
        const operationEventSource = eventListenerCreation(aliceSdk)

        // Contract creation
        let simpleContractAccount: string = await contractCreation(tezosToolkitAlice, codeContract, '"test"')

        //Check the baker identity
        const bakerIdentity = await aliceSdk.getBakerIdentity();
        console.log(`bakerIdentity=${JSON.stringify(bakerIdentity)}`);

        //Create a simple contract
        const contractNominal = await aliceSdk.wallet.at(simpleContractAccount)

        //Check the next slot
        const level = await waitForSlot(1000, 30, bakerIdentity.bakerAddress)
        if (level == undefined) {
            fail('No slot available!!!');
        }
        console.log(`The next slot is ${level}`)

        let nextSlot = 0;
        while (true){
            //Check the current slot
            const currentSlot = await aliceSdk.getBakingSlots({maxPriority : 0})
            //console.log(`bakingSlots=${JSON.stringify(currentSlot)}`);
            const currentLevel=currentSlot[0].level
            //console.log(`currentLevel=${currentLevel}`);

            //Get the next slots
            const levels = Array.from({length: 10}, (_, i) => i + currentLevel + 5)
            const bakingSlots = await aliceSdk.getBakingSlots({maxPriority : 0, levels: levels, delegates: [bakerIdentity.bakerAddress]}).then((bakingSlots) => {
                return bakingSlots
            })
                .catch((error) => {
                    console.log(`Error: ${JSON.parse(error.body).errorCode} ${JSON.parse(error.body).message} \n ${JSON.stringify(error, null, 2)}`);
                    return undefined
                });

            if (bakingSlots != undefined && bakingSlots.length >=2){
                console.log(`bakingSlots=${JSON.stringify(bakingSlots)}`);
                nextSlot=bakingSlots[1].level
                break
            }
        }

        //Add a transfer and a contract call to the batch
        const targetString="test123456"
        const params : ParamsWithKind[] =[
            {
                kind : OpKind.TRANSACTION,
                to: bobAddress,
                amount: 0.1,
                fee: 0,
            },
            {
                kind: OpKind.TRANSACTION,
                ...contractNominal.methods.updateName(targetString).toTransferParams({}),
                fee: 0
            },
        ]

        //Estimate for CYB
        const estimateCyb = await aliceSdk.cybEstimate(params)
        console.log(`estimateCyb=${JSON.stringify(estimateCyb)}`);

        //Send the transaction
        aliceSdk.modifyContext({level: nextSlot})
        const sendOp = await aliceSdk.generateBatch(estimateCyb).send()

        const sendOpTz = await tezosToolkitAlice.contract.batch(estimateCyb).send()

        //Retrieve the transaction
        const insertedOp = await aliceSdk.operationService.getOperation({hash : sendOp.opHash})
        console.log(`insertedOp=${JSON.stringify(insertedOp)}`);

        const event = await onStatusEvent(aliceSdk,insertedOp.tzOperation.hash,OperationStatus.BAKED_BY_ANOTHER)
        aliceSdk.operationService.getOperationEventSource().close()

    }, 150000)


})
