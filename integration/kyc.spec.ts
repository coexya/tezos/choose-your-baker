//Launch npm run test

import {InMemorySigner} from '@taquito/signer';



import {MichelsonMap, OpKind, ParamsWithKind} from '@taquito/taquito';
import {OperationStatus} from "@coexya/cyb-sdk/dist/lib/types/enums";
import {CybKycAdminSdk} from "@coexya/cyb-sdk/src/services/CybKycAdminSdk"
import {CybSdk, CybAuth} from '@coexya/cyb-sdk';

import {
    aliceSdk, auth,
    bobAddress,
    chain,
    codeContract,
    contractCreation,
    eventListenerCreation, httpbackend, issuerToken, mainAddress,
    nodeUrl,
    onStatusEvent, pkAlice, pkBob,
    tezosToolkitAlice, tmUrl,
    waitForSlot
} from './common';
import {inspect} from "util";

const amount = 0.1;
const tooBigAmount = 1000;
const bakerAddress = "tz1YPSCGWXwBdTncK2aCctSZAXWvGsGwVJqU";
const kycContractAddress = "KT1Ms3jgBf34d2J3yjUkNv8NuaSTY19Y5Gtr";

//jest --forceExit -t 'kyc kyc_creation'


describe('kyc', () => {

    it('kyc_creation', async () => {

        const fs = require('fs');
        const data = fs.readFileSync('../kyc/output/kyc_contract/step_000_cont_0_contract.tz', {encoding:'utf8', flag:'r'});


        console.log(data);

        let kycAddress: string
        await tezosToolkitAlice.contract.originate({
            code: data,
            init: `(Pair {} {"${mainAddress}"})`,
        })
            .then((originationOp) => {
                console.log(`Waiting for confirmation of origination for ${originationOp.contractAddress}...`);
                kycAddress = originationOp.contractAddress
                return originationOp.contract();
            })
            .then((contract) => {
                console.log(`Origination completed.`);

            })
            .catch((error) => console.log(`Error: ${JSON.stringify(error, null, 2)}`));


        const adminKycSdk: CybKycAdminSdk = new CybKycAdminSdk(kycAddress, nodeUrl, chain, httpbackend)
        adminKycSdk.setProvider({
            signer: new InMemorySigner(pkAlice),
        })

        let param =new Map<string, string>([
            ["country" , "Britain :-) !"],
            ["company", "COEXYA1"],
            ["url",  "http://localhost:9090"],
        ])
        await adminKycSdk.addBaker(bakerAddress, param)


        param =new Map<string, string>([
            ["country" , "Britain :-) !"],
            ["company", "COEXYA2"],
            ["url",  "http://localhost:9091"],
        ])
        await adminKycSdk.addBaker(bakerAddress, param)

        const CertifiedBaker = await aliceSdk.getCertifiedBakers(kycAddress)

        console.log(CertifiedBaker)
        

    


    }, 150000)

    
})
