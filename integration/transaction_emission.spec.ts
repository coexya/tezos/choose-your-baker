//Launch npm run test

import {InMemorySigner} from '@taquito/signer';



import {MichelsonMap, OpKind, ParamsWithKind} from '@taquito/taquito';
import {OperationStatus} from "@coexya/cyb-sdk/dist/lib/types/enums";
import {CybKycAdminSdk} from "../sdk/src/services/CybKycAdminSdk"
import {CybSdk, CybAuth} from '@coexya/cyb-sdk';

import {
    aliceSdk, auth,
    bobAddress,
    chain,
    codeContract,
    contractCreation,
    eventListenerCreation, httpbackend, issuerToken, mainAddress,
    nodeUrl,
    onStatusEvent, pkAlice, pkBob,
    tezosToolkitAlice, tmUrl,
    waitForSlot
} from './common';
import {inspect} from "util";

const amount = 0.1;
const tooBigAmount = 1000;
const bakerAddress = "tz1YPSCGWXwBdTncK2aCctSZAXWvGsGwVJqU";
const kycContractAddress = "KT1KpthquYBacscywKfLEeh2qpfFXazyjMSB";

//jest --forceExit -t 'fee BAKED_BY_ANOTHER'
// TM
//fees:
//  mode: FEES
//  minimalNtzFees: 0
//  minimalNtzPerGasUnit: 0
//  minimalNtzPerByte: 0
//Baker
//  minimalNtzFees: 100000
//  minimalNtzPerGasUnit: 100
//  minimalNtzPerByte: 100

// Nominal test
describe('status_daemon', () => {

    it('TES_RTRV_NX_SLT', async () => { //Transaction emission sdk retrieve the next slots of the certified baker


        //Check the baker identity
        const bakerIdentity = await aliceSdk.getBakerIdentity();
        console.log(`bakerIdentity=${JSON.stringify(bakerIdentity)}`);

        let nextSlot = 0;
        while (true) {
            //Check the current slot
            const currentSlot = await aliceSdk.getBakingSlots({maxPriority: 0})
            //console.log(`bakingSlots=${JSON.stringify(currentSlot)}`);
            const currentLevel = currentSlot[0].level
            //console.log(`currentLevel=${currentLevel}`);

            //Get the next slots
            const levels = Array.from({length: 10}, (_, i) => i + currentLevel)
            const bakingSlots = await aliceSdk.getBakingSlots({
                maxPriority: 0,
                levels: levels,
                delegates: [bakerIdentity.bakerAddress]
            }).then((bakingSlots) => {
                return bakingSlots
            })
                .catch((error) => {
                    console.log(`Error: ${JSON.parse(error.body).errorCode} ${JSON.parse(error.body).message} \n ${JSON.stringify(error, null, 2)}`);
                    return undefined
                });

            if (bakingSlots != undefined && bakingSlots.length >= 2) {
                console.log(`bakingSlots=${JSON.stringify(bakingSlots)}`);
                nextSlot = bakingSlots[0].level
                break
            }
        }

    }, 150000)

    it('TREM_MANAGER', async () => {

        //Event Listener creation
        //const operationEventSource = eventListenerCreation(aliceSdk)

        //Check the baker identity
        const bakerIdentity = await aliceSdk.getBakerIdentity();
        console.log(`bakerIdentity=${JSON.stringify(bakerIdentity)}`);
        expect(bakerIdentity.minimalNtzFees).toEqual(100000)
        expect(bakerIdentity.minimalNtzPerGasUnit).toEqual(100)
        expect(bakerIdentity.minimalNtzPerByte).toEqual(1000)

        /*const adminKycSdk: CybKycAdminSdk = new CybKycAdminSdk("KT1GCTaV3sSbPVp7rmM16rKFDk3kg84WznZM", nodeUrl, chain)
        adminKycSdk.setProvider({
            signer: new InMemorySigner(pkAlice),
        })*/

        const adminBobKycSdk: CybKycAdminSdk = new CybKycAdminSdk(kycContractAddress, nodeUrl, chain)
        adminBobKycSdk.setProvider({
            signer: new InMemorySigner(pkBob),
        })
        const contractNominal = await aliceSdk.wallet.at(kycContractAddress)
        const params : ParamsWithKind[] =[
            {
                kind : OpKind.TRANSACTION,
                ...contractNominal.methods.add_manager(bobAddress).toTransferParams()
            }]

        const operationTaquito = await aliceSdk.generateBatch(params).send()
            .then((hash) => {
                console.log(`Operation injected ${hash}`);
                return hash
            })
            .catch((error) => {
                console.log(`Error: ${error} ${JSON.stringify(error, null, 2)}`);
                return undefined
            });

        console.log(`hash=${operationTaquito.opHash}`)
        let ret = await onStatusEvent(aliceSdk, operationTaquito.opHash, OperationStatus.BAKED)
        await aliceSdk.operationService.getOperationEventSource().close()
        //await adminBobKycSdk.removeBaker(bakerAddress)
        //await adminKycSdk.removeBaker("tz1YPSCGWXwBdTncK2aCctSZAXWvGsGwVJqU")
        /*let param ={
            address: "tz1YPSCGWXwBdTncK2aCctSZAXWvGsGwVJqU",
            country: "France",
            company: "Coexya",
            url: "https://coexya.eu",
        }
        // Add a new manager to the contract
        await adminBobKycSdk.addBaker(param)*/

        // Remove a manager from the contract
        //await adminKycSdk.removeManager("tz1VSUr8wwNhLAzempoch5d6hLRiTh8Cjcjb")

        // Remove baker from the contract
        //await adminKycSdk.removeBaker("tz1YPSCGWXwBdTncK2aCctSZAXWvGsGwVJqU")


        //Check the next slot
        /*const level = await waitForSlot(1000, 30, bakerIdentity.bakerAddress)
        if (level == undefined) {
            fail('No slot available!!!');
        }
        console.log(`The next slot is ${level}`)

        //Add a transfer and a contract call to the batch
        const targetString = "tototo"
        const params: ParamsWithKind[] = [
            {
                kind : OpKind.TRANSACTION,
                to: bobAddress,
                amount: 0.1,
            },
        ]

        //Estimate for Taquito
        const estimateTaquito = await aliceSdk.estimate.batch(params)
        console.log(`estimateTaquito=${JSON.stringify(estimateTaquito)}`);

        //Estimate for CYB
        const estimateCyb = await aliceSdk.cybEstimate(params)
        console.log(`estimateCyb=${JSON.stringify(estimateCyb)}`);

        //Send the transaction
        const sendOp = await aliceSdk.generateBatch(estimateCyb).send()

        //Retrieve the transaction
        let insertedOp = await aliceSdk.operationService.getOperation({hash : sendOp.opHash})
        console.log(`insertedOp=${JSON.stringify(insertedOp)}`);

        //Wait for the baked status
        const event = await onStatusEvent(aliceSdk, insertedOp.tzOperation.hash, OperationStatus.BAKED)
        expect(event).toEqual(true)

        //Do some tests here to check the fees.
        aliceSdk.operationService.getOperationEventSource().close()*/

    }, 150000)

    it('TREM_ADD_2CERTBAKER', async () => {

        //Event Listener creation
        //const operationEventSource = eventListenerCreation(aliceSdk)

        //Check the baker identity
        const bakerIdentity = await aliceSdk.getBakerIdentity();
        console.log(`bakerIdentity=${JSON.stringify(bakerIdentity)}`);
        expect(bakerIdentity.minimalNtzFees).toEqual(100000)
        expect(bakerIdentity.minimalNtzPerGasUnit).toEqual(100)
        expect(bakerIdentity.minimalNtzPerByte).toEqual(1000)

        const adminBobKycSdk: CybKycAdminSdk = new CybKycAdminSdk(kycContractAddress, nodeUrl, chain)
        adminBobKycSdk.setProvider({
            signer: new InMemorySigner(pkBob),
        })
        const contractNominal = await aliceSdk.wallet.at(kycContractAddress)

        let param ={
            address: "tz1aSkwEot3L2kmUvcoxzjMomb9mvBNuzFK6",
            country: "Tutuland",
            company: "TuTu inc.",
            url: "https://coexya.eu",
        }
        const map: MichelsonMap<string, string> = new MichelsonMap()
        map.set("country", param.country)
        map.set("company", param.company)
        map.set("url", param.url)
        // Add a new manager to the contract
        await adminBobKycSdk.addBaker(param)

        const params : ParamsWithKind[] =[
            {
                kind : OpKind.TRANSACTION,
                ...contractNominal.methods.add_baker(param.address,map).toTransferParams()
            }]

        const operationTaquito = await aliceSdk.generateBatch(params).send()
            .then((hash) => {
                console.log(`Operation injected ${hash}`);
                return hash
            })
            .catch((error) => {
                console.log(`Error: ${error} ${JSON.stringify(error, null, 2)}`);
                return undefined
            });

        console.log(`hash=${operationTaquito.opHash}`)
        let ret = await onStatusEvent(aliceSdk, operationTaquito.opHash, OperationStatus.BAKED)
        await aliceSdk.operationService.getOperationEventSource().close()

    }, 150000)

    it('TREM_CERTBAKER', async () => {

        //Check the baker identity
        const bakerIdentity = await aliceSdk.getBakerIdentity();
        console.log(`bakerIdentity=${JSON.stringify(bakerIdentity)}`);
        expect(bakerIdentity.minimalNtzFees).toEqual(100000)
        expect(bakerIdentity.minimalNtzPerGasUnit).toEqual(100)
        expect(bakerIdentity.minimalNtzPerByte).toEqual(1000)

        const certifiedBakerIdentity = await aliceSdk.getCertifiedBakers();

        certifiedBakerIdentity.forEach((property : any) =>
            console.log(`certifiedBakerInfos= ${property.address} ${JSON.stringify(Array.from(property.baker_infos.entries()))}`) )

    }, 150000)

    it('2Cert_TREM_CERTBAKER', async () => {

        const totoUrl = "http://localhost:9092"
        const totoAuth = new CybAuth(totoUrl, issuerToken)
        const totoSdk: CybSdk = new CybSdk(totoAuth, nodeUrl, chain, httpbackend)

        totoSdk.setProvider({
            signer: new InMemorySigner(pkAlice)
        })


        const bakerIdentity = await totoSdk.getBakerIdentity();
        console.log(`bakerIdentity=${JSON.stringify(bakerIdentity)}`);
        expect(bakerIdentity.minimalNtzFees).toEqual(100000)
        expect(bakerIdentity.minimalNtzPerGasUnit).toEqual(100)
        expect(bakerIdentity.minimalNtzPerByte).toEqual(1000)

        const params : ParamsWithKind[] =[
            {
                kind : OpKind.TRANSACTION,
                to: bobAddress,
                amount: amount,
            }]

        let nextSlot = 0;
        while (true) {
            //Check the current slot
            const currentSlot = await totoSdk.getBakingSlots({maxPriority: 0})
            //console.log(`bakingSlots=${JSON.stringify(currentSlot)}`);
            const currentLevel = currentSlot[0].level
            //console.log(`currentLevel=${currentLevel}`);

            //Get the next slots
            const levels = Array.from({length: 10}, (_, i) => i + currentLevel)
            const bakingSlots = await aliceSdk.getBakingSlots({
                maxPriority: 0,
                levels: levels,
                delegates: [bakerIdentity.bakerAddress]
            }).then((bakingSlots) => {
                return bakingSlots
            })
                .catch((error) => {
                    console.log(`Error: ${JSON.parse(error.body).errorCode} ${JSON.parse(error.body).message} \n ${JSON.stringify(error, null, 2)}`);
                    return undefined
                });

            if (bakingSlots != undefined && bakingSlots.length >= 2) {
                console.log(`bakingSlots=${JSON.stringify(bakingSlots)}`);
                nextSlot = bakingSlots[0].level
                break
            }
        }

        const response = await totoSdk.generateBatch(params).send()
        const operation = await totoSdk.operationService.getOperation({hash: response.opHash})
        console.log(inspect(operation))
        console.log(`hash=${operation.tzOperation.hash}`)
        let ret = await onStatusEvent(totoSdk, operation.tzOperation.hash, OperationStatus.BAKED)

        //Check the baker identity

        /*const certifiedBakerIdentity = await aliceSdk.getCertifiedBakers();
        console.log(`certifiedBakerIdentity=${JSON.stringify(certifiedBakerIdentity)}`);
        console.log(`certifiedBakerInfos=${JSON.stringify(certifiedBakerIdentity[0].baker_infos)}`)
        console.log(`certifiedBakerInfos=${JSON.stringify(certifiedBakerIdentity[1].baker_infos)}`)*/




    }, 150000)

})
