import smartpy as sp


class KycContract(sp.Contract):

    def __init__(self, originator):
        self.init(
            managers = sp.set([originator]),
            bakers = sp.map(
                l = {},
                tkey = sp.TAddress,
                tvalue = sp.TMap(sp.TString, sp.TString)
            )
        )

    def check_manager(self):
        sp.verify(self.data.managers.contains(sp.sender), "Not a manager")

    def verify_field(self, baker_infos, field):
        sp.verify(baker_infos.contains(field), "Field '{0}' undefined in baker infos".format(field))

    def check_baker_infos(self, baker_infos):
        self.verify_field(baker_infos, "company")
        self.verify_field(baker_infos, "country")
        self.verify_field(baker_infos, "url")

    @sp.entry_point
    def add_manager(self, manager):
        self.check_manager()
        self.data.managers.add(manager)

    @sp.entry_point
    def remove_manager(self, manager):
        self.check_manager()
        self.data.managers.remove(manager)

    @sp.entry_point
    def add_baker(self, baker_address, baker_infos):
        self.check_manager()
        self.check_baker_infos(baker_infos)
        self.data.bakers[baker_address] = baker_infos


    @sp.entry_point
    def remove_baker(self, baker_address):
        self.check_manager()
        sp.verify(self.data.bakers.contains(baker_address), "Baker does not exist")
        del self.data.bakers[baker_address]

@sp.add_test(name = "add_manager")
def add_manager_test():
    originator = sp.test_account("Originator")
    alice = sp.test_account("Alice")
    bob = sp.test_account("Bob")

    sc = sp.test_scenario()
    contract = KycContract(originator.address)
    sc += contract

    sc.h1("Add alice as manager with non manager")
    contract.add_manager(alice.address).run(
        sender = bob.address,
        valid = False,
        exception = "Not a manager"
    )
    sc.p("managers").show(contract.data.managers)
    sc.verify(~contract.data.managers.contains(alice.address))

    sc.h1("Add alice and bob as managers")
    contract.add_manager(alice.address).run(
        sender = originator.address
    )
    contract.add_manager(bob.address).run(
        sender = originator.address
    )
    sc.p("managers").show(contract.data.managers)
    sc.verify(contract.data.managers.contains(alice.address))
    sc.verify(contract.data.managers.contains(bob.address))

@sp.add_test("remove_manager")
def remove_manager_test():
    originator = sp.test_account("Originator")
    alice = sp.test_account("Alice")
    bob = sp.test_account("Bob")

    sc = sp.test_scenario()
    contract = KycContract(originator.address)
    sc += contract

    sc.h1("Add alice and bob as managers")
    contract.add_manager(alice.address).run(
        sender = originator.address
    )
    contract.add_manager(bob.address).run(
        sender = originator.address
    )
    sc.p("managers").show(contract.data.managers)
    sc.verify(contract.data.managers.contains(alice.address))
    sc.verify(contract.data.managers.contains(bob.address))

    sc.h2("Remove alice from manager")
    contract.remove_manager(alice.address).run(
        sender = bob.address
    )
    sc.p("managers").show(contract.data.managers)
    sc.verify(~contract.data.managers.contains(alice.address))

    sc.h2("Remove bob from manager with non manager")
    contract.remove_manager(alice.address).run(
        sender = alice.address,
        valid = False,
        exception = "Not a manager"
    )
    sc.p("managers").show(contract.data.managers)
    sc.verify(contract.data.managers.contains(bob.address))

@sp.add_test(name = "add_baker")
def add_baker_test():
    originator = sp.test_account("Originator")
    alice = sp.test_account("Alice")
    alice_infos = sp.map({
        "company": "SWORD",
        "country": "FRANCE",
        "url": "yourbaker@sword-group.com"
    })
    alice_updated_infos = sp.map({
        "company": "COEXYA",
        "country": "FRANCE",
        "url": "yourbaker@coexya.eu"
    })
    bob = sp.test_account("Bob")
    bob_infos = sp.map({
        "company": "Blockchain company",
        "country": "USA",
        "url": "yourbaker@blockchain-company.com"
    })

    sc = sp.test_scenario()
    contract = KycContract(originator.address)
    sc += contract

    sc.h1("Add alice as baker with undefined company")
    contract.add_baker(
        sp.record(
            baker_address = alice.address,
            baker_infos = sp.map({
                "country": "FRANCE",
                "url": "yourbaker@sword-group.com"
            })
        )
    ).run(
        sender = originator.address,
        valid = False,
        exception = "Field 'company' undefined in baker infos"
    )
    sc.p("bakers").show(contract.data.bakers)
    sc.verify(~contract.data.bakers.contains(alice.address))

    sc.h1("Add alice as baker with undefined country")
    contract.add_baker(
        sp.record(
            baker_address = alice.address,
            baker_infos = sp.map({
                "company": "SWORD",
                "url": "yourbaker@sword-group.com"
            })
        )
    ).run(
        sender = originator.address,
        valid = False,
        exception = "Field 'country' undefined in baker infos"
    )
    sc.p("bakers").show(contract.data.bakers)
    sc.verify(~contract.data.bakers.contains(alice.address))

    sc.h1("Add alice as baker with undefined url")
    contract.add_baker(
        sp.record(
            baker_address = alice.address,
            baker_infos = sp.map({
                "company": "SWORD",
                "country": "FRANCE"
            })
        )
    ).run(
        sender = originator.address,
        valid = False,
        exception = "Field 'url' undefined in baker infos"
    )
    sc.p("bakers").show(contract.data.bakers)
    sc.verify(~contract.data.bakers.contains(alice.address))

    sc.h1("Add alice as baker")
    contract.add_baker(
        sp.record(
            baker_address = alice.address,
            baker_infos = alice_infos
        )
    ).run(
        sender = originator.address
    )
    sc.p("bakers").show(contract.data.bakers)
    sc.verify(contract.data.bakers.contains(alice.address))
    sc.verify(contract.data.bakers[alice.address]["company"] == alice_infos["company"])
    sc.verify(contract.data.bakers[alice.address]["country"] == alice_infos["country"])
    sc.verify(contract.data.bakers[alice.address]["url"] == alice_infos["url"])

    sc.h1("Add bob as baker with non manager")
    contract.add_baker(
        sp.record(
            baker_address = bob.address,
            baker_infos = bob_infos
        )
    ).run(
        sender = alice.address,
        valid = False,
        exception = "Not a manager"
    )
    sc.p("bakers").show(contract.data.bakers)
    sc.verify(~contract.data.bakers.contains(bob.address))

    sc.h1("Update alice as baker")
    contract.add_baker(
        sp.record(
            baker_address = alice.address,
            baker_infos = alice_updated_infos
        )
    ).run(
        sender = originator.address
    )
    sc.p("bakers").show(contract.data.bakers)
    sc.verify(contract.data.bakers.contains(alice.address))
    sc.verify(contract.data.bakers[alice.address]["company"] == alice_updated_infos["company"])
    sc.verify(contract.data.bakers[alice.address]["country"] == alice_updated_infos["country"])
    sc.verify(contract.data.bakers[alice.address]["url"] == alice_updated_infos["url"])

@sp.add_test(name = "remove_baker")
def remove_baker_test():
    originator = sp.test_account("Originator")
    alice = sp.test_account("Alice")
    alice_infos = sp.map({
        "company": "SWORD",
        "country": "FRANCE",
        "url": "yourbaker@sword-group.com"
    })
    bob = sp.test_account("Bob")
    bob_infos = sp.map({
        "company": "Blockchain company",
        "country": "USA",
        "url": "yourbaker@blockchain-company.com"
    })

    sc = sp.test_scenario()
    contract = KycContract(originator.address)
    sc += contract

    sc.h1("Add alice and bob as bakers")
    contract.add_baker(
        sp.record(
            baker_address = alice.address,
            baker_infos = alice_infos
        )
    ).run(
        sender = originator.address
    )
    contract.add_baker(
        sp.record(
            baker_address = bob.address,
            baker_infos = bob_infos
        )
    ).run(
        sender = originator.address
    )
    sc.p("bakers").show(contract.data.bakers)
    sc.verify(contract.data.bakers.contains(alice.address))
    sc.verify(contract.data.bakers.contains(bob.address))

    sc.h1("Remove alice from baker")
    contract.remove_baker(alice.address).run(
        sender = originator.address
    )
    sc.p("bakers").show(contract.data.bakers)
    sc.verify(~contract.data.bakers.contains(alice.address))

    sc.h1("Remove bob from baker with non manager")
    contract.remove_baker(bob.address).run(
        sender = alice.address,
        valid = False,
        exception = "Not a manager"
    )
    sc.p("bakers").show(contract.data.bakers)
    sc.verify(contract.data.bakers.contains(bob.address))

sp.add_compilation_target("kyc_contract", KycContract(sp.address("tz1bDCu64RmcpWahdn9bWrDMi6cu7mXZynHm")))
