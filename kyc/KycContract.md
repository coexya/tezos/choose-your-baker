# Kyc Contract

## Prerequisite: SmartPy

* Install SmartPy using the installation script ([SmartPy](https://smartpy.io/docs/cli/)):
```shell
sh <(curl -s https://smartpy.io/cli/install.sh)
```

* Check the installed SmartPy version
```shell
~/smartpy-cli/SmartPy.sh --version
```

## How to compile

* To compile the smart contract michelson:
```shell
~/smartpy-cli/SmartPy.sh compile kyc_contract.py output
```

* Retrieve the Michelson contrat code from **build/kyc_contract/step_000_cont_0_contract.tz**.

* Retrieve the initial storage code from **build/kyc_contract/step_000_cont_0_storage.tz**. Please note that the initial manager of the contract is hardcoded in the storage. The value of the initial manager should be changed accordingly.

## How to run tests

* To run tests:
```shell
~/smartpy-cli/SmartPy.sh test kyc_contract.py output
```

## On-chain testing

* Originate the contract:
```shell
make originate
```

* Add **bob** as manager:
```shell
../tezos-sandbox-network/tezos/tezos-client -E http://localhost:20000 transfer 0 from sword to kyc_contract --entrypoint 'add_manager' --arg '"tz1aSkwEot3L2kmUvcoxzjMomb9mvBNuzFK6"' --burn-cap 0.00675
```

* Add **alice** as baker:
```shell
../tezos-sandbox-network/tezos/tezos-client -E http://localhost:20000 transfer 0 from sword to kyc_contract --entrypoint 'add_baker' --arg '(Pair "tz1VSUr8wwNhLAzempoch5d6hLRiTh8Cjcjb" {Elt "company" "Alice"; Elt "country" "FRANCE"; Elt "url" "alice@bakery.com"})' --burn-cap 0.0295
```
