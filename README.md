[Index](/README.md) | [Specification](/documentation/specification.md) | [Sandbox/demo](/demo/README.md)  | [Deployment](/documentation/deployment.md) | [Typescript SDK Quick start](/documentation/Quickstart.md) | [Examples](/documentation/Examples.md) | [Tezos Sandbox Mempool](/tezos-sandbox-mempool/README.md)

# Resources

## Introduction

The “Choose Your Baker”(CYB) initiative aims to propose to the Tezos ecosystem a communication protocol enabling issuers of Tezos transactions to select the baker they want to send their transactions to. This topic is indeed particularly important for the compliance department of financial institutions working in the security tokens space.
 
## Benefits of Choose Your Baker

In most countries, including in Europe, the regulation now authorizes the tokenisation of non-listed financial instruments, called “Securities Token”.
  
Digitalised financial instruments bring a lot of advantages compared to the more traditional paper-based OTC process. More specifically, it enables instant transactions between parties.
  
Securities Token are financial instruments, and as such fall under different national and international regulations, including anti money laundering, countering the financing of terrorism, and trade and financial embargos.
  
The French authorities as well as the European Commission require that these securities tokens be managed on a distributed ledger. The Tezos Blockchain currently represents the best technical solution for managing securities token by corporate entities, including from a RSE perspective.
   
One of its comparative advantage is its consensus: the delegated Proof of Stake. The compliance of the transactions with the rules set up by the Tezos blockchain is validated by “baker” nodes, which are participants that are proposing to run different controls on their IT infrastructure against a remuneration (fees on transactions + rewards granted by the blockchain).
    
To abide by the law, we believe that transactions on Security Tokens should not be validated by bakers that do not comply with the regulations on financial instruments. More explicitly, financial institutions may want to make sure they did not pay fees to bakers that could be associated to fund of terrorist organisations or money laundering activity. The risk of working with non-compliant bakers is bear by the issuer of the transaction. Therefore, many compliance officers believe that the development to the Security Tokens market requires from the issuer of transactions to be able to select the bakers they want to work with from a predefined whitelist.
    
Note: the same problem and the same solution may apply to other markets than security tokens where regulation on data location exists (ex: health data, other type of crypto assets, etc).  


## Choose Your Baker : an open source project  

This project has been developed by a CORE TEAM of skilled collaborators of Coexya ([www.coexya.eu/](https://www.coexya.eu/)) and Blockchain EZ ([www.Blockchain-ez.com](http://www.Blockchain-ez.com)) companies, with the support of Nomadic Labs ([www.nomadic-labs.com](http://www.nomadic-labs.com)) and the Tezos Foundation ([www.tezos.foundation](http://www.tezos.foundation))  
  
None of these companies are responsible for the usage made by third parties of the Choose Your Baker project. Coexya, Blockchain EZ, Nomadic Labs and Tezos Foundation are brands that cannot be used without the written approval of the said organizations.  
  
If you want to be part of this CORE TEAM, please contact us (CYB@coexya.eu).  
  
Choose Your Baker is an Open Source project complying with the definition given by the Open Source Initiative, as in its 1.9 version ([https://opensource.org/docs/osd](https://opensource.org/docs/osd)). 
  
However, any organization using Choose Your Baker must:
* Declare its usage to CYB@coexya.eu  
* Accept its name to be communicated as a user of the solution for communication purposes by the core development team
  * If  this is legally complicated for  your organization, details can be discussed : CYB@coexya.eu  
* Include in your own software solution / website using Choose Your Baker and its related documentation a reference to this gitlab page 
  
If you like it, use it and modify it, we would be thankful if you could provide to the community any improvement you make to the solution.  
  
If you need help for implementing the solution, or possibly customizing it, please contact the CORE TEAM (CYB@coexya.eu).


## Choose Your Baker : Getting started

As a baker, if you want to deploy Choose Your Baker on your baking node:[Deployment](/documentation/deployment.md)

As a baker, if you want to generate a token for issuers to reach you: [Token Management](/token-management/README.md)

As an issuer, if you want to user Choose Your Baker : [Typescript SDK Quick start](/documentation/Quickstart.md)

As an issuer, if you want to test the typescript SDK : [Examples](/documentation/Examples.md)

As a baker / issuer, if you want to test Choose Your Baker : [Sandbox/demo](/demo/README.md)

As a baker / issuer, if you want to modify the sandbox network : [Tezos Sandbox Mempool](/tezos-sandbox-mempool/README.md)

