import {OpKind} from "@taquito/rpc"
import {OperationContents} from "@taquito/rpc/dist/types/types"
import {CybOperationObject, isSupportedOp} from "../../src/types/operation"
import {OperationCreate} from "../../src/types/api"

const branch: string = "testBranch"
const contents: OperationContents[] = [{
    kind: OpKind.ENDORSEMENT,
    level: 1,
}]
const protocol: string = "testProtocol"
const signature: string = "testSignature"
const hash = "testHash"

describe('CybOperationObject', () => {
    const operation: CybOperationObject = {
        branch,
        contents,
        protocol,
        signature,
        hash,
    }
    it('Validates creation', () => {
        expect(operation.branch).toEqual(branch)
        expect(operation.contents).toEqual(contents)
        expect(operation.protocol).toEqual(protocol)
        expect(operation.signature).toEqual(signature)
        expect(operation.hash).toEqual(hash)
    })
})


describe('CybInsertOperation', () => {
    const level = 1024

    const insertOperation: OperationCreate = {
        tzOperation: {
            branch,
            contents,
            protocol,
            signature,
            hash,
        },
        level,
    }
    it('Validates creation', () => {
        console.log(JSON.stringify(insertOperation.tzOperation))
        expect(insertOperation.tzOperation?.branch).toEqual(branch)
        expect(insertOperation.tzOperation?.contents).toEqual(contents)
        expect(insertOperation.tzOperation?.protocol).toEqual(protocol)
        expect(insertOperation.tzOperation?.signature).toEqual(signature)
        expect(insertOperation.tzOperation?.hash).toEqual(hash)
        expect(insertOperation.level).toEqual(1024)
    })
})

describe("isSupportedOp", () => {

    it("Check result", () => {
        // OperationContentsEndorsement
        expect(isSupportedOp({
            kind: OpKind.ENDORSEMENT,
            level: 100,
        })).toEqual(false)

        // OperationContentsRevelation
        expect(isSupportedOp({
            kind: OpKind.SEED_NONCE_REVELATION,
            level: 100,
            nonce: "qzd",
        })).toEqual(false)

        // OperationContentsDoubleEndorsement
        expect(isSupportedOp({
            kind: OpKind.DOUBLE_ENDORSEMENT_EVIDENCE,
            op1: {} as any,
            op2: {} as any,
            slot: 100,
        })).toEqual(false)

        // OperationContentsDoubleBaking
        expect(isSupportedOp({
            kind: OpKind.DOUBLE_BAKING_EVIDENCE,
            bh1: {} as any,
            bh2: {} as any,
        })).toEqual(false)

        // OperationContentsActivateAccount
        expect(isSupportedOp({
            kind: OpKind.ACTIVATION,
            pkh: "aaa",
            secret: "aaa",
        })).toEqual(false)

        // OperationContentsProposals
        expect(isSupportedOp({
            kind: OpKind.PROPOSALS,
            source: "aaa",
            period: 12,
            proposals: ["aa"],
        })).toEqual(false)

        // OperationContentsBallot
        expect(isSupportedOp({
            kind: OpKind.BALLOT,
            source: "aaa",
            period: 12,
            proposal: "aaa",
            ballot: "nay",
        })).toEqual(false)

        // OperationContentsReveal
        expect(isSupportedOp({
            kind: OpKind.REVEAL,
            source: "aaa",
            fee: "aaa",
            counter: "aaa",
            gas_limit: "aaa",
            storage_limit: "aaa",
            public_key: "aaa",
        })).toEqual(true)

        // OperationContentsTransaction
        expect(isSupportedOp({
            kind: OpKind.TRANSACTION,
            source: "aaa",
            fee: "aaa",
            counter: "aaa",
            gas_limit: "aaa",
            storage_limit: "aaa",
            amount: "aaa",
            destination: "aaa",
        })).toEqual(true)

        // OperationContentsOrigination
        expect(isSupportedOp({
            kind: OpKind.ORIGINATION,
            source: "aaa",
            fee: "aaa",
            counter: "aaa",
            gas_limit: "aaa",
            storage_limit: "aaa",
            balance: "aaa",
            delegate: "aaa",
            script: {} as any,
        })).toEqual(true)

        // OperationContentsDelegation
        expect(isSupportedOp({
            kind: OpKind.DELEGATION,
            source: "aaa",
            fee: "aaa",
            counter: "aaa",
            gas_limit: "aaa",
            storage_limit: "aaa",
            delegate: "aaa",
        })).toEqual(true)

        // OperationContentsEndorsementWithSlot
        expect(isSupportedOp({
            kind: OpKind.ENDORSEMENT_WITH_SLOT,
            endorsement: {} as any,
            slot: 100,
        })).toEqual(false)

        // OperationContentsFailingNoop
        expect(isSupportedOp({
            kind: OpKind.FAILING_NOOP,
            arbitrary: "aaa",
        })).toEqual(false)
    })

})
