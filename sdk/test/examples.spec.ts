import {CybSdk} from "../src/services/CybSdk"
import CybAuth from "../src/services/auth/CybAuth"
import {InMemorySigner} from "@taquito/signer"
import {inspect} from "util"
import {BatchWalletOperation} from "@taquito/taquito/dist/types/wallet/batch-operation"
import {OperationUpdateEvent} from "../src/types/api"
import {OperationStatus} from "../src/types/enums"
import {OpKind} from "@taquito/rpc"
import {CybKycAdminSdk} from "../src/services/CybKycAdminSdk"

const nodeUrl = "http://localhost:20000"
const cybUrl = "http://localhost:9090"
const token = "eyJ0eXAiOiJKV1QiLCJhbGciOiJIUzI1NiJ9.eyJvd25lciI6IjYxODkyYmZhZWE3ODY1NjRjYjE3YWM0NSIsImNyZWF0aW9uVGltZSI6IjIwMjEtMTEtMDhUMDU6NTQ6MDIuOTYwNjgyLTA4OjAwIiwicm9sZXMiOlsiQURNSU4iXSwiaXNzIjoiVGV6b3NAQ3liIiwicGVyc2lzdGVkIjp0cnVlfQ.Gy2q0YhRsndfEJgQ1boa6JhIj5XOSYytt9JMkwOLgCE"
const chain = "main"

const aliceAddress = "tz1VSUr8wwNhLAzempoch5d6hLRiTh8Cjcjb"
const bobPK = "edsk3RFfvaFaxbHx8BMtEW1rKQcPtDML3LXjNqMNLCzC3wLC1bWbAt"
const alicePK = "edsk3QoqBuvdamxouPhin7swCvkQNgq4jP5KZPbwWNnwdZpSpJiEbq"

// 3min timeout for operation taking time to confirm
const timeout = 3 * 60 * 1000

// Ignored test to make experiments with actual local test blockchain network
describe.skip("Examples", () => {
    const auth: CybAuth = new CybAuth(cybUrl, token)
    const bobSDK: CybSdk = new CybSdk(auth, nodeUrl, chain)
    const bobSigner = new InMemorySigner(bobPK)
    bobSDK.setProvider({
        signer: bobSigner,
    })
    const aliceSDK: CybSdk = new CybSdk(auth, nodeUrl, chain)
    aliceSDK.setProvider({
        signer: new InMemorySigner(alicePK),
    })

    const sendSimpleTransaction = async (): Promise<BatchWalletOperation> => {
        const batch = bobSDK.wallet.batch()
            .withTransfer({
                to: aliceAddress,
                amount: 1,
            })
        return batch.send()
    }

    const handleError = (error: any) => {
        console.log(`Error: ${error} ${JSON.stringify(error, null, 2)}`)
        throw error
    }

    it("Check baker balance", async () => {
        const identity = await bobSDK.getBakerIdentity()
        const balance = await bobSDK.tz.getBalance(identity.bakerAddress)

        console.log(`Baker: \"${identity.name}\" has a total of ${balance.toNumber() / 1000000} ꜩ.`)
    })

    it("Subscribe to operation events", async () => {
        await new Promise<void>(resolve => {
            let operationEventSource = bobSDK.operationService.getOperationEventSource()

            operationEventSource.onmessage = (e) => {
                const opUpdateEvent: OperationUpdateEvent = JSON.parse(e.data)
                let status = opUpdateEvent.operation.status
                console.log(`Operation updated with status: ${status}`)
                if (status === OperationStatus.BAKED
                    || status === OperationStatus.BAKED_BY_ANOTHER
                    || status === OperationStatus.REJECTED
                ) {
                    console.log("Operation has been processed.")
                    resolve()
                }
            }

            sendSimpleTransaction().catch(handleError)
        })
    }, timeout)

    it("Specify a level", async () => {
        if (!bobSDK.isContextLocked) {
            bobSDK.modifyContext({
                level: 77,
            })
        }
        const response = await sendSimpleTransaction()
        const operation = await bobSDK.operationService.getOperation({hash: response.opHash})
        console.log(inspect(operation))
    })

    it("Delete an operation", async () => {
        const response = await sendSimpleTransaction()
        const deletedOperation = await bobSDK.operationService.deleteOperation({hash: response.opHash})
        console.log(inspect(deletedOperation))
    })

    it("Update an operation", async () => {
        // await sendSimpleTransaction() // first operation
        const response = await bobSDK.updateCurrentOperation([{
            kind: OpKind.TRANSACTION,
            to: "tz1Nbbyzqtte3YZAK5BdFkC9xkHKdk4V63Qp",
            amount: 8,
        }])
        const operation = await bobSDK.operationService.getOperation({hash: response.opHash})
        console.log(inspect(operation))
    })

    it("Estimate before sending operation", async () => {
        const params = await bobSDK.cybEstimate([{
            kind: OpKind.TRANSACTION,
            to: "tz1Nbbyzqtte3YZAK5BdFkC9xkHKdk4V63Qp",
            amount: 1,
        }])
        const response = await bobSDK.generateBatch(params).send()
        const operation = await bobSDK.operationService.getOperation({hash: response.opHash})
        console.log(inspect(operation))
    })

    it("Get baking rights", async () => {
        const bakingSlots = await bobSDK.getBakingSlots({
            delegates: ["tz1YPSCGWXwBdTncK2aCctSZAXWvGsGwVJqU"],
            cycles: [9, 10],
            maxPriority: 0,
        })
        console.log(inspect(bakingSlots))
    })

    it("Unauthenticated user", async () => {
        const auth: CybAuth = new CybAuth(cybUrl)
        const unauthenticatedSDK: CybSdk = new CybSdk(auth, nodeUrl, chain)
        unauthenticatedSDK.setProvider({
            signer: new InMemorySigner(bobPK),
        })
        const batch = unauthenticatedSDK.wallet.batch()
            .withTransfer({
                to: aliceAddress,
                amount: 1,
            })
        const response = await batch.send()
        const operation = await unauthenticatedSDK.operationService.getOperation({hash: response.opHash})
        console.log(inspect(operation))
    })

    it("Administer KYC contract", async () => {
        const adminKycSdk: CybKycAdminSdk = new CybKycAdminSdk("KT1DhtLFZ8tfvKiT6ZuoH3i2SMd4oqKgE2bK", nodeUrl, chain)
        adminKycSdk.setProvider({
            signer: new InMemorySigner(alicePK),
        })

        // Add a new manager to the contract
        await adminKycSdk.addManager("tz1VSUr8wwNhLAzempoch5d6hLRiTh8Cjcjb")

        // Add a new baker to the contract
        await adminKycSdk.addBaker(
            "tz1YPSCGWXwBdTncK2aCctSZAXWvGsGwVJqU",
            new Map([
                ["country", 'France'],
                ["company", 'Coexya'],
                ["url", 'https://coexya.eu'],
            ]),
        )

        // // Remove a manager from the contract
        // await adminKycSdk.removeManager("tz1VSUr8wwNhLAzempoch5d6hLRiTh8Cjcjb")
        //
        // // Remove baker from the contract
        // await adminKycSdk.removeBaker("tz1YPSCGWXwBdTncK2aCctSZAXWvGsGwVJqU")

    }, timeout)

})
