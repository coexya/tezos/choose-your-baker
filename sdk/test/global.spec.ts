import {CybSdk} from "../src/services/CybSdk"
import CybAuth from "../src/services/auth/CybAuth"
import {InMemorySigner} from "@taquito/signer"
import {inspect} from "util"
import {BatchWalletOperation} from "@taquito/taquito/dist/types/wallet/batch-operation"
import {OperationUpdateEvent, TmBakingSlot} from "../src/types/api"
import {OperationStatus} from "../src/types/enums"
import {OpKind} from "@taquito/rpc"
import {CybKycAdminSdk} from "../src/services/CybKycAdminSdk"
import {ForgeParams} from "@taquito/taquito/dist/types/forger/interface"
import { TezosToolkit } from "@taquito/taquito"
import {HttpBackend} from '@taquito/http-utils';

const tmPort="9090"
const nodeUrl = "http://localhost:20000"
const cybUrl = "http://localhost:" + tmPort
const issuerToken="eyJ0eXAiOiJKV1QiLCJhbGciOiJIUzI1NiJ9.eyJvd25lciI6IjYxODkyYmQ1ZWE3ODY1NjRjYjE3YWM0MyIsImNyZWF0aW9uVGltZSI6IjIwMjEtMTEtMDhUMDU6NTM6MjUuMzgxMzg4LTA4OjAwIiwicm9sZXMiOlsiSVNTVUVSIl0sImlzcyI6IlRlem9zQEN5YiIsInBlcnNpc3RlZCI6dHJ1ZX0.xOdzsl7OffzRe3BYyAxexxlGHMceHXfRVX2veB3mFN8"
const chain = "main"

const aliceAddress = "tz1VSUr8wwNhLAzempoch5d6hLRiTh8Cjcjb"
const bobPK = "edsk3RFfvaFaxbHx8BMtEW1rKQcPtDML3LXjNqMNLCzC3wLC1bWbAt"
const alicePK = "edsk3QoqBuvdamxouPhin7swCvkQNgq4jP5KZPbwWNnwdZpSpJiEbq"
const certifiedBakerAddress = "tz1VSUr8wwNhLAzempoch5d6hLRiTh8Cjcjb"
const certifiedBakerAddress2 = "tz1aSkwEot3L2kmUvcoxzjMomb9mvBNuzFK6"

// 3min timeout for operation taking time to confirm
const timeout = 3 * 60 * 1000

// Ignored test to make experiments with actual local test blockchain network
describe.skip("Global", () => {
    const auth: CybAuth = new CybAuth(cybUrl, issuerToken)
    const bobSDK: CybSdk = new CybSdk(auth, nodeUrl, chain)
    const bobSigner = new InMemorySigner(bobPK)
    bobSDK.setProvider({
        signer: bobSigner,
    })
    const aliceSDK: CybSdk = new CybSdk(auth, nodeUrl, chain)
    aliceSDK.setProvider({
        signer: new InMemorySigner(alicePK),
    })


    const tezosToolkitAlice = new TezosToolkit(nodeUrl)
    tezosToolkitAlice.setProvider({signer: new InMemorySigner(alicePK)});
    


    const sendSimpleTransaction = async (): Promise<BatchWalletOperation> => {
        const batch = bobSDK.wallet.batch()
            .withTransfer({
                to: aliceAddress,
                amount: 1,
            })
        return batch.send()
    }

    const handleError = (error: any) => {
        console.log(`Error: ${error} ${JSON.stringify(error, null, 2)}`)
        throw error
    }

    let kycAddress: string|undefined

    beforeAll(async () => {

        const fs = require('fs');
        const data = fs.readFileSync('../kyc/output/kyc_contract/step_000_cont_0_contract.tz', {encoding:'utf8', flag:'r'});


        console.log(data);

        
        await tezosToolkitAlice.contract.originate({
            code: data,
            init: `(Pair {} {"${aliceAddress}"})`,
        })
            .then((originationOp) => {
                console.log(`Waiting for confirmation of origination for ${originationOp.contractAddress}...`);
                kycAddress = originationOp.contractAddress
                return originationOp.contract();
            })
            .then(() => {
                console.log(`Origination completed.`);

            })
            .catch((error) => console.log(`Error: ${JSON.stringify(error, null, 2)}`));

        if (kycAddress!=undefined){
                
            const adminKycSdk: CybKycAdminSdk = new CybKycAdminSdk(kycAddress, nodeUrl, chain, new HttpBackend())
            adminKycSdk.setProvider({
                signer: new InMemorySigner(alicePK),
            })

            let param =new Map<string, string>([
                ["country" , "Britain :-) !"],
                ["company", "COEXYA"],
                ["url",  "http://localhost:" + tmPort],
            ])
            await adminKycSdk.addBaker(certifiedBakerAddress, param)

            param =new Map<string, string>([
                ["country" , "Britain :-) !"],
                ["company", "COEXYA1"],
                ["url",  "http://localhostx:" + tmPort],
            ])
            await adminKycSdk.addBaker(certifiedBakerAddress2, param)


        }

      

    }, timeout)

 


    it("global_operation", async () =>  {

        //Get the certified bakers
        const certifiedBakers = await bobSDK.getCertifiedBakers(kycAddress)
        
        //Retrieve the most matching certified baker
        const certifiedBaker = await bobSDK.getFirstMatchingBaker(certifiedBakers)
        if (!certifiedBakers){
            throw new Error ("no certified baker with an available slot")
        }


        //Create a sdk targeting the baker url
        const sdk: CybSdk = new CybSdk(new CybAuth(certifiedBaker!.certifiedBaker.baker_infos.get("url")!, issuerToken), nodeUrl, chain)
        const bobSigner = new InMemorySigner(bobPK)
        sdk.setProvider({
            signer: bobSigner,
        })

        //Do the transaction
        const batch = sdk.wallet.batch()
        .withTransfer({
            to: aliceAddress, amount: 0.1
        });

        const operationTaquito = await batch.send()
        .then((hash) => {
            console.log(`Operation injected ${hash}`);
            return hash
        })
        .catch((error) => {
            console.log(`Error: ${error} ${JSON.stringify(error, null, 2)}`);
            return undefined
        });

       
    }, timeout)



})
