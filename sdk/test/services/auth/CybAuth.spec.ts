import CybAuth from "../../../src/services/auth/CybAuth"
import {TransactionManagerEndpoints} from "../../../src/types/enums"

describe('CybAuth', () => {
    const url = "http://testurl.com"
    const anyToken = "8ad802f8142a8d043305d595b95b75e9"
    const otherToken = "32254f7607fe13d1ac2c4744e4599f5f"

    it('Validates endpointUrl', () => {
        const auth = new CybAuth(url, anyToken)
        for (let endpoint in TransactionManagerEndpoints) {
            expect(auth.endpointUrl(endpoint as TransactionManagerEndpoints)).toEqual(url + endpoint)
        }
    })

    it('Authorization header', () => {
        const auth = new CybAuth(url, anyToken)

        expect(auth.authorizationHeader()).toEqual({"Authorization": `Bearer ${anyToken}`})
        expect(auth.authorizationHeader(otherToken)).toEqual({"Authorization": `Bearer ${otherToken}`})

        const authNoToken = new CybAuth(url)
        expect(authNoToken.authorizationHeader()).toEqual({})
        expect(authNoToken.authorizationHeader(otherToken)).toEqual({"Authorization": `Bearer ${otherToken}`})
    })
})
