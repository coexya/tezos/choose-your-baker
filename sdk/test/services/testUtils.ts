import {OperationContents} from "@taquito/rpc/dist/types/types"
import {OperationObject, OpKind} from "@taquito/rpc"

const characters = "ABCDEFGHIJKLMNOPQRSTUVWXYZabcdefghijklmnopqrstuvwxyz0123456789"

export default class TestUtils {

    static anyOperation(): OperationObject {
        const contents = []
        for (let i = 0; i < (Math.floor(Math.random() * 3)); i++) {
            contents.push(this.anyTransaction())
        }
        return {
            branch: this.randomString(),
            contents: contents,
            protocol: this.randomString(),
            signature: this.randomString(),
        }
    }

    static anyTransaction(): OperationContents {
        switch (Math.floor(Math.random() * 3)) {
            case 0:
                return {
                    kind: OpKind.TRANSACTION,
                    source: this.randomString(),
                    fee: this.randomString(),
                    counter: this.randomString(),
                    gas_limit: this.randomString(),
                    storage_limit: this.randomString(),
                    amount: this.randomString(),
                    destination: this.randomString(),
                }
            case 1:
                return {
                    kind: OpKind.ORIGINATION,
                    source: this.randomString(),
                    fee: this.randomString(),
                    counter: this.randomString(),
                    gas_limit: this.randomString(),
                    storage_limit: this.randomString(),
                    balance: this.randomString(),
                }
            case 2:
                return {
                    kind: OpKind.DELEGATION,
                    source: this.randomString(),
                    fee: this.randomString(),
                    counter: this.randomString(),
                    gas_limit: this.randomString(),
                    storage_limit: this.randomString(),
                }
        }
        throw new Error("Unexpected error in TestUtils.anyTransaction.")
    }

    static randomString(length: number = 32): string {
        return Array.from(
            {length: length},
            () => characters[Math.floor(Math.random() * characters.length)],
        ).join("")
    }

}
