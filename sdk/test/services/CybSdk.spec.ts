import {InMemorySigner} from '@taquito/signer';
import BigNumber from 'bignumber.js';
import CybAuth from "../../src/services/auth/CybAuth"
import {CybSdk} from "../../src/services/CybSdk"

describe('CybSdk', () => {
    const auth = new CybAuth("http://localhost:9090", "token")
    const nodeUrl = "http://node"
    const chain = "test"
    const tzAddress = "tz1VSUr8wwNhLAzempoch5d6hLRiTh8Cjcjb"

    let httpBackend = {
        createRequest: jest.fn()
    }
    let sdk: CybSdk

    beforeEach(() => {
        httpBackend = {
            createRequest: jest.fn()
        }
        sdk = new CybSdk(auth, nodeUrl, chain, httpBackend as any)
        sdk.setProvider({
            signer: new InMemorySigner('edsk3QoqBuvdamxouPhin7swCvkQNgq4jP5KZPbwWNnwdZpSpJiEbq')
        })
    })

    it('get balance', async () => {
        const expectedBalance = "10000"
        httpBackend.createRequest.mockResolvedValue(new BigNumber(expectedBalance))

        const balance = await sdk.tz.getBalance(tzAddress)

        expect(httpBackend.createRequest.mock.calls[0][0]).toEqual({
            method: 'GET',
            url: `${nodeUrl}/chains/${chain}/blocks/head/context/contracts/${tzAddress}/balance`
        })
        expect(balance).toBeInstanceOf(BigNumber)
        expect(balance.toString()).toEqual(expectedBalance)
    })
})
