import {CybOperationService} from "../../src/services/CybSdk"
import {UnknownOperationException, WrongParameterException} from "../../src/types/error"
import CybAuth from "../../src/services/auth/CybAuth"
import CybRpcClient from "../../src/services/CybRpcClient"
import CybContext from "../../src/services/CybContext"
import {HttpBackendMock} from "./types"
import {CybToken, ProcessOperationResponse} from "../../src/types/api"
import TestUtils from "./testUtils"
import {TransactionManagerEndpoints} from "../../src/types/enums"

describe("CybOperationService", () => {
    const cybUrl = "http://localhost:9090"
    const nodeUrl = "http://node"
    const token = "token"
    const auth: CybAuth = new CybAuth(cybUrl, token)

    let httpBackend: HttpBackendMock
    let client: CybRpcClient
    let operationService: CybOperationService
    let response: ProcessOperationResponse

    beforeEach(() => {
        httpBackend = {
            createRequest: jest.fn(),
        }
        client = new CybRpcClient(auth, new CybContext(), nodeUrl, "main", httpBackend as any)
        operationService = new CybOperationService(client)
        client._attach(operationService)
    })

    it("No injection", async () => {
        let anotherId = "anotherId"
        await operationService.getOperation({id: anotherId})
        expect(httpBackend.createRequest.mock.calls[0][0]).toEqual({
            method: "GET",
            url: `${auth.endpointUrl(TransactionManagerEndpoints.OPERATION)}?id=${anotherId}`,
            headers: auth.authorizationHeader(),
        })

        await expect(operationService.getOperation({hash: "another_hash"}))
            .rejects
            .toBeInstanceOf(UnknownOperationException)

        await operationService.deleteOperation({id: anotherId})
        expect(httpBackend.createRequest.mock.calls[1][0]).toEqual({
            method: "POST",
            url: `${auth.endpointUrl(TransactionManagerEndpoints.DELETE_OPERATION)}`,
            headers: auth.authorizationHeader(),
        })
        expect(httpBackend.createRequest.mock.calls[1][1]).toEqual({id: anotherId})

        await expect(operationService.deleteOperation({hash: "another_hash"}))
            .rejects
            .toBeInstanceOf(UnknownOperationException)
    })

    it("Check parameter", async () => {
        await expect(operationService.getOperation({}))
            .rejects
            .toBeInstanceOf(WrongParameterException)
    })

    describe("Authenticated", () => {
        beforeEach(async () => {
            await client.preapplyOperations([TestUtils.anyOperation()])

            response = {
                message: TestUtils.randomString(),
                operationId: TestUtils.randomString(),
                operationHash: TestUtils.randomString(),
            }
            httpBackend.createRequest.mockReturnValue(response)

            await client.injectOperation(TestUtils.randomString())
        })

        it("Get operation", async () => {
            await operationService.getOperation({id: response.operationId})
            expect(httpBackend.createRequest.mock.calls[2][0]).toEqual({
                method: "GET",
                url: `${auth.endpointUrl(TransactionManagerEndpoints.OPERATION)}?id=${response.operationId}`,
                headers: auth.authorizationHeader(),
            })

            await operationService.getOperation({hash: response.operationHash})
            expect(httpBackend.createRequest.mock.calls[3][0]).toEqual({
                method: "GET",
                url: `${auth.endpointUrl(TransactionManagerEndpoints.OPERATION)}?id=${response.operationId}`,
                headers: auth.authorizationHeader(),
            })
        })

        it("Delete operation", async () => {
            await operationService.deleteOperation({id: response.operationId})
            expect(httpBackend.createRequest.mock.calls[2][0]).toEqual({
                method: "POST",
                url: `${auth.endpointUrl(TransactionManagerEndpoints.DELETE_OPERATION)}`,
                headers: auth.authorizationHeader(),
            })
            expect(httpBackend.createRequest.mock.calls[2][1]).toEqual({id: response.operationId})

            await operationService.deleteOperation({hash: response.operationHash})
            expect(httpBackend.createRequest.mock.calls[3][0]).toEqual({
                method: "POST",
                url: `${auth.endpointUrl(TransactionManagerEndpoints.DELETE_OPERATION)}`,
                headers: auth.authorizationHeader(),
            })
            expect(httpBackend.createRequest.mock.calls[3][1]).toEqual({id: response.operationId})
        })

    })

    describe("Not Authenticated", () => {
        let token: CybToken
        beforeEach(async () => {
            await client.preapplyOperations([TestUtils.anyOperation()])

            token = {
                id: TestUtils.randomString(),
                name: TestUtils.randomString(),
                expirationDate: TestUtils.randomString(),
                creationDate: TestUtils.randomString(),
                roles: [TestUtils.randomString()],
                owner: TestUtils.randomString(),
                revoked: false,
                jwtToken: TestUtils.randomString(),
            }
            response = {
                message: TestUtils.randomString(),
                operationId: TestUtils.randomString(),
                operationHash: TestUtils.randomString(),
                token: token,
            }
            httpBackend.createRequest.mockReturnValue(response)

            await client.injectOperation("signedBytes")
        })

        it("Get operation", async () => {
            await operationService.getOperation({id: response.operationId})
            expect(httpBackend.createRequest.mock.calls[2][0]).toEqual({
                method: "GET",
                url: `${auth.endpointUrl(TransactionManagerEndpoints.OPERATION)}?id=${response.operationId}`,
                headers: auth.authorizationHeader(token.jwtToken),
            })

            await operationService.getOperation({hash: response.operationHash})
            expect(httpBackend.createRequest.mock.calls[3][0]).toEqual({
                method: "GET",
                url: `${auth.endpointUrl(TransactionManagerEndpoints.OPERATION)}?id=${response.operationId}`,
                headers: auth.authorizationHeader(token.jwtToken),
            })
        })

        it("Delete operation", async () => {
            await operationService.deleteOperation({id: response.operationId})
            expect(httpBackend.createRequest.mock.calls[2][0]).toEqual({
                method: "POST",
                url: `${auth.endpointUrl(TransactionManagerEndpoints.DELETE_OPERATION)}`,
                headers: auth.authorizationHeader(token.jwtToken),
            })
            expect(httpBackend.createRequest.mock.calls[2][1]).toEqual({id: response.operationId})

            await operationService.deleteOperation({hash: response.operationHash})
            expect(httpBackend.createRequest.mock.calls[3][0]).toEqual({
                method: "POST",
                url: `${auth.endpointUrl(TransactionManagerEndpoints.DELETE_OPERATION)}`,
                headers: auth.authorizationHeader(token.jwtToken),
            })
            expect(httpBackend.createRequest.mock.calls[3][1]).toEqual({id: response.operationId})
        })
    })
})

