import {CybContext} from "../../src/services/CybSdk"
import {CybContextValues} from "../../src/types/operation"
import TestUtils from "./testUtils"
import {ContextLockedException} from "../../src/types/error"

describe("CybContext", () => {

    let context: CybContext
    const contextValues: CybContextValues = {
        operations: [
            TestUtils.anyOperation(),
            TestUtils.anyOperation(),
        ],
        level: 23,
    }

    beforeEach(() => {
        context = new CybContext()
    })

    it("Semaphore is locked", async () => {
        expect(context.isLocked).toEqual(false)
        await context.acquireWithValues(contextValues)
        expect(context.isLocked).toEqual(true)

        expect(context.getContext()).toEqual(contextValues)
        context.release()

        expect(context.isLocked).toEqual(false)
    })

    it("Cannot modify context when locked", async () => {
        await context.acquireWithValues(contextValues)

        expect(() => {
            context.setContext({level: 13})
        }).toThrow(ContextLockedException)

        expect(() => {
            context.setContext({operations: [TestUtils.anyOperation()]})
        }).toThrow(ContextLockedException)

        context.getContext().level = 13
        expect(context.getContext()).toEqual(contextValues)

        context.getContext().operations = [TestUtils.anyOperation()]
        expect(context.getContext()).toEqual(contextValues)

        context.release()
    })

    it("Can modify context when not locked", async () => {
        const anyContextValues: CybContextValues = {
            operations: [
                TestUtils.anyOperation(),
                TestUtils.anyOperation(),
                TestUtils.anyOperation(),
            ],
        }
        context.setContext(anyContextValues)
        expect(context.getContext()).toEqual(anyContextValues)

        const otherContextValues = {
            ...anyContextValues,
            level: 12,
        }
        context.setContext(otherContextValues)
        expect(context.getContext()).toEqual(otherContextValues)
    })


    it("Check double acquire", async () => {
        await context.acquireWithValues(contextValues)
        expect(context.isLocked).toEqual(true)
        expect(context.getContext()).toEqual(contextValues)

        const newContextValues: CybContextValues = {
            operations: [
                TestUtils.anyOperation(),
                TestUtils.anyOperation(),
                TestUtils.anyOperation(),
            ],
            level: 156,
        }

        // Will wait until context is released
        context.acquireWithValues(newContextValues).then()
        expect(context.isLocked).toEqual(true)
        expect(context.getContext()).toEqual(contextValues)

        context.release()
        await new Promise(resolve => setTimeout(resolve, 1000)) // Wait for the semaphore to be released and taken again
        expect(context.isLocked).toEqual(true)
        expect(context.getContext()).toEqual(newContextValues)
    })

})
