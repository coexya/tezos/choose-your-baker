import {CybKycAdminSdk} from "../../src/services/CybKycAdminSdk"
import {WrongParameterException} from "../../src/types/error"

describe("CybKycAdminSdk", () => {

    it("Check KYC contract format", () => {
        const kycSdkCreate = (contractAddress: string) => {
            new CybKycAdminSdk(contractAddress, "localhost:20000")
        }

        expect(() => kycSdkCreate("dHoxVlNVcjh3d05oTEF6ZW1wb2NoNWQ2aExS"))
            .toThrow(WrongParameterException)

        expect(() => kycSdkCreate("tz1VSUr8wwNhLAzempoch5d6hLRiTh8Cjcjb"))
            .toThrow(WrongParameterException)

        expect(() => kycSdkCreate("KT1DhtLFZ8tfvKiT6ZuoH3i2SMd4oqKg"))
            .toThrow(WrongParameterException)

        // Correct contract format
        kycSdkCreate("KT1DhtLFZ8tfvKiT6ZuoH3i2SMd4oqKgE2bK")
    })

})
