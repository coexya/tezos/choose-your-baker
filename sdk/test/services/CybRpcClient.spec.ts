import {BlockHeaderResponse, OperationContents} from "@taquito/rpc/dist/types/types"
import {OperationObject, OpKind} from "@taquito/rpc"
import CybAuth from "../../src/services/auth/CybAuth"
import CybRpcClient from "../../src/services/CybRpcClient"
import {FeeMode, TezosNodeEndpoint, TransactionManagerEndpoints} from "../../src/types/enums"
import {BakerIdentity, OperationCreate, ProcessOperationResponse} from "../../src/types/api"
import {ForgeParams} from "@taquito/taquito"
import CybContext from "../../src/services/CybContext"
import TestUtils from "./testUtils"
import {HttpBackendMock} from "./types"

const nodeUrl = "http://localhost:20000"
const cybUrl = "http://localhost:9090"
const token = "token"
const chain = "test"

const branch: string = "testBranch"
const contents: OperationContents[] = [{
    kind: OpKind.ENDORSEMENT,
    level: 1,
}]
const protocol: string = "testProtocol"
const signature: string = "testSignature"
const operation: OperationObject = {
    branch,
    contents,
    protocol,
    signature,
}

describe("CybRpcClient", () => {
    const auth: CybAuth = new CybAuth(cybUrl, token)

    let httpBackend: HttpBackendMock
    let client: CybRpcClient

    beforeEach(() => {
        httpBackend = {
            createRequest: jest.fn(),
        }
        client = new CybRpcClient(auth, new CybContext(), nodeUrl, chain, httpBackend as any)
    })

    it("Validates creation", () => {
        expect(client.getRpcUrl()).toEqual(nodeUrl)
    })

    it("Gets head", async () => {
        const response: BlockHeaderResponse = {
            protocol: "protocol",
            chain_id: "chain_id",
            hash: "testHash",
            level: 1,
            proto: 1,
            predecessor: "predecessor",
            timestamp: "timestamp",
            validation_pass: 4,
            operations_hash: "operation_pass",
            fitness: [],
            context: "context",
            priority: 0,
            proof_of_work_nonce: "proof_nonce",
            signature: "signature",
        }
        httpBackend.createRequest.mockResolvedValue(response)

        const result = await client.getBlockHeader()

        expect(httpBackend.createRequest.mock.calls[0][0]).toEqual({
            method: "GET",
            url: `${nodeUrl}/chains/${chain}/blocks/head/header`,
        })
        expect(result).toEqual(response)
    })

    it("Forge operation", async () => {

        const bakerAddress = "tz1YPSCGWXwBdTncK2aCctSZAXWvGsGwVJqU"
        const identity: BakerIdentity = {
            name: "The Chosen Baker",
            nbBakingSlotSearch: 10,
            bakerAddress: bakerAddress,
            kycSmartContractAddress: "KT1Q7PPnRrWw3GKGz28GmK6f5BSkwS5wtLPQ",
            allowUnauthenticated: true,
            feeMode: FeeMode.TRANSACTION,
            minimalNtzFees: 100000,
            minimalNtzPerGasUnit: 100,
            minimalNtzPerByte: 1000,
        }
        client.getBakerIdentity = jest.fn().mockReturnValue(identity)

        const op: OperationContents = {
            kind: OpKind.TRANSACTION,
            source: "source",
            fee: "403",
            counter: "5",
            gas_limit: "1527",
            storage_limit: "0",
            amount: "1000000",
            destination: "destination",
        }
        const ops: ForgeParams = {
            branch: "branch",
            contents: [op],
        }

        let expectedOps: ForgeParams = {
            branch: ops.branch,
            contents: [op],
        }

        // Tested call
        await client.forgeOperations(ops)

        expect(httpBackend.createRequest.mock.calls[0][0]).toEqual({
            method: "POST",
            url: `${nodeUrl}/chains/${chain}/blocks/head/helpers/forge/operations`,
        })
        expect(httpBackend.createRequest.mock.calls[0][1]).toEqual(expectedOps)
    })

    it("Preapply operation", async () => {
        await client.preapplyOperations({} as any)

        expect(httpBackend.createRequest.mock.calls[0][0]).toEqual({
            method: "POST",
            url: `${nodeUrl}/chains/${chain}/blocks/head/helpers/preapply/operations`,
        })
        expect(httpBackend.createRequest.mock.calls[0][1]).toEqual({})
    })

    it("Inject operation", async () => {

        const operations: OperationObject[] = [operation]

        await client.preapplyOperations(operations)

        expect(httpBackend.createRequest.mock.calls[0][0]).toEqual({
            method: "POST",
            url: `${nodeUrl}/chains/${chain}/blocks/head/helpers/preapply/operations`,
        })
        expect(httpBackend.createRequest.mock.calls[0][1]).toEqual(operations)

        const response: ProcessOperationResponse = {
            message: "message",
            operationId: "operationId",
            operationHash: "operationHash",
        }
        httpBackend.createRequest.mockReturnValue(response)

        await client.injectOperation("signedBytes")

        const insertOperation: OperationCreate = {
            tzOperation: {
                ...operation,
                hash: "opGT2v5S2GAKoPKHUgcVPjisQMEy7AzF5R2FqNRF8Gx1HWERm2V",
            },
        }

        expect(httpBackend.createRequest.mock.calls[1][0]).toEqual({
            method: "POST",
            url: `${auth.endpointUrl(TransactionManagerEndpoints.PROCESS_OPERATION)}`,
            headers: auth.authorizationHeader(),
        })
        expect(httpBackend.createRequest.mock.calls[1][1]).toEqual(insertOperation)
    })

    it("Get baking slots", async () => {
        let callCount = 0
        const expectParams = (params: string) => {
            expect(httpBackend.createRequest.mock.calls[callCount][0]).toEqual({
                method: "GET",
                url: `${nodeUrl}${TezosNodeEndpoint.BAKING_RIGHTS}${params}`,
                headers: {},
            })
            callCount++
        }

        const anyDelegate = TestUtils.randomString()
        const otherDelegate = TestUtils.randomString()

        const anyCycle = 10
        const otherCycle = 12

        const anyLevel = 1500
        const otherLevel = 1622

        const anyMaxPriority = 0
        const otherMaxPriority = 1

        await client.getBakingSlots()
        expectParams("")

        await client.getBakingSlots([], [], [], undefined, true)
        expectParams("?all=true")

        await client.getBakingSlots([], [], [], anyMaxPriority, true)
        expectParams(`?max_priority=${anyMaxPriority}&all=true`)

        await client.getBakingSlots([], [], [], otherMaxPriority)
        expectParams(`?max_priority=${otherMaxPriority}`)

        await client.getBakingSlots([], [], [anyDelegate], otherMaxPriority)
        expectParams(`?delegate=${anyDelegate}&max_priority=${otherMaxPriority}`)

        await client.getBakingSlots([], [], [anyDelegate, otherDelegate], otherMaxPriority)
        expectParams(`?delegate=${anyDelegate}&delegate=${otherDelegate}&max_priority=${otherMaxPriority}`)

        await client.getBakingSlots([], [], [anyDelegate, otherDelegate])
        expectParams(`?delegate=${anyDelegate}&delegate=${otherDelegate}`)

        await client.getBakingSlots([], [anyCycle], [anyDelegate, otherDelegate])
        expectParams(`?cycle=${anyCycle}&delegate=${anyDelegate}&delegate=${otherDelegate}`)

        await client.getBakingSlots([], [anyCycle, otherCycle], [anyDelegate, otherDelegate])
        expectParams(`?cycle=${anyCycle}&cycle=${otherCycle}&delegate=${anyDelegate}&delegate=${otherDelegate}`)

        await client.getBakingSlots([anyLevel], [anyCycle, otherCycle], [anyDelegate, otherDelegate])
        expectParams(`?level=${anyLevel}&cycle=${anyCycle}&cycle=${otherCycle}&delegate=${anyDelegate}&delegate=${otherDelegate}`)

        await client.getBakingSlots([anyLevel], [anyCycle, otherCycle])
        expectParams(`?level=${anyLevel}&cycle=${anyCycle}&cycle=${otherCycle}`)

        await client.getBakingSlots([anyLevel, otherLevel], [anyCycle])
        expectParams(`?level=${anyLevel}&level=${otherLevel}&cycle=${anyCycle}`)

        await client.getBakingSlots([anyLevel, otherLevel], [], [anyDelegate, otherDelegate])
        expectParams(`?level=${anyLevel}&level=${otherLevel}&delegate=${anyDelegate}&delegate=${otherDelegate}`)

        await client.getBakingSlots([anyLevel], [anyCycle], [anyDelegate], anyMaxPriority, true)
        expectParams(`?level=${anyLevel}&cycle=${anyCycle}&delegate=${anyDelegate}&max_priority=${anyMaxPriority}&all=true`)

        await client.getBakingSlots([], [anyCycle], [], anyMaxPriority)
        expectParams(`?cycle=${anyCycle}&max_priority=${anyMaxPriority}`)

    })

})
