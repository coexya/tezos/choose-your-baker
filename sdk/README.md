# Choose Your Baker SDK v1.0.1

The SDK for Choose Your Baker. It enables issuers to issue Tezos transactions to a chosen baker.

## Installation

Using npm:
```shell
npm install @coexyaedi/cyb-sdk
```

## Configuration

### SDK configuration

The constructor of `CybAuth` takes a **Choose Your Baker Transaction Manager** URL and an authentication token as parameters.
The constructor of `CybSdk` takes a Tezos node URL as parameter.

```typescript
import {CybSdk, CybAuth} from '@coexyaedi/cyb-sdk'

const auth = new CybAuth('https://TRANSACTION_MANAGER_URL', 'ISSUER_TOKEN')
const sdk: CybSdk = new CybSdk(auth, 'https://YOUR_PREFERRED_RPC_URL')
```

If the transaction manager is configured to accept unauthenticated submission of operations, the SDk can be used without a token.

```typescript
import {CybSdk, CybAuth} from '@coexyaedi/cyb-sdk'

const auth = new CybAuth('https://TRANSACTION_MANAGER_URL')
const sdk: CybSdk = new CybSdk(auth, 'https://YOUR_PREFERRED_RPC_URL')
```

### Tezos keys configuration

In order to sign operations, Tezos private key has to be provided to a signer.

```typescript
import {InMemorySigner} from "@taquito/signer"

sdk.setProvider({
    signer: new InMemorySigner('SECRET_KEY')
})
```

## Examples

### Transfer some XTZ

It's mandatory to call the *cybEstimate* function before sending the request in order to get the right gas limit, storage limit, and the fees related to the transaction manager of the certified baker.

```typescript
// Build a transaction to transfer 0.1 XTZ to destinationAddress
const params : ParamsWithKind[] = [
    {
        kind: OpKind.TRANSACTION,
        to: destinationAddress,
        amount: 0.1,
        fee: 0,
    }
]

 // Estimate for CYB
const estimateCyb = await sdk.cybEstimate(params)

// Send the operation
const response = await sdk.generateBatch(estimateCyb).send()
```

### Transfer some XTZ and call a smart contract

```typescript
// Smart Contract with on method: updateName
const simpleContract = await sdk.wallet.at('SMART_CONTRACT_ADDRESS')

const params : ParamsWithKind[] =[
    {
        kind: OpKind.TRANSACTION,
        to: destinationAddress,
        amount: 0.1,
        fee: 0,
    },
    {
        kind: OpKind.TRANSACTION,
        ...simpleContract.methods.updateName('MY_STRING').toTransferParams({})
    },
]

// Estimate for CYB
const estimateCyb = await sdk.cybEstimate(params)

// Send the operation
const response = await sdk.generateBatch(estimateCyb).send()
```

### Specify a level

It is possible to specify at which level we want our operation to be baked (it has to be a baking slot of the chosen baker).

```typescript
// Change context for next call
if (!sdk.isContextLocked) {
    sdk.modifyContext({
        level: WANTED_LEVEL,
    })
}

// Prepare and send operation
// ...
const response = await sdk.generateBatch(estimateCyb).send()
 
// Reset context for next call
if (!sdk.isContextLocked) {
    sdk.modifyContext({
        level: undefined,
    })
}
```

### Get CYB information about an operation

```typescript
// Send the operation
const response = await sdk.generateBatch(estimateCyb).send()

// Get operation
const operation = await sdk.operationService.getOperation({hash: response.opHash})
```

### Delete an operation

It is possible to delete an operation from the mempool of CYB only if the operation has the PENDING status.

```typescript
// Send the operation
const response = await sdk.generateBatch(estimateCyb).send()

// Delete the operation
const deletedOperation = await sdk.operationService.deleteOperation({hash: response.opHash})
```

You might want to verify the validity of the operation that was sent back.
```typescript
// Verify the signature
const tzOperation = deletedOperation.tzOperation
const forgeParams: ForgeParams = {
    branch: tzOperation.branch,
    contents: tzOperation.contents,
}
const bytes = await sdk.cybRpc.forgeOperations(forgeParams)
const signature = await sdk.signer.sign(bytes, new Uint8Array([3]))

if (signature.prefixSig === tzOperation.signature) {
    console.log("Deleted operation is valid")
}
```

### Update an operation

```typescript
// Send the operation
const response = await sdk.generateBatch(estimateCyb).send()

// Delete and make a new batch from deleted operation
const deletedOperation = await sdk.operationService.deleteOperation({hash: response.opHash})
const batch = sdk.generateBatchFrom(deletedOperation)
batch.withTransfer({
    to: "tz1aSkwEot3L2kmUvcoxzjMomb9mvBNuzFK6",
    amount: 1,
})

// Re-submit the operation
const secondOp = await batch.send()
const operation = await sdk.operationService.getOperation({hash: secondOp.opHash})
```
