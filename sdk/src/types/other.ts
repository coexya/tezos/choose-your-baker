/**
 * @description Parameter of the getBakingSlots method in CybSdk
 */
export interface BakingSlotParams {
    levels?: number[]
    cycles?: number[]
    delegates?: string[]
    maxPriority?: number
    all?: boolean
}

/**
 * @description Parameter of the getOperation and deleteOperation methods in CybOperationService
 */
export interface OperationIdentification {
    id?: string
    hash?: string
}
