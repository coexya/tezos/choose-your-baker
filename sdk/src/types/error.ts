export class UnsupportedOperationKindException implements Error {
    name: string = "Unsupported Operation Kind Exception"
    message: string

    constructor(public kind: string) {
        this.message = `The following operation kind is not supported: ${kind}.`
    }
}

export class UnknownOperationException implements Error {
    name: string = "Unknown Operation Exception"
    message: string

    constructor() {
        this.message = "Operation is unknown."
    }
}

export class ContextLockedException implements Error {
    name: string = "Context Locked Exception"
    message: string

    constructor() {
        this.message = "Context is locked and cannot be modified."
    }
}

export class WrongParameterException implements Error {
    name: string = "Wrong Parameter Exception"
    message: string

    constructor(message: string) {
        this.message = message
    }
}

export class KycContractNotFoundException implements Error {
    name: string = "Kyc Contract Not Found Exception"
    message: string

    constructor() {
        this.message = "The KYC contract was not found on the Blockchain."
    }
}

