import {OperationObject, OpKind} from "@taquito/rpc"
import {
    OperationContents,
    OperationContentsDelegation,
    OperationContentsOrigination,
    OperationContentsReveal,
    OperationContentsTransaction,
} from "@taquito/rpc/dist/types/types"
import {OperationUpdateEvent} from "./api"

/**
 * @description Operation objet with corresponding hash
 */
export interface CybOperationObject extends OperationObject {
    hash?: string
}

/**
 * @description Subtype of OperationContents in taquito containing only supported operation types
 */
export declare type SupportedOperationContents =
    OperationContentsReveal
    | OperationContentsTransaction
    | OperationContentsOrigination
    | OperationContentsDelegation

/**
 * Check if given operationContents is supported
 * @param op operationContents to check
 */
export const isSupportedOp = (op: OperationContents) => {
    return (
        op.kind === OpKind.TRANSACTION ||
        op.kind === OpKind.ORIGINATION ||
        op.kind === OpKind.DELEGATION ||
        op.kind === OpKind.REVEAL
    )
}

/**
 * Override of EventSource type to specify type of message events
 */
export interface OperationEventSource extends Omit<EventSource, 'onmessage'> {
    onmessage: ((this: OperationEventSource, ev: MessageEvent<OperationUpdateEvent>) => any) | null;
}

/**
 * Protected context during operation injection
 */
export interface CybContextValues {
    /**
     * Params of preapply operation, used in inject operation
     */
    operations?: OperationObject[]
    level?: number
}
