/**
 * @description All endpoints of the CYB TransactionManager
 */
export enum TransactionManagerEndpoints {
    PROCESS_OPERATION = "/api/operation",
    DELETE_OPERATION = "/api/operation/delete",
    PROCESS_OPERATION_DRY_RUN = "/api/operation/dryRun",
    BAKER_IDENTITY = "/api/identity",
    SLOTS = "/api/slots",
    OPERATION = "/api/operation",
    OPERATION_SUBSCRIBE = "/api/operation/subscribe",
    OPERATIONS = "/api/operations",
}

/**
 * @description All endpoints of the CYB TransactionManager
 */
export enum TezosNodeEndpoint {
    BAKING_RIGHTS = "/chains/main/blocks/head/helpers/baking_rights",
    KYC_BAKERS = "/chains/main/blocks/head/context/contracts",
    BLOCK_HEAD = "/chains/main/blocks/head/header",
}

/**
 * @description All statuses for operations
 */
export enum OperationStatus {
    BAKED = "BAKED",
    BAKED_BY_ANOTHER = "BAKED_BY_ANOTHER",
    DELETED = "DELETED",
    IN_PROGRESS = "IN_PROGRESS",
    PENDING = "PENDING",
    REJECTED = "REJECTED",
    STALE = "STALE",
}

/**
 * @description All fee modes
 */
export enum FeeMode {
    FEES = "FEES",
    TRANSACTION = "TRANSACTION",
}

/**
 * @description Http methods
 */
export enum HttpMethod {
    GET = "GET",
    POST = "POST",
    DELETE = "DELETE",
}
