interface Wait {
    resolve: (value: (void | PromiseLike<void>)) => void,
    err: (reason?: any) => void,
}

export class Semaphore {

    private _locked = false

    private _waiting: Wait[] = []

    acquire(): Promise<void> {
        if (!this._locked) {
            this._locked = true
            return Promise.resolve()
        } else {
            return new Promise((resolve, err) => {
                this._waiting.push({
                    resolve: resolve,
                    err: err,
                })
            })
        }
    }

    release() {
        this._locked = false
        if (this._waiting.length > 0) {
            this._locked = true
            this._waiting.shift()?.resolve()
        }
    }

    get isLocked() {
        return this._locked
    }

}
