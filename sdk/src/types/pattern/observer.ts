export interface Observer<T> {
    _update(data: T): void
}

export interface Subject<T> {
    _attach(observer: Observer<T>): void

    _update(data: T): void
}
