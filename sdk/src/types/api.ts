import {FeeMode, OperationStatus} from "./enums"
import {CybOperationObject} from "./operation"
import {OperationContents} from "@taquito/rpc/dist/types/types"

/**
 * @description response after submitting an operation to be baked
 */
export interface ProcessOperationResponse {
    message: string
    operationId: string
    operationHash: string
    token?: CybToken
}

/**
 * @description token information provided by the transaction manager
 */
export interface CybToken {
    id: string
    name: string
    expirationDate: string
    creationDate: string
    roles: string[]
    owner: string
    revoked: boolean
    jwtToken: string
}

/**
 * @description An operation in CYB
 */
export interface Operation {
    id: string
    lastStatusUpdate: Date
    creationDate: Date
    level?: number
    requestedLevel?: number
    fetchedLevel?: number
    timesFetched: number
    errorMessage?: string
    bakerAddress?: string
    status: OperationStatus
    owner: string
    totalFee?: number
    estimatedConsumption?: Consumption
    actualConsumption?: Consumption
    tzOperation: TzOperation
}

/**
 * @description A Tezos operation in CYB
 */
export interface TzOperation {
    hash: string
    contents: OperationContents[]
    branch: string
    signature: string
}

export interface Consumption {
    totalStorageSize: number
    totalConsumedGas: number
}

/**
 * @description Event received when subscribing to operations
 */
export interface OperationUpdateEvent {
    date: Date
    operation: Operation
}

/**
 * @description Insert Operation objet to be used to call CYB service
 */
export interface OperationCreate {
    tzOperation?: CybOperationObject
    level?: number
}

/**
 * @description Delete Operation
 */
export interface OperationDelete {
    id: string
}

/**
 * @description Identity of a CYB Baker
 */
export interface BakerIdentity {
    name: string
    nbBakingSlotSearch: number
    bakerAddress: string
    kycSmartContractAddress: string
    allowUnauthenticated: boolean
    feeMode: FeeMode
    minimalNtzFees: number
    minimalNtzPerGasUnit: number
    minimalNtzPerByte: number
}


/**
 * @description Information about baking rights of a delegate on a specific level
 */
export interface TmBakingSlot {
    level: number
    cycle: number
    baker: string
}

/**
 * @description Information about baking rights of a delegate on a specific level
 */
export interface HeaderLevel {
    level: number
    hash: string
}

/**
 * @description Information about the first matching baker
 */
export interface MatchingBaker {
    certifiedBaker: CertifiedBaker,
    level: number,
    currentLevel: number
}


/**
 * @description Information about baking rights of a delegate on a specific level
 */
export interface BakingSlot {
    level: number
    delegate: string
    priority: number
    estimated_time: Date
}

/**
 * @description Format of KYC contract storage
 */
export interface KycStorage {
    prim: string
    args: any[]
}

/**
 * Format of certified baker information in the KYC contract storage
 */
export interface CertifiedBaker {
    address: string
    baker_infos: Map<string, string>
}
