import {
    defaultRPCOptions,
    OperationHash,
    OpKind,
    PreapplyParams,
    PreapplyResponse,
    RpcClient,
    RPCOptions,
} from "@taquito/rpc"
import {encodeOpHash} from "@taquito/utils"
import {HttpBackend} from "@taquito/http-utils"
import {ForgeParams} from "@taquito/taquito"
import CybAuth from "./auth/CybAuth"
import {FeeMode, OperationStatus, TezosNodeEndpoint, TransactionManagerEndpoints} from "../types/enums"
import {
    BakerIdentity,
    BakingSlot,
    CertifiedBaker,
    HeaderLevel,
    KycStorage,
    Operation,
    OperationCreate,
    OperationDelete,
    ProcessOperationResponse,
    TmBakingSlot,
} from "../types/api"
import {Observer, Subject} from "../types/pattern/observer"
import EventSource from "eventsource"
import CybContext from "./CybContext"
import {CybSdk} from "./CybSdk"
import {KycContractNotFoundException} from "../types/error"

interface GetParam {
    key: string,
    value: string,
}

const OP_SIZE_REVEAL = 64

/**
 * @description CYB RPC client based on Taquito to prepare Tezos operations
 */
export default class CybRpcClient extends RpcClient implements Subject<ProcessOperationResponse> {

    constructor(
        /**
         * Authentication to CYB service
         * @private
         */
        private readonly cybAuth: CybAuth,
        /**
         * Context used during an injection
         * @private
         */
        private cybContext: CybContext,
        /**
         * Taquito RpcClient params
         */
        nodeUrl: string,
        chain?: string,
        httpBackend?: HttpBackend,
    ) {
        super(nodeUrl, chain, httpBackend)
    }

    //
    // Observer Pattern Implementation
    //

    /**
     * Observers that subscribed to submitted operations
     * @private
     */
    private observers: Observer<ProcessOperationResponse>[] = []

    _attach(observer: Observer<ProcessOperationResponse>) {
        this.observers.push(observer)
    }

    _update(data: ProcessOperationResponse) {
        for (let observer of this.observers) {
            observer._update(data)
        }
    }

    //
    // Override Taquito RpcClient Methods
    //

    async preapplyOperations(
        ops: PreapplyParams,
        {block}: RPCOptions = defaultRPCOptions,
    ): Promise<PreapplyResponse[]> {
        // Here, we keep the operation structure to re-inject it later in the cyb service
        await this.cybContext.acquireWithValues({operations: ops})

        return super.preapplyOperations(ops, {block})
    }

    async forgeOperations(
        ops: ForgeParams,
        {block}: RPCOptions = defaultRPCOptions,
    ): Promise<string> {
        const revealOperation = ops.contents[0]
        if (revealOperation.kind == OpKind.REVEAL) {
            const identity = await this.getBakerIdentity()

            let revealCost = identity.minimalNtzFees / ops.contents.length
                + identity.minimalNtzPerByte * OP_SIZE_REVEAL
                + identity.minimalNtzPerGasUnit * parseInt(revealOperation.gas_limit, 10)

            switch (identity.feeMode) {
                case FeeMode.TRANSACTION:
                    const feeTransaction = ops.contents[ops.contents.length - 1]
                    if (feeTransaction.kind == OpKind.TRANSACTION
                        && feeTransaction.destination == identity.bakerAddress) {
                        feeTransaction.amount = `${parseInt(feeTransaction.amount, 10) + CybSdk._nTzTouTz(revealCost)}`
                        revealOperation.fee = "0"
                    }
                    break
                case FeeMode.FEES:
                    revealOperation.fee = `${CybSdk._nTzTouTz(revealCost)}`
                    break
            }
        }
        return super.forgeOperations(ops, {block})
    }

    async injectOperation(signedOpBytes: string): Promise<OperationHash> {
        // Compute hash
        const operationHash: string = encodeOpHash(JSON.stringify(signedOpBytes))

        const context = this.cybContext.getContext()
        const requestBody: OperationCreate = {
            tzOperation: {
                ...(context.operations as PreapplyParams)[0],
                hash: operationHash,
            },
            level: context.level,
        }

        let data: ProcessOperationResponse =
            await this.post<ProcessOperationResponse>(
                this.cybAuth.endpointUrl(TransactionManagerEndpoints.PROCESS_OPERATION),
                requestBody,
            )

        this._update(data)
        this.cybContext.release()
        return operationHash
    }

    //
    // CYB Specific methods
    //

    async getBakerIdentity(): Promise<BakerIdentity> {
        return this.noAuthGet<BakerIdentity>(
            this.cybAuth.endpointUrl(TransactionManagerEndpoints.BAKER_IDENTITY),
        )
    }


    async getTmBakingSlots(url?: string): Promise<TmBakingSlot[]> {
        if (url){
            return this.noAuthGet<TmBakingSlot[]>(
                url + TransactionManagerEndpoints.SLOTS,
            )
        }
        else {
            return this.noAuthGet<TmBakingSlot[]>(
                this.cybAuth.endpointUrl(TransactionManagerEndpoints.SLOTS),
            )
        }
    }

    async getHeader(
    ): Promise<HeaderLevel> {
        return this.noAuthGet<HeaderLevel>(
            `${this.url}${TezosNodeEndpoint.BLOCK_HEAD}`,
            [],
        )
    }



    async getBakingSlots(
        levels: number[] = [],
        cycles: number[] = [],
        delegates: string[] = [],
        maxPriority?: number,
        all?: boolean,
    ): Promise<BakingSlot[]> {
        let params: GetParam[] = []
        levels.forEach((it) => params.push({
            key: "level",
            value: `${it}`,
        }))
        cycles.forEach((it) => params.push({
            key: "cycle",
            value: `${it}`,
        }))
        delegates.forEach((it) => params.push({
            key: "delegate",
            value: `${it}`,
        }))

        if (maxPriority !== undefined) {
            params.push({
                key: "max_priority",
                value: `${maxPriority}`,
            })
        }

        if (all !== undefined) {
            params.push({
                key: "all",
                value: `${all}`,
            })
        }

        return this.noAuthGet<BakingSlot[]>(
            `${this.url}${TezosNodeEndpoint.BAKING_RIGHTS}`,
            params,
        )
    }

    async getCertifiedBakers(kycContractAddress?: string): Promise<CertifiedBaker[]> {
        let kycContractAddressCurrent = kycContractAddress
        if (kycContractAddressCurrent == undefined){
            const identity = await this.getBakerIdentity()
            kycContractAddressCurrent = identity.kycSmartContractAddress
        }

        let storage = await this.noAuthGet<KycStorage>(
            `${this.url}${TezosNodeEndpoint.KYC_BAKERS}/${kycContractAddressCurrent}/storage`,
        )
            .catch((e) => {
                if (e.status === 404) {
                    throw new KycContractNotFoundException()
                } else throw e
            })

        return storage.args[0].map((certifiedBaker: any) => {
            let infos: Map<string, string> = new Map<string, string>()
            certifiedBaker.args[1].forEach(
                (property: any) => infos.set(property.args[0].string, property.args[1].string),
            )
            return {
                address: certifiedBaker.args[0].string,
                baker_infos: infos,
            }
        })
    }

    async getOperation(id: string, token?: string): Promise<Operation> {
        return this.get<Operation>(
            this.cybAuth.endpointUrl(TransactionManagerEndpoints.OPERATION),
            [
                {
                    key: "id",
                    value: id,
                },
            ],
            token,
        )
    }

    async deleteOperation(id: string, token?: string): Promise<Operation> {
        const operationDelete: OperationDelete = {id: id}
        return this.post<Operation>(
            this.cybAuth.endpointUrl(TransactionManagerEndpoints.DELETE_OPERATION),
            operationDelete,
            token,
        )
    }

    getOperationEventSource(): EventSource {
        const url = `${this.cybAuth.endpointUrl(TransactionManagerEndpoints.OPERATION_SUBSCRIBE)}`
        return new EventSource(url, {
            headers: {
                ...this.cybAuth.authorizationHeader(),
                "Accept": "text/event-stream",
            },
        })
    }

    async getOperations(
        id?: string,
        level?: number,
        bakerAddress?: string,
        status?: OperationStatus,
        source?: string,
        token?: string,
    ): Promise<Operation[]> {
        let params = []

        if (id !== undefined) {
            params.push({
                key: "id",
                value: id,
            })
        }
        if (level !== undefined) {
            params.push({
                key: "level",
                value: level.toString(),
            })
        }
        if (bakerAddress !== undefined) {
            params.push({
                key: "bakerAddress",
                value: bakerAddress,
            })
        }
        if (status !== undefined) {
            params.push({
                key: "status",
                value: status.toString(),
            })
        }
        if (source !== undefined) {
            params.push({
                key: "source",
                value: source,
            })
        }

        return this.get<Operation[]>(
            this.cybAuth.endpointUrl(TransactionManagerEndpoints.OPERATIONS),
            params,
            token,
        )
    }

    private post<T>(url: string, body: any, token?: string, authenticated: boolean = true): Promise<T> {
        return this.httpBackend.createRequest<T>(
            {
                url: url,
                method: "POST",
                headers: authenticated ? this.cybAuth.authorizationHeader(token) : {},
            },
            body,
        )
    }

    private get<T>(url: string, params: GetParam[] = [], token?: string, authenticated: boolean = true): Promise<T> {
        return this.httpBackend.createRequest<T>(
            {
                url: `${url}${CybRpcClient.generateGetParams(params)}`,
                method: "GET",
                headers: authenticated ? this.cybAuth.authorizationHeader(token) : {},
            },
        )
    }

    private noAuthGet<T>(url: string, params: GetParam[] = []): Promise<T> {
        return this.get(url, params, undefined, false)
    }

    private static generateGetParams(params: GetParam[]): string {
        let parameters = ""
        for (let i = 0; i < params.length; i++) {
            let p = `${params[i].key}=${params[i].value}`
            parameters += i == 0 ? `?${p}` : `&${p}`
        }
        return parameters
    }

}
