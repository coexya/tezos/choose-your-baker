import {ForgeParams, OpKind, ParamsWithKind, TezosToolkit, WalletOperationBatch} from "@taquito/taquito"
import {HttpBackend} from "@taquito/http-utils"
import CybRpcClient from "./CybRpcClient"
import CybAuth from "./auth/CybAuth"
import CybOperationService from "./CybOperationService"
import CybContext from "./CybContext"
import {CybContextValues, SupportedOperationContents} from "../types/operation"
import {BakerIdentity, BakingSlot, CertifiedBaker, HeaderLevel, MatchingBaker, Operation, TmBakingSlot} from "../types/api"
import {UnsupportedOperationKindException} from "../types/error"
import {FeeMode, OperationStatus} from "../types/enums"
import {BakingSlotParams} from "../types/other"
import {WalletParamsWithKind} from "@taquito/taquito/dist/types/wallet/wallet"
import {OperationContents} from "@taquito/rpc/dist/types/types"
import {BatchWalletOperation} from "@taquito/taquito/dist/types/wallet/batch-operation"

export {CybContext, CybOperationService, CybAuth, CybRpcClient}

export * from "../types/api"
export * from "../types/enums"
export * from "../types/error"
export * from "../types/operation"
export * from "../types/other"

export class CybSdk extends TezosToolkit {

    private readonly _operationService: CybOperationService

    private readonly _cybContext: CybContext

    constructor(auth: CybAuth, nodeUrl: string, chain?: string, httpBackend?: HttpBackend,
    ) {
        super(nodeUrl)
        this._cybContext = new CybContext()
        super.setRpcProvider(new CybRpcClient(auth, this._cybContext, nodeUrl, chain, httpBackend))

        this._operationService = new CybOperationService(this.cybRpc)
        this.cybRpc._attach(this._operationService)
    }

    get cybRpc(): CybRpcClient {
        return this.rpc as CybRpcClient
    }

    /**
     * Provide the service to handle operations on the transaction manager
     */
    get operationService(): CybOperationService {
        return this._operationService
    }

    get isContextLocked() {
        return this._cybContext.isLocked
    }

    /**
     * Modify the context for operation sending
     * @param contextValues
     */
    modifyContext(contextValues: CybContextValues) {
        this._cybContext.setContext(contextValues)
    }

    /**
     * Provide all the information about the chosen baker
     */
    async getBakerIdentity() : Promise<BakerIdentity> {
        return this.cybRpc.getBakerIdentity()
    }

    /**
     * Provide all the information about the slots
     */
    async getTmBakingSlots() : Promise<TmBakingSlot[]> {
        return this.cybRpc.getTmBakingSlots()
    }

    /**
     * Provide the current level
     */
    async getHeader() : Promise<HeaderLevel> {
        return this.cybRpc.getHeader()
    }

    
    /**
     * Provide the most matching baker from a list of certified bakers
     */
    async getFirstMatchingBaker(certifiedBakers: CertifiedBaker[]): Promise<MatchingBaker|undefined> {

        //Retrieve the slots of all the certified bakers
        const slots = await Promise.all(
            certifiedBakers.map(async (element) => {
                const bakerUrl = element.baker_infos.get("url")
                if (bakerUrl){
                    const bakingSlots = await this.cybRpc.getTmBakingSlots(bakerUrl).catch(() => undefined)
                    if (bakingSlots){
                        return  {
                            "certifiedBaker": element,
                            "slots": bakingSlots
                        }
                    }
                    else {
                        return undefined
                    }
                }
                else {
                    return undefined
                }
            })
        )
       
        //Retrieve the current level
        const header = await this.getHeader()
        let certifiedBakerSelected: CertifiedBaker|undefined = undefined
        let targetLevel: number|undefined = undefined
        //I take the minimal slot greater than the current level
        slots.forEach(bakerSlots => {
            if (bakerSlots && bakerSlots.slots.length>0){
                bakerSlots.slots.forEach(cs => {
                    if (cs.level > header.level && (targetLevel==undefined || cs.level<targetLevel)){
                        targetLevel=cs.level
                        certifiedBakerSelected=bakerSlots.certifiedBaker
                    }
                })
            } 
        })

       if (certifiedBakerSelected && targetLevel){
            const result : MatchingBaker = {
                certifiedBaker: certifiedBakerSelected,
                level: targetLevel,
                currentLevel: header.level
            } 
            return result
        }
        
    }


    /**
     * Retrieve the list of delegates allowed to bake a block.
     * See https://tezos.gitlab.io/active/rpc.html#get-block-id-helpers-baking-rights
     * @param params
     */
    async getBakingSlots(
        params: BakingSlotParams,
    ): Promise<BakingSlot[]> {
        return this.cybRpc.getBakingSlots(
            params.levels,
            params.cycles,
            params.delegates,
            params.maxPriority,
            params.all,
        )
    }

    /**
     * Use the KYC contract specified in the chosen baker to discover all certified bakers and their information
     */
    async getCertifiedBakers(kycContractAddress?: string): Promise<CertifiedBaker[]> {
        return this.cybRpc.getCertifiedBakers(kycContractAddress)
    }

    /**
     * Generate a wallet batch operation from a given operation, useful when updating an operation
     * on the transaction manager
     * @param operation
     */
    generateBatchFrom(operation: Operation): WalletOperationBatch {
        return this.getBatchFrom(operation.tzOperation.contents)
    }

    /**
     * Generate a wallet batch operation from a list of transactions, useful with cybEstimate
     * @param params
     */
    generateBatch(params: ParamsWithKind[]): WalletOperationBatch {
        const batch = this.wallet.batch()
        return CybSdk.completeBatchWith(batch, params)
    }

    /**
     * Estimate the cost of the given transactions, taking into account the configuration of the chosen baker.
     * @param params
     */
    async cybEstimate(params: ParamsWithKind[]): Promise<ParamsWithKind[]> {
        let parameters: ParamsWithKind[] = params.map((p) => {
            return {...p}
        })

        const estimations = await this.estimate.batch(parameters)

        let revealOffset = 0
        if (estimations.length == parameters.length + 1) { // reveal operation was added by taquito
            revealOffset = 1
        }

        const identity = await this.getBakerIdentity()

        let totalNtzFee = 0
        for (let i = 0; i < parameters.length; i++) {
            let param = parameters[i]

            if (param.kind == OpKind.ACTIVATION) {
                throw new UnsupportedOperationKindException(OpKind.ACTIVATION)
            }

            let gasLimit = (estimations[i + revealOffset].gasLimit)
            if (param.gasLimit !== undefined && Number(param.gasLimit) > gasLimit) {
                gasLimit = Number(param.gasLimit)
            }

            let currentNtzCost = identity.minimalNtzPerByte * Number(estimations[i + revealOffset].opSize)
                + identity.minimalNtzPerGasUnit * gasLimit

            switch (identity.feeMode) {
                case FeeMode.FEES:
                    param.fee = CybSdk._nTzTouTz(identity.minimalNtzFees / (parameters.length + revealOffset) + currentNtzCost)
                    break
                case FeeMode.TRANSACTION:
                    param.fee = 0
                    totalNtzFee += currentNtzCost
                    break
            }
        }

        if (identity.feeMode == FeeMode.TRANSACTION) {
            const estimate = await this.estimate.transfer({
                to: identity.bakerAddress,
                amount: CybSdk._nTzToTz(totalNtzFee + identity.minimalNtzFees),
                fee: 0,
            })

            const feeTransactionNtzCost = identity.minimalNtzPerByte * Number(estimate.opSize)
                + identity.minimalNtzPerGasUnit * estimate.gasLimit

            parameters.push({
                kind: OpKind.TRANSACTION,
                to: identity.bakerAddress,
                amount: CybSdk._nTzToTz(identity.minimalNtzFees + totalNtzFee + feeTransactionNtzCost),
                fee: 0,
            })
        }

        return parameters
    }

    /**
     * Update pending operation
     */
    async updateCurrentOperation(params: ParamsWithKind[]): Promise<BatchWalletOperation> {
        const source = await this.signer.publicKeyHash()
        const pendingOperations = await this.operationService.getOperations(
            undefined,
            undefined,
            undefined,
            OperationStatus.PENDING,
            source,
        )

        if (pendingOperations.length > 1) {
            throw Error("There are two PENDING operations with same source, please delete one to proceed.")
        }
        if (pendingOperations.length < 1) {
            return this.generateBatch(params).send()
        }

        const operation = pendingOperations[0]

        // Delete and make a new batch from deleted operation
        const deletedOperation = await this.operationService.deleteOperation({id: operation.id})

        // Verify the signature
        const tzOperation = deletedOperation.tzOperation
        const forgeParams: ForgeParams = {
            branch: tzOperation.branch,
            contents: tzOperation.contents,
        }
        const bytes = await this.cybRpc.forgeOperations(forgeParams)
        const signature = await this.signer.sign(bytes, new Uint8Array([3]))

        if (signature.prefixSig === tzOperation.signature) {

            const batch = this.generateBatchFrom(deletedOperation)
            return CybSdk.completeBatchWith(batch, params).send()

        } else {
            throw Error("The signature is not correct.")
        }
    }

    /**
     * Convert nanotez to tez
     * @param amount
     */
    static _nTzToTz(amount: number): number {
        return this._nTzTouTz(amount) / 1000000
    }

    /**
     * Convert nanotez to mutez
     * @param amount
     */
    static _nTzTouTz(amount: number): number {
        return Math.ceil(amount / 1000)
    }

    private static _toWalletParamsWithKind(param: ParamsWithKind): WalletParamsWithKind {
        if (param.kind == OpKind.ACTIVATION) {
            throw new UnsupportedOperationKindException(OpKind.ACTIVATION)
        }
        return param
    }

    private getBatchFrom(contents: OperationContents[]): WalletOperationBatch {
        const batch = this.wallet.batch()

        for (let content of contents) {
            const operation: SupportedOperationContents = content as unknown as SupportedOperationContents

            switch (operation.kind) {
                case OpKind.REVEAL:
                    // Reveal operation should add itself again at submit with taquito
                    break
                case OpKind.TRANSACTION:
                    batch.withTransfer({
                        to: operation.destination,
                        amount: Number(operation.amount),
                        fee: Number(operation.fee),
                        parameter: operation.parameters,
                        gasLimit: Number(operation.gas_limit),
                        storageLimit: Number(operation.storage_limit),
                        mutez: true,
                    })
                    break
                case OpKind.ORIGINATION:
                    batch.withOrigination({
                        fee: Number(operation.fee),
                        gasLimit: Number(operation.gas_limit),
                        storageLimit: Number(operation.storage_limit),
                        balance: operation.balance,
                        delegate: operation.delegate,
                        code: operation.script?.code as object[],
                        init: operation.script?.storage as object,
                        mutez: true,
                    })
                    break
                case OpKind.DELEGATION:
                    batch.withDelegation({
                        fee: Number(operation.fee),
                        storageLimit: Number(operation.storage_limit),
                        gasLimit: Number(operation.gas_limit),
                        delegate: operation.delegate,
                    })
                    break
            }
        }
        return batch
    }

    private static completeBatchWith(batch: WalletOperationBatch, params: ParamsWithKind[]): WalletOperationBatch {
        batch.with(
            params.map((p) => CybSdk._toWalletParamsWithKind(p)),
        )
        return batch
    }

}
