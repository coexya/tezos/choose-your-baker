import {Semaphore} from "../types/pattern/semaphore"
import {CybContextValues} from "../types/operation"
import {ContextLockedException} from "../types/error"

export default class CybContext {

    private _semaphore: Semaphore = new Semaphore()

    private _context: CybContextValues = {}

    get isLocked(): boolean {
        return this._semaphore.isLocked
    }

    getContext(): CybContextValues {
        return {...this._context}
    }

    setContext(contextValues: CybContextValues) {
        if (!this._semaphore.isLocked) {
            this._setContext(contextValues)
        } else {
            throw new ContextLockedException()
        }
    }

    async acquireWithValues(contextValues: CybContextValues) {
        await this._semaphore.acquire()
        this._setContext(contextValues)
    }

    release() {
        this._setContext({operations: []})
        this._semaphore.release()
    }

    private _setContext(contextValues: CybContextValues) {
        if (contextValues.operations !== undefined) {
            this._context.operations = contextValues.operations
        }
        if (contextValues.level !== undefined) {
            this._context.level = contextValues.level
        }
    }

}
