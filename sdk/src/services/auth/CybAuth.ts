import {TransactionManagerEndpoints} from "../../types/enums"

export default class CybAuth {

    constructor(
        private readonly _url: string,
        private readonly _token?: string,
    ) {
    }

    authorizationHeader(token?: string): { Authorization?: string } {
        if (token !== undefined) {
            return {"Authorization": `Bearer ${token}`}
        }
        return this._token !== undefined ? {"Authorization": `Bearer ${this._token}`} : {}
    }

    endpointUrl(endpoint: TransactionManagerEndpoints) {
        return `${this._url}${endpoint}`
    }

}
