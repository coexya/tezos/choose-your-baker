import {ContractAbstraction, MichelsonMap, TezosToolkit, Wallet} from "@taquito/taquito"
import {HttpBackend} from "@taquito/http-utils"
import {RpcClient} from "@taquito/rpc"
import {WrongParameterException} from "../types/error"

const TEZOS_CONTRACT_ADDRESS_LENGTH = 36
const TEZOS_CONTRACT_ADDRESS_PREFIX = "KT1"

export class CybKycAdminSdk extends TezosToolkit {

    private _kycContract: ContractAbstraction<Wallet> | undefined

    constructor(
        private kycContractAddress: string,
        nodeUrl: string,
        chain?: string,
        httpBackend?: HttpBackend,
    ) {
        super(nodeUrl)
        CybKycAdminSdk.checkContractFormat(kycContractAddress)
        super.setRpcProvider(new RpcClient(nodeUrl, chain, httpBackend))
    }

    async addManager(address: string) {
        const kycContract = await this.kycContract()
        const op = await kycContract.methods.add_manager(address).send()
        await op.confirmation()
    }

    async removeManager(address: string) {
        const kycContract = await this.kycContract()
        const op = await kycContract.methods.remove_manager(address).send()
        await op.confirmation()
    }

    async addBaker(address: string, params: Map<string, string>) {
        CybKycAdminSdk.checkAddBakerParams(params)
        const kycContract = await this.kycContract()
        const map: MichelsonMap<string, string> = new MichelsonMap()
        params.forEach(((value, key) => map.set(key, value)))
        const op = await kycContract.methods.add_baker(address, map).send()
        await op.confirmation()
    }

    async removeBaker(address: string) {
        const kycContract = await this.kycContract()
        const op = await kycContract.methods.remove_baker(address).send()
        await op.confirmation()
    }

    private async kycContract(): Promise<ContractAbstraction<Wallet>> {
        if (this._kycContract === undefined) {
            this._kycContract = await this.wallet.at(this.kycContractAddress)
        }
        return this._kycContract
    }

    private static checkAddBakerParams(params: Map<string, string>) {
        if (!params.has("country")
            || !params.has("company")
            || !params.has("url")
        ) {
            throw new WrongParameterException("Params should contain fields : " +
                "'address' " +
                "'country' " +
                "'company' " +
                "'url' ")
        }
    }

    private static checkContractFormat(address: string) {
        if (address.length !== TEZOS_CONTRACT_ADDRESS_LENGTH
            || address.substring(0, 3) !== TEZOS_CONTRACT_ADDRESS_PREFIX
        ) {
            throw new WrongParameterException("Contract address has the wrong format.")
        }
    }
}
