import CybRpcClient from "./CybRpcClient"
import {Operation, ProcessOperationResponse} from "../types/api"
import {Observer} from "../types/pattern/observer"
import {UnknownOperationException, WrongParameterException} from "../types/error"
import EventSource from "eventsource"
import {OperationIdentification} from "../types/other"
import {OperationStatus} from "../types/enums"

export default class CybOperationService implements Observer<ProcessOperationResponse> {

    private _operationSubmitResponses: ProcessOperationResponse[] = []

    constructor(
        private readonly cybRpc: CybRpcClient,
    ) {
    }

    /**
     * Observer pattern implementation
     * @param data
     */
    _update(data: ProcessOperationResponse): void {
        this._operationSubmitResponses.push(data)
    }

    /**
     * Get an operation from the transaction manager, using the given id or hash
     * To get an operation from its hash it needs to be an operation that was submitted with the same SDK.
     * @param identification
     */
    async getOperation(
        identification: OperationIdentification,
    ): Promise<Operation> {
        const operationResponse = this.getOperationFromCache(identification)
        if (operationResponse != undefined) {
            return this.cybRpc.getOperation(operationResponse.operationId, operationResponse.token?.jwtToken)
        } else if (identification.id !== undefined) {
            return this.cybRpc.getOperation(identification.id)
        } else {
            throw new UnknownOperationException()
        }
    }

    /**
     * Delete a PENDING operation from the transaction manager, using the given id or hash
     * To get an operation from its hash it needs to be an operation that was submitted with the same SDK.
     * @param identification
     */
    async deleteOperation(
        identification: OperationIdentification,
    ): Promise<Operation> {
        const operationResponse = this.getOperationFromCache(identification)
        if (operationResponse != undefined) {
            return this.cybRpc.deleteOperation(operationResponse.operationId, operationResponse.token?.jwtToken)
        } else if (identification.id !== undefined) {
            return this.cybRpc.deleteOperation(identification.id)
        } else {
            throw new UnknownOperationException()
        }
    }

    /**
     * Provide an EventSource to follow operation status update
     * See https://developer.mozilla.org/fr/docs/Web/API/EventSource
     */
    getOperationEventSource(): EventSource {
        return this.cybRpc.getOperationEventSource()
    }

    async getOperations(
        id?: string,
        level?: number,
        bakerAddress?: string,
        status?: OperationStatus,
        source?: string,
    ): Promise<Operation[]> {
        return this.cybRpc.getOperations(
            id,
            level,
            bakerAddress,
            status,
            source,
        )
    }

    private static checkOperationIdentification(identification: OperationIdentification) {
        if (identification.id === undefined && identification.hash === undefined)
            throw new WrongParameterException("Parameter should contain either 'hash' or 'id'.")
    }

    private getOperationFromCache(identification: OperationIdentification): ProcessOperationResponse | undefined {
        CybOperationService.checkOperationIdentification(identification)
        return this._operationSubmitResponses.find(response =>
            (identification.id !== undefined && response.operationId === identification.id)
            || (identification.hash !== undefined && response.operationHash === identification.hash),
        )
    }

}
