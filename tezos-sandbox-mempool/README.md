[Index](/README.md) | [Specification](/documentation/specification.md) | [Sandbox/demo](/demo/README.md)  | [Deployment](/documentation/deployment.md) | [Typescript SDK Quick start](/documentation/Quickstart.md) | [Examples](/documentation/Examples.md) | [Tezos Sandbox Mempool](/tezos-sandbox-mempool/README.md)

# Context of usage

This image provides a way to launch a tezos sandbox environment with 5 nodes and 5 bootstrap accounts as bakers.
The baker binary has the option *--mempool* available to prioritize some operations from a private mempool before merging it with the node mempool operations.

# Available parameters

__Network parameters__
* **BASE_PORT**: port of the first sandbox node. The 5 nodes will be exposed through 5 consecutive ports starting at this base port for the first node. Default value: **20000**.
* **BLOCK_LIMIT**: limit of blocks before the network stops. Default value: **200_000_000**.
* **BLOCKS_PER_CYCLE**: number of blocks per cycle. Default value: **128**.
* **ROOT_PATH**: path where data are stored with the docker container. Default value: **/tmp/zz-mininet-test**
* **TIME_BETWEEK_BLOCKS**: the minimal time between 2 consecutive blocks. Default value: **5**.

__Baker parameters__
* **PRIVATE_BAKERS**: number of baker that will use a private mempool. Value between 0 and 5. Default value: **1**.
* **CYB_BAKER_URL_0**: url to retrieve the private mempool for private baker 1. Used only if **PRIVATE_BAKERS** >= 1. Default value: **http://localhost:9091/api/mempool**.
* **CYB_BAKER_URL_1**: url to retrieve the private mempool for private baker 2. Used only if **PRIVATE_BAKERS** >= 2. Default value: **http://localhost:9092/api/mempool**.
* **CYB_BAKER_URL_2**: url to retrieve the private mempool for private baker 3. Used only if **PRIVATE_BAKERS** >= 3. Default value: **http://localhost:9093/api/mempool**.
* **CYB_BAKER_URL_3**: url to retrieve the private mempool for private baker 4. Used only if **PRIVATE_BAKERS** >= 4. Default value: **http://localhost:9094/api/mempool**.
* **CYB_BAKER_URL_5**: url to retrieve the private mempool for private baker 5. Used only if **PRIVATE_BAKERS** >= 5. Default value: **http://localhost:9095/api/mempool**.
* **MINIMAL_FEES**: minimal fees for private bakers. Default value: **0**.
* **MINIMAL_NANOTEZ_PER_GAS**: minimal nanotez per gas. Default value: **0**.
* **MINIMAL_NANOTEZ_PER_BYTE**: minimal nanotez per byte. Default value: **0**.

# Compose deployment sample
```yaml
version: '3.4'
volumes:
  tezos-data:
services:
  node:
    image: 'coexyaedi/tezos-sandbox-mempool'
    container_name: 'tezos-sandbox-mempool'
    network_mode: host
    volumes:
      - 'tezos-data:/tezos-data'
    environment:
      PRIVATE_BAKERS: 1
      BLOCK_LIMIT: "200_000_000"
      CYB_BAKER_URL_0: "http://localhost:9091/api/mempool"
      ROOT_PATH: "/tezos-data/sandbox"
```
