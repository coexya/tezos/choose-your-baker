#!/usr/bin/env bash

alice="alice,edpkvGfYw3LyB1UcCahKQk4rF2tvbMUk8GFiTuMjL75uGXrpvKXhjn,tz1VSUr8wwNhLAzempoch5d6hLRiTh8Cjcjb,unencrypted:edsk3QoqBuvdamxouPhin7swCvkQNgq4jP5KZPbwWNnwdZpSpJiEbq"
bob="bob,edpkurPsQ8eUApnLUJ9ZPDvu98E8VNj4KtJa1aZr16Cr5ow5VHKnz4,tz1aSkwEot3L2kmUvcoxzjMomb9mvBNuzFK6,unencrypted:edsk3RFfvaFaxbHx8BMtEW1rKQcPtDML3LXjNqMNLCzC3wLC1bWbAt"

if [[ -z $BASE_PORT ]]; then BASE_PORT=20000; fi
if [[ -z $BLOCK_LIMIT ]]; then BLOCK_LIMIT="200_000_000"; fi
if [[ -z $BLOCKS_PER_CYCLE ]]; then BLOCKS_PER_CYCLE=128; fi
if [[ -z $ROOT_PATH ]]; then ROOT_PATH="/tmp/zz-mininet-test"; fi
if [[ -z $TIME_BETWEEN_BLOCKS ]]; then TIME_BETWEEN_BLOCKS=5; fi

flextesa mini-network \
  --root-path $ROOT_PATH \
  -P $BASE_PORT \
  --size 5 --time-between-blocks $TIME_BETWEEN_BLOCKS  \
  --blocks-per-cycle $BLOCKS_PER_CYCLE \
  --set-history-mode N000:archive \
  --set-history-mode N001:archive \
  --set-history-mode N002:archive \
  --set-history-mode N003:archive \
  --set-history-mode N004:archive \
  --number-of-bootstrap-accounts 5 \
  --tezos-node-binary tezos-node \
  --tezos-baker-binary tezos-baker-alpha.switch.sh \
  --tezos-endorser-binary tezos-endorser-alpha \
  --tezos-accuser-binary tezos-accuser-alpha \
  --tezos-client-binary tezos-client \
  --add-bootstrap-account="$alice@2_000_000_000_000" \
  --add-bootstrap-account="$bob@2_000_000_000_000" \
  --no-daemons-for=alice \
  --no-daemons-for=bob \
  --until-level $BLOCK_LIMIT "$@"
