#!/usr/bin/env bash

# Default variables
if [[ -z $PRIVATE_BAKERS ]]; then PRIVATE_BAKERS=1; fi
if [[ -z $CYB_BAKER_URL_0 ]]; then CYB_BAKER_URL_0="http://localhost:9091/api/mempool"; fi
if [[ -z $CYB_BAKER_URL_1 ]]; then CYB_BAKER_URL_1="http://localhost:9092/api/mempool"; fi
if [[ -z $CYB_BAKER_URL_2 ]]; then CYB_BAKER_URL_2="http://localhost:9093/api/mempool"; fi
if [[ -z $CYB_BAKER_URL_3 ]]; then CYB_BAKER_URL_3="http://localhost:9094/api/mempool"; fi
if [[ -z $CYB_BAKER_URL_4 ]]; then CYB_BAKER_URL_4="http://localhost:9095/api/mempool"; fi
if [[ -z $MINIMAL_FEES ]]; then MINIMAL_FEES=0; fi
if [[ -z $MINIMAL_NANOTEZ_PER_GAS ]]; then MINIMAL_NANOTEZ_PER_GAS=0; fi
if [[ -z $MINIMAL_NANOTEZ_PER_BYTE ]]; then MINIMAL_NANOTEZ_PER_BYTE=0; fi

# Launch baker
if [[ "$@" =~ ^.*bootacc-([0-9]+).*$ && ${BASH_REMATCH[1]} < ${PRIVATE_BAKERS} ]]
then
	echo "Starting private mempool baker"
	CYB_BAKER_URL="CYB_BAKER_URL_${BASH_REMATCH[1]}"
	tezos-baker-alpha $@ --operations-pool ${!CYB_BAKER_URL} --minimal-fees ${MINIMAL_FEES} --minimal-nanotez-per-gas-unit ${MINIMAL_NANOTEZ_PER_GAS} --minimal-nanotez-per-byte ${MINIMAL_NANOTEZ_PER_BYTE}
else
	echo "Starting normal baker"
	tezos-baker-alpha $@
fi
