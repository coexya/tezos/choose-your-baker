package eu.coexya.cyb.tm.rest.configuration

import eu.coexya.cyb.business.configuration.TezosConfig
import eu.coexya.cyb.common.enums.AuthenticationRole
import eu.coexya.cyb.tm.rest.authentication.SecurityContextRepository
import io.swagger.v3.oas.models.Components
import io.swagger.v3.oas.models.OpenAPI
import io.swagger.v3.oas.models.security.SecurityScheme
import org.springframework.beans.factory.annotation.Value
import org.springframework.context.annotation.Bean
import org.springframework.context.annotation.Configuration
import org.springframework.http.HttpHeaders.WWW_AUTHENTICATE
import org.springframework.http.HttpMethod
import org.springframework.http.HttpStatus
import org.springframework.security.config.annotation.web.reactive.EnableWebFluxSecurity
import org.springframework.security.config.web.server.ServerHttpSecurity
import org.springframework.security.core.AuthenticationException
import org.springframework.security.web.server.SecurityWebFilterChain
import org.springframework.web.server.ServerWebExchange
import reactor.core.publisher.Mono

@Configuration
@EnableWebFluxSecurity
class ApplicationSecurity(
    private val tezosConfig: TezosConfig,
    @Value("\${sse.enabled:false}") private val sseEnabled: Boolean,
) {

    @Bean
    fun securityWebFilterChain(
        http: ServerHttpSecurity,
        securityContextRepository: SecurityContextRepository,
    ): SecurityWebFilterChain {
        http.csrf().disable()
        http.httpBasic().disable()
        http.securityContextRepository(securityContextRepository)
        http.authorizeExchange { exchanges ->
            // Authorize every OPTIONS request for browser verification purpose.
            exchanges.pathMatchers(HttpMethod.OPTIONS, "/**").permitAll()

            // Accessible without authentication
            exchanges.pathMatchers(HttpMethod.GET, "/api/identity").permitAll()
            exchanges.pathMatchers(HttpMethod.GET, "/api/slots").permitAll()

            val operationEndpoint = "/api/operation"

            if(!sseEnabled) {
                exchanges.pathMatchers(HttpMethod.GET, "$operationEndpoint/subscribe")
                    .denyAll()
            }

            if (tezosConfig.allowUnauthenticated) {

                exchanges.pathMatchers(HttpMethod.GET, operationEndpoint)
                    .hasAnyRole(
                        AuthenticationRole.ADMIN,
                        AuthenticationRole.ISSUER,
                        AuthenticationRole.UNAUTHENTICATED_ISSUER,
                    )

                exchanges.pathMatchers(HttpMethod.GET, "$operationEndpoint/subscribe")
                    .hasAnyRole(
                        AuthenticationRole.ISSUER,
                    )

                exchanges.pathMatchers(HttpMethod.POST, operationEndpoint).permitAll()

                exchanges.pathMatchers(HttpMethod.POST, "$operationEndpoint/delete")
                    .hasAnyRole(
                        AuthenticationRole.ISSUER,
                        AuthenticationRole.UNAUTHENTICATED_ISSUER,
                    )

            } else {

                exchanges.pathMatchers(HttpMethod.GET, operationEndpoint)
                    .hasAnyRole(
                        AuthenticationRole.ADMIN,
                        AuthenticationRole.ISSUER,
                    )

                exchanges.pathMatchers(HttpMethod.GET, "$operationEndpoint/subscribe")
                    .hasAnyRole(
                        AuthenticationRole.ISSUER,
                    )

                exchanges.pathMatchers(HttpMethod.POST, operationEndpoint)
                    .hasAnyRole(
                        AuthenticationRole.ISSUER,
                    )

                exchanges.pathMatchers(HttpMethod.POST, "$operationEndpoint/delete")
                    .hasAnyRole(
                        AuthenticationRole.ISSUER,
                    )

            }

            // By default, all endpoints are only accessible by an Admin
            exchanges.pathMatchers("/api/**").hasRole(AuthenticationRole.ADMIN)
            // Authorize all other requests (client, SwaggerUI).
            exchanges.anyExchange().permitAll()
        }
        http.exceptionHandling().authenticationEntryPoint { exchange: ServerWebExchange, _: AuthenticationException ->
            val response = exchange.response
            response.statusCode = HttpStatus.UNAUTHORIZED
            val requestedWith = exchange.request.headers["X-Requested-With"]
            if (requestedWith == null || !requestedWith.contains("XMLHttpRequest")) {
                response.headers.set(
                    WWW_AUTHENTICATE,
                    String.format(WWW_AUTHENTICATE_FORMAT, DEFAULT_REALM)
                )
            }
            exchange.mutate().response(response)
            Mono.empty()
        }

        return http.build()
    }

    @Bean
    fun customOpenAPI(): OpenAPI {
        return OpenAPI()
            .components(
                Components()
                    .addSecuritySchemes(
                        "bearer-key",
                        SecurityScheme().type(SecurityScheme.Type.HTTP).scheme("bearer").bearerFormat("JWT")
                    )
            )
    }

    companion object {
        private const val DEFAULT_REALM = "Realm"
        private const val WWW_AUTHENTICATE_FORMAT = "Basic realm=\"%s\""
    }
}
