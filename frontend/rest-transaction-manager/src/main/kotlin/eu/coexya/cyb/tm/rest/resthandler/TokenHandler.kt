package eu.coexya.cyb.tm.rest.resthandler

import eu.coexya.cyb.business.model.Token
import eu.coexya.cyb.business.model.TokenCreate
import eu.coexya.cyb.business.model.TokenPatch
import eu.coexya.cyb.business.service.TokenService
import eu.coexya.cyb.webcore.authentication.JwtTokenService
import kotlinx.coroutines.flow.Flow
import org.springframework.http.MediaType
import org.springframework.web.bind.annotation.*

@RestController
@RequestMapping("\${api.base-path:/api/tokens}")
class TokenHandler(
    private val tokenService: TokenService,
    private val jwtTokenService: JwtTokenService,
) {

    @RequestMapping(
        produces = [MediaType.APPLICATION_JSON_VALUE],
        consumes = [MediaType.APPLICATION_JSON_VALUE],
        method = [RequestMethod.POST]
    )
    suspend fun createToken(
        @RequestBody tokenDetails: TokenCreate,
    ): Token {
        return tokenService.createToken(tokenDetails).let {
            it.copy(
                jwtToken = if (tokenDetails.duration != null) {
                    jwtTokenService.generateVolatileToken(it.owner, it.roles, tokenDetails.duration!!)
                } else {
                    jwtTokenService.generatePersistentToken(it.owner, it.roles)
                }
            )
        }
    }

    @RequestMapping(
        produces = [MediaType.APPLICATION_JSON_VALUE],
        method = [RequestMethod.GET]
    )
    suspend fun getTokens(): Flow<Token> {
        return tokenService.findAll()
    }

    @RequestMapping(
        value = ["/revoke/{tokenId}"],
        produces = [MediaType.APPLICATION_JSON_VALUE],
        method = [RequestMethod.POST]
    )
    suspend fun revokeToken(
        @PathVariable("tokenId") tokenId: String,
    ): Token {
        return tokenService.patchToken(
            tokenId = tokenId,
            tokenDetails = TokenPatch(revoked = true)
        )
    }

    @RequestMapping(
        value = ["/{tokenId}"],
        produces = [MediaType.APPLICATION_JSON_VALUE],
        method = [RequestMethod.DELETE]
    )
    suspend fun deleteToken(
        @PathVariable("tokenId") tokenId: String,
    ) {
        tokenService.deleteToken(tokenId)
    }
}
