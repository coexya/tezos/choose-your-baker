package eu.coexya.cyb.tm.rest.resthandler

import eu.coexya.cyb.api.operation.ProcessOperationResponse
import eu.coexya.cyb.business.model.*
import eu.coexya.cyb.business.service.OperationNotificationService
import eu.coexya.cyb.business.service.OperationService
import eu.coexya.cyb.business.service.TokenService
import eu.coexya.cyb.common.criteria.OperationCriteria
import eu.coexya.cyb.common.enums.AuthenticationRole
import eu.coexya.cyb.common.enums.OperationStatus
import eu.coexya.cyb.tm.rest.data.pagedSorted
import eu.coexya.cyb.webcore.authentication.CustomUserDetails
import eu.coexya.cyb.webcore.authentication.JwtTokenService
import kotlinx.coroutines.flow.Flow
import org.springframework.http.MediaType
import org.springframework.security.core.annotation.AuthenticationPrincipal
import org.springframework.web.bind.annotation.*
import java.time.Duration

@RestController
@RequestMapping("\${api.base-path:/api}")
class OperationHandler(
    private val operationService: OperationService,
    private val operationNotificationService: OperationNotificationService?,
    private val tokenService: TokenService,
    private val jwtTokenService: JwtTokenService,
) {

    @RequestMapping(
        value = ["/operation"],
        consumes = [MediaType.APPLICATION_JSON_VALUE],
        method = [RequestMethod.POST]
    )
    suspend fun processOperation(
        @AuthenticationPrincipal user: CustomUserDetails?,
        @RequestBody operationDetails: OperationCreate,
    ): ProcessOperationResponse {
        return if (user == null) {
            // Unauthenticated version.
            val duration = Duration.ofDays(1L)
            val volatileToken = tokenService.createToken(
                TokenCreate(
                    name = "temporary_token",
                    duration = duration,
                    roles = listOf(AuthenticationRole.UNAUTHENTICATED_ISSUER),
                )
            )

            val processedOperation = operationService.processOperation(volatileToken.owner, operationDetails)
            ProcessOperationResponse(
                message = "Operation added. You can use the token to access information about the operation.",
                operationId = processedOperation.id!!,
                operationHash = processedOperation.tzOperation.hash,
                token = volatileToken.copy(
                    jwtToken = jwtTokenService.generateVolatileToken(
                        volatileToken.owner,
                        volatileToken.roles,
                        duration,
                    )
                ),
            )
        } else {
            val processedOperation = operationService.processOperation(user.id, operationDetails)
            ProcessOperationResponse(
                message = "Operation added.",
                operationId = processedOperation.id!!,
                operationHash = processedOperation.tzOperation.hash,
            )
        }
    }

    @RequestMapping(
        value = ["/operation/delete"],
        consumes = ["application/json"],
        method = [RequestMethod.POST]
    )
    suspend fun deleteOperation(
        @AuthenticationPrincipal user: CustomUserDetails,
        @RequestBody operationDelete: OperationDelete,
    ): Operation {
        return operationService.deleteOperation(user.id, operationDelete.id)
    }

    @RequestMapping(
        value = ["/operation"],
        produces = [MediaType.APPLICATION_JSON_VALUE],
        method = [RequestMethod.GET]
    )
    suspend fun getOperation(
        @AuthenticationPrincipal user: CustomUserDetails,
        @RequestParam(value = "id", required = true) id: String,
    ): Operation {
        return operationService.findByIdOperation(user.id, id)
    }

    @RequestMapping(
        value = ["/operation/subscribe"],
        produces = ["text/event-stream"],
        method = [RequestMethod.GET]
    )
    fun subscribeOperation(
        @AuthenticationPrincipal user: CustomUserDetails,
    ): Flow<OperationUpdateEvent> {
        if (operationNotificationService != null) {
            return operationNotificationService.subscribeOperationUpdateEvents(user.id)
        } else {
            throw UnsupportedOperationException("SSE is disabled")
        }
    }

    @RequestMapping(
        value = ["/operations"],
        produces = [MediaType.APPLICATION_JSON_VALUE],
        method = [RequestMethod.GET]
    )
    suspend fun getOperations(
        @AuthenticationPrincipal user: CustomUserDetails,
        @RequestHeader("Content-Type") contentType: String?,

        @RequestParam(value = "id", required = false) id: String?,
        @RequestParam(value = "level", required = false) level: Long?,
        @RequestParam(value = "bakerAddress", required = false) bakerAddress: String?,
        @RequestParam(value = "status", required = false) status: OperationStatus?,
        @RequestParam(value = "source", required = false) source: String?,

        @RequestParam(value = "sort", required = false) sort: List<String>?,
        @RequestParam(value = "page", required = false) page: Int?,
        @RequestParam(value = "size", required = false) size: Int?,
    ): Flow<Operation> {
        val paged = pagedSorted(page, size, sort)
        val criteria = OperationCriteria(
            id = id,
            level = level,
            bakerAddress = bakerAddress,
            status = status,
            owner = user.id,
            source = source,
        )

        return operationService.getOperations(criteria, paged)
    }

}
