package eu.coexya.cyb.tm.rest.resthandler

import eu.coexya.cyb.business.exception.UnsupportedContentTypeException
import eu.coexya.cyb.business.model.mapper.toCsv
import eu.coexya.cyb.business.service.OperationService
import eu.coexya.cyb.common.criteria.OperationCriteria
import eu.coexya.cyb.common.enums.OperationStatus
import eu.coexya.cyb.common.utils.APPLICATION_CSV_VALUE
import eu.coexya.cyb.tm.rest.data.pagedSorted
import eu.coexya.cyb.webcore.mapper.toWeb
import io.swagger.v3.oas.annotations.Operation
import io.swagger.v3.oas.annotations.security.SecurityRequirement
import kotlinx.coroutines.flow.map
import kotlinx.coroutines.flow.toList
import org.springframework.core.io.InputStreamResource
import org.springframework.http.HttpHeaders
import org.springframework.http.MediaType
import org.springframework.http.ResponseEntity
import org.springframework.web.bind.annotation.*

@RestController
@RequestMapping("\${api.base-path:/api/stats}")
class StatHandler(
    private val operationService: OperationService,
) {

    @Operation(security = [SecurityRequirement(name = "bearer-key")])
    @RequestMapping(
        value = ["/operations"],
        produces = [APPLICATION_CSV_VALUE, MediaType.APPLICATION_JSON_VALUE],
        method = [RequestMethod.GET]
    )
    suspend fun getOperations(
        @RequestHeader("Content-Type") contentType: String?,

        @RequestParam(value = "id", required = false) id: String?,
        @RequestParam(value = "level", required = false) level: Long?,
        @RequestParam(value = "bakerAddress", required = false) bakerAddress: String?,
        @RequestParam(value = "status", required = false) status: OperationStatus?,
        @RequestParam(value = "owner", required = false) owner: String?,
        @RequestParam(value = "source", required = false) source: String?,

        @RequestParam(value = "sort", required = false) sort: List<String>?,
        @RequestParam(value = "page", required = false) page: Int?,
        @RequestParam(value = "size", required = false) size: Int?,
    ): ResponseEntity<Any> {
        val paged = pagedSorted(page, size, sort)

        val criteria = OperationCriteria(
            id = id,
            level = level,
            bakerAddress = bakerAddress,
            status = status,
            owner = owner,
            source = source,
        )

        val operations = operationService.getOperations(criteria, paged)
            .map { it.toWeb() }
            .toList()

        return when (contentType) {
            MediaType.APPLICATION_JSON_VALUE ->
                ResponseEntity.ok()
                    .contentType(MediaType.APPLICATION_JSON)
                    .body(operations)

            APPLICATION_CSV_VALUE ->
                ResponseEntity.ok()
                    .header(HttpHeaders.CONTENT_TYPE, APPLICATION_CSV_VALUE)
                    .header(HttpHeaders.CONTENT_DISPOSITION, "attachment; filename=operations.csv")
                    .body(InputStreamResource(operations.toCsv()))

            else -> throw UnsupportedContentTypeException()
        }
    }

}
