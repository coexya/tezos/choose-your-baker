package eu.coexya.cyb.tm.rest.resthandler

import eu.coexya.cyb.api.tezos.BakerIdentity
import eu.coexya.cyb.business.configuration.TezosConfig
import eu.coexya.cyb.business.model.BakingSlot
import eu.coexya.cyb.business.service.BakerService
import org.springframework.http.MediaType
import org.springframework.web.bind.annotation.RequestMapping
import org.springframework.web.bind.annotation.RequestMethod
import org.springframework.web.bind.annotation.RestController

@RestController
@RequestMapping("\${api.base-path:/api}")
class InformationHandler(
    private val tezosConfig: TezosConfig,
    private val bakerService: BakerService,
) {

    @RequestMapping(
        value = ["/identity"],
        produces = [MediaType.APPLICATION_JSON_VALUE],
        method = [RequestMethod.GET]
    )
    suspend fun identity(): BakerIdentity {
        return BakerIdentity(
            name = tezosConfig.name,
            nbBakingSlotSearch = tezosConfig.nbBakingSlotSearch,
            bakerAddress = tezosConfig.bakerAddress,
            kycSmartContractAddress = tezosConfig.kycSmartContractAddress,
            allowUnauthenticated = tezosConfig.allowUnauthenticated,
            feeMode = tezosConfig.fees.mode,
            minimalNtzFees = tezosConfig.fees.minimalNtzFees,
            minimalNtzPerGasUnit = tezosConfig.fees.minimalNtzPerGasUnit,
            minimalNtzPerByte = tezosConfig.fees.minimalNtzPerByte,
        )
    }

    @RequestMapping(
        value = ["/slots"],
        produces = [MediaType.APPLICATION_JSON_VALUE],
        method = [RequestMethod.GET]
    )
    suspend fun slots(): List<BakingSlot> {
        return bakerService.getBakingSlots()
    }

}
