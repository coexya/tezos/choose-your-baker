package eu.coexya.cyb.bakertm.rest.resthandler

import eu.coexya.cyb.bakertm.rest.model.TzOperationBaker
import eu.coexya.cyb.bakertm.rest.model.toBaker
import eu.coexya.cyb.business.logger
import eu.coexya.cyb.business.service.OperationService
import org.slf4j.Logger
import org.springframework.web.bind.annotation.RequestMapping
import org.springframework.web.bind.annotation.RequestMethod
import org.springframework.web.bind.annotation.RestController

@RestController
@RequestMapping("\${api.base-path:/api}")
class MempoolHandler(
    val operationService: OperationService
) {

    @RequestMapping(
        value = ["/mempool"],
        produces = ["application/json"],
        method = [RequestMethod.GET]
    )
    suspend fun fetchMempool(): List<TzOperationBaker> {
        return operationService.getMempool().map { it.toBaker() }
    }

    companion object {
        val LOGGER: Logger = logger()
    }
}
