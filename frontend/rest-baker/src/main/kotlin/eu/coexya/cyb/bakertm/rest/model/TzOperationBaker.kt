package eu.coexya.cyb.bakertm.rest.model

import com.fasterxml.jackson.annotation.JsonProperty
import eu.coexya.tezos.connector.node.model.TzOperation

data class TzOperationBaker(
    @JsonProperty("contents")
    val contents: List<Map<String, Any>> = emptyList(),
    @JsonProperty("branch")
    val branch: String,
    @JsonProperty("signature")
    val signature: String?,
)

fun TzOperation.toBaker() = TzOperationBaker(
    contents, branch, signature
)
