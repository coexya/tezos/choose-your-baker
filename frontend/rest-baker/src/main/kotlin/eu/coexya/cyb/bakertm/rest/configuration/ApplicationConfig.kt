package eu.coexya.cyb.bakertm.rest.configuration

import org.springframework.context.annotation.ComponentScan
import org.springframework.context.annotation.Configuration

@Configuration
@ComponentScan(basePackages = ["eu.coexya"])
class ApplicationConfig
