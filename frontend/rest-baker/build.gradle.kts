import org.springframework.boot.gradle.tasks.run.BootRun

plugins {
    id("kotlin")
    kotlin("plugin.spring")
    id("org.springframework.boot")
}

val artefactName = "rest-baker"
description = "Transaction manager for the baker node"

dependencies {
    // Kotlin
    implementation("org.jetbrains.kotlin:kotlin-reflect")
    implementation("org.jetbrains.kotlin:kotlin-stdlib-jdk8")
    implementation("org.jetbrains.kotlinx:kotlinx-coroutines-core")
    implementation("org.jetbrains.kotlinx:kotlinx-coroutines-reactor")
    implementation("io.projectreactor.kotlin:reactor-kotlin-extensions")

    // Jackson
    implementation("com.fasterxml.jackson.module:jackson-module-kotlin")
    implementation("com.fasterxml.jackson.datatype:jackson-datatype-jdk8")

    // Spring
    implementation("org.springframework.boot:spring-boot-starter-webflux")
    implementation("org.springframework.data:spring-data-commons")

    // Project libs
    implementation(project(":lib:business"))
    implementation(project(":lib:api"))
}

springBoot {
    buildInfo()
}

tasks {
    jar {
        enabled = false
    }

    bootJar {
        enabled = true
        archiveBaseName.set(artefactName)
    }

    val unpack by registering(Copy::class) {
        dependsOn("bootJar")
        from(zipTree("build/libs/$artefactName-$version.jar"))
        into("build/unpacked")
    }

    withType<BootRun> {
        args = listOf("--spring.profiles.active=developer")
    }
}

publishing {
    publications {
        create<MavenPublication>("maven") {
            artifactId = artefactName
            artifact(tasks["bootJar"])
        }
    }
}
