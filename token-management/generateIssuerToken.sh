#!/usr/bin/env bash

if [ "$#" -ne 1 ]; then
  echo "Illegal number of parameters, token name not specified">&2
  exit 2
fi

# Config
source utils.sh
issuer_token_name=$1

if [ "$admin_token" ]; then

  # Generate issuer token
  log "\nGenerate issuer token\n"

  body="{\"name\":\"$issuer_token_name\", \"roles\":[\"ISSUER\"]}"
  response=$(post "$token_url" "$admin_token" "$body")

  if [ "$(echo "$response" | jq '.revoked')" = "false" ]; then
    log "Issuer token generated:" green
    log "$response" | jq '.'
    log "Please save this token, it will not be given to you again." red

  else
    log "$response" | jq '.'
    log "There was an error." red
  fi

else
  log "Admin token is not defined." red
fi
