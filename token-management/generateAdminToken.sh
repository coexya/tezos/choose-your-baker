#!/usr/bin/env bash

# Config
source utils.sh

#tmp_admin_token=eyJ0eXAiOiJKV1QiLCJhbGciOiJIUzI1NiJ9.eyJvd25lciI6IjYxODkyYmZhZWE3ODY1NjRjYjE3YWM0NSIsImNyZWF0aW9uVGltZSI6IjIwMjEtMTEtMDhUMDU6NTQ6MDIuOTYwNjgyLTA4OjAwIiwicm9sZXMiOlsiQURNSU4iXSwiaXNzIjoiVGV6b3NAQ3liIiwicGVyc2lzdGVkIjp0cnVlfQ.Gy2q0YhRsndfEJgQ1boa6JhIj5XOSYytt9JMkwOLgCE
admin_id=61892bfaea786564cb17ac45
tmp_admin_token_id=61892bfaea786564cb17ac46

# Compute existing admin token
jwt_header=$(echo -n '{"typ":"JWT","alg":"HS256"}' | base64 | sed s/\+/-/g | sed 's/\//_/g'| sed -E s/=+$// | sed -z 's/\n//g')
payload=$(echo -n '{"owner":"'$admin_id'","creationTime":"2021-11-08T05:54:02.960682-08:00","roles":["ADMIN"],"iss":"Tezos@Cyb","persisted":true}' | base64 | sed s/\+/-/g | sed 's/\//_/g'| sed -E s/=+$// | sed -z 's/\n//g')
hexsecret=$(echo -n "$jwt_secret" | xxd -p | paste -sd "")
hmac_signature=$(echo -n "${jwt_header}.${payload}" | openssl dgst -sha256 -mac HMAC -macopt hexkey:$hexsecret -binary | base64  | sed s/\+/-/g | sed 's/\//_/g' | sed -E s/=+$//)
tmp_admin_token="${jwt_header}.${payload}.${hmac_signature}"

# Generate admin token
log "\nGenerate admin token\n"

body='{"name":"admin", "roles":["ADMIN"]}'
response=$(post "$token_url" "$tmp_admin_token" "$body")

if [ "$(echo "$response" | jq '.revoked')" = "false" ]; then
  log "Admin token generated:" green
  log "$response" | jq '.'
  log "Please save this token, it will not be given to you again." red
  sleep 1

  # Revoke default admin token
  log "\nRevoke temporary admin token\n"

  response=$(post "$token_url/revoke/$tmp_admin_token_id" "$tmp_admin_token")

  if [ "$(echo "$response" | jq '.revoked')" = "true" ]; then
    log "Temporary admin token revoked." green
  else
    log "There was an error." red
    log "$response" | jq '.'
  fi

else
  log "There was an error." red
  log "$response" | jq '.'
fi
