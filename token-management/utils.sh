# shellcheck disable=SC2034

trap "exit 1" TERM
export TOP_PID=$$

### Config
source config.sh

## Urls
token_url="$transaction_manager_url/api/tokens"

## Colors
GREEN='\033[0;32m'
RED='\033[0;31m'
NC='\033[0m'

function log() {
  case $2 in
    red)
      echo -e "${RED}$1${NC}"
      ;;

    green)
      echo -e "${GREEN}$1${NC}"
      ;;

    *)
      echo -e "$1"
      ;;
  esac
}

# $1 url
# $2 jwt token
# $3 body
function post() {
  # log "--\nPOST $1 \n$(echo $3 | jq ".")\n--">&2 # Logs POST requests
  response=$(
    curl -s --location --request POST "$1" \
      --header "Authorization: Bearer $2" \
      --header 'Content-Type: application/json' \
      --data "$3"
  )

  if [ "$response" = "Access Denied" ]; then
    log "Access Denied" red >&2
    log "Url: $1\nToken: $2" >&2
    kill -s TERM $TOP_PID
  fi

  echo "$response"
}
