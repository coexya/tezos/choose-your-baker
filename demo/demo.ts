import {CybAuth, CybSdk, OperationStatus, OperationUpdateEvent} from '@coexyaedi/cyb-sdk'
import {InMemorySigner} from "@taquito/signer"
import {OpKind, ParamsWithKind} from "@taquito/taquito"

const TRANSACTION_MANAGER_URL = "http://localhost:9090"
const JWT_TOKEN = "eyJ0eXAiOiJKV1QiLCJhbGciOiJIUzI1NiJ9.eyJvd25lciI6IjYxODkyYmZhZWE3ODY1NjRjYjE3YWM0NSIsImNyZWF0aW9uVGltZSI6IjIwMjEtMTEtMDhUMDU6NTQ6MDIuOTYwNjgyLTA4OjAwIiwicm9sZXMiOlsiQURNSU4iXSwiaXNzIjoiVGV6b3NAQ3liIiwicGVyc2lzdGVkIjp0cnVlfQ.Gy2q0YhRsndfEJgQ1boa6JhIj5XOSYytt9JMkwOLgCE"
const NODE_RPC_URL = "http://localhost:20000"

const BOB_TZ1_ADDRESS = "tz1aSkwEot3L2kmUvcoxzjMomb9mvBNuzFK6"
const BOB_TEZOS_SK = "edsk3RFfvaFaxbHx8BMtEW1rKQcPtDML3LXjNqMNLCzC3wLC1bWbAt"

const ALICE_TZ1_ADDRESS = "tz1VSUr8wwNhLAzempoch5d6hLRiTh8Cjcjb"

// Configure Transaction Manager connection
const auth = new CybAuth(TRANSACTION_MANAGER_URL, JWT_TOKEN)
const sdk: CybSdk = new CybSdk(auth, NODE_RPC_URL)

sdk.setProvider({
    signer: new InMemorySigner(BOB_TEZOS_SK),
})

async function demo() {
    // Set up event listener on my operations
    await new Promise<void>(async resolve => {
        let operationEventSource = sdk.operationService.getOperationEventSource()

        operationEventSource.onmessage = (e) => {
            const opUpdateEvent: OperationUpdateEvent = JSON.parse(e.data)

            let status = opUpdateEvent.operation.status
            let opId = opUpdateEvent.operation.id
            console.log(`Operation with id ${opId} updated with status: ${status}`)

            if (status === OperationStatus.BAKED
                || status === OperationStatus.BAKED_BY_ANOTHER
                || status === OperationStatus.REJECTED
            ) {
                console.log(`Operation has been processed at level: ${opUpdateEvent.operation.level}.`)
                resolve()
            }
        }

        console.log("Preparation of operation")
        // Build a transaction to transfer 0.1 XTZ to destinationAddress
        const params: ParamsWithKind[] = [
            {
                kind: OpKind.TRANSACTION,
                to: ALICE_TZ1_ADDRESS,
                amount: 0.1,
                fee: 0,
            },
        ]

        console.log("Estimate operation:")
        // Estimate for CYB (calculates correct fees)
        const estimateCyb = await sdk.cybEstimate(params).catch(handleError)
        console.log(estimateCyb)

        console.log("Sending operation to Transaction Manager")
        // Generate taquito batch and send the operation with it
        const response = await sdk.generateBatch(estimateCyb).send().catch(handleError)

        // Get operation
        const operation = await sdk.operationService.getOperation({hash: response.opHash}).catch(handleError)

        console.log("Operation sent:")
        console.log(operation)
        console.log("Waiting for operation status update")

        // Wait for promise to be resolved.
        setTimeout(() => null, 99999999)
    })
}

const handleError = (error: any) => {
    if (error.body != undefined) {
        const exception = JSON.parse(error.body)
        if (exception.error.includes("MissingBakingRightsException")) {
            console.log("Baker is missing baking rights in the next 10 blocks, please wait for a bit and restart the demo.")
            process.exit()
        }
    }
    console.log("Please wait for the application to be correctly initialized and restart the demo.")
    process.exit()
}

demo().then(_ => {
    console.log("End of the demo.")
    process.exit()
})



