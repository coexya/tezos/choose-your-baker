[Index](/README.md) | [Specification](/documentation/specification.md) | [Sandbox/demo](/demo/README.md)  | [Deployment](/documentation/deployment.md) | [Typescript SDK Quick start](/documentation/Quickstart.md) | [Examples](/documentation/Examples.md) | [Tezos Sandbox Mempool](/tezos-sandbox-mempool/README.md)

# Sandbox Demo

### Start CYB services

Run the following commands to start CYB Service:

```shell
# In demo directory
docker-compose -f docker-compose.demo.yml up -d

# Stop it with
docker-compose -f docker-compose.demo.yml down
```
Startup might take a little time since the sandbox blockchain needs to initialize properly.

This version of the application runs:

| Service                  | Address         |
|--------------------------|-----------------|
| Sandbox Tezos Blockchain | localhost:20000 |
| Transaction Manager      | localhost:9090  |
| Mongo express            | localhost:8081  |

See [Token Management](../token-management/README.md) to change default authentication tokens.

### Make a transaction with CYB SDK

This demo prepares a transaction from Bob to Alice and sends it to the transaction manager to be baked by a certified baker. For more detail please read the `demo.ts` file.

Run the following commands:

```shell
# Install demo dependencies
npm install
# Run the demo
npx ts-node demo.ts

# Check result in the sandbox blockchain 
# $BLOCK_LEVEL should be the one specified in the demo
curl http://localhost:20000/chains/main/blocks/$BLOCK_LEVEL | jq .

# Alternatively you can use the given script or check a web browser at the same address
./check.sh $BLOCK_LEVEL

# Baker identity can be obtained at url
curl http://localhost:9090/api/identity | jq .
```
### Useful links

If you want to test others functionalities : [Examples](/documentation/Quickstart.md#examples)

#### Tezos sandbox blockchain

To visualize the sandbox Tezos blockchain: **https://YOUR_PREFERRED_RPC_URL/chains/main/blocks/head** (by default: localhost:20000)

To visualize a specific block: **https://YOUR_PREFERRED_RPC_URL/chains/main/blocks/BLOCK_NUMBER**

To visualize the contracts and addresses: **https://YOUR_PREFERRED_RPC_URL/chains/main/blocks/head/context/contracts**

To visualize a specific contract or address: **https://YOUR_PREFERRED_RPC_URL/chains/main/blocks/head/context/contracts/ADDRESS_OR_CONTRACT**

#### Transaction Manager

To visualize the Api documentation: **https://TRANSACTION_MANAGER_URL/swagger-ui** (by default: localhost:9090)

To visualize the baker information: **https://TRANSACTION_MANAGER_URL/api/identity**




