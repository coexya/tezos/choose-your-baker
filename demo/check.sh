#!/bin/bash

if [ "$#" -ne 1 ]; then
  echo "Illegal number of parameters, provide level to check">&2
  exit 2
fi

block=$(curl -s http://localhost:20000/chains/main/blocks/"$1")

echo "Block at level $1:"
echo " - Baker : $(echo $block | jq .metadata.baker)"
echo " - Operation hash : $(echo $block | jq .operations[3][0].hash)"
echo " - Operation contents : $(echo $block | jq .operations[3][0].contents)"
