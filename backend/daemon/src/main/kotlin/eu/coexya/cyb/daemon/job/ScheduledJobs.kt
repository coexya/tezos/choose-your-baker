package eu.coexya.cyb.daemon.job

import eu.coexya.cyb.business.logger
import eu.coexya.cyb.business.service.OperationUpdateService
import eu.coexya.cyb.business.service.TokenService
import kotlinx.coroutines.runBlocking
import org.springframework.scheduling.annotation.Scheduled
import org.springframework.stereotype.Component

@Component
class ScheduledJobs(
    private val operationsUpdateJob: OperationsUpdateJob,
    private val bakingSlotsUpdateJob: BakingSlotsUpdateJob,
    private val operationUpdateService: OperationUpdateService,
    private val tokenService: TokenService,
) {

    @Scheduled(cron = "\${daemon.update.cron}")
    fun updateOperations() {
        operationsUpdateJob.scheduledUpdate()
    }

    @Scheduled(cron = "\${daemon.purge.cron}")
    fun purgeDatabase() {
        LOGGER.debug("Database purge STARTED.")
        runBlocking { operationUpdateService.purgeOldOperations() }
        runBlocking { tokenService.purgeExpiredTokens() }
        LOGGER.debug("Database purge COMPLETED.")
    }

    @Scheduled(cron = "\${tezos.bakingSlots.update.cron}")
    fun updateBakingSlots() {
        bakingSlotsUpdateJob.scheduledUpdate()
    }

    companion object {
        val LOGGER = logger()
    }

}
