package eu.coexya.cyb.daemon.job

import eu.coexya.cyb.business.service.OperationUpdateService
import kotlinx.coroutines.runBlocking
import org.springframework.stereotype.Component

@Component
class OperationsUpdateJob(
    val operationUpdateService: OperationUpdateService,
) {

    var started = false

    /**
     * Before launching the daemon, we process all the remaining operations.
     */
    fun startUp() {
        runBlocking {
            operationUpdateService.startupUpdateOperationsStatus()
        }
        started = true
    }

    fun scheduledUpdate() {
        if (started) {
            runBlocking {
                operationUpdateService.updateOperationStatus()
            }
        }
    }
}
