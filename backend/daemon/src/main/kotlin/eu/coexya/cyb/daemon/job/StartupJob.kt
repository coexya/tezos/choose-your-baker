package eu.coexya.cyb.daemon.job

import eu.coexya.cyb.business.logger
import org.springframework.boot.context.event.ApplicationReadyEvent
import org.springframework.context.event.EventListener
import org.springframework.stereotype.Component
import java.util.concurrent.Executors
import java.util.concurrent.TimeUnit

@Component
class StartupJob(
    private val initDataBaseJob: InitDataBaseJob,
    private val bakingSlotsUpdateJob: BakingSlotsUpdateJob,
    private val operationsUpdateJob: OperationsUpdateJob,
) {

    var bakingSlotUpdate = false
    var operationUpdate = false

    @EventListener(ApplicationReadyEvent::class)
    fun executeStartupJobs() {
        LOGGER.info("Execute startup jobs...")
        LOGGER.info("Execute initDatabase job.")
        initDataBaseJob.initDatabase()
        executeRetryOnFailJobs()
    }

    fun executeRetryOnFailJobs(
        delayBeforeRetry: Long = 30,
        timeUnit: TimeUnit = TimeUnit.SECONDS,
        nbRetry: Int = 5,
    ) {
        if (!bakingSlotUpdate) {
            LOGGER.info("Execute baking slot caching startup.")

            try {
                bakingSlotsUpdateJob.startUp()
                bakingSlotUpdate = true
            } catch (exception: Exception) {
                LOGGER.error("Baking slot caching startup failed : ${exception.message}.")
            }
        }

        if (!operationUpdate) {
            LOGGER.info("Execute operation update startup.")

            try {
                operationsUpdateJob.startUp()
                operationUpdate = true
            } catch (exception: Exception) {
                LOGGER.error("Operation update startup failed : ${exception.message}.")
            }
        }

        if (bakingSlotUpdate && operationUpdate) {
            LOGGER.info("All startup jobs fully executed.")
        } else if (nbRetry > 0) {
            LOGGER.error("Retrying in $delayBeforeRetry ${timeUnit.name}.")

            Executors.newScheduledThreadPool(1)
                .schedule(
                    {
                        executeRetryOnFailJobs(delayBeforeRetry, timeUnit, nbRetry - 1)
                    },
                    delayBeforeRetry,
                    timeUnit,
                )
        } else {
            LOGGER.error("Startup jobs have retried too many times and failed.")
        }
    }

    companion object {
        private val LOGGER = logger()
    }
}
