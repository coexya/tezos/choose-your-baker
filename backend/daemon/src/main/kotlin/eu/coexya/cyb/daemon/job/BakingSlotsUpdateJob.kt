package eu.coexya.cyb.daemon.job

import eu.coexya.cyb.business.service.BakerService
import kotlinx.coroutines.runBlocking
import org.springframework.beans.factory.annotation.Value
import org.springframework.stereotype.Component

@Component
class BakingSlotsUpdateJob(
    private val bakingSlotsService: BakerService,
    @Value("\${daemon.purgeSlotsAtStartup:false}") private val purgeSlotsAtStartup: Boolean,
) {

    var started = false

    fun startUp() {
        runBlocking {
            if (purgeSlotsAtStartup) {
                bakingSlotsService.purgeBakingSlotsCache()
            }
            bakingSlotsService.updateBakingSlotsCache()
        }
        started = true
    }

    fun scheduledUpdate() {
        if (started) {
            runBlocking {
                bakingSlotsService.updateBakingSlotsCache()
            }
        }
    }

}
