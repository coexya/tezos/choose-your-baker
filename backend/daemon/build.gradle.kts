plugins {
    id("kotlin")
    kotlin("plugin.spring")
    id("org.springframework.boot")
}

val artefactName = "daemon"
description = "Status update daemon"

dependencies {
    // Kotlin
    implementation("org.jetbrains.kotlin:kotlin-reflect")
    implementation("org.jetbrains.kotlin:kotlin-stdlib-jdk8")
    implementation("org.jetbrains.kotlinx:kotlinx-coroutines-core")
    implementation("org.jetbrains.kotlinx:kotlinx-coroutines-reactor")
    implementation("io.projectreactor.kotlin:reactor-kotlin-extensions")

    // Jackson
    implementation("com.fasterxml.jackson.module:jackson-module-kotlin")

    // Spring
    implementation("org.springframework.boot:spring-boot-starter")
    implementation("org.springframework.boot:spring-boot-starter-actuator")

    // Project libs
    implementation(project(":lib:business"))
    implementation(project(":lib:model"))
}

springBoot {
    buildInfo()
}

tasks {
    jar {
        enabled = false
    }

    bootJar {
        enabled = true
        archiveBaseName.set(artefactName)
    }

    val unpack by registering(Copy::class) {
        dependsOn("bootJar")
        from(zipTree("build/libs/$artefactName-$version.jar"))
        into("build/unpacked")
    }
}

publishing {
    publications {
        create<MavenPublication>("maven") {
            artifactId = artefactName
            artifact(tasks["bootJar"])
        }
    }
}
