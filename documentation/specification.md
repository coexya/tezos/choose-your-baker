[Index](/README.md) | [Specification](/documentation/specification.md) | [Sandbox/demo](/demo/README.md)  | [Deployment](/documentation/deployment.md) | [Typescript SDK Quick start](/documentation/Quickstart.md) | [Examples](/documentation/Examples.md) | [Tezos Sandbox Mempool](/tezos-sandbox-mempool/README.md)

## Table of contents

[[_TOC_]]

# Choose Your Baker
  	 
## Introduction

### Document presentation
The “Choose Your Baker”(CYB) initiative aims to propose to the Tezos ecosystem a communication protocol enabling issuers of Tezos transactions to select the baker they want to send their transactions to. This topic is indeed particularly important for the compliance department of financial institutions working in the security tokens space.
  
A feasibility study has been carried out by Sword France from April to June 2021. This document is the specification of all the features.

### The CYB concept
In most countries, including in Europe, the regulation now authorizes the tokenisation of non-listed financial instruments, called “Securities Token”.
  
Digitalised financial instruments bring a lot of advantages compared to the more traditional paper-based OTC process. More specifically, it enables instant transactions between parties.
  
Securities Token are financial instruments, and as such fall under different national and international regulations, including anti money laundering, countering the financing of terrorism, and trade and financial embargos.
  
The French authorities as well as the European Commission require that these securities tokens be managed on a distributed ledger. The Tezos Blockchain currently represents the best technical solution for managing securities token by corporate entities, including from a RSE perspective.
   
One of its comparative advantage is its consensus: the delegated Proof of Stake. The compliance of the transactions with the rules set up by the Tezos blockchain is validated by “baker” nodes, which are participants that are proposing to run different controls on their IT infrastructure against a remuneration (fees on transactions + rewards granted by the blockchain).
    
To abide by the law, we believe that transactions on Security Tokens should not be validated by bakers that do not comply with the regulations on financial instruments. More explicitly, financial institutions may want to make sure they did not pay fees to bakers that could be associated to fund of terrorist organisations or money laundering activity. The risk of working with non-compliant bakers is bear by the issuer of the transaction. Therefore, many compliance officers believe that the development to the Security Tokens market requires from the issuer of transactions to be able to select the bakers they want to work with from a predefined whitelist.
    
Note: the same problem and the same solution may apply to other markets than security tokens where regulation on data location exists (ex: health data, other type of crypto assets, etc).  

## Overall architecture

### Concepts
The main actors involved in a CYB operation are:
- The corporate baker, selected by the issuer to process its transaction
- The issuer of the transaction (the terminology does not refer to the issuer of the token, but to any entity sending a Tezos transaction)
- The KYC certification authority who is responsible of the listing of the certified bakers.

We provide tools for the issuer to communicate with the baker of its choice. However, the issuer may know all corporate baker while still being interested in using their services. We therefore imagine the possibility to have a register of enrolled corporate baker listed in a smart contract, and for each of them, one or several certification authorities confirming the quality of the baker (identity, nationality, localization of its server, and possibly more). The issuer could access in the smart contract the list of bakers enrolled in the CYB process and contact them and/or the certification authority off-chain to check their credentials (different options are possible here).

### Architecture overview
 
![alt text](./images/architecture.jpg) 
 
 
#### Transaction emission  
The issuer calls the targeted Transaction Manager thanks to the Transaction Emission SDK (1). It sends its transactions enclose into a signed operation. The Transaction Manager stores the operations in a database. Before the baker begins the block, it calls the Transaction Manager (2) and gets the stored operations thanks to the private mempool API. It concatenates these operations with the operation coming from the public mempool.
 
The baker creates the block and puts it in the Tezos Blockchain (3). The Transaction Manager retrieves the blocks (pulling) and the operations from the node (4) and fires events to the issuer (5).

#### Certified bakers  
The certified bakers are stored in the Tezos blockchain. A Tezos smart contract stores all the information related to the certified bakers. This smart contract is managed by a Certification Authority.
  
The Transaction Emission SDK is linked to this Smart contract in order to retrieve the certified bakers (6).
  
It’s possible to have several smart contracts and hence, several Certification Authorities.  

#### Registering a certified baker
Thanks to the Directory SDK, the Certification authority can register a certified baker (7).

## Transaction manager
### Authentication
#### Issuer
To connect to the Transaction Manager, there is two ways:

- Strong authentication: the issuer connects to the Transaction manager with a token provided by the certified baker (see §3.2.10).
- Light authentication: the Operation Storage Insertion service is reachable as the current Tezos nodes. However, the operations must be valid to be taken into account (A dry run is performed before storing the operation) and the fee policy respected. The insertion service provides in return a temporary token. The other services are restricted (no event channel, the status/deletion service are limited to the inserted request).

For the second way of authentication, it is strongly recommended to have a firewall in order to avoid the ddos attacks. Indeed, with only some valid accounts, it’s possible to ask for impossible transactions which trigger fake dry run.

#### Mempool
The baker daemon connects to the Transaction Manager with a basic authentication over the https protocol. 

### Services
The services are managed by two servers and one daemon:

-	Transaction Manager: it manages all the requests coming from the external network.
-	Baker Manager: it manages only the request coming from the baking node.
-	Daemon: it updates the status of the request accordingly with the Tezos blockchain.

The two servers communicate through the database. This architecture avoids exposing the private mempool directly to the external network.

The JSON APIs of the Transaction Manager are:

-	Operation storage Insertion (POST): to insert an operation in the blockchain 
-	Operation storage deletion (DELETE): to delete an operation in the CYB cache
-	Operation (GET): get the content of the operation
-	Operations (GET): get the content of operations related to an Tezos account and an issuer
-	SSE to emits events (operation status) when the operation is stored in the Tezos blockchain
-	Baker identity(GET): to get the identity of the baker
-   Slots (GET): get the future slots available of the corporate baker
-	Token Management (POST, GET, DELETE): to create, delete, revoke and list the tokens
-   Statistics (GET): to retrieve alle operations with criteria (only for administrator) 

The JSON APIs of the Baker Manager is:
- Operation storage Retrieval (GET): the baker gets the stored operations to bake them.

The status daemon scans the blockchain to retrieve new blocks and update the database and broadcast events.

#### Operation Storage Insertion (POST /api/operation)
The Operation Storage service stores the signed operations sent by the issuer in the operation database. Before storing it, the service checks the validity of the request.

Input:
- Token in case of strong authentication mode
- Operation including transactions
- Targeted block level (optional)

Output:
- operationId: id of the operation in the database
- operationHash: hash of the operation
- message: message
- token: Temporary token in case of the light authentication mode. This token contains the expiration date and the id of the request. If it’s a dry run, no token is provided.


The service performs the following actions:
- If the hash operation is already in the database with the status pending or in progress, the request fails.
- If the block id is in the request, the service checks that it belongs to a slot of the baker and that the slot has the priority 0 (the baker is the main baker)
- If the block level is not in the request, the service checks that there is a slot granted to the baker in the next N blocks (N=60 and the slot must be the priority 0. The baker is the main baker for the slot). The value of N is parameterized in the configuration file (see §6.2).
- A dry run is performed for the operation to check (this step can be disabled in case of strong authentication)
- The fee is checked to see if it meets the fee rules (see §3.2.9). It's possible to set the fees to 0.

If these requirements are met, the operation is stored in the database with the pending status and the expected_level. 

Warning: the fee policy is checked only when the operation is inserted. This policy must be higher than the baker policy if you don’t want to have some inserted operation which are ignored by the baker because they are excluded from the baker fee policy.


#### Operation Storage Deletion (POST /api/operation/delete)
This service deletes a request. The request must be in the pending status, otherwise, the service fails. The request is updated to the deleted status.

Input: 
- Token
- Id of the request (Mandatory in case of strong authentication. Optional in case of light authentication mode: the token carries this information)

Output: void 

Note: The request is a shared data in the MongoDb database. Hence, it’s possible to have a simultaneous access to it. If the Back Transaction Manager retrieves it before it is flagged deleted, the operation will be baked.  
The service is POST (instead of DELETE) in order to be compliant with the Taquito library which manages only POST/GET request.



#### Operation Storage Retrieval by the baker (GET /api/mempool)
Before the baking, the baker calls this service with the private Mempool API. The Operation Storage Service checks the current block number of the head of the related node and gets the block number targeted by the baker (adding 1). The Operation Storage retrieves 
- all the pending operations not older than N minutes (N=60) and related to this block 
- all the pending operations not older than N minutes (N=60) not related to a block 
It gives them to the baker. 

In the database, these operations are flagged as in progress with the block number. A daemon is launched to pull regularly the status, metadata and contents of the blocks being baked.

When the baking node asks for new operations to bake, it does not provide the block number. Hence, before providing the response, the service asks to the public node related to the baking node the head metadata to retrieve the level of the last baked block.

#### Operation status retrieval (GET /api/operation)
This service enables the issuer to retrieves the status of an operation (status, baker, blocks level). The issuer can also use the Event Channel to get the same information.

Input: 
- Token
- criteria
    - id of the request
    - tezos account

Output:
- restricted information related to the request or to the related Tezos account. In case of a strong authentication, only the requests related to the token are retrieved. In case of light authentication context, only the request related to the temporary token is retrieved (i.e. only one request). The provided information is
    - id: Id of the request
    - lastStatusUpdate: date of the last update
    - creationDate: date of the operation creation
    - level: baked level
    - requestedLevel: level requested
    - fetchedLevel: level when fetched
    - timesFetched: number of tries (the operation is PENDING, then IN PROGESS, then back in PENDING, then back in IN PROGRESS...)
    - errorMessage: message
    - bakerAddress: address of the baker who baked the operation
    - status: status of the operation:
        - PENDING: the operation is inserted in the database and is waiting to be fetched by the baker
        - IN_PROGRESS: the operation has been fetched and the daemon is scanning the created blocks to check it it has been baked
        - REJECTED: the operation is rejected
        - BAKED: the operation is baked
        - STALE: the operation is too old
        - BAKED_BY_ANOTHER: another baker has baked the operation
    - owner: id of the owner
    - totalFee: total fee of the operation
    - opSize: size of the operation
    - estimatedConsumption: estimated consumption before the baking
        - totalStorageSize: storage used
        - totalConsumedGas: consumed gas
    - actualConsumption: real consumtion after the baking
        - totalStorageSize: storage used
        - totalConsumedGas: consumed gas   
    - tzOperation : operation 
        - branch
        - hash: hash of the operation
        - signature: signature of the operation
        - contents: contents of the operation (complex object)    


#### Event channel (GET /api/operation/subscribe)
The event channel uses the Server Sent Event protocol. It forwards the events to the subscriber. This service is only available for the strong authentication context.

Input: 
- Token

Output:
- Only the events related to the token are retrieved.

An event is fired every time a status of an operation related to the input criteria is updated. The provided information is the same as the Operation status retrieval service (see [here](#operation-status-retrieval-get)) (only the content of the operation is not provided).

#### Baker Identity (GET /api/identity)
This service returns the account of the baker and the address of the smart contract (KYC) listing all the certified bakers. 

Input: nothing

Output:
- name: Name of the Choose Your Baker entity
- nbBakingSlotSearch: range of the slots (the Transaction Manager is available only if there is a slot in this range)
- bakerAddress: Tezos account of the baking node
- kycSmartContractAddress: Smart contract address (KYC)
- allowUnauthenticated: true if unauthenticated request are authorized
- feeMode: FEES if the fees are in the transactions or TRANSACTION if the fees are agregated in one transaction
- Fee policy : If all the hereafter fees are 0, there is no fee management. It's not possible to have the TRANSACTION mode and no fees.
    - minimal_fees: fees >= (minimal_fees + minimal_nanotez_per_byte * size + minimal_nanotez_per_gas_unit * gas)
    - minimal_nanotez_per_byte
    - minimal_nanotez_per_gas_unit


#### Available slots (GET /api/slots)
This service returns the slots available. 

Input: nothing

Output:
- List of the slots:
  - level
  - cycle
  - baker



#### Status daemon
The status daemon manages the status of the stored operation.

This daemon checks regularly the status of the baked blocks (with an offset) in order to update the operations in the database. 

If a new block is created in the node by the certified baker, it checks the in progress operations in the database. Keep in mind that the daemon retrieves new blocks with an offset in order to be sure that the block is really inserted in the blockchain.
- The in progress operations in the block are updated to baked. The used gas, the size and the fees are also retrieved.
- The in progress operations not in the block and requested with the block number are flagged as rejected.
- The in progress operations not in the block and not requested with a block number and with a block number (updated when they have been retrieved by the baker) lower or equal to the current block are updated back to pending or stale if the operation is too old.

If a new block is created by another baker, it checks the operations in the database.
- The in progress operations not in the block,not requested with a block number, and with a block number (updated when they have been retrieved by the baker) lower or equal to the current block are updated to pending if the next slot of the baker is very soon, otherwise, it is set to REJECTED.
- The in progress operations not in the block and requested with a block number equals to the current block number are flagged as rejected.
- The in progress operations in the block are updated to managed_by another_baker.

When a daemon updates an operation to baked, rejected, stale or baked_by_another_baker, an event is created and put in the event channel (if requested).

The operations older than P hours (P=24) are deleted.

When it is launched, the daemon retrieves all the blocks in a depth (N=240 (2 hours)) and process all the operations:
- In progress operations:
    - If the operation is in the blocks and baked by the baker, the operation is updated to baked even if the block number is not compliant.
    - If the operation is in the blocks and baked by another baker, the operation is updated to baked_by_another_baker even if the block number is not compliant.
    - If the operation is not in the blocks
        - If the operation is too old, the operation is updated to stale
        - Otherwise
            - If the request is related to a block and the block level is outdated (it means the head is further), the block is updated to rejected
            - Otherwise the operation is updated to pending
- Pending operation
    - If the operation is too old, the operation is updated to stale
    - It he operation is related to a block and the block level is outdated (it means the head is further), the operation is updated to rejected

Note: this status daemon is a basic one. That means it does not retrieve all the information of the Tezos blochain. For example, if an issuer submits a rightful request to the Transaction Manager and submit the same operation to another baker, the baker will bake the operation before the certified baker. In the transaction manager, the request is still pending. When the Transaction manager will deliver the operation to its related baker, the baking will fail for the operation and the request will be flagged as rejected and not baked_by_another_baker.

#### Node slots
Regularly, the front transaction manager retrieves the slots related to the baking node in order to check if the requests are affordable or not.

To retrieve the slots, a (rolling) node is called with the URL:
```
<url>/chains/main/blocks/head/helpers/baking_rights?delegate=<id_of_the_delegate>&max_priority=0&level=<level>
``` 
If the retrieved list is not empty, that means there is a slot for the baker.

#### Fee management  
There are 3 types of fee management. The fee management is configurable.

**No fee management**  
The operation has a rewarding set to 0. The baker node has also a rewarding policy set to 0. The rewarding is managed off chain. The authentication is set to strong authentication in order to avoid some 0 fees requests from unknown issuers.

**Weak fee management**   
The expected fee is calculated following the basic linear rule:    
Expected fee of the transaction =  minimal_fees + minimal_nanotez_per_byte * size + minimal_nanotez_per_gas_unit * gas  
If the fee is lower, the operation is rejected.      
The linear rule must be higher or equals to the linear rule set in the baker node.  

**Strong fee management**  
The Tezos blockchain is not centralized. Hence, it’s possible that another baker steals the operations of a block baked by a certified baker (there is 2 parallelized branches and the second block can override the original block). The odd is very low but not null. Hence, to avoid this situation, the technical fees of the transactions could be null but the operation must contain a transaction to transfer the fees to a specific account. This way, it’s not profitable for a non-certified baker to steal a block. However, this solution implies to set the fee management to 0 for the baking node. Currently, there is no way to distinguish the private and the public mempool, hence, the baker node must be protected by a node which does not broadcast 0 fees operations.


#### Token management (GET, POST, DELETE)
This service manages the token in the database. It’s a basic CRUD service.

##### Creation (POST /api/tokens)
Input:
- Unique id
- Expiration date (optional)
- Profile (admin, issuer)

Output:
- Token
The token is never inserted in the database.

##### Deactivation (POST /api/tokens/revoke)
Input:
-	Unique id

Update:
Input:
-	Unique id
-	Expiration date

##### List (GET /api/tokens)

Get the list of the existing tokens

##### Delete (DELETE /api/tokens)
Input:
-	Unique id
Only the outdated/deactivated token can be deleted


A basic client (shell) is available to call this service. The outdated/deactivated token older than n days are deleted automatically.


#### Statistics (GET /api/stats/operations)
This service provides statistics in order to monitor the transactions. 

Input:
- Criteria
    - Date range
    - Issuer (owner)
    - Status

Output: CSV file or classic json file
- Id of the request
- Hash of the operation
- Account of the issuer
- Id of the token
- Requested block level
- Expected block level
- Date of the request
- Status
- Status Date
- Burnt gas (The sum is done on all the transactions of the operation)
- Storage size (The sum is done on all the transactions of the operation)
- Fees (The sum is done on all the transactions of the operation)
- Block level 
- Baker id
- Comment (in case of an error)

#### Profiles and Authorization
The services are protected by authentication and authorization.

|Service|Admin|Issuer (strong authentication)|Issuer (light authentication)|Baker node|
|-------|-----|------------------------------|-----------------------------|----------|
|Operation Storage Insertion| |X|X||	
|Operation Storage Deletion| |X|X||	
|Operation Storage Retrieval (Baker)| | | |There is only a basic authentication.|
|Operations Retrieval|X|X|||
|Operation Retrieval|X|X|X||	
|Event channel|X|X||	
|Baker identity|X|X|X||
|Slots|X|X|X||		
|Token management|X||||			
|Statistics|X||||	


## Transaction emission SDK
The SDK provides the following functions (javascript library):
- Get the next available slots of a list of bakers
- Get the next available slots of the corporate baker
- Operation creation/deletion
- Get the expected fees of a list of bakers
- Operation emission
- Retrieve the status of an operation
- Channel event
The SDK implements the basic calls to the Transaction manager. It uses the Taquito library. Examples will be provided to use the library.

### Next slots
This function retrieves the next slots of a list of bakers.

Input: list of bakers’ accounts and related Transaction Manager urls, range of the blocks levels

Output: list of slots related to the bakers’ accounts

The method calls the following URL:
```
…/chains/main/blocks/head/helpers/baking_rights?delegate=<baker’s account>&max_priority=0&level=<level>
```
It returns the list of the bakers with a priority 0 slot included in the range (input parameter).

### Next slots of the corporate baker
This function retrieves the next slots of the corporate baker.

Name: getBakingSlots

Input: none

Output: list of slots related to the corporate baker

It calls the Transaction Manager API /api/slots.

### Baker identity

Name: getBakerIdentity

Input: none

Output: Baker identity

The method calls the baker identity endpoint of the Transaction Manager.

### Transaction emission

Name: send (Taquito library)

Input: Transactions

Output: temporary token (in case of light authentication context)

The method embeds the transactions in a single operation and signs it. It calls the Operation Storage Insertion service (see §3.2.1) and retrieves the temporary token (in case of no authentication context).
If the force mode is set, it checks before if there is a pending operation for the Tezos account. If it is the case, before creating the operation, it retrieves the pending operation, it checks the signature, concatenates the new transactions to the former transactions, repackage a new operation, deletes the former operation and sends the new operation with the fees calculated (thanks to the baker identity information).
Warning: to do this operation, the token (strong authentication) or the related token tohe previous request (light authentication) must be provided in order to retrieve/delete the previous operation).


### Operation status retrieval
Input: token and criteria

Output: status of the operation

This method calls the Operation status service (see §3.2.4). 


### Event channel
Input: token

Output: channel

This method calls the event channel service (see §3.2.5).

### Certified bakers retrieval
Input: criteria (country)

Output: list of bakers

The certified bakers are stored in the storage of a dedicated smart contract in the Tezos blockchain. This method provides the list of all concerned bakers.
 
The smart contract has an interface identification (Type/version) which has to be compliant with the library. It’s a way to manage several types of future smart contracts.

The method calls the smart contract and retrieves the certified bakers meeting some criteria:
- The expiration date of the certified baker is higher than the current date
- The type of baker (localization, properties…) meets the requirements of the issuer (input parameter)

The method can optionally check the validity of the url, baker accounts and certificates.

### Global Transaction Emission
Input: transaction parameters, fee/gas consumption max, token in case of strong authentication context

Output: token (in case of light authentication context)

This method calls:
- Certified baker retrievals
- Next slots method
- Transaction creation method
- Expected fees method. The fee has to be less or equal to the max fee (input parameters).
- The first matching baker is selected
- Operation emission  

This function will be provided as an example.

### Process diagram
The below diagram illustrates the interactions between the different parties. 

#### Basic diagram
The hereafter diagram illustrates the interactions between the different parties for a basic case. 

 ![alt text](./images/cyb1_V0.jpg) 

- The issuer thanks to the Certified bakers retrieval (1), retrieves the list of bakers meeting his requirement.
- The issuer thanks to the Next slots method (2), retrieves the available slots in the next N cycles for the certified bakers.
- The issuer performs a dry run of the operation (3) to get the expected fees (4). 
- The issuer signs and emits the operation (5). The certified baker checks the operation with a dry run (6). It checks also the fees. It gives back the request id to the issuer (7).
- the issuer can subscribe to the event channel (8).
- The certified baker retrieves the operations to bake (9) and put them in the blockchain (10).
- The daemon checks the status of the transaction in the database and in the blockchain (1). It pushes back the events in the channel (12). 
- The issuer can override the first step (Retrieve the certified bakers) if he wants to manage his own list. He can also override the Get the expected fees (3).

#### Diagram with a cache case

The hereafter diagram illustrates the interactions between the different parties when the issuer emits 2 bunch of transactions to the Transaction Manager.

![alt text](./images/cyb1_cache.jpg) 
 
- The issuer thanks to the Certified bakers retrieval (1), retrieves the list of bakers meeting his requirement. 
- The issuer thanks to the Next slots method (2), retrieves the available slots in the next N cycles for the certified bakers.
- The issuer performs a dry run of the operation (3) to get the expected fees (4). 
- The issuer signs and emits the operation (5). The certified baker checks the operation with a dry run (6). It checks also the fees. It gives back the request id to the issuer (7).
- Some time after, the issuer wants to emit another operation. It checks the next slots (8).
- The issuer retrieves the pending operation in the transaction manager (9).
- The issuer forges a new operation with the transactions of the previous operation and the new transactions and signs this new operation (10).
- The issuer performs a dry run of the operation (11) to chek the expected fees (12).
- The issuer deletes the previous operation (13).
- The issuer deletes the new operation (14). The transaction manager performs a dry run.
- The baker node retrieve the operation to bake (16), creates a block and sends it to the Tezos Blockchain.

## KYC SDK
### Concept
This SDK enables a certification authority to manage a Tezos Smart Contract listing the certified bakers. 
### Add/Update bakers
This method adds or updates new bakers. The properties of a baker are:
- Account of the baker (public key hash tz1…)
- url (mandatory) & hash of the certificate public key (optional) of the Transaction Manager Server (SSL certificate).
- expiration date (optional).
- baker type (financial institutions, corporate, private individuals, etc)
- nationality of the baker
- servers localisation
All this information is stored in the storage of the Smart Contract.
### Remove bakers
This method removes bakers.
### Update of the version
The type and the version of the smart contract is used by the libraries to check the compliancy of the interface.

This method updates the type & version of the smart contract. It updates also the expiration date of the smart contract.



## Annexes
### Architecture
The transaction management has 3 components:
- Transaction manager: it manages all the front request
- Baker manager: It provides the operations to the baker
- Daemon: it updates the status
 ![alt text](./images/cyb1_deployment.jpg) 

### Configuration
This section is not completed. It will be enhanced during the project.

time_interval : If the request is related to a specific node, the time between the request and the baking must be less than this time interval.

fee_policy.minimal_fees : if set to none, the transaction manager does not do any filters
fee_policy.minimal_nanotez_per_byte : 
fee_policy.minimal_nanotez_per_gas_unit
fees >= (minimal_fees + minimal_nanotez_per_byte * size + minimal_nanotez_per_gas_unit * gas).


daemon.update.cron: pulling crontab
daemon.update.startupBlocks: Number of blocks to check when updating operations at startup
daemon.update.headOffset: Offset the head to update operations (30 to be sure the blocks are validated)
daemon.update.retries: Number of times an operation is put back to pending before dry running it again


### Database
This section is not completed. It will be enhanced during the project.
The MongoDb database stores all the information related to the transactions.

#### Operation collection

|Name|Type|Comment|	
|--|------|-------|
|id|UID|Unique id of the object|
|level|Long|Level of the block including the operation|
|requestedLevel|	Long|	Level requested (optional)|
|fetchedLevel|	Long|	Level when the request was fetched by the baking node.|
|timesFetched|	Long|	Number of times the requested was fetched by the baking node|
|errorMessage|	String|	|
|bakerAddress|	String|	Tezos account of the baker address|
|status|		String|Status of the request|
|owner|		String|Id of the owner|
|totalFee|		Long|Total fee of the operation in mutez|
|opSize|		Long|Total size of the operation in bytes|
|estimatedConsumption|		Long, Long| Estimation of the totalStorageSize and the totalConsumedGas|
|actualConsumption|		Long, Long| Actual value of the totalStorageSize and the totalConsumedGas|
|tzOperation|Operation|	Detail of the operation (detail below)|
| _class | String | Class of the operation object|

##### tzOperation
|Name|Type|Comment|	
|--|------|-------|
|hash|String|	Calculated hash of the operation|
|contents| Object| Contents and details of the different transaction (Listed below)|
|kind| String| Type of the transaction|
|source| String| Tezos account of the emitter|
|fee| Long| Fee of the transaction in mutez|
|counter| Long| Counter of the account|
|gas_limit| Long| Maximum gas allowed for the transaction|
|storage_limit| Long| Maximum storage allowed for the transaction|
|amount| Long| Amount of the transaction in mutez|
|destination| String| Tezos account of the receiver|
|parameters| | Parameters of the transaction (in case of contract call)|
|metadata| |Data containing the balance updates and the operation result|
|branch|String|	Hash value of the parent node|
|signature|String|	Signature of the emitter|

#### BakingSlots collection
|Name|Type|Comment|	
|--|------|-------|
|_id|UID|Unique id of the object|
|baker| String| Tezos address of the baker|
|level|   Long|    Block number when the baker will have the priority to bake|
|cycle|Long|    Current cycle value|
|_class|String|Class of the object|

#### OperationUpdateEvents collection
|Name|Type|Comment|	
|--|------|-------|
|_id|UID|Unique id of the object|
|date| Date| Date of the event|
|operation| | See operation collection|
|_class|String|Class of the object|

#### Tokens collection
|Name|Type|Comment|	
|--|------|-------|
|_id|UID|Unique id of the object|
|name| String| Name of the token|
|creationDate| Date| Date of creation|
|roles| String| Roles of the token (ADMIN or ISSUER)|
|revoked| Boolean| Revocation status of the token|
|owner| String| Owner of the token|
|_class|String|Class of the object|

#### Migrations collection
|Name|Type|Comment|	
|--|------|-------|
|_id|UID|Unique id of the object|
|name| String| Name of file|
|version| String| Version of the database|
|hash| String| Hash of the migration|
|createdDate| Date| Date of creation|
|_class|String|Class of the object|
