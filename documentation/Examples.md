[Index](/README.md) | [Specification](/documentation/specification.md) | [Sandbox/demo](/demo/README.md)  | [Deployment](/documentation/deployment.md) | [Typescript SDK Quick start](/documentation/Quickstart.md) | [Examples](/documentation/Examples.md) | [Tezos Sandbox Mempool](/tezos-sandbox-mempool/README.md)

# Examples

## Get the baker identity

```typescript
const bakerIdentity = await aliceSdk.getBakerIdentity();
```

## Transfer some tez

It's mandatory to call the cybEstimate function before sending the request in order to get the right gas limit, storage limit and the fees related to the Transaction Manager of the certified baker.

```typescript
const params : ParamsWithKind[] =[
    {
        kind: OpKind.TRANSACTION,
        to: bobAddress,
        amount: 0.1,
        fee: 0,
    }
]
 // Estimate for CYB
const estimateCyb = await aliceSdk.cybEstimate(params)

// Send the operation
const sendOp = await aliceSdk.generateBatch(estimateCyb).send()
```


## Transfer some tez and call a smart contract

```typescript
// Smart Contract with on method: updateName
const simpleContract = await aliceSdk.wallet.at('ADDRESS_OF_THE_SMART_CONTRACT')

const params : ParamsWithKind[] =[
    {
        kind: OpKind.TRANSACTION,
        to: bobAddress,
        amount: 0.1,
        fee: 0,
    },
    {
        kind: OpKind.TRANSACTION,
        ...simpleContract.methods.updateName('MY_STRING').toTransferParams({})
    },
]

// Estimate for CYB
const estimateCyb = await aliceSdk.cybEstimate(params)

// Send the operation
const sendOp = await aliceSdk.generateBatch(estimateCyb).send()
```

## Specify a level

It is possible to decide at which level we want our operation to be baked (it has to be a baking slot of the chosen baker)

```typescript
// Change context for next call
if (!aliceSdk.isContextLocked) {
    aliceSdk.modifyContext({
        level: WANTED_LEVEL,
    })
}

// Prepare and send operation
// ...
const sendOp = await aliceSdk.generateBatch(estimateCyb).send()
 
// Reset context for next call
if (!aliceSdk.isContextLocked) {
    aliceSdk.modifyContext({
        level: undefined,
    })
}
```

## Get CYB information about an operation

```typescript
// Send the operation
const sendOp = await aliceSdk.generateBatch(estimateCyb).send()

// Get operation
const operation = await aliceSdk.operationService.getOperation({hash: response.opHash})
```

## Delete an operation

It is possible to delete an operation from the mempool of CYB only if the operation has the PENDING status.

```typescript
// Send the operation
const sendOp = await aliceSdk.generateBatch(estimateCyb).send()

// Delete the operation
const deletedOperation = await aliceSdk.operationService.deleteOperation({hash: sendOp.opHash})
```

You might want to verify the validity of the operation that was sent back
```typescript
// Verify the signature
const tzOperation = deletedOperation.tzOperation
const forgeParams: ForgeParams = {
    branch: tzOperation.branch,
    contents: tzOperation.contents,
}
const bytes = await bobSDK.cybRpc.forgeOperations(forgeParams)
const signature = await bobSDK.signer.sign(bytes, new Uint8Array([3]))

if (signature.prefixSig === tzOperation.signature) {
    console.log("Deleted operation is valid")
}
```

## Update an operation

```typescript
// Send the operation
const sendOp = await aliceSdk.generateBatch(estimateCyb).send()

// Delete and make a new batch from deleted operation
const deletedOperation = await aliceSdk.operationService.deleteOperation({hash: sendOp.opHash})
const batch = aliceSdk.generateBatchFrom(deletedOperation)
batch.withTransfer({
    to: "tz1aSkwEot3L2kmUvcoxzjMomb9mvBNuzFK6",
    amount: 1,
})

// Re-submit the operation
const secondOp = await batch.send()
const operation = await aliceSdk.operationService.getOperation({hash: secondOp.opHash})
```

## Subscribe to the event channel

```typescript
let operationEventSource = sdk.operationService.getOperationEventSource()

operationEventSource.onmessage = (e) => {
    const opUpdateEvent: OperationUpdateEvent = JSON.parse(e.data)

    let status = opUpdateEvent.operation.status
    let opId = opUpdateEvent.operation.id
    console.log(`Operation with id ${opId} updated with status: ${status}`)

    if (status === OperationStatus.BAKED
        || status === OperationStatus.BAKED_BY_ANOTHER
        || status === OperationStatus.REJECTED
    ) {
        console.log(`Operation has been processed at level: ${opUpdateEvent.operation.level}.`)
        resolve()
    }
}
```

## Check the baking rights of your baker for next levels

```typescript
//Check the current slot
const currentSlot = await aliceSdk.getBakingSlots({maxPriority : 0})
const currentLevel = currentSlot[0].level
const bakerIdentity = await aliceSdk.getBakerIdentity()

//Check the 10 next slots for your baker
const levels = Array.from({length: 10}, (_, i) => i + currentLevel)
const bakingSlots = await aliceSdk.getBakingSlots({maxPriority : 0, levels: levels, delegates: [bakerIdentity.bakerAddress]})
```