[Index](/README.md) | [Specification](/documentation/specification.md) | [Sandbox/demo](/demo/README.md)  | [Deployment](/documentation/deployment.md) | [Typescript SDK Quick start](/documentation/Quickstart.md) | [Examples](/documentation/Examples.md) | [Tezos Sandbox Mempool](/tezos-sandbox-mempool/README.md)

# Quick Start

## Installing CYB sdk using npm

The following instructions assume you have a project already created, and you have npm installed and operable.
```shell
npm install @coexya/cyb-sdk
```

## Import the library

The constructor of the `CybAuth` class takes the Transaction Manager URL and the token issuer as parameter.  
The constructor of the `CybSdk` class takes a node URL as parameter.

```typescript
import {CybSdk, CybAuth} from '@coexya/cyb-sdk';
const auth = new CybAuth('https://TRANSACTION_MANAGER_URL', 'THE_ISSUER_TOKEN')
const aliceSdk: CybSdk = new CybSdk(auth, 'https://YOUR_PREFERRED_RPC_URL')
```

### Without authentication
When the transaction manager was configured to accept unauthenticated submission of operations, the SDK can be used without a token
```typescript
const auth = new CybAuth('https://TRANSACTION_MANAGER_URL')
```

## Configuration

Provide a signer to the CybSdk

```typescript
import {InMemorySigner} from "@taquito/signer"
aliceSdk.setProvider({
    signer: new InMemorySigner('PK_OF_ALICE')
})
```