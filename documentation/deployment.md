[Index](/README.md) | [Specification](/documentation/specification.md) | [Sandbox/demo](/demo/README.md)  | [Deployment](/documentation/deployment.md) | [Typescript SDK Quick start](/documentation/Quickstart.md) | [Examples](/documentation/Examples.md) | [Tezos Sandbox Mempool](/tezos-sandbox-mempool/README.md)

# Choose Your Baker deployment

## Docker deployment

Docker deployment requires to have both **docker** and **docker-compose** installed on the server.

### Docker deployment - Sandbox mode

* Launch the docker containers.
```shell
cd deployment/compose/sandbox
docker-compose up -d
```

The tezos sandbox mempool node is available at `http://localhost:20000/`, e.g. `http://localhost:20000/chains/main/blocks/head` for the head.

The transaction manager REST API is reachable at `http://localhost:9090/`. The swagger documentation is available at `http://localhost:9090/swagger-ui`.

### Docker deployment - Production mode

* Go to the configuration folder.
```shell
cd deployment/compose/mainnet
```

* Update the **.env** file to match your baker settings. The mandatory variables to update are the following: **CYB_BAKER_ADDRESS**, **CYB_BAKER_NAME**, **CYB_JWT_SECRET**, **CYB_NODE_URL**.

* Check the ports to open: **CYB_REST_BAKER_PORT** needs to be reachable by the baker service and **CYB_REST_TM_IMAGE** needs to be exposed to the Internet to allow issuers transactions.

WARNING: **CYB_REST_BAKER_PORT** access needs to be restricted since it does not have any authentication mechanism. It should only be accessible by the baker service.

* Once configured, deploy the application.
```shell
docker-compose up -d
```

* In order to update CYB, update **CYB_DAEMON_IMAGE**, **CYB_REST_BAKER_IMAGE**, and **CYB_REST_TM_IMAGE** to the updated version.

After each configuration update, it's mandatory to redeploy the stack by running `docker-compose up -d`.

* To stop CYB:
```
docker-compose down
```

* To uninstall CYB (and WIPE all data):
```shell
docker-compose down -v
```

Please refer to [Baker configuration](#baker-configuration) for the connection between the baker service and CYB and to [Token Management](../token-management/README.md) for the token generation.

## JAR deployment

### Source compiling

* Retrieve the Tezos Node Connector code.
```shell
git clone https://gitlab.com/coexya/tezos/tezos-node-connector.git
cd tezos-node-connector
git checkout 1.0.0
```

* Compile Tezos Node Connector and publish it to local maven.
```shell
./gradlew publishToMavenLocal
```

* Compile Choose Your Baker and generate JARs.
```shell
./gradlew assemble
```

### MongoDB installation and initialization

A mongoDB database is required to run the application. Please refer to [official MongoDB documentation](https://www.mongodb.com/docs/manual/administration/install-on-linux/) to install MongoDB.

Once installed, access the mongo shell and create a database and a user for Choose Your Baker.
```mongodb-json-query
mongosh
use tezos-cyb
db.createUser({user: "cyb", pwd: passwordPrompt(), roles: [{role: "readWrite", db: "tezos-cyb"}]})
```
Please save the password of the generated **cyb** user.

### Configuration and deployment

* Configure the JAR deployment by editing the config file **deployment/jar/config.sh**.

* Run the deployment script **deployment/jar/config.sh**.

## Deployment for development

* Start the mongo and mongo express containers with docker-compose:
```shell
docker-compose up -d
```

* Launch the transaction manager REST API (you have to retrieve and compile the tezos-node-connector project beforehand (see above)):
```shell
./gradlew :frontend:rest-transaction-manager:bootRun
```

* Launch the baker REST API:
```shell
./gradlew :frontend:rest-baker:bootRun
```

To launch the Daemon:
```shell
./gradlew :backend:daemon:bootRun
```

The transaction manager swagger documentation is available at `http://localhost:9090/swagger-ui/`.

## Default JWT tokens

```yaml
admin: eyJ0eXAiOiJKV1QiLCJhbGciOiJIUzI1NiJ9.eyJvd25lciI6IjYxODkyYmZhZWE3ODY1NjRjYjE3YWM0NSIsImNyZWF0aW9uVGltZSI6IjIwMjEtMTEtMDhUMDU6NTQ6MDIuOTYwNjgyLTA4OjAwIiwicm9sZXMiOlsiQURNSU4iXSwiaXNzIjoiVGV6b3NAQ3liIiwicGVyc2lzdGVkIjp0cnVlfQ.Gy2q0YhRsndfEJgQ1boa6JhIj5XOSYytt9JMkwOLgCE
issuer: eyJ0eXAiOiJKV1QiLCJhbGciOiJIUzI1NiJ9.eyJvd25lciI6IjYxODkyYmQ1ZWE3ODY1NjRjYjE3YWM0MyIsImNyZWF0aW9uVGltZSI6IjIwMjEtMTEtMDhUMDU6NTM6MjUuMzgxMzg4LTA4OjAwIiwicm9sZXMiOlsiSVNTVUVSIl0sImlzcyI6IlRlem9zQEN5YiIsInBlcnNpc3RlZCI6dHJ1ZX0.xOdzsl7OffzRe3BYyAxexxlGHMceHXfRVX2veB3mFN8
```

If you want to generate/manage token : [Token Management](/token-management/README.md)

## Baker configuration

In order to Choose Your Baker to work, you need to configure the baker daemon so that it calls the Rest Baker API to retrieve the private mempool.
For that purpose, you need to add the option `--operations-pool REST_BAKER_URL/api/mempool` to the baker daemon command (e.g., `--operations-pool http://localhost:8080/api/mempool`. This option is only available with version 12.0+ of Tezos binaries.
