# Sandbox Kubernetes Deployment

## How to deploy

* Get the config ready by having your config in *~/.kube/config* or by passing it as an argument with *--kubeconfig*.

* Deploy the stack:
```shell
kubectl -k sandbox-full apply
```

* If you ever need to delete the stack:
```shell
kuberctl -k sandbox-full delete
```
