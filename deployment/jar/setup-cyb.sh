#!/usr/bin/env bash

deploymentDir=$(dirname "$0")
projectDir=$deploymentDir/../..

# Config
source $deploymentDir/config.sh

printf "Choose Your Baker will be installed in $install_dir with NODE_URL='$node_url' and BAKER_ADDRESS='$baker_address'.\n"
mkdir -p $install_dir/daemon
mkdir -p $install_dir/rest-baker
mkdir -p $install_dir/rest-transaction-manager
chown -R $user $install_dir
printf "Directory structure creation completed.\n"


cp "$projectDir"/backend/daemon/build/libs/*.jar $install_dir/daemon/daemon.jar
cp "$projectDir"/frontend/rest-baker/build/libs/*.jar $install_dir/rest-baker/rest-baker.jar
cp "$projectDir"/frontend/rest-transaction-manager/build/libs/*.jar $install_dir/rest-transaction-manager/rest-transaction-manager.jar
printf "Copy of Choose Your Baker JARs completed.\n"

sed -e "s|\$NODE_URL|$node_url|g; s|\$BAKER_ADDRESS|$baker_address|g; s|\$MONGO_URL|$mongo_url|g; s|\$MONGO_DATABASE|$mongo_database|g; s|\$MONGO_USER|$mongo_user|g; s|\$MONGO_PASSWORD|$mongo_password|g" "$deploymentDir"/config/application-daemon.yml > "$install_dir"/daemon/application.yml
sed -e "s|\$NODE_URL|$node_url|g; s|\$BAKER_ADDRESS|$baker_address|g; s|\$MONGO_URL|$mongo_url|g; s|\$MONGO_DATABASE|$mongo_database|g; s|\$MONGO_USER|$mongo_user|g; s|\$MONGO_PASSWORD|$mongo_password|g; s|\$REST_BAKER_PORT|$rest_baker_port|g" "$deploymentDir"/config/application-rest-baker.yml > "$install_dir"/rest-baker/application.yml
sed -e "s|\$NODE_URL|$node_url|g; s|\$BAKER_ADDRESS|$baker_address|g; s|\$MONGO_URL|$mongo_url|g; s|\$MONGO_DATABASE|$mongo_database|g; s|\$MONGO_USER|$mongo_user|g; s|\$MONGO_PASSWORD|$mongo_password|g; s|\$REST_TM_PORT|$rest_tm_port|g" "$deploymentDir"/config/application-rest-transaction-manager.yml > "$install_dir"/rest-transaction-manager/application.yml
printf "Copy of configuration files completed.\n"


sed -e "s|\$USER|$user|g; s|\$INSTALL_DIR|$install_dir|g" "$deploymentDir"/services/cyb-daemon.service > /lib/systemd/system/cyb-daemon.service
sed -e "s|\$USER|$user|g; s|\$INSTALL_DIR|$install_dir|g" "$deploymentDir"/services/cyb-rest-baker.service > /lib/systemd/system/cyb-rest-baker.service
sed -e "s|\$USER|$user|g; s|\$INSTALL_DIR|$install_dir|g; s|\$JWT_SECRET|$jwtSecret|g" "$deploymentDir"/services/cyb-rest-transaction-manager.service > /lib/systemd/system/cyb-rest-transaction-manager.service
printf "Copy of services completed.\n"

systemctl daemon-reload
systemctl enable cyb-daemon
systemctl enable cyb-rest-baker
systemctl enable cyb-rest-transaction-manager
printf "All services enabled.\n"
systemctl restart cyb-daemon
systemctl restart cyb-rest-baker
systemctl restart cyb-rest-transaction-manager
printf "All services restarted.\n"
exit 0
