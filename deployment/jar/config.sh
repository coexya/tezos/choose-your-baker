install_dir=/opt/cyb # Directory where the JARs and config files will be installed
node_url=http://localhost:8732 # Url of a tezos node. It's recommended to be the baking node but it's not mandatory (at least on the same tezos network)
baker_address=tz1VSUr8wwNhLAzempoch5d6hLRiTh8Cjcjb # Address (public key hash) of the baker
user=tezos # UNIX user that will run the systemd services

mongo_url=127.0.0.1:27017 # Url of the mongoDB database
mongo_database=tezos-cyb # Name of the mongoDB database
mongo_user=cyb # User to connect to the mongoDB database
mongo_password=password # Password of the user to connect to the mongoDB database

rest_baker_port=8080 # Port exposed by the REST Baker service (required to be accessible by the baking node)
rest_tm_port=9090 # Port exposed by the REST Transaction Manager (required to be open to the internet for issuers to issue transactions)
jwt_secret=6ta4d7w4xrebbtzs5ttxjnw7d4dek59q # JWT secret used to encrypt JWT authentication token (NEED to be change before initializing tokens)
